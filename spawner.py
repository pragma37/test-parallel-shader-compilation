from subprocess import Popen
import time

start_time = time.time()

processes = []
for i in range(64):
    processes.append(Popen(['python', 'main.py', '--thread_count', '0', '--shader_count', '1']))

end_time = time.time()
print("SPAWNER FULL SYNC COMPILE TIME: ", end_time - start_time)

for p in processes:
    p.wait()

end_time = time.time()
print("SPAWNER FULL ASYNC COMPILE TIME: ", end_time - start_time)
