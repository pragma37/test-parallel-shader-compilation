/////////////////////////////// Source file 0/////////////////////////////
#version 430
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_viewport_layer_array: enable
#define gpu_Layer gl_Layer
#define gpu_ViewportIndex gl_ViewportIndex
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_EmitVertex EmitVertex
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1
#define DFDY_SIGN 1




/** Type aliases. */
/** IMPORTANT: Be wary of size and alignment matching for types that are present
 * in C++ shared code. */

/* Boolean in GLSL are 32bit in interface structs. */
#define bool32_t bool
#define bool2 bvec2
#define bool3 bvec3
#define bool4 bvec4

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define int2 ivec2
#define int3 ivec3
#define int4 ivec4
#define uint2 uvec2
#define uint3 uvec3
#define uint4 uvec4
/* GLSL already follows the packed alignment / size rules for vec3. */
#define packed_float3 float3
#define packed_int3 int3
#define packed_uint3 uint3

#define float2x2 mat2x2
#define float3x2 mat3x2
#define float4x2 mat4x2
#define float2x3 mat2x3
#define float3x3 mat3x3
#define float4x3 mat4x3
#define float2x4 mat2x4
#define float3x4 mat3x4
#define float4x4 mat4x4

/* Small types are unavailable in GLSL (or badly supported), promote them to bigger type. */
#define char int
#define char2 int2
#define char3 int3
#define char4 int4
#define short int
#define short2 int2
#define short3 int3
#define short4 int4
#define uchar uint
#define uchar2 uint2
#define uchar3 uint3
#define uchar4 uint4
#define ushort uint
#define ushort2 uint2
#define ushort3 uint3
#define ushort4 uint4
#define half float
#define half2 float2
#define half3 float3
#define half4 float4

/* Aliases for supported fixed width types. */
#define int32_t int
#define uint32_t uint

/* Fast store variant macro. In GLSL this is the same as imageStore, but assumes no bounds
 * checking. */
#define imageStoreFast imageStore
#define imageLoadFast imageLoad

/* Texture format tokens -- Type explicitness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

#define usampler2DArrayAtomic usampler2DArray
#define usampler2DAtomic usampler2D
#define usampler3DAtomic usampler3D
#define isampler2DArrayAtomic isampler2DArray
#define isampler2DAtomic isampler2D
#define isampler3DAtomic isampler3D

/* Pass through functions. */
#define imageFence(image)

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}

/////////////////////////////// Source file 1/////////////////////////////

/////////////////////////////// Source file 2/////////////////////////////
#define GPU_SHADER

/////////////////////////////// Source file 3/////////////////////////////
#define GPU_NVIDIA

/////////////////////////////// Source file 4/////////////////////////////
#define OS_WIN

/////////////////////////////// Source file 5/////////////////////////////
#define GPU_OPENGL

/////////////////////////////// Source file 6/////////////////////////////
#define GPU_VERTEX_SHADER

/////////////////////////////// Source file 7/////////////////////////////
#define PRINCIPLED_DIELECTRIC 
#define MAT_DIFFUSE 
#define MAT_REFLECTION 
#define CLOSURE_BIN_COUNT 2
#define GBUFFER_LAYER_MAX 2
#define LIGHT_CLOSURE_EVAL_COUNT 2
#define MAT_RENDER_PASS_SUPPORT 
#define MAT_GEOM_MESH 
#define DRAW_MODELMAT_CREATE_INFO 
#define drw_ModelMatrixInverse drw_matrix_buf[resource_id].model_inverse
#define drw_ModelMatrix drw_matrix_buf[resource_id].model
#define ModelMatrixInverse drw_ModelMatrixInverse
#define ModelMatrix drw_ModelMatrix
#define UNIFORM_RESOURCE_ID_NEW 
#define drw_ResourceID resource_id_buf[gpu_BaseInstance + gl_InstanceID]
#define DRAW_VIEW_CREATE_INFO 
#define drw_view drw_view_[drw_view_id]
#define MAT_DEFERRED 
#define GBUFFER_WRITE 
#define EEVEE_UTILITY_TX 
#define EEVEE_SAMPLING_DATA 
#define SPHERE_PROBE 
#define IRRADIANCE_GRID_SAMPLING 
#define SHADOW_READ_ATOMIC 
#define USE_GPU_SHADER_CREATE_INFO

/////////////////////////////// Source file 8/////////////////////////////




/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool32_t a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
/* Silence macros when compiling for shaders. */
#  define BLI_STATIC_ASSERT(cond, msg)
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define ENUM_OPERATORS(a, b)
/* Incompatible keywords. */
#  define static
#  define inline
/* Math function renaming. */
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  include  BLI_math_matrix_types.hh 
#  include  BLI_math_vector_types.hh 

using bool32_t = int32_t;
// using bool2 = blender::int2; /* Size is not consistent across backend. */
// using bool3 = blender::int3; /* Size is not consistent across backend. */
// using bool4 = blender::int4; /* Size is not consistent across backend. */

using blender::float2;
using blender::float4;
using blender::int2;
using blender::int4;
using blender::uint2;
using blender::uint4;
/** IMPORTANT: Do not use in shared struct. Use packed_(float/int/uint)3 instead.
 * Here for static functions usage only. */
using blender::float3;
using blender::int3;
using blender::uint3;
/** Packed types are needed for MSL which have different alignment rules for float3. */
using packed_float3 = blender::float3;
using packed_int3 = blender::int3;
using packed_uint3 = blender::uint3;

using blender::float2x2;
// using blender::float3x2; /* Does not follow alignment rules of GPU. */
using blender::float4x2;
// using blender::float2x3; /* Does not follow alignment rules of GPU. */
// using blender::float3x3; /* Does not follow alignment rules of GPU. */
// using blender::float4x3; /* Does not follow alignment rules of GPU. */
using blender::float2x4;
using blender::float3x4;
using blender::float4x4;

#endif

/////////////////////////////// Source file 9/////////////////////////////
struct NodeTree {
vec4 u3;
vec4 u16;
vec4 u24;
vec4 u28;
vec4 u29;
vec3 u11;
float crypto_hash;
float u4;
float u5;
float u6;
float u7;
float u9;
float u10;
float u12;
float u13;
float u14;
float u15;
float u17;
float u18;
float u20;
float u21;
float u22;
float u23;
float u26;
float u27;
float u30;
float u41;
float u42;
};


/////////////////////////////// Source file 10/////////////////////////////




/** \file
 * \ingroup eevee
 *
 * List of defines that are shared with the GPUShaderCreateInfos. We do this to avoid
 * dragging larger headers into the createInfo pipeline which would cause problems.
 */

#ifndef GPU_SHADER
#  pragma once
#endif

#ifndef SQUARE
#  define SQUARE(x) ((x) * (x))
#endif

/* Look Up Tables. */
#define LUT_WORKGROUP_SIZE 16

/* Hierarchical Z down-sampling. */
#define HIZ_MIP_COUNT 7
/* NOTE: The shader is written to update 5 mipmaps using LDS. */
#define HIZ_GROUP_SIZE 32

/* Avoid too much overhead caused by resizing the light buffers too many time. */
#define LIGHT_CHUNK 256

#define CULLING_SELECT_GROUP_SIZE 256
#define CULLING_SORT_GROUP_SIZE 256
#define CULLING_ZBIN_GROUP_SIZE 1024
#define CULLING_TILE_GROUP_SIZE 256

/* Reflection Probes. */
#define SPHERE_PROBE_REMAP_GROUP_SIZE 32
#define SPHERE_PROBE_GROUP_SIZE 16
#define SPHERE_PROBE_SELECT_GROUP_SIZE 64
#define SPHERE_PROBE_MIPMAP_LEVELS 5
#define SPHERE_PROBE_SH_GROUP_SIZE 256
#define SPHERE_PROBE_SH_SAMPLES_PER_GROUP 64
/* Must be power of two for correct partitioning. */
#define SPHERE_PROBE_ATLAS_MAX_SUBDIV 10
#define SPHERE_PROBE_ATLAS_RES (1 << SPHERE_PROBE_ATLAS_MAX_SUBDIV)
/* Maximum number of thread-groups dispatched for remapping a probe to octahedral mapping. */
#define SPHERE_PROBE_MAX_HARMONIC SQUARE(SPHERE_PROBE_ATLAS_RES / SPHERE_PROBE_REMAP_GROUP_SIZE)
/* Start and end value for mixing sphere probe and volume probes. */
#define SPHERE_PROBE_MIX_START_ROUGHNESS 0.7
#define SPHERE_PROBE_MIX_END_ROUGHNESS 0.9
/* Roughness of the last mip map for sphere probes. */
#define SPHERE_PROBE_MIP_MAX_ROUGHNESS 0.7
/**
 * Limited by the UBO size limit `(16384 bytes / sizeof(SphereProbeData))`.
 */
#define SPHERE_PROBE_MAX 128

/**
 * Limited by the performance impact it can cause.
 * Limited by the max layer count supported by a hardware (256).
 * Limited by the UBO size limit `(16384 bytes / sizeof(PlanarProbeData))`.
 */
#define PLANAR_PROBE_MAX 16

/**
 * IMPORTANT: Some data packing are tweaked for these values.
 * Be sure to update them accordingly.
 * SHADOW_TILEMAP_RES max is 32 because of the shared bitmaps used for LOD tagging.
 * It is also limited by the maximum thread group size (1024).
 */
#if 0
/* Useful for debugging the tile-copy version of the shadow rendering without making debugging
 * tools unresponsive. */
#  define SHADOW_TILEMAP_RES 4
#  define SHADOW_TILEMAP_LOD 2 /* LOG2(SHADOW_TILEMAP_RES) */
#else
#  define SHADOW_TILEMAP_RES 32
#  define SHADOW_TILEMAP_LOD 5 /* LOG2(SHADOW_TILEMAP_RES) */
#endif
#define SHADOW_TILEMAP_LOD0_LEN ((SHADOW_TILEMAP_RES / 1) * (SHADOW_TILEMAP_RES / 1))
#define SHADOW_TILEMAP_LOD1_LEN ((SHADOW_TILEMAP_RES / 2) * (SHADOW_TILEMAP_RES / 2))
#define SHADOW_TILEMAP_LOD2_LEN ((SHADOW_TILEMAP_RES / 4) * (SHADOW_TILEMAP_RES / 4))
#define SHADOW_TILEMAP_LOD3_LEN ((SHADOW_TILEMAP_RES / 8) * (SHADOW_TILEMAP_RES / 8))
#define SHADOW_TILEMAP_LOD4_LEN ((SHADOW_TILEMAP_RES / 16) * (SHADOW_TILEMAP_RES / 16))
#define SHADOW_TILEMAP_LOD5_LEN ((SHADOW_TILEMAP_RES / 32) * (SHADOW_TILEMAP_RES / 32))
#define SHADOW_TILEMAP_PER_ROW 64
#define SHADOW_TILEDATA_PER_TILEMAP \
  (SHADOW_TILEMAP_LOD0_LEN + SHADOW_TILEMAP_LOD1_LEN + SHADOW_TILEMAP_LOD2_LEN + \
   SHADOW_TILEMAP_LOD3_LEN + SHADOW_TILEMAP_LOD4_LEN + SHADOW_TILEMAP_LOD5_LEN)
/* Maximum number of relative LOD distance we can store. */
#define SHADOW_TILEMAP_MAX_CLIPMAP_LOD 8
#if 0
/* Useful for debugging the tile-copy version of the shadow rendering without making debugging
 * tools unresponsive. */
#  define SHADOW_PAGE_CLEAR_GROUP_SIZE 8
#  define SHADOW_PAGE_RES 8
#  define SHADOW_PAGE_LOD 3 /* LOG2(SHADOW_PAGE_RES) */
#else
#  define SHADOW_PAGE_CLEAR_GROUP_SIZE 32
#  define SHADOW_PAGE_RES 256
#  define SHADOW_PAGE_LOD 8 /* LOG2(SHADOW_PAGE_RES) */
#endif
/* For testing only. */
// #define SHADOW_FORCE_LOD0
#define SHADOW_MAP_MAX_RES (SHADOW_PAGE_RES * SHADOW_TILEMAP_RES)
#define SHADOW_DEPTH_SCAN_GROUP_SIZE 8
#define SHADOW_AABB_TAG_GROUP_SIZE 64
#define SHADOW_MAX_TILEMAP 4096
#define SHADOW_MAX_TILE (SHADOW_MAX_TILEMAP * SHADOW_TILEDATA_PER_TILEMAP)
#define SHADOW_MAX_PAGE 4096
#define SHADOW_BOUNDS_GROUP_SIZE 64
#define SHADOW_CLIPMAP_GROUP_SIZE 64
#define SHADOW_VIEW_MAX 64 /* Must match DRW_VIEW_MAX. */
#define SHADOW_RENDER_MAP_SIZE (SHADOW_VIEW_MAX * SHADOW_TILEMAP_LOD0_LEN)
#define SHADOW_ATOMIC 1
#define SHADOW_PAGE_PER_ROW 4
#define SHADOW_PAGE_PER_COL 4
#define SHADOW_PAGE_PER_LAYER (SHADOW_PAGE_PER_ROW * SHADOW_PAGE_PER_COL)
#define SHADOW_MAX_STEP 16
#define SHADOW_MAX_RAY 4
#define SHADOW_ROG_ID 0

/* Deferred Lighting. */
#define DEFERRED_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define DEFERRED_GBUFFER_ROG_ID 0

/* Ray-tracing. */
#define RAYTRACE_GROUP_SIZE 8
/* Keep this as a define to avoid shader variations. */
#define RAYTRACE_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define RAYTRACE_RAYTIME_FORMAT GPU_R32F
#define RAYTRACE_VARIANCE_FORMAT GPU_R16F
#define RAYTRACE_TILEMASK_FORMAT GPU_R8UI

/* Sub-Surface Scattering. */
#define SUBSURFACE_GROUP_SIZE RAYTRACE_GROUP_SIZE
#define SUBSURFACE_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define SUBSURFACE_OBJECT_ID_FORMAT GPU_R16UI

/* Film. */
#define FILM_GROUP_SIZE 16

/* Motion Blur. */
#define MOTION_BLUR_GROUP_SIZE 32
#define MOTION_BLUR_DILATE_GROUP_SIZE 512

/* Irradiance Cache. */
/** Maximum number of entities inside the cache. */
#define IRRADIANCE_GRID_MAX 64

/* Depth Of Field. */
#define DOF_TILES_SIZE 8
#define DOF_TILES_FLATTEN_GROUP_SIZE DOF_TILES_SIZE
#define DOF_TILES_DILATE_GROUP_SIZE 8
#define DOF_BOKEH_LUT_SIZE 32
#define DOF_MAX_SLIGHT_FOCUS_RADIUS 5
#define DOF_SLIGHT_FOCUS_SAMPLE_MAX 16
#define DOF_MIP_COUNT 4
#define DOF_REDUCE_GROUP_SIZE (1 << (DOF_MIP_COUNT - 1))
#define DOF_DEFAULT_GROUP_SIZE 32
#define DOF_STABILIZE_GROUP_SIZE 16
#define DOF_FILTER_GROUP_SIZE 8
#define DOF_GATHER_GROUP_SIZE DOF_TILES_SIZE
#define DOF_RESOLVE_GROUP_SIZE (DOF_TILES_SIZE * 2)

/* Ambient Occlusion. */
#define AMBIENT_OCCLUSION_PASS_TILE_SIZE 16

/* IrradianceBake. */
#define SURFEL_GROUP_SIZE 256
#define SURFEL_LIST_GROUP_SIZE 256
#define IRRADIANCE_GRID_GROUP_SIZE 4 /* In each dimension, so 4x4x4 workgroup size. */
#define IRRADIANCE_GRID_BRICK_SIZE 4 /* In each dimension, so 4x4x4 brick size. */
#define IRRADIANCE_BOUNDS_GROUP_SIZE 64

/* Volumes. */
#define VOLUME_GROUP_SIZE 4
#define VOLUME_INTEGRATION_GROUP_SIZE 8
#define VOLUME_HIT_DEPTH_MAX 16

/* Velocity. */
#define VERTEX_COPY_GROUP_SIZE 64

/* Resource bindings. */

/* Textures. */
/* Used anywhere. (Starts at index 2, since 0 and 1 are used by draw_gpencil) */
#define RBUFS_UTILITY_TEX_SLOT 2
#define HIZ_TEX_SLOT 3
/* Only during surface shading (forward and deferred eval). */
#define SHADOW_TILEMAPS_TEX_SLOT 4
#define SHADOW_ATLAS_TEX_SLOT 5
#define VOLUME_PROBE_TEX_SLOT 6
#define SPHERE_PROBE_TEX_SLOT 7
#define VOLUME_SCATTERING_TEX_SLOT 8
#define VOLUME_TRANSMITTANCE_TEX_SLOT 9
/* Currently only used by ray-tracing, but might become used by forward too. */
#define PLANAR_PROBE_DEPTH_TEX_SLOT 10
#define PLANAR_PROBE_RADIANCE_TEX_SLOT 11

/* Images. */
#define RBUFS_COLOR_SLOT 0
#define RBUFS_VALUE_SLOT 1
#define RBUFS_CRYPTOMATTE_SLOT 2
#define GBUF_CLOSURE_SLOT 3
#define GBUF_NORMAL_SLOT 4
#define GBUF_HEADER_SLOT 5
/* Volume properties pass do not write to `rbufs`. Reuse the same bind points. */
#define VOLUME_PROP_SCATTERING_IMG_SLOT 0
#define VOLUME_PROP_EXTINCTION_IMG_SLOT 1
#define VOLUME_PROP_EMISSION_IMG_SLOT 2
#define VOLUME_PROP_PHASE_IMG_SLOT 3
#define VOLUME_OCCUPANCY_SLOT 4
/* Only during volume pre-pass. */
#define VOLUME_HIT_DEPTH_SLOT 0
#define VOLUME_HIT_COUNT_SLOT 1
/* Only during shadow rendering. */
#define SHADOW_ATLAS_IMG_SLOT 4

/* Uniform Buffers. */
/* Slot 0 is GPU_NODE_TREE_UBO_SLOT. */
#define UNIFORM_BUF_SLOT 1
/* Only during surface shading (forward and deferred eval). */
#define IRRADIANCE_GRID_BUF_SLOT 2
#define SPHERE_PROBE_BUF_SLOT 3
#define PLANAR_PROBE_BUF_SLOT 4
/* Only during pre-pass. */
#define VELOCITY_CAMERA_PREV_BUF 2
#define VELOCITY_CAMERA_CURR_BUF 3
#define VELOCITY_CAMERA_NEXT_BUF 4
#define CLIP_PLANE_BUF 5

/* Storage Buffers. */
#define LIGHT_CULL_BUF_SLOT 0
#define LIGHT_BUF_SLOT 1
#define LIGHT_ZBIN_BUF_SLOT 2
#define LIGHT_TILE_BUF_SLOT 3
#define IRRADIANCE_BRICK_BUF_SLOT 4
#define SAMPLING_BUF_SLOT 6
#define CRYPTOMATTE_BUF_SLOT 7
/* Only during surface capture. */
#define SURFEL_BUF_SLOT 4
/* Only during surface capture. */
#define CAPTURE_BUF_SLOT 5
/* Only during shadow rendering. */
#define SHADOW_RENDER_MAP_BUF_SLOT 3
#define SHADOW_PAGE_INFO_SLOT 4
#define SHADOW_VIEWPORT_INDEX_BUF_SLOT 5

/* Only during pre-pass. */
#define VELOCITY_OBJ_PREV_BUF_SLOT 0
#define VELOCITY_OBJ_NEXT_BUF_SLOT 1
#define VELOCITY_GEO_PREV_BUF_SLOT 2
#define VELOCITY_GEO_NEXT_BUF_SLOT 3
#define VELOCITY_INDIRECTION_BUF_SLOT 4

/* Treat closure as singular if the roughness is below this threshold. */
#define BSDF_ROUGHNESS_THRESHOLD 2e-2

/////////////////////////////// Source file 11/////////////////////////////




/**
 * Shared structures, enums & defines between C++ and GLSL.
 * Can also include some math functions but they need to be simple enough to be valid in both
 * language.
 */

#ifndef USE_GPU_SHADER_CREATE_INFO
#  pragma once

#  include  BLI_math_bits.h 
#  include  BLI_memory_utils.hh 

#  include  DRW_gpu_wrapper.hh 

#  include  draw_manager.hh 
#  include  draw_pass.hh 

#  include  eevee_defines.hh 

#  include  GPU_shader_shared.hh 

namespace blender::eevee {

class ShadowDirectional;
class ShadowPunctual;

using namespace draw;

constexpr GPUSamplerState no_filter = GPUSamplerState::default_sampler();
constexpr GPUSamplerState with_filter = {GPU_SAMPLER_FILTERING_LINEAR};
#endif

/* __cplusplus is true when compiling with MSL, so ensure we are not inside a shader. */
#ifdef GPU_SHADER
#  define IS_CPP 0
#else
#  define IS_CPP 1
#endif

#define UBO_MIN_MAX_SUPPORTED_SIZE 1 << 14

/* -------------------------------------------------------------------- */
/** \name Debug Mode
 * \{ */

/** These are just to make more sense of G.debug_value's values. Reserved range is 1-30. */
#define  eDebugMode  uint
const uint 
  DEBUG_NONE = 0u,
  /**
   * Gradient showing light evaluation hot-spots.
   */
  DEBUG_LIGHT_CULLING = 1u,
  /**
   * Show incorrectly down-sample tiles in red.
   */
  DEBUG_HIZ_VALIDATION = 2u,
  /**
   * Display IrradianceCache surfels.
   */
  DEBUG_IRRADIANCE_CACHE_SURFELS_NORMAL = 3u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_IRRADIANCE = 4u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_VISIBILITY = 5u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_CLUSTER = 6u,
  /**
   * Display IrradianceCache virtual offset.
   */
  DEBUG_IRRADIANCE_CACHE_VIRTUAL_OFFSET = 7u,
  DEBUG_IRRADIANCE_CACHE_VALIDITY = 8u,
  /**
   * Show tiles depending on their status.
   */
  DEBUG_SHADOW_TILEMAPS = 10u,
  /**
   * Show content of shadow map. Used to verify projection code.
   */
  DEBUG_SHADOW_VALUES = 11u,
  /**
   * Show random color for each tile. Verify allocation and LOD assignment.
   */
  DEBUG_SHADOW_TILE_RANDOM_COLOR = 12u,
  /**
   * Show random color for each tile. Verify distribution and LOD transitions.
   */
  DEBUG_SHADOW_TILEMAP_RANDOM_COLOR = 13u,
  /**
   * Show storage cost of each pixel in the gbuffer.
   */
  DEBUG_GBUFFER_STORAGE = 14u,
  /**
   * Show evaluation cost of each pixel.
   */
  DEBUG_GBUFFER_EVALUATION = 15u;

/** \} */

/* -------------------------------------------------------------------- */
/** \name Look-Up Table Generation
 * \{ */

#define  PrecomputeType  uint
const uint 
  LUT_GGX_BRDF_SPLIT_SUM = 0u,
  LUT_GGX_BTDF_IOR_GT_ONE = 1u,
  LUT_GGX_BSDF_SPLIT_SUM = 2u,
  LUT_BURLEY_SSS_PROFILE = 3u,
  LUT_RANDOM_WALK_SSS_PROFILE = 4u;

/** \} */

/* -------------------------------------------------------------------- */
/** \name Sampling
 * \{ */

#define  eSamplingDimension  uint
const uint 
  SAMPLING_FILTER_U = 0u,
  SAMPLING_FILTER_V = 1u,
  SAMPLING_LENS_U = 2u,
  SAMPLING_LENS_V = 3u,
  SAMPLING_TIME = 4u,
  SAMPLING_SHADOW_U = 5u,
  SAMPLING_SHADOW_V = 6u,
  SAMPLING_SHADOW_W = 7u,
  SAMPLING_SHADOW_X = 8u,
  SAMPLING_SHADOW_Y = 9u,
  SAMPLING_CLOSURE = 10u,
  SAMPLING_LIGHTPROBE = 11u,
  SAMPLING_TRANSPARENCY = 12u,
  SAMPLING_SSS_U = 13u,
  SAMPLING_SSS_V = 14u,
  SAMPLING_RAYTRACE_U = 15u,
  SAMPLING_RAYTRACE_V = 16u,
  SAMPLING_RAYTRACE_W = 17u,
  SAMPLING_RAYTRACE_X = 18u,
  SAMPLING_AO_U = 19u,
  SAMPLING_AO_V = 20u,
  SAMPLING_CURVES_U = 21u,
  SAMPLING_VOLUME_U = 22u,
  SAMPLING_VOLUME_V = 23u,
  SAMPLING_VOLUME_W = 24u
;

/**
 * IMPORTANT: Make sure the array can contain all sampling dimensions.
 * Also note that it needs to be multiple of 4.
 */
#define SAMPLING_DIMENSION_COUNT 28

/* NOTE(@fclem): Needs to be used in #StorageBuffer because of arrays of scalar. */
struct SamplingData {
  /** Array containing random values from Low Discrepancy Sequence in [0..1) range. */
  float dimensions[SAMPLING_DIMENSION_COUNT];
};
BLI_STATIC_ASSERT_ALIGN(SamplingData, 16)

/* Returns total sample count in a web pattern of the given size. */
static inline int sampling_web_sample_count_get(int web_density, int in_ring_count)
{
  return ((in_ring_count * in_ring_count + in_ring_count) / 2) * web_density + 1;
}

/* Returns lowest possible ring count that contains at least sample_count samples. */
static inline int sampling_web_ring_count_get(int web_density, int sample_count)
{
  /* Inversion of web_sample_count_get(). */
  float x = 2.0f * (float(sample_count) - 1.0f) / float(web_density);
  /* Solving polynomial. We only search positive solution. */
  float discriminant = 1.0f + 4.0f * x;
  return int(ceilf(0.5f * (sqrtf(discriminant) - 1.0f)));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Camera
 * \{ */

#define  eCameraType  uint
const uint 
  CAMERA_PERSP = 0u,
  CAMERA_ORTHO = 1u,
  CAMERA_PANO_EQUIRECT = 2u,
  CAMERA_PANO_EQUISOLID = 3u,
  CAMERA_PANO_EQUIDISTANT = 4u,
  CAMERA_PANO_MIRROR = 5u
;

static inline bool is_panoramic(eCameraType type)
{
  return type > CAMERA_ORTHO;
}

struct CameraData {
  /* View Matrices of the camera, not from any view! */
  float4x4 persmat;
  float4x4 persinv;
  float4x4 viewmat;
  float4x4 viewinv;
  float4x4 winmat;
  float4x4 wininv;
  /** Camera UV scale and bias. */
  float2 uv_scale;
  float2 uv_bias;
  /** Panorama parameters. */
  float2 equirect_scale;
  float2 equirect_scale_inv;
  float2 equirect_bias;
  float fisheye_fov;
  float fisheye_lens;
  /** Clipping distances. */
  float clip_near;
  float clip_far;
  eCameraType type;
  /** World space distance between view corners at unit distance from camera. */
  float screen_diagonal_length;
  float _pad0;
  float _pad1;
  float _pad2;

  bool32_t initialized;

#ifdef __cplusplus
  /* Small constructor to allow detecting new buffers. */
  CameraData() : initialized(false){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(CameraData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Film
 * \{ */

#define FILM_PRECOMP_SAMPLE_MAX 16

#define  eFilmWeightLayerIndex  uint
const uint 
  FILM_WEIGHT_LAYER_ACCUMULATION = 0u,
  FILM_WEIGHT_LAYER_DISTANCE = 1u;

#define  ePassStorageType  uint
const uint 
  PASS_STORAGE_COLOR = 0u,
  PASS_STORAGE_VALUE = 1u,
  PASS_STORAGE_CRYPTOMATTE = 2u;

#define  PassCategory  uint
const uint 
  PASS_CATEGORY_DATA = 1u << 0,
  PASS_CATEGORY_COLOR_1 = 1u << 1,
  PASS_CATEGORY_COLOR_2 = 1u << 2,
  PASS_CATEGORY_COLOR_3 = 1u << 3,
  PASS_CATEGORY_AOV = 1u << 4,
  PASS_CATEGORY_CRYPTOMATTE = 1u << 5;
ENUM_OPERATORS(PassCategory, PASS_CATEGORY_CRYPTOMATTE)

struct FilmSample {
  int2 texel;
  float weight;
  /** Used for accumulation. */
  float weight_sum_inv;
};
BLI_STATIC_ASSERT_ALIGN(FilmSample, 16)

struct FilmData {
  /** Size of the film in pixels. */
  int2 extent;
  /** Offset to convert from Display space to Film space, in pixels. */
  int2 offset;
  /** Size of the render buffers including overscan when rendering the main views, in pixels. */
  int2 render_extent;
  /**
   * Sub-pixel offset applied to the window matrix.
   * NOTE: In final film pixel unit.
   * NOTE: Positive values makes the view translate in the negative axes direction.
   * NOTE: The origin is the center of the lower left film pixel of the area covered by a render
   * pixel if using scaled resolution rendering.
   */
  float2 subpixel_offset;
  /** Scaling factor to convert texel to uvs. */
  float2 extent_inv;
  /**
   * Number of border pixels on all sides inside the render_extent that do not contribute to the
   * final image.
   */
  int overscan;
  /** Is true if history is valid and can be sampled. Bypass history to resets accumulation. */
  bool32_t use_history;
  /** Controlled by user in lookdev mode or by render settings. */
  float background_opacity;
  /** Output counts per type. */
  int color_len, value_len;
  /** Index in color_accum_img or value_accum_img of each pass. -1 if pass is not enabled. */
  int mist_id;
  int normal_id;
  int position_id;
  int vector_id;
  int diffuse_light_id;
  int diffuse_color_id;
  int specular_light_id;
  int specular_color_id;
  int volume_light_id;
  int emission_id;
  int environment_id;
  int shadow_id;
  int ambient_occlusion_id;
  int transparent_id;
  /** Not indexed but still not -1 if enabled. */
  int depth_id;
  int combined_id;
  /** Id of the render-pass to be displayed. -1 for combined. */
  int display_id;
  /** Storage type of the render-pass to be displayed. */
  ePassStorageType display_storage_type;
  /** True if we bypass the accumulation and directly output the accumulation buffer. */
  bool32_t display_only;
  /** Start of AOVs and number of aov. */
  int aov_color_id, aov_color_len;
  int aov_value_id, aov_value_len;
  /** Start of cryptomatte per layer (-1 if pass is not enabled). */
  int cryptomatte_object_id;
  int cryptomatte_asset_id;
  int cryptomatte_material_id;
  /** Max number of samples stored per layer (is even number). */
  int cryptomatte_samples_len;
  /** Settings to render mist pass */
  float mist_scale, mist_bias, mist_exponent;
  /** Scene exposure used for better noise reduction. */
  float exposure_scale;
  /** Scaling factor for scaled resolution rendering. */
  int scaling_factor;
  /** Film pixel filter radius. */
  float filter_radius;
  /** Precomputed samples. First in the table is the closest one. The rest is unordered. */
  int samples_len;
  /** Sum of the weights of all samples in the sample table. */
  float samples_weight_total;
  int _pad1;
  int _pad2;
  FilmSample samples[FILM_PRECOMP_SAMPLE_MAX];
};
BLI_STATIC_ASSERT_ALIGN(FilmData, 16)

static inline float film_filter_weight(float filter_radius, float sample_distance_sqr)
{
#if 1 /* Faster */
  /* Gaussian fitted to Blackman-Harris. */
  float r = sample_distance_sqr / (filter_radius * filter_radius);
  const float sigma = 0.284;
  const float fac = -0.5 / (sigma * sigma);
  float weight = expf(fac * r);
#else
  /* Blackman-Harris filter. */
  float r = M_TAU * saturate(0.5 + sqrtf(sample_distance_sqr) / (2.0 * filter_radius));
  float weight = 0.35875 - 0.48829 * cosf(r) + 0.14128 * cosf(2.0 * r) - 0.01168 * cosf(3.0 * r);
#endif
  return weight;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name RenderBuffers
 * \{ */

/* Theoretical max is 128 as we are using texture array and VRAM usage.
 * However, the output_aov() function perform a linear search inside all the hashes.
 * If we find a way to avoid this we could bump this number up. */
#define AOV_MAX 16

struct AOVsInfoData {
  /* Use uint4 to workaround std140 packing rules.
   * Only the x value is used. */
  uint4 hash_value[AOV_MAX];
  uint4 hash_color[AOV_MAX];
  /* Length of used data. */
  int color_len;
  int value_len;
  /** Id of the AOV to be displayed (from the start of the AOV array). -1 for combined. */
  int display_id;
  /** True if the AOV to be displayed is from the value accumulation buffer. */
  bool32_t display_is_value;
};
BLI_STATIC_ASSERT_ALIGN(AOVsInfoData, 16)

struct RenderBuffersInfoData {
  AOVsInfoData aovs;
  /* Color. */
  int color_len;
  int normal_id;
  int position_id;
  int diffuse_light_id;
  int diffuse_color_id;
  int specular_light_id;
  int specular_color_id;
  int volume_light_id;
  int emission_id;
  int environment_id;
  int transparent_id;
  /* Value */
  int value_len;
  int shadow_id;
  int ambient_occlusion_id;
  int _pad0, _pad1;
};
BLI_STATIC_ASSERT_ALIGN(RenderBuffersInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name VelocityModule
 * \{ */

#define VELOCITY_INVALID 512.0

#define  eVelocityStep  uint
const uint 
  STEP_PREVIOUS = 0,
  STEP_NEXT = 1,
  STEP_CURRENT = 2;

struct VelocityObjectIndex {
  /** Offset inside #VelocityObjectBuf for each time-step. Indexed using eVelocityStep. */
  packed_int3 ofs;
  /** Temporary index to copy this to the #VelocityIndexBuf. */
  uint resource_id;

#ifdef __cplusplus
  VelocityObjectIndex() : ofs(-1, -1, -1), resource_id(-1){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(VelocityObjectIndex, 16)

struct VelocityGeometryIndex {
  /** Offset inside #VelocityGeometryBuf for each time-step. Indexed using eVelocityStep. */
  packed_int3 ofs;
  /** If true, compute deformation motion blur. */
  bool32_t do_deform;
  /**
   * Length of data inside #VelocityGeometryBuf for each time-step.
   * Indexed using eVelocityStep.
   */
  packed_int3 len;

  int _pad0;

#ifdef __cplusplus
  VelocityGeometryIndex() : ofs(-1, -1, -1), do_deform(false), len(-1, -1, -1), _pad0(1){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(VelocityGeometryIndex, 16)

struct VelocityIndex {
  VelocityObjectIndex obj;
  VelocityGeometryIndex geo;
};
BLI_STATIC_ASSERT_ALIGN(VelocityGeometryIndex, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Motion Blur
 * \{ */

#define MOTION_BLUR_TILE_SIZE 32
#define MOTION_BLUR_MAX_TILE 512 /* 16384 / MOTION_BLUR_TILE_SIZE */
struct MotionBlurData {
  /** As the name suggests. Used to avoid a division in the sampling. */
  float2 target_size_inv;
  /** Viewport motion scaling factor. Make blur relative to frame time not render time. */
  float2 motion_scale;
  /** Depth scaling factor. Avoid blurring background behind moving objects. */
  float depth_scale;

  float _pad0, _pad1, _pad2;
};
BLI_STATIC_ASSERT_ALIGN(MotionBlurData, 16)

/* For some reasons some GLSL compilers do not like this struct.
 * So we declare it as a uint array instead and do indexing ourselves. */
#ifdef __cplusplus
struct MotionBlurTileIndirection {
  /**
   * Stores indirection to the tile with the highest velocity covering each tile.
   * This is stored using velocity in the MSB to be able to use atomicMax operations.
   */
  uint prev[MOTION_BLUR_MAX_TILE][MOTION_BLUR_MAX_TILE];
  uint next[MOTION_BLUR_MAX_TILE][MOTION_BLUR_MAX_TILE];
};
BLI_STATIC_ASSERT_ALIGN(MotionBlurTileIndirection, 16)
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volumes
 * \{ */

struct VolumesInfoData {
  /* During object voxelization, we need to use an infinite projection matrix to avoid clipping
   * faces. But they cannot be used for recovering the view position from froxel position as they
   * are not invertible. We store the finite projection matrix and use it for this purpose. */
  float4x4 winmat_finite;
  float4x4 wininv_finite;
  /* Copies of the matrices above but without jittering. Used for re-projection. */
  float4x4 wininv_stable;
  float4x4 winmat_stable;
  /* Previous render sample copy of winmat_stable. */
  float4x4 history_winmat_stable;
  /* Transform from current view space to previous render sample view space. */
  float4x4 curr_view_to_past_view;
  /* Size of the froxel grid texture. */
  packed_int3 tex_size;
  /* Maximum light intensity during volume lighting evaluation. */
  float light_clamp;
  /* Inverse of size of the froxel grid. */
  packed_float3 inv_tex_size;
  /* Maximum light intensity during volume lighting evaluation. */
  float shadow_steps;
  /* 2D scaling factor to make froxel squared. */
  float2 coord_scale;
  /* Extent and inverse extent of the main shading view (render extent, not film extent). */
  float2 main_view_extent;
  float2 main_view_extent_inv;
  /* Size in main view pixels of one froxel in XY. */
  int tile_size;
  /* Hi-Z LOD to use during volume shadow tagging. */
  int tile_size_lod;
  /* Depth to froxel mapping. */
  float depth_near;
  float depth_far;
  float depth_distribution;
  /* Previous render sample copy of the depth mapping parameters. */
  float history_depth_near;
  float history_depth_far;
  float history_depth_distribution;
  /* Amount of history to blend during the scatter phase. */
  float history_opacity;

  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(VolumesInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Depth of field
 * \{ */

/* 5% error threshold. */
#define DOF_FAST_GATHER_COC_ERROR 0.05
#define DOF_GATHER_RING_COUNT 5
#define DOF_DILATE_RING_COUNT 3

struct DepthOfFieldData {
  /** Size of the render targets for gather & scatter passes. */
  int2 extent;
  /** Size of a pixel in uv space (1.0 / extent). */
  float2 texel_size;
  /** Scale factor for anisotropic bokeh. */
  float2 bokeh_anisotropic_scale;
  float2 bokeh_anisotropic_scale_inv;
  /* Correction factor to align main target pixels with the filtered mipmap chain texture. */
  float2 gather_uv_fac;
  /** Scatter parameters. */
  float scatter_coc_threshold;
  float scatter_color_threshold;
  float scatter_neighbor_max_color;
  int scatter_sprite_per_row;
  /** Number of side the bokeh shape has. */
  float bokeh_blades;
  /** Rotation of the bokeh shape. */
  float bokeh_rotation;
  /** Multiplier and bias to apply to linear depth to Circle of confusion (CoC). */
  float coc_mul, coc_bias;
  /** Maximum absolute allowed Circle of confusion (CoC). Min of computed max and user max. */
  float coc_abs_max;
  /** Copy of camera type. */
  eCameraType camera_type;
  /** Weights of spatial filtering in stabilize pass. Not array to avoid alignment restriction. */
  float4 filter_samples_weight;
  float filter_center_weight;
  /** Max number of sprite in the scatter pass for each ground. */
  int scatter_max_rect;

  int _pad0, _pad1;
};
BLI_STATIC_ASSERT_ALIGN(DepthOfFieldData, 16)

struct ScatterRect {
  /** Color and CoC of the 4 pixels the scatter sprite represents. */
  float4 color_and_coc[4];
  /** Rect center position in half pixel space. */
  float2 offset;
  /** Rect half extent in half pixel space. */
  float2 half_extent;
};
BLI_STATIC_ASSERT_ALIGN(ScatterRect, 16)

/** WORKAROUND(@fclem): This is because this file is included before common_math_lib.glsl. */
#ifndef M_PI
#  define EEVEE_PI
#  define M_PI 3.14159265358979323846 /* pi */
#endif

static inline float coc_radius_from_camera_depth(DepthOfFieldData dof, float depth)
{
  depth = (dof.camera_type != CAMERA_ORTHO) ? 1.0f / depth : depth;
  return dof.coc_mul * depth + dof.coc_bias;
}

static inline float regular_polygon_side_length(float sides_count)
{
  return 2.0f * sinf(M_PI / sides_count);
}

/* Returns intersection ratio between the radius edge at theta and the regular polygon edge.
 * Start first corners at theta == 0. */
static inline float circle_to_polygon_radius(float sides_count, float theta)
{
  /* From Graphics Gems from CryENGINE 3 (SIGGRAPH 2013) by Tiago Sousa (slide 36). */
  float side_angle = (2.0f * M_PI) / sides_count;
  return cosf(side_angle * 0.5f) /
         cosf(theta - side_angle * floorf((sides_count * theta + M_PI) / (2.0f * M_PI)));
}

/* Remap input angle to have homogenous spacing of points along a polygon edge.
 * Expects theta to be in [0..2pi] range. */
static inline float circle_to_polygon_angle(float sides_count, float theta)
{
  float side_angle = (2.0f * M_PI) / sides_count;
  float halfside_angle = side_angle * 0.5f;
  float side = floorf(theta / side_angle);
  /* Length of segment from center to the middle of polygon side. */
  float adjacent = circle_to_polygon_radius(sides_count, 0.0f);

  /* This is the relative position of the sample on the polygon half side. */
  float local_theta = theta - side * side_angle;
  float ratio = (local_theta - halfside_angle) / halfside_angle;

  float halfside_len = regular_polygon_side_length(sides_count) * 0.5f;
  float opposite = ratio * halfside_len;

  /* NOTE: atan(y_over_x) has output range [-M_PI_2..M_PI_2]. */
  float final_local_theta = atanf(opposite / adjacent);

  return side * side_angle + final_local_theta;
}

#ifdef EEVEE_PI
#  undef M_PI
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light Culling
 * \{ */

/* Number of items we can cull. Limited by how we store CullingZBin. */
#define CULLING_MAX_ITEM 65536
/* Fine grained subdivision in the Z direction. Limited by the LDS in z-binning compute shader. */
#define CULLING_ZBIN_COUNT 4096
/* Max tile map resolution per axes. */
#define CULLING_TILE_RES 16

struct LightCullingData {
  /** Scale applied to tile pixel coordinates to get target UV coordinate. */
  float2 tile_to_uv_fac;
  /** Scale and bias applied to linear Z to get zbin. */
  float zbin_scale;
  float zbin_bias;
  /** Valid item count in the source data array. */
  uint items_count;
  /** Items that are processed by the 2.5D culling. */
  uint local_lights_len;
  /** Items that are **NOT** processed by the 2.5D culling (i.e: Sun Lights). */
  uint sun_lights_len;
  /** Number of items that passes the first culling test. (local lights only) */
  uint visible_count;
  /** Extent of one square tile in pixels. */
  float tile_size;
  /** Number of tiles on the X/Y axis. */
  uint tile_x_len;
  uint tile_y_len;
  /** Number of word per tile. Depends on the maximum number of lights. */
  uint tile_word_len;
};
BLI_STATIC_ASSERT_ALIGN(LightCullingData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Lights
 * \{ */

#define LIGHT_NO_SHADOW -1

#define  eLightType  uint
const uint 
  LIGHT_SUN = 0u,
  LIGHT_SUN_ORTHO = 1u,
  /* Point light. */
  LIGHT_OMNI_SPHERE = 10u,
  LIGHT_OMNI_DISK = 11u,
  /* Spot light. */
  LIGHT_SPOT_SPHERE = 12u,
  LIGHT_SPOT_DISK = 13u,
  /* Area light. */
  LIGHT_RECT = 20u,
  LIGHT_ELLIPSE = 21u
;

#define  LightingType  uint
const uint 
  LIGHT_DIFFUSE = 0u,
  LIGHT_SPECULAR = 1u,
  LIGHT_TRANSMISSION = 2u,
  LIGHT_VOLUME = 3u;

static inline bool is_area_light(eLightType type)
{
  return type >= LIGHT_RECT;
}

static inline bool is_point_light(eLightType type)
{
  return type >= LIGHT_OMNI_SPHERE && type <= LIGHT_SPOT_DISK;
}

static inline bool is_spot_light(eLightType type)
{
  return type == LIGHT_SPOT_SPHERE || type == LIGHT_SPOT_DISK;
}

static inline bool is_sphere_light(eLightType type)
{
  return type == LIGHT_SPOT_SPHERE || type == LIGHT_OMNI_SPHERE;
}

static inline bool is_oriented_disk_light(eLightType type)
{
  return type == LIGHT_SPOT_DISK || type == LIGHT_OMNI_DISK;
}

static inline bool is_sun_light(eLightType type)
{
  return type < LIGHT_OMNI_SPHERE;
}

static inline bool is_local_light(eLightType type)
{
  return type >= LIGHT_OMNI_SPHERE;
}

/* Using define because GLSL doesn't have inheritance, and encapsulation forces us to add some
 * unneeded padding. */
#define LOCAL_LIGHT_COMMON \
  /** Special radius factor for point lighting (volume). */ \
  float radius_squared; \
  /** Maximum influence radius. Used for culling. Equal to clip far distance. */ \
  float influence_radius_max; \
  /** Influence radius (inverted and squared) adjusted for Surface / Volume power. */ \
  float influence_radius_invsqr_surface; \
  float influence_radius_invsqr_volume; \
  /** --- Shadow Data --- */ \
  /** Other parts of the perspective matrix. Assumes symmetric frustum. */ \
  float clip_side; \
  /** Number of allocated tilemap for this local light. */ \
  int tilemaps_count; \
  /** Scaling factor to the light shape for shadow ray casting. */ \
  float shadow_scale; \
  /** Shift to apply to the light origin to get the shadow projection origin. */ \
  float shadow_projection_shift;

/* Untyped local light data. Gets reinterpreted to LightSpotData and LightAreaData.
 * Allow access to local light common data without casting. */
struct LightLocalData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  float _pad1;
  float _pad2;

  float2 _pad3;
  float _pad4;
  float _pad5;
};
BLI_STATIC_ASSERT_ALIGN(LightLocalData, 16)

/* Despite the name, is also used for omni light. */
struct LightSpotData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  /** Sphere light radius. */
  float radius;
  /** Scale and bias to spot equation parameter. Used for adjusting the falloff. */
  float spot_mul;

  /** Inverse spot size (in X and Y axes). */
  float2 spot_size_inv;
  /** Spot angle tangent. */
  float spot_tan;
  float spot_bias;
};
BLI_STATIC_ASSERT(sizeof(LightSpotData) == sizeof(LightLocalData),  Data size must match )

struct LightAreaData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  float _pad2;
  float _pad3;

  /** Shape size. */
  float2 size;
  float _pad5;
  float _pad6;
};
BLI_STATIC_ASSERT(sizeof(LightAreaData) == sizeof(LightLocalData),  Data size must match )

struct LightSunData {
  float radius;
  float _pad0;
  float _pad1;
  float _pad2;

  float _pad3;
  float _pad4;
  /** --- Shadow Data --- */
  /** Offset of the LOD min in LOD min tile units. Split positive and negative for bit-shift. */
  int2 clipmap_base_offset_neg;

  int2 clipmap_base_offset_pos;
  /** Angle covered by the light shape for shadow ray casting. */
  float shadow_angle;
  /** Trace distance around the shading point. */
  float shadow_trace_distance;

  /** Offset to convert from world units to tile space of the clipmap_lod_max. */
  float2 clipmap_origin;
  /** Clip-map LOD range to avoid sampling outside of valid range. */
  int clipmap_lod_min;
  int clipmap_lod_max;
};
BLI_STATIC_ASSERT(sizeof(LightSunData) == sizeof(LightLocalData),  Data size must match )

/* Enable when debugging. This is quite costly. */
#define SAFE_UNION_ACCESS 0

#if IS_CPP
/* C++ always uses union. */
#  define USE_LIGHT_UNION 1
#elif defined(GPU_BACKEND_METAL) && !SAFE_UNION_ACCESS
/* Metal supports union, but force usage of the getters if SAFE_UNION_ACCESS is enabled. */
#  define USE_LIGHT_UNION 1
#else
/* Use getter functions on GPU if not supported or if SAFE_UNION_ACCESS is enabled. */
#  define USE_LIGHT_UNION 0
#endif

struct LightData {
  /** Normalized object to world matrix. */
  /* TODO(fclem): Use float4x3. */
  float4x4 object_mat;
  /** Aliases for axes. */
#ifndef USE_GPU_SHADER_CREATE_INFO
#  define _right object_mat[0]
#  define _up object_mat[1]
#  define _back object_mat[2]
#  define _position object_mat[3]
#else
#  define _right object_mat[0].xyz
#  define _up object_mat[1].xyz
#  define _back object_mat[2].xyz
#  define _position object_mat[3].xyz
#endif

  /** Power depending on shader type. Referenced by LightingType. */
  float4 power;
  /** Light Color. */
  packed_float3 color;
  /** Light Type. */
  eLightType type;

  /** --- Shadow Data --- */
  /** Near clip distances. Float stored as orderedIntBitsToFloat for atomic operations. */
  int clip_near;
  int clip_far;
  /** Index of the first tile-map. Set to LIGHT_NO_SHADOW if light is not casting shadow. */
  int tilemap_index;
  /* Radius in pixels for shadow filtering. */
  float pcf_radius;

  /* Shadow Map resolution bias. */
  float lod_bias;
  float _pad0;
  float _pad1;
  float _pad2;

#if USE_LIGHT_UNION
  union {
    LightLocalData local;
    LightSpotData spot;
    LightAreaData area;
    LightSunData sun;
  };
#else
  /* Use `light_*_data_get(light)` to access typed data. */
  LightLocalData do_not_access_directly;
#endif
};
BLI_STATIC_ASSERT_ALIGN(LightData, 16)

#ifdef GPU_SHADER
#  define CHECK_TYPE_PAIR(a, b)
#  define CHECK_TYPE(a, b)
#  define FLOAT_AS_INT floatBitsToInt
#  define TYPECAST_NOOP

#else /* C++ */
#  define FLOAT_AS_INT float_as_int
#  define TYPECAST_NOOP
#endif

/* In addition to the static asserts that verify correct member assignment, also verify access on
 * the GPU so that only lights of a certain type can read for the appropriate union member.
 * Return cross platform garbage data as some platform can return cleared memory if we early exit.
 */
#if SAFE_UNION_ACCESS
#  ifdef GPU_SHADER
/* Should result in a beautiful zebra pattern on invalid load. */
#    if defined(GPU_FRAGMENT_SHADER)
#      define GARBAGE_VALUE sin(gl_FragCoord.x + gl_FragCoord.y)
#    elif defined(GPU_COMPUTE_SHADER)
#      define GARBAGE_VALUE \
        sin(float(gl_GlobalInvocationID.x + gl_GlobalInvocationID.y + gl_GlobalInvocationID.z))
#    else
#      define GARBAGE_VALUE sin(float(gl_VertexID))
#    endif

/* Can be set to zero if zebra creates out-of-bound accesses and crashes. At least avoid UB. */
// #    define GARBAGE_VALUE 0.0

#  else /* C++ */
#    define GARBAGE_VALUE 0.0f
#  endif

#  define SAFE_BEGIN(data_type, check) \
    data_type data; \
    bool _validity_check = check; \
    float _garbage = GARBAGE_VALUE;

/* Assign garbage value if the light type check fails. */
#  define SAFE_ASSIGN_LIGHT_TYPE_CHECK(_type, _value) \
    (_validity_check ? (_value) : _type(_garbage))
#else
#  define SAFE_BEGIN(data_type, check) data_type data;
#  define SAFE_ASSIGN_LIGHT_TYPE_CHECK(_type, _value) _value
#endif

#if USE_LIGHT_UNION
#  define DATA_MEMBER local
#else
#  define DATA_MEMBER do_not_access_directly
#endif

#define ERROR_OFS(a, b)  Offset of   STRINGIFY(a)   mismatch offset of   STRINGIFY(b)

/* This is a dangerous process, make sure to static assert every assignment. */
#define SAFE_ASSIGN(a, reinterpret_fn, in_type, b) \
  CHECK_TYPE_PAIR(data.a, reinterpret_fn(light.DATA_MEMBER.b)); \
  data.a = reinterpret_fn(SAFE_ASSIGN_LIGHT_TYPE_CHECK(in_type, light.DATA_MEMBER.b)); \
  BLI_STATIC_ASSERT(offsetof(decltype(data), a) == offsetof(LightLocalData, b), ERROR_OFS(a, b))

#define SAFE_ASSIGN_FLOAT(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, float, b);
#define SAFE_ASSIGN_FLOAT2(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, float2, b);
#define SAFE_ASSIGN_INT(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, int, b);
#define SAFE_ASSIGN_FLOAT_AS_INT(a, b) SAFE_ASSIGN(a, FLOAT_AS_INT, float, b);
#define SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(a, b, c) \
  SAFE_ASSIGN_FLOAT_AS_INT(a.x, b); \
  SAFE_ASSIGN_FLOAT_AS_INT(a.y, c);

#if !USE_LIGHT_UNION || IS_CPP

/* These functions are not meant to be used in C++ code. They are only defined on the C++ side for
 * static assertions. Hide them. */
#  if IS_CPP
namespace do_not_use {
#  endif

static inline LightSpotData light_local_data_get(LightData light)
{
  SAFE_BEGIN(LightSpotData, is_local_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  return data;
}

static inline LightSpotData light_spot_data_get(LightData light)
{
  SAFE_BEGIN(LightSpotData, is_spot_light(light.type) || is_point_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  SAFE_ASSIGN_FLOAT(radius, _pad1)
  SAFE_ASSIGN_FLOAT(spot_mul, _pad2)
  SAFE_ASSIGN_FLOAT2(spot_size_inv, _pad3)
  SAFE_ASSIGN_FLOAT(spot_tan, _pad4)
  SAFE_ASSIGN_FLOAT(spot_bias, _pad5)
  return data;
}

static inline LightAreaData light_area_data_get(LightData light)
{
  SAFE_BEGIN(LightAreaData, is_area_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  SAFE_ASSIGN_FLOAT2(size, _pad3)
  return data;
}

static inline LightSunData light_sun_data_get(LightData light)
{
  SAFE_BEGIN(LightSunData, is_sun_light(light.type))
  SAFE_ASSIGN_FLOAT(radius, radius_squared)
  SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(clipmap_base_offset_neg, shadow_scale, shadow_projection_shift)
  SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(clipmap_base_offset_pos, _pad0_reserved, _pad1_reserved)
  SAFE_ASSIGN_FLOAT(shadow_angle, _pad1)
  SAFE_ASSIGN_FLOAT(shadow_trace_distance, _pad2)
  SAFE_ASSIGN_FLOAT2(clipmap_origin, _pad3)
  SAFE_ASSIGN_FLOAT_AS_INT(clipmap_lod_min, _pad4)
  SAFE_ASSIGN_FLOAT_AS_INT(clipmap_lod_max, _pad5)
  return data;
}

#  if IS_CPP
}  // namespace do_not_use
#  endif

#endif

#if USE_LIGHT_UNION
#  define light_local_data_get(light) light.local
#  define light_spot_data_get(light) light.spot
#  define light_area_data_get(light) light.area
#  define light_sun_data_get(light) light.sun
#endif

#undef DATA_MEMBER
#undef GARBAGE_VALUE
#undef FLOAT_AS_INT
#undef TYPECAST_NOOP
#undef SAFE_BEGIN
#undef SAFE_ASSIGN_LIGHT_TYPE_CHECK
#undef ERROR_OFS
#undef SAFE_ASSIGN
#undef SAFE_ASSIGN_FLOAT
#undef SAFE_ASSIGN_FLOAT2
#undef SAFE_ASSIGN_INT
#undef SAFE_ASSIGN_FLOAT_AS_INT
#undef SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE

static inline int light_tilemap_max_get(LightData light)
{
  /* This is not something we need in performance critical code. */
  if (is_sun_light(light.type)) {
    return light.tilemap_index +
           (light_sun_data_get(light).clipmap_lod_max - light_sun_data_get(light).clipmap_lod_min);
  }
  return light.tilemap_index + light_local_data_get(light).tilemaps_count - 1;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Shadows
 *
 * Shadow data for either a directional shadow or a punctual shadow.
 *
 * A punctual shadow is composed of 1, 5 or 6 shadow regions.
 * Regions are sorted in this order -Z, +X, -X, +Y, -Y, +Z.
 * Face index is computed from light's object space coordinates.
 *
 * A directional light shadow is composed of multiple clip-maps with each level
 * covering twice as much area as the previous one.
 * \{ */

#define  eShadowProjectionType  uint
const uint 
  SHADOW_PROJECTION_CUBEFACE = 0u,
  SHADOW_PROJECTION_CLIPMAP = 1u,
  SHADOW_PROJECTION_CASCADE = 2u;

static inline int2 shadow_cascade_grid_offset(int2 base_offset, int level_relative)
{
  return (base_offset * level_relative) / (1 << 16);
}

/**
 * Small descriptor used for the tile update phase. Updated by CPU & uploaded to GPU each redraw.
 */
struct ShadowTileMapData {
  /** Cached, used for rendering. */
  float4x4 viewmat;
  /** Precomputed matrix, not used for rendering but for tagging. */
  float4x4 winmat;
  /** Punctual : Corners of the frustum. (vec3 padded to vec4) */
  float4 corners[4];
  /** Integer offset of the center of the 16x16 tiles from the origin of the tile space. */
  int2 grid_offset;
  /** Shift between previous and current grid_offset. Allows update tagging. */
  int2 grid_shift;
  /** True for punctual lights. */
  eShadowProjectionType projection_type;
  /** Multiple of SHADOW_TILEDATA_PER_TILEMAP. Offset inside the tile buffer. */
  int tiles_index;
  /** Index of persistent data in the persistent data buffer. */
  int clip_data_index;
  /** Bias LOD to tag for usage to lower the amount of tile used. */
  float lod_bias;
  int _pad0;
  int _pad1;
  int _pad2;
  /** Near and far clip distances for punctual. */
  float clip_near;
  float clip_far;
  /** Half of the tilemap size in world units. Used to compute window matrix. */
  float half_size;
  /** Offset in local space to the tilemap center in world units. Used for directional winmat. */
  float2 center_offset;
};
BLI_STATIC_ASSERT_ALIGN(ShadowTileMapData, 16)

/**
 * Per tilemap data persistent on GPU.
 */
struct ShadowTileMapClip {
  /** Clip distances that were used to render the pages. */
  float clip_near_stored;
  float clip_far_stored;
  /** Near and far clip distances for directional. Float stored as int for atomic operations. */
  /** NOTE: These are positive just like camera parameters. */
  int clip_near;
  int clip_far;
};
BLI_STATIC_ASSERT_ALIGN(ShadowTileMapClip, 16)

struct ShadowPagesInfoData {
  /** Number of free pages in the free page buffer. */
  int page_free_count;
  /** Number of page allocations needed for this cycle. */
  int page_alloc_count;
  /** Index of the next cache page in the cached page buffer. */
  uint page_cached_next;
  /** Index of the first page in the buffer since the last defragment. */
  uint page_cached_start;
  /** Index of the last page in the buffer since the last defragment. */
  uint page_cached_end;

  int _pad0;
  int _pad1;
  int _pad2;
};
BLI_STATIC_ASSERT_ALIGN(ShadowPagesInfoData, 16)

struct ShadowStatistics {
  /** Statistics that are read back to CPU after a few frame (to avoid stall). */
  int page_used_count;
  int page_update_count;
  int page_allocated_count;
  int page_rendered_count;
  int view_needed_count;
  int _pad0;
  int _pad1;
  int _pad2;
};
BLI_STATIC_ASSERT_ALIGN(ShadowStatistics, 16)

/** Decoded tile data structure. */
struct ShadowTileData {
  /** Page inside the virtual shadow map atlas. */
  uint3 page;
  /** Page index inside pages_cached_buf. Only valid if `is_cached` is true. */
  uint cache_index;
  /** If the tile is needed for rendering. */
  bool is_used;
  /** True if an update is needed. This persists even if the tile gets unused. */
  bool do_update;
  /** True if the tile owns the page (mutually exclusive with `is_cached`). */
  bool is_allocated;
  /** True if the tile has been staged for rendering. This will remove the `do_update` flag. */
  bool is_rendered;
  /** True if the tile is inside the pages_cached_buf (mutually exclusive with `is_allocated`). */
  bool is_cached;
};
/** \note Stored packed as a uint. */
#define ShadowTileDataPacked uint

#define  eShadowFlag  uint
const uint 
  SHADOW_NO_DATA = 0u,
  SHADOW_IS_CACHED = (1u << 27u),
  SHADOW_IS_ALLOCATED = (1u << 28u),
  SHADOW_DO_UPDATE = (1u << 29u),
  SHADOW_IS_RENDERED = (1u << 30u),
  SHADOW_IS_USED = (1u << 31u)
;

/* NOTE: Trust the input to be in valid range (max is [3,3,255]).
 * If it is in valid range, it should pack to 12bits so that `shadow_tile_pack()` can use it.
 * But sometime this is used to encode invalid pages uint3(-1) and it needs to output uint(-1). */
static inline uint shadow_page_pack(uint3 page)
{
  return (page.x << 0u) | (page.y << 2u) | (page.z << 4u);
}
static inline uint3 shadow_page_unpack(uint data)
{
  uint3 page;
  BLI_STATIC_ASSERT(SHADOW_PAGE_PER_ROW <= 4 && SHADOW_PAGE_PER_COL <= 4,  Update page packing )
  page.x = (data >> 0u) & 3u;
  page.y = (data >> 2u) & 3u;
  BLI_STATIC_ASSERT(SHADOW_MAX_PAGE <= 4096,  Update page packing )
  page.z = (data >> 4u) & 255u;
  return page;
}

static inline ShadowTileData shadow_tile_unpack(ShadowTileDataPacked data)
{
  ShadowTileData tile;
  tile.page = shadow_page_unpack(data);
  /* -- 12 bits -- */
  /* Unused bits. */
  /* -- 15 bits -- */
  BLI_STATIC_ASSERT(SHADOW_MAX_PAGE <= 4096,  Update page packing )
  tile.cache_index = (data >> 15u) & 4095u;
  /* -- 27 bits -- */
  tile.is_used = (data & SHADOW_IS_USED) != 0;
  tile.is_cached = (data & SHADOW_IS_CACHED) != 0;
  tile.is_allocated = (data & SHADOW_IS_ALLOCATED) != 0;
  tile.is_rendered = (data & SHADOW_IS_RENDERED) != 0;
  tile.do_update = (data & SHADOW_DO_UPDATE) != 0;
  return tile;
}

static inline ShadowTileDataPacked shadow_tile_pack(ShadowTileData tile)
{
  uint data;
  /* NOTE: Page might be set to invalid values for tracking invalid usages.
   * So we have to mask the result. */
  data = shadow_page_pack(tile.page) & uint(SHADOW_MAX_PAGE - 1);
  data |= (tile.cache_index & 4095u) << 15u;
  data |= (tile.is_used ? uint(SHADOW_IS_USED) : 0);
  data |= (tile.is_allocated ? uint(SHADOW_IS_ALLOCATED) : 0);
  data |= (tile.is_cached ? uint(SHADOW_IS_CACHED) : 0);
  data |= (tile.is_rendered ? uint(SHADOW_IS_RENDERED) : 0);
  data |= (tile.do_update ? uint(SHADOW_DO_UPDATE) : 0);
  return data;
}

/**
 * Decoded tile data structure.
 * Similar to ShadowTileData, this one is only used for rendering and packed into `tilemap_tx`.
 * This allow to reuse some bits for other purpose.
 */
struct ShadowSamplingTile {
  /** Page inside the virtual shadow map atlas. */
  uint3 page;
  /** LOD pointed to LOD 0 tile page. */
  uint lod;
  /** Offset to the texel position to align with the LOD page start. (directional only). */
  uint2 lod_offset;
  /** If the tile is needed for rendering. */
  bool is_valid;
};
/** \note Stored packed as a uint. */
#define ShadowSamplingTilePacked uint

/* NOTE: Trust the input to be in valid range [0, (1 << SHADOW_TILEMAP_MAX_CLIPMAP_LOD) - 1].
 * Maximum LOD level index we can store is SHADOW_TILEMAP_MAX_CLIPMAP_LOD,
 * so we need SHADOW_TILEMAP_MAX_CLIPMAP_LOD bits to store the offset in each dimension.
 * Result fits into SHADOW_TILEMAP_MAX_CLIPMAP_LOD * 2 bits. */
static inline uint shadow_lod_offset_pack(uint2 ofs)
{
  BLI_STATIC_ASSERT(SHADOW_TILEMAP_MAX_CLIPMAP_LOD <= 8,  Update page packing )
  return ofs.x | (ofs.y << SHADOW_TILEMAP_MAX_CLIPMAP_LOD);
}
static inline uint2 shadow_lod_offset_unpack(uint data)
{
  return (uint2(data) >> uint2(0, SHADOW_TILEMAP_MAX_CLIPMAP_LOD)) &
         uint2((1 << SHADOW_TILEMAP_MAX_CLIPMAP_LOD) - 1);
}

static inline ShadowSamplingTile shadow_sampling_tile_unpack(ShadowSamplingTilePacked data)
{
  ShadowSamplingTile tile;
  tile.page = shadow_page_unpack(data);
  /* -- 12 bits -- */
  /* Max value is actually SHADOW_TILEMAP_MAX_CLIPMAP_LOD but we mask the bits. */
  tile.lod = (data >> 12u) & 15u;
  /* -- 16 bits -- */
  tile.lod_offset = shadow_lod_offset_unpack(data >> 16u);
  /* -- 32 bits -- */
  tile.is_valid = data != 0u;
#ifndef GPU_SHADER
  /* Make tests pass on CPU but it is not required for proper rendering. */
  if (tile.lod == 0) {
    tile.lod_offset.x = 0;
  }
#endif
  return tile;
}

static inline ShadowSamplingTilePacked shadow_sampling_tile_pack(ShadowSamplingTile tile)
{
  if (!tile.is_valid) {
    return 0u;
  }
  /* Tag a valid tile of LOD0 valid by setting their offset to 1.
   * This doesn't change the sampling and allows to use of all bits for data.
   * This makes sure no valid packed tile is 0u. */
  if (tile.lod == 0) {
    tile.lod_offset.x = 1;
  }
  uint data = shadow_page_pack(tile.page);
  /* Max value is actually SHADOW_TILEMAP_MAX_CLIPMAP_LOD but we mask the bits. */
  data |= (tile.lod & 15u) << 12u;
  data |= shadow_lod_offset_pack(tile.lod_offset) << 16u;
  return data;
}

static inline ShadowSamplingTile shadow_sampling_tile_create(ShadowTileData tile_data, uint lod)
{
  ShadowSamplingTile tile;
  tile.page = tile_data.page;
  tile.lod = lod;
  tile.lod_offset = uint2(0, 0); /* Computed during tilemap amend phase. */
  /* At this point, it should be the case that all given tiles that have been tagged as used are
   * ready for sampling. Otherwise tile_data should be SHADOW_NO_DATA. */
  tile.is_valid = tile_data.is_used;
  return tile;
}

struct ShadowSceneData {
  /* Number of shadow rays to shoot for each light. */
  int ray_count;
  /* Number of shadow samples to take for each shadow ray. */
  int step_count;
  /* Bias the shading point by using the normal to avoid self intersection. */
  float normal_bias;
  /* Ratio between tile-map pixel world  radius  and film pixel world  radius . */
  float tilemap_projection_ratio;
};
BLI_STATIC_ASSERT_ALIGN(ShadowSceneData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light-probe Sphere
 * \{ */

struct ReflectionProbeLowFreqLight {
  packed_float3 direction;
  float ambient;
};
BLI_STATIC_ASSERT_ALIGN(ReflectionProbeLowFreqLight, 16)

#define  LightProbeShape  uint
const uint 
  SHAPE_ELIPSOID = 0u,
  SHAPE_CUBOID = 1u;

/* Sampling coordinates using UV space. */
struct SphereProbeUvArea {
  /* Offset in UV space to the start of the sampling space of the octahedron map. */
  float2 offset;
  /* Scaling of the squared UV space of the octahedron map. */
  float scale;
  /* Layer of the atlas where the octahedron map is stored. */
  float layer;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeUvArea, 16)

/* Pixel read/write coordinates using pixel space. */
struct SphereProbePixelArea {
  /* Offset in pixel space to the start of the writing space of the octahedron map.
   * Note that the writing space is not the same as the sampling space as we have borders. */
  int2 offset;
  /* Size of the area in pixel that is covered by this probe mip-map. */
  int extent;
  /* Layer of the atlas where the octahedron map is stored. */
  int layer;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbePixelArea, 16)

/** Mapping data to locate a reflection probe in texture. */
struct SphereProbeData {
  /** Transform to probe local position with non-uniform scaling. */
  float3x4 world_to_probe_transposed;

  packed_float3 location;
  /** Shape of the parallax projection. */
  float parallax_distance;
  LightProbeShape parallax_shape;
  LightProbeShape influence_shape;
  /** Influence factor based on the distance to the parallax shape. */
  float influence_scale;
  float influence_bias;

  SphereProbeUvArea atlas_coord;

  /**
   * Irradiance at the probe location encoded as spherical harmonics.
   * Only contain the average luminance. Used for cube-map normalization.
   */
  ReflectionProbeLowFreqLight low_freq_light;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeData, 16)

/** Viewport Display Pass. */
struct SphereProbeDisplayData {
  int probe_index;
  float display_size;
  float _pad0;
  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeDisplayData, 16)

/* Used for sphere probe spherical harmonics extraction. Output one for each thread-group
 * and do a sum afterward. Reduces bandwidth usage. */
struct SphereProbeHarmonic {
  float4 L0_M0;
  float4 L1_Mn1;
  float4 L1_M0;
  float4 L1_Mp1;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeHarmonic, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volume Probe Cache
 * \{ */

struct SurfelRadiance {
  /* Actually stores radiance and world (sky) visibility. Stored normalized. */
  float4 front;
  float4 back;
  /* Accumulated weights per face. */
  float front_weight;
  float back_weight;
  float _pad0;
  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SurfelRadiance, 16)

struct Surfel {
  /** World position of the surfel. */
  packed_float3 position;
  /** Previous surfel index in the ray link-list. Only valid after sorting. */
  int prev;
  /** World orientation of the surface. */
  packed_float3 normal;
  /** Next surfel index in the ray link-list. */
  int next;
  /** Surface albedo to apply to incoming radiance. */
  packed_float3 albedo_front;
  /** Distance along the ray direction for sorting. */
  float ray_distance;
  /** Surface albedo to apply to incoming radiance. */
  packed_float3 albedo_back;
  /** Cluster this surfel is assigned to. */
  int cluster_id;
  /** True if the light can bounce or be emitted by the surfel back face. */
  bool32_t double_sided;
  int _pad0;
  int _pad1;
  int _pad2;
  /** Surface radiance: Emission + Direct Lighting. */
  SurfelRadiance radiance_direct;
  /** Surface radiance: Indirect Lighting. Double buffered to avoid race conditions. */
  SurfelRadiance radiance_indirect[2];
};
BLI_STATIC_ASSERT_ALIGN(Surfel, 16)

struct CaptureInfoData {
  /** Grid size without padding. */
  packed_int3 irradiance_grid_size;
  /** True if the surface shader needs to write the surfel data. */
  bool32_t do_surfel_output;
  /** True if the surface shader needs to increment the surfel_len. */
  bool32_t do_surfel_count;
  /** Number of surfels inside the surfel buffer or the needed len. */
  uint surfel_len;
  /** Total number of a ray for light transportation. */
  float sample_count;
  /** 0 based sample index. */
  float sample_index;
  /** Transform of the light-probe object. */
  float4x4 irradiance_grid_local_to_world;
  /** Transform of the light-probe object. */
  float4x4 irradiance_grid_world_to_local;
  /** Transform vectors from world space to local space. Does not have location component. */
  /** TODO(fclem): This could be a float3x4 or a float3x3 if padded correctly. */
  float4x4 irradiance_grid_world_to_local_rotation;
  /** Scene bounds. Stored as min & max and as int for atomic operations. */
  int scene_bound_x_min;
  int scene_bound_y_min;
  int scene_bound_z_min;
  int scene_bound_x_max;
  int scene_bound_y_max;
  int scene_bound_z_max;
  /* Max intensity a ray can have. */
  float clamp_direct;
  float clamp_indirect;
  float _pad1;
  float _pad2;
  /** Minimum distance between a grid sample and a surface. Used to compute virtual offset. */
  float min_distance_to_surface;
  /** Maximum world scale offset an irradiance grid sample can be baked with. */
  float max_virtual_offset;
  /** Radius of surfels. */
  float surfel_radius;
  /** Capture options. */
  bool32_t capture_world_direct;
  bool32_t capture_world_indirect;
  bool32_t capture_visibility_direct;
  bool32_t capture_visibility_indirect;
  bool32_t capture_indirect;
  bool32_t capture_emission;
  int _pad0;
  /* World light probe atlas coordinate. */
  SphereProbeUvArea world_atlas_coord;
};
BLI_STATIC_ASSERT_ALIGN(CaptureInfoData, 16)

struct SurfelListInfoData {
  /** Size of the grid used to project the surfels into linked lists. */
  int2 ray_grid_size;
  /** Maximum number of list. Is equal to `ray_grid_size.x * ray_grid_size.y`. */
  int list_max;

  int _pad0;
};
BLI_STATIC_ASSERT_ALIGN(SurfelListInfoData, 16)

struct VolumeProbeData {
  /** World to non-normalized local grid space [0..size-1]. Stored transposed for compactness. */
  float3x4 world_to_grid_transposed;
  /** Number of bricks for this grid. */
  packed_int3 grid_size_padded;
  /** Index in brick descriptor list of the first brick of this grid. */
  int brick_offset;
  /** Biases to apply to the shading point in order to sample a valid probe. */
  float normal_bias;
  float view_bias;
  float facing_bias;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(VolumeProbeData, 16)

struct IrradianceBrick {
  /* Offset in pixel to the start of the data inside the atlas texture. */
  uint2 atlas_coord;
};
/** \note Stored packed as a uint. */
#define IrradianceBrickPacked uint

static inline IrradianceBrickPacked irradiance_brick_pack(IrradianceBrick brick)
{
  uint2 data = (uint2(brick.atlas_coord) & 0xFFFFu) << uint2(0u, 16u);
  IrradianceBrickPacked brick_packed = data.x | data.y;
  return brick_packed;
}

static inline IrradianceBrick irradiance_brick_unpack(IrradianceBrickPacked brick_packed)
{
  IrradianceBrick brick;
  brick.atlas_coord = (uint2(brick_packed) >> uint2(0u, 16u)) & uint2(0xFFFFu);
  return brick;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Hierarchical-Z Buffer
 * \{ */

struct HiZData {
  /** Scale factor to remove HiZBuffer padding. */
  float2 uv_scale;

  float2 _pad0;
};
BLI_STATIC_ASSERT_ALIGN(HiZData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light Clamping
 * \{ */

struct ClampData {
  float surface_direct;
  float surface_indirect;
  float volume_direct;
  float volume_indirect;
};
BLI_STATIC_ASSERT_ALIGN(ClampData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Ray-Tracing
 * \{ */

#define  eClosureBits  uint
const uint 
  CLOSURE_NONE = 0u,
  CLOSURE_DIFFUSE = (1u << 0u),
  CLOSURE_SSS = (1u << 1u),
  CLOSURE_REFLECTION = (1u << 2u),
  CLOSURE_REFRACTION = (1u << 3u),
  CLOSURE_TRANSLUCENT = (1u << 4u),
  CLOSURE_TRANSPARENCY = (1u << 8u),
  CLOSURE_EMISSION = (1u << 9u),
  CLOSURE_HOLDOUT = (1u << 10u),
  CLOSURE_VOLUME = (1u << 11u),
  CLOSURE_AMBIENT_OCCLUSION = (1u << 12u),
  CLOSURE_SHADER_TO_RGBA = (1u << 13u),
  CLOSURE_CLEARCOAT = (1u << 14u);

#define  GBufferMode  uint
const uint 
  /** None mode for pixels not rendered. */
  GBUF_NONE = 0u,

  GBUF_DIFFUSE = 1u,
  GBUF_TRANSLUCENT = 2u,
  GBUF_REFLECTION = 3u,
  GBUF_REFRACTION = 4u,
  GBUF_SUBSURFACE = 5u,

  /** Used for surfaces that have no lit closure and just encode a normal layer. */
  GBUF_UNLIT = 11u,

  /** Parameter Optimized. Packs one closure into less layer. */
  GBUF_REFLECTION_COLORLESS = 12u,
  GBUF_REFRACTION_COLORLESS = 13u,

  /** Special configurations. Packs multiple closures into less layer. */
  /* TODO(@fclem): This is isn't currently working due to monolithic nature of the evaluation. */
  GBUF_METAL_CLEARCOAT = 15u;

struct RayTraceData {
  /** ViewProjection matrix used to render the previous frame. */
  float4x4 history_persmat;
  /** ViewProjection matrix used to render the radiance texture. */
  float4x4 radiance_persmat;
  /** Input resolution. */
  int2 full_resolution;
  /** Inverse of input resolution to get screen UVs. */
  float2 full_resolution_inv;
  /** Scale and bias to go from ray-trace resolution to input resolution. */
  int2 resolution_bias;
  int resolution_scale;
  /** View space thickness the objects. */
  float thickness;
  /** Scale and bias to go from horizon-trace resolution to input resolution. */
  int2 horizon_resolution_bias;
  int horizon_resolution_scale;
  /** Determine how fast the sample steps are getting bigger. */
  float quality;
  /** Maximum roughness for which we will trace a ray. */
  float roughness_mask_scale;
  float roughness_mask_bias;
  /** If set to true will bypass spatial denoising. */
  bool32_t skip_denoise;
  /** If set to false will bypass tracing for refractive closures. */
  bool32_t trace_refraction;
  /** Closure being ray-traced. */
  int closure_index;
  int _pad0;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(RayTraceData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Ambient Occlusion
 * \{ */

struct AOData {
  float2 pixel_size;
  float distance;
  float quality;

  float thickness;
  float angle_bias;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(AOData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Subsurface
 * \{ */

#define SSS_SAMPLE_MAX 64
#define SSS_BURLEY_TRUNCATE 16.0
#define SSS_BURLEY_TRUNCATE_CDF 0.9963790093708328
#define SSS_TRANSMIT_LUT_SIZE 64.0
#define SSS_TRANSMIT_LUT_RADIUS 2.0
#define SSS_TRANSMIT_LUT_SCALE ((SSS_TRANSMIT_LUT_SIZE - 1.0) / float(SSS_TRANSMIT_LUT_SIZE))
#define SSS_TRANSMIT_LUT_BIAS (0.5 / float(SSS_TRANSMIT_LUT_SIZE))
#define SSS_TRANSMIT_LUT_STEP_RES 64.0

struct SubsurfaceData {
  /** xy: 2D sample position [-1..1], zw: sample_bounds. */
  /* NOTE(fclem) Using float4 for alignment. */
  float4 samples[SSS_SAMPLE_MAX];
  /** Sample index after which samples are not randomly rotated anymore. */
  int jitter_threshold;
  /** Number of samples precomputed in the set. */
  int sample_len;
  int _pad0;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SubsurfaceData, 16)

static inline float3 burley_setup(float3 radius, float3 albedo)
{
  /* TODO(fclem): Avoid constant duplication. */
  const float m_1_pi = 0.318309886183790671538;

  float3 A = albedo;
  /* Diffuse surface transmission, equation (6). */
  float3 s = 1.9 - A + 3.5 * ((A - 0.8) * (A - 0.8));
  /* Mean free path length adapted to fit ancient Cubic and Gaussian models. */
  float3 l = 0.25 * m_1_pi * radius;

  return l / s;
}

static inline float3 burley_eval(float3 d, float r)
{
  /* Slide 33. */
  float3 exp_r_3_d;
  /* TODO(fclem): Vectorize. */
  exp_r_3_d.x = expf(-r / (3.0 * d.x));
  exp_r_3_d.y = expf(-r / (3.0 * d.y));
  exp_r_3_d.z = expf(-r / (3.0 * d.z));
  float3 exp_r_d = exp_r_3_d * exp_r_3_d * exp_r_3_d;
  /* NOTE:
   * - Surface albedo is applied at the end.
   * - This is normalized diffuse model, so the equation is multiplied
   *   by 2*pi, which also matches `cdf()`.
   */
  return (exp_r_d + exp_r_3_d) / (4.0 * d);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light-probe Planar Data
 * \{ */

struct PlanarProbeData {
  /** Matrices used to render the planar capture. */
  float4x4 viewmat;
  float4x4 winmat;
  /** Transform world to local position with influence distance as Z scale. */
  float3x4 world_to_object_transposed;
  /** World space plane normal. */
  packed_float3 normal;
  /** Layer in the planar capture textures used by this probe. */
  int layer_id;
};
BLI_STATIC_ASSERT_ALIGN(PlanarProbeData, 16)

struct ClipPlaneData {
  /** World space clip plane equation. Used to render planar light-probes. */
  float4 plane;
};
BLI_STATIC_ASSERT_ALIGN(ClipPlaneData, 16)

/** Viewport Display Pass. */
struct PlanarProbeDisplayData {
  float4x4 plane_to_world;
  int probe_index;
  float _pad0;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(PlanarProbeDisplayData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pipeline Data
 * \{ */

struct PipelineInfoData {
  float alpha_hash_scale;
  bool32_t is_probe_reflection;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(PipelineInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Data
 * \{ */

/* Combines data from several modules to avoid wasting binding slots. */
struct UniformData {
  AOData ao;
  CameraData camera;
  ClampData clamp;
  FilmData film;
  HiZData hiz;
  RayTraceData raytrace;
  RenderBuffersInfoData render_pass;
  ShadowSceneData shadow;
  SubsurfaceData subsurface;
  VolumesInfoData volumes;
  PipelineInfoData pipeline;
};
BLI_STATIC_ASSERT_ALIGN(UniformData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Utility Texture
 * \{ */

#define UTIL_TEX_SIZE 64
#define UTIL_BTDF_LAYER_COUNT 16
/* Scale and bias to avoid interpolation of the border pixel.
 * Remap UVs to the border pixels centers. */
#define UTIL_TEX_UV_SCALE ((UTIL_TEX_SIZE - 1.0f) / UTIL_TEX_SIZE)
#define UTIL_TEX_UV_BIAS (0.5f / UTIL_TEX_SIZE)

#define UTIL_BLUE_NOISE_LAYER 0
#define UTIL_SSS_TRANSMITTANCE_PROFILE_LAYER 1
#define UTIL_LTC_MAT_LAYER 2
#define UTIL_BSDF_LAYER 3
#define UTIL_BTDF_LAYER 4
#define UTIL_DISK_INTEGRAL_LAYER UTIL_SSS_TRANSMITTANCE_PROFILE_LAYER
#define UTIL_DISK_INTEGRAL_COMP 3

#ifdef GPU_SHADER

#  if defined(GPU_FRAGMENT_SHADER)
#    define UTIL_TEXEL vec2(gl_FragCoord.xy)
#  elif defined(GPU_COMPUTE_SHADER)
#    define UTIL_TEXEL vec2(gl_GlobalInvocationID.xy)
#  else
#    define UTIL_TEXEL vec2(gl_VertexID, 0)
#  endif

/* Fetch texel. Wrapping if above range. */
float4 utility_tx_fetch(sampler2DArray util_tx, float2 texel, float layer)
{
  return texelFetch(util_tx, int3(int2(texel) % UTIL_TEX_SIZE, layer), 0);
}

/* Sample at uv position. Filtered & Wrapping enabled. */
float4 utility_tx_sample(sampler2DArray util_tx, float2 uv, float layer)
{
  return textureLod(util_tx, float3(uv, layer), 0.0);
}

/* Sample at uv position but with scale and bias so that uv space bounds lie on texel centers. */
float4 utility_tx_sample_lut(sampler2DArray util_tx, float2 uv, float layer)
{
  /* Scale and bias coordinates, for correct filtered lookup. */
  uv = uv * UTIL_TEX_UV_SCALE + UTIL_TEX_UV_BIAS;
  return textureLod(util_tx, float3(uv, layer), 0.0);
}

/* Sample GGX BSDF LUT. */
float4 utility_tx_sample_bsdf_lut(sampler2DArray util_tx, float2 uv, float layer)
{
  /* Scale and bias coordinates, for correct filtered lookup. */
  uv = uv * UTIL_TEX_UV_SCALE + UTIL_TEX_UV_BIAS;
  layer = layer * UTIL_BTDF_LAYER_COUNT + UTIL_BTDF_LAYER;

  float layer_floored;
  float interp = modf(layer, layer_floored);

  float4 tex_low = textureLod(util_tx, float3(uv, layer_floored), 0.0);
  float4 tex_high = textureLod(util_tx, float3(uv, layer_floored + 1.0), 0.0);

  /* Manual trilinear interpolation. */
  return mix(tex_low, tex_high, interp);
}

/* Sample LTC or BSDF LUTs with `cos_theta` and `roughness` as inputs. */
float4 utility_tx_sample_lut(sampler2DArray util_tx, float cos_theta, float roughness, float layer)
{
  /* LUTs are parameterized by `sqrt(1.0 - cos_theta)` for more precision near grazing incidence.
   */
  vec2 coords = vec2(roughness, sqrt(clamp(1.0 - cos_theta, 0.0, 1.0)));
  return utility_tx_sample_lut(util_tx, coords, layer);
}

#endif

/** \} */

#if IS_CPP

using AOVsInfoDataBuf = draw::StorageBuffer<AOVsInfoData>;
using CameraDataBuf = draw::UniformBuffer<CameraData>;
using ClosureTileBuf = draw::StorageArrayBuffer<uint, 1024, true>;
using DepthOfFieldDataBuf = draw::UniformBuffer<DepthOfFieldData>;
using DepthOfFieldScatterListBuf = draw::StorageArrayBuffer<ScatterRect, 16, true>;
using DrawIndirectBuf = draw::StorageBuffer<DrawCommand, true>;
using DispatchIndirectBuf = draw::StorageBuffer<DispatchCommand>;
using UniformDataBuf = draw::UniformBuffer<UniformData>;
using VolumeProbeDataBuf = draw::UniformArrayBuffer<VolumeProbeData, IRRADIANCE_GRID_MAX>;
using IrradianceBrickBuf = draw::StorageVectorBuffer<IrradianceBrickPacked, 16>;
using LightCullingDataBuf = draw::StorageBuffer<LightCullingData>;
using LightCullingKeyBuf = draw::StorageArrayBuffer<uint, LIGHT_CHUNK, true>;
using LightCullingTileBuf = draw::StorageArrayBuffer<uint, LIGHT_CHUNK, true>;
using LightCullingZbinBuf = draw::StorageArrayBuffer<uint, CULLING_ZBIN_COUNT, true>;
using LightCullingZdistBuf = draw::StorageArrayBuffer<float, LIGHT_CHUNK, true>;
using LightDataBuf = draw::StorageArrayBuffer<LightData, LIGHT_CHUNK>;
using MotionBlurDataBuf = draw::UniformBuffer<MotionBlurData>;
using MotionBlurTileIndirectionBuf = draw::StorageBuffer<MotionBlurTileIndirection, true>;
using RayTraceTileBuf = draw::StorageArrayBuffer<uint, 1024, true>;
using SubsurfaceTileBuf = RayTraceTileBuf;
using SphereProbeDataBuf = draw::UniformArrayBuffer<SphereProbeData, SPHERE_PROBE_MAX>;
using SphereProbeDisplayDataBuf = draw::StorageArrayBuffer<SphereProbeDisplayData>;
using PlanarProbeDataBuf = draw::UniformArrayBuffer<PlanarProbeData, PLANAR_PROBE_MAX>;
using PlanarProbeDisplayDataBuf = draw::StorageArrayBuffer<PlanarProbeDisplayData>;
using SamplingDataBuf = draw::StorageBuffer<SamplingData>;
using ShadowStatisticsBuf = draw::StorageBuffer<ShadowStatistics>;
using ShadowPagesInfoDataBuf = draw::StorageBuffer<ShadowPagesInfoData>;
using ShadowPageHeapBuf = draw::StorageVectorBuffer<uint, SHADOW_MAX_PAGE>;
using ShadowPageCacheBuf = draw::StorageArrayBuffer<uint2, SHADOW_MAX_PAGE, true>;
using ShadowTileMapDataBuf = draw::StorageVectorBuffer<ShadowTileMapData, SHADOW_MAX_TILEMAP>;
using ShadowTileMapClipBuf = draw::StorageArrayBuffer<ShadowTileMapClip, SHADOW_MAX_TILEMAP, true>;
using ShadowTileDataBuf = draw::StorageArrayBuffer<ShadowTileDataPacked, SHADOW_MAX_TILE, true>;
using SurfelBuf = draw::StorageArrayBuffer<Surfel, 64>;
using SurfelRadianceBuf = draw::StorageArrayBuffer<SurfelRadiance, 64>;
using CaptureInfoBuf = draw::StorageBuffer<CaptureInfoData>;
using SurfelListInfoBuf = draw::StorageBuffer<SurfelListInfoData>;
using VelocityGeometryBuf = draw::StorageArrayBuffer<float4, 16, true>;
using VelocityIndexBuf = draw::StorageArrayBuffer<VelocityIndex, 16>;
using VelocityObjectBuf = draw::StorageArrayBuffer<float4x4, 16>;
using CryptomatteObjectBuf = draw::StorageArrayBuffer<float2, 16>;
using ClipPlaneBuf = draw::UniformBuffer<ClipPlaneData>;
}  // namespace blender::eevee
#endif

/////////////////////////////// Source file 12/////////////////////////////




#ifndef GPU_SHADER
#  pragma once

#  include  GPU_shader.hh 
#  include  GPU_shader_shared_utils.hh 
#  include  draw_defines.hh 

struct ViewCullingData;
struct ViewMatrices;
struct ObjectMatrices;
struct ObjectInfos;
struct ObjectBounds;
struct VolumeInfos;
struct CurvesInfos;
struct ObjectAttribute;
struct LayerAttribute;
struct DrawCommand;
struct DispatchCommand;
struct DRWDebugPrintBuffer;
struct DRWDebugVert;
struct DRWDebugDrawBuffer;
struct FrustumCorners;
struct FrustumPlanes;

/* __cplusplus is true when compiling with MSL. */
#  if defined(__cplusplus) && !defined(GPU_SHADER)
/* C++ only forward declarations. */
struct Object;
struct Scene;
struct ViewLayer;
struct GPUUniformAttr;
struct GPULayerAttr;

namespace blender::draw {

struct ObjectRef;

}  // namespace blender::draw

#  endif
#endif

#define DRW_SHADER_SHARED_H

#define DRW_RESOURCE_CHUNK_LEN 512

/* Define the maximum number of grid we allow in a volume UBO. */
#define DRW_GRID_PER_VOLUME_MAX 16

/* Define the maximum number of attribute we allow in a curves UBO.
 * This should be kept in sync with `GPU_ATTR_MAX` */
#define DRW_ATTRIBUTE_PER_CURVES_MAX 15

/* -------------------------------------------------------------------- */
/** \name Views
 * \{ */

#ifndef DRW_VIEW_LEN
/* Single-view case (default). */
#  define drw_view_id 0
#  define DRW_VIEW_LEN 1
#  define DRW_VIEW_SHIFT 0
#  define DRW_VIEW_FROM_RESOURCE_ID
#else

/* Multi-view case. */
/** This should be already defined at shaderCreateInfo level. */
// #  define DRW_VIEW_LEN 64
/** Global that needs to be set correctly in each shader stage. */
uint drw_view_id = 0;
/**
 * In order to reduce the memory requirements, the view id is merged with resource id to avoid
 * doubling the memory required only for view indexing.
 */
/** \note This is simply log2(DRW_VIEW_LEN) but making sure it is optimized out. */
#  define DRW_VIEW_SHIFT \
    ((DRW_VIEW_LEN > 32) ? 6 : \
     (DRW_VIEW_LEN > 16) ? 5 : \
     (DRW_VIEW_LEN > 8)  ? 4 : \
     (DRW_VIEW_LEN > 4)  ? 3 : \
     (DRW_VIEW_LEN > 2)  ? 2 : \
                           1)
#  define DRW_VIEW_MASK ~(0xFFFFFFFFu << DRW_VIEW_SHIFT)
#  define DRW_VIEW_FROM_RESOURCE_ID drw_view_id = (uint(drw_ResourceID) & DRW_VIEW_MASK)
#endif

struct FrustumCorners {
  float4 corners[8];
};
BLI_STATIC_ASSERT_ALIGN(FrustumCorners, 16)

struct FrustumPlanes {
  /* [0] left
   * [1] right
   * [2] bottom
   * [3] top
   * [4] near
   * [5] far */
  float4 planes[6];
};
BLI_STATIC_ASSERT_ALIGN(FrustumPlanes, 16)

struct ViewCullingData {
  /** \note vec3 array padded to vec4. */
  /** Frustum corners. */
  FrustumCorners frustum_corners;
  FrustumPlanes frustum_planes;
  float4 bound_sphere;
};
BLI_STATIC_ASSERT_ALIGN(ViewCullingData, 16)

struct ViewMatrices {
  float4x4 viewmat;
  float4x4 viewinv;
  float4x4 winmat;
  float4x4 wininv;
};
BLI_STATIC_ASSERT_ALIGN(ViewMatrices, 16)

/* Do not override old definitions if the shader uses this header but not shader info. */
#ifdef USE_GPU_SHADER_CREATE_INFO
/* TODO(@fclem): Mass rename. */
#  define ViewMatrix drw_view.viewmat
#  define ViewMatrixInverse drw_view.viewinv
#  define ProjectionMatrix drw_view.winmat
#  define ProjectionMatrixInverse drw_view.wininv
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug draw shapes
 * \{ */

struct ObjectMatrices {
  float4x4 model;
  float4x4 model_inverse;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync(const Object &object);
  void sync(const float4x4 &model_matrix);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectMatrices, 16)

#define  eObjectInfoFlag  uint
const uint 
  OBJECT_SELECTED = (1u << 0u),
  OBJECT_FROM_DUPLI = (1u << 1u),
  OBJECT_FROM_SET = (1u << 2u),
  OBJECT_ACTIVE = (1u << 3u),
  OBJECT_NEGATIVE_SCALE = (1u << 4u),
  /* Avoid skipped info to change culling. */
  OBJECT_NO_INFO = ~OBJECT_NEGATIVE_SCALE
;

struct ObjectInfos {
#if defined(GPU_SHADER) && !defined(DRAW_FINALIZE_SHADER)
  /* TODO Rename to struct member for GLSL too. */
  float4 orco_mul_bias[2];
  float4 ob_color;
  float4 infos;
#else
  /** Uploaded as center + size. Converted to mul+bias to local coord. */
  packed_float3 orco_add;
  uint object_attrs_offset;
  packed_float3 orco_mul;
  uint object_attrs_len;

  float4 ob_color;
  uint index;
  uint _pad2;
  float random;
  eObjectInfoFlag flag;
#endif

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync();
  void sync(const blender::draw::ObjectRef ref, bool is_active_object);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectInfos, 16)

struct ObjectBounds {
  /**
   * Uploaded as vertex (0, 4, 3, 1) of the bbox in local space, matching XYZ axis order.
   * Then processed by GPU and stored as (0, 4-0, 3-0, 1-0) in world space for faster culling.
   */
  float4 bounding_corners[4];
  /** Bounding sphere derived from the bounding corner. Computed on GPU. */
  float4 bounding_sphere;
  /** Radius of the inscribed sphere derived from the bounding corner. Computed on GPU. */
#define _inner_sphere_radius bounding_corners[3].w

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync();
  void sync(const Object &ob, float inflate_bounds = 0.0f);
  void sync(const float3 &center, const float3 &size);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectBounds, 16)

inline bool drw_bounds_are_valid(ObjectBounds bounds)
{
  return bounds.bounding_sphere.w >= 0.0f;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Object attributes
 * \{ */

struct VolumeInfos {
  /** Object to grid-space. */
  float4x4 grids_xform[DRW_GRID_PER_VOLUME_MAX];
  /** \note vec4 for alignment. Only float3 needed. */
  float4 color_mul;
  float density_scale;
  float temperature_mul;
  float temperature_bias;
  float _pad;
};
BLI_STATIC_ASSERT_ALIGN(VolumeInfos, 16)

struct CurvesInfos {
  /** Per attribute scope, follows loading order.
   * \note uint as bool in GLSL is 4 bytes.
   * \note GLSL pad arrays of scalar to 16 bytes (std140). */
  uint4 is_point_attribute[DRW_ATTRIBUTE_PER_CURVES_MAX];
};
BLI_STATIC_ASSERT_ALIGN(CurvesInfos, 16)

#pragma pack(push, 4)
struct ObjectAttribute {
  /* Workaround the padding cost from alignment requirements.
   * (see GL spec : 7.6.2.2 Standard Uniform Block Layout) */
  float data_x, data_y, data_z, data_w;
  uint hash_code;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  bool sync(const blender::draw::ObjectRef &ref, const GPUUniformAttr &attr);
#endif
};
#pragma pack(pop)
/** \note we only align to 4 bytes and fetch data manually so make sure
 * C++ compiler gives us the same size. */
BLI_STATIC_ASSERT_ALIGN(ObjectAttribute, 20)

#pragma pack(push, 4)
struct LayerAttribute {
  float4 data;
  uint hash_code;
  uint buffer_length; /* Only in the first record. */
  uint _pad1, _pad2;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  bool sync(const Scene *scene, const ViewLayer *layer, const GPULayerAttr &attr);
#endif
};
#pragma pack(pop)
BLI_STATIC_ASSERT_ALIGN(LayerAttribute, 32)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Indirect commands structures.
 * \{ */

struct DrawCommand {
  /* TODO(fclem): Rename */
  uint vertex_len;
  uint instance_len;
  uint vertex_first;
#if defined(GPU_SHADER)
  uint base_index;
  /** \note base_index is i_first for non-indexed draw-calls. */
#  define _instance_first_array base_index
#else
  union {
    uint base_index;
    /* Use this instead of instance_first_indexed for non indexed draw calls. */
    uint instance_first_array;
  };
#endif

  uint instance_first_indexed;

  uint _pad0, _pad1, _pad2;
};
BLI_STATIC_ASSERT_ALIGN(DrawCommand, 16)

struct DispatchCommand {
  uint num_groups_x;
  uint num_groups_y;
  uint num_groups_z;
  uint _pad0;
};
BLI_STATIC_ASSERT_ALIGN(DispatchCommand, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug print
 * \{ */

/* Take the header (DrawCommand) into account. */
#define DRW_DEBUG_PRINT_MAX (8 * 1024) - 4
/** \note Cannot be more than 255 (because of column encoding). */
#define DRW_DEBUG_PRINT_WORD_WRAP_COLUMN 120u

/* The debug print buffer is laid-out as the following struct.
 * But we use plain array in shader code instead because of driver issues. */
struct DRWDebugPrintBuffer {
  DrawCommand command;
  /** Each character is encoded as 3 `uchar` with char_index, row and column position. */
  uint char_array[DRW_DEBUG_PRINT_MAX];
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugPrintBuffer, 16)

/* Use number of char as vertex count. Equivalent to `DRWDebugPrintBuffer.command.v_count`. */
#define drw_debug_print_cursor drw_debug_print_buf[0]
/* Reuse first instance as row index as we don't use instancing. Equivalent to
 * `DRWDebugPrintBuffer.command.i_first`. */
#define drw_debug_print_row_shared drw_debug_print_buf[3]
/**
 * Offset to the first data. Equal to: `sizeof(DrawCommand) / sizeof(uint)`.
 * This is needed because we bind the whole buffer as a `uint` array.
 */
#define drw_debug_print_offset 8

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug draw shapes
 * \{ */

struct DRWDebugVert {
  /* This is a weird layout, but needed to be able to use DRWDebugVert as
   * a DrawCommand and avoid alignment issues. See drw_debug_verts_buf[] definition. */
  uint pos0;
  uint pos1;
  uint pos2;
  /* Named vert_color to avoid global namespace collision with uniform color. */
  uint vert_color;
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugVert, 16)

inline DRWDebugVert debug_vert_make(uint in_pos0, uint in_pos1, uint in_pos2, uint in_vert_color)
{
  DRWDebugVert debug_vert;
  debug_vert.pos0 = in_pos0;
  debug_vert.pos1 = in_pos1;
  debug_vert.pos2 = in_pos2;
  debug_vert.vert_color = in_vert_color;
  return debug_vert;
}

/* Take the header (DrawCommand) into account. */
#define DRW_DEBUG_DRAW_VERT_MAX (64 * 8192) - 1

/* The debug draw buffer is laid-out as the following struct.
 * But we use plain array in shader code instead because of driver issues. */
struct DRWDebugDrawBuffer {
  DrawCommand command;
  DRWDebugVert verts[DRW_DEBUG_DRAW_VERT_MAX];
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugPrintBuffer, 16)

/* Equivalent to `DRWDebugDrawBuffer.command.v_count`. */
#define drw_debug_draw_v_count drw_debug_verts_buf[0].pos0
/**
 * Offset to the first data. Equal to: `sizeof(DrawCommand) / sizeof(DRWDebugVert)`.
 * This is needed because we bind the whole buffer as a `DRWDebugVert` array.
 */
#define drw_debug_draw_offset 2

/** \} */

/////////////////////////////// Source file 13/////////////////////////////

/* Pass Resources. */
layout(binding = 0, rgba16f) uniform restrict writeonly image2DArray rp_color_img;
layout(binding = 1, r16f) uniform restrict writeonly image2DArray rp_value_img;
layout(binding = 1, std140) uniform uniform_buf { UniformData _uniform_buf; };
layout(binding = 7, std430) restrict readonly buffer cryptomatte_object_buf { vec2 _cryptomatte_object_buf[]; };
layout(binding = 2, rgba32f) uniform restrict writeonly image2D rp_cryptomatte_img;
layout(binding = 10, std430) restrict readonly buffer drw_matrix_buf { ObjectMatrices _drw_matrix_buf[]; };
layout(binding = 11, std430) restrict readonly buffer resource_id_buf { int _resource_id_buf[]; };
layout(binding = 11, std140) uniform drw_view_ { ViewMatrices _drw_view_[DRW_VIEW_LEN]; };
layout(binding = 3, rgb10_a2) uniform restrict writeonly image2DArray out_gbuf_closure_img;
layout(binding = 4, rg16) uniform restrict writeonly image2DArray out_gbuf_normal_img;
layout(binding = 2) uniform sampler2DArray utility_tx;
layout(binding = 6, std430) restrict readonly buffer sampling_buf { SamplingData _sampling_buf; };
layout(binding = 3) uniform sampler2D hiz_tx;
layout(binding = 0, std430) restrict readonly buffer light_cull_buf { LightCullingData _light_cull_buf; };
layout(binding = 1, std430) restrict readonly buffer light_buf { LightData _light_buf[]; };
layout(binding = 2, std430) restrict readonly buffer light_zbin_buf { uint _light_zbin_buf[]; };
layout(binding = 3, std430) restrict readonly buffer light_tile_buf { uint _light_tile_buf[]; };
layout(binding = 3, std140) uniform reflection_probe_buf { SphereProbeData _reflection_probe_buf[SPHERE_PROBE_MAX]; };
layout(binding = 7) uniform sampler2DArray reflection_probes_tx;
layout(binding = 2, std140) uniform grids_infos_buf { VolumeProbeData _grids_infos_buf[IRRADIANCE_GRID_MAX]; };
layout(binding = 4, std430) restrict readonly buffer bricks_infos_buf { uint _bricks_infos_buf[]; };
layout(binding = 6) uniform sampler3D irradiance_atlas_tx;
layout(binding = 5) uniform usampler2DArray shadow_atlas_tx;
layout(binding = 4) uniform usampler2D shadow_tilemaps_tx;
#define uniform_buf (_uniform_buf)
#define cryptomatte_object_buf (_cryptomatte_object_buf)
#define drw_matrix_buf (_drw_matrix_buf)
#define resource_id_buf (_resource_id_buf)
#define drw_view_ (_drw_view_)
#define sampling_buf (_sampling_buf)
#define light_cull_buf (_light_cull_buf)
#define light_buf (_light_buf)
#define light_zbin_buf (_light_zbin_buf)
#define light_tile_buf (_light_tile_buf)
#define reflection_probe_buf (_reflection_probe_buf)
#define grids_infos_buf (_grids_infos_buf)
#define bricks_infos_buf (_bricks_infos_buf)

/* Batch Resources. */
layout(binding = 0, std140) uniform node_tree { NodeTree _node_tree; };
#define node_tree (_node_tree)

/* Push Constants. */


/////////////////////////////// Source file 14/////////////////////////////

/* Inputs. */
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 nor;

/* Interfaces. */
out eevee_surf_iface{
  smooth vec3 P;
  smooth vec3 N;
}
interp;
out draw_resource_id_iface{
  flat int resource_index;
}
drw_ResourceID_iface;


/////////////////////////////// Source file 15/////////////////////////////




#ifndef DRAW_VIEW_CREATE_INFO
#  error Missing draw_view additional create info on shader create info
#endif

/* Returns true if the current view has a perspective projection matrix. */
bool drw_view_is_perspective()
{
  return drw_view.winmat[3][3] == 0.0;
}

/* Returns the view forward vector, going towards the viewer. */
vec3 drw_view_forward()
{
  return drw_view.viewinv[2].xyz;
}

/* Returns the view origin. */
vec3 drw_view_position()
{
  return drw_view.viewinv[3].xyz;
}

/* Returns the projection matrix far clip distance. */
float drw_view_far()
{
  if (drw_view_is_perspective()) {
    return -drw_view.winmat[3][2] / (drw_view.winmat[2][2] + 1.0);
  }
  return -(drw_view.winmat[3][2] - 1.0) / drw_view.winmat[2][2];
}

/* Returns the projection matrix near clip distance. */
float drw_view_near()
{
  if (drw_view_is_perspective()) {
    return -drw_view.winmat[3][2] / (drw_view.winmat[2][2] - 1.0);
  }
  return -(drw_view.winmat[3][2] + 1.0) / drw_view.winmat[2][2];
}

/**
 * Returns the world incident vector `V` (going towards the viewer)
 * from the world position `P` and the current view.
 */
vec3 drw_world_incident_vector(vec3 P)
{
  return drw_view_is_perspective() ? normalize(drw_view_position() - P) : drw_view_forward();
}

/**
 * Returns the view incident vector `vV` (going towards the viewer)
 * from the view position `vP` and the current view.
 */
vec3 drw_view_incident_vector(vec3 vP)
{
  return drw_view_is_perspective() ? normalize(-vP) : vec3(0.0, 0.0, 1.0);
}

/**
 * Transform position on screen UV space [0..1] to Normalized Device Coordinate space [-1..1].
 */
vec3 drw_screen_to_ndc(vec3 ss_P)
{
  return ss_P * 2.0 - 1.0;
}
vec2 drw_screen_to_ndc(vec2 ss_P)
{
  return ss_P * 2.0 - 1.0;
}
float drw_screen_to_ndc(float ss_P)
{
  return ss_P * 2.0 - 1.0;
}

/**
 * Transform position in Normalized Device Coordinate [-1..1] to screen UV space [0..1].
 */
vec3 drw_ndc_to_screen(vec3 ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}
vec2 drw_ndc_to_screen(vec2 ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}
float drw_ndc_to_screen(float ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 * \{ */

vec3 drw_normal_view_to_world(vec3 vN)
{
  return (mat3x3(drw_view.viewinv) * vN);
}

vec3 drw_normal_world_to_view(vec3 N)
{
  return (mat3x3(drw_view.viewmat) * N);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Position
 * \{ */

vec3 drw_perspective_divide(vec4 hs_P)
{
  return hs_P.xyz / hs_P.w;
}

vec3 drw_point_view_to_world(vec3 vP)
{
  return (drw_view.viewinv * vec4(vP, 1.0)).xyz;
}
vec4 drw_point_view_to_homogenous(vec3 vP)
{
  return (drw_view.winmat * vec4(vP, 1.0));
}
vec3 drw_point_view_to_ndc(vec3 vP)
{
  return drw_perspective_divide(drw_point_view_to_homogenous(vP));
}

vec3 drw_point_world_to_view(vec3 P)
{
  return (drw_view.viewmat * vec4(P, 1.0)).xyz;
}
vec4 drw_point_world_to_homogenous(vec3 P)
{
  return (drw_view.winmat * (drw_view.viewmat * vec4(P, 1.0)));
}
vec3 drw_point_world_to_ndc(vec3 P)
{
  return drw_perspective_divide(drw_point_world_to_homogenous(P));
}

vec3 drw_point_ndc_to_view(vec3 ssP)
{
  return drw_perspective_divide(drw_view.wininv * vec4(ssP, 1.0));
}
vec3 drw_point_ndc_to_world(vec3 ssP)
{
  return drw_point_view_to_world(drw_point_ndc_to_view(ssP));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Screen Positions
 * \{ */

vec3 drw_point_view_to_screen(vec3 vP)
{
  return drw_ndc_to_screen(drw_point_view_to_ndc(vP));
}
vec3 drw_point_world_to_screen(vec3 vP)
{
  return drw_ndc_to_screen(drw_point_world_to_ndc(vP));
}

vec3 drw_point_screen_to_view(vec3 ssP)
{
  return drw_point_ndc_to_view(drw_screen_to_ndc(ssP));
}
vec3 drw_point_screen_to_world(vec3 ssP)
{
  return drw_point_view_to_world(drw_point_screen_to_view(ssP));
}

float drw_depth_view_to_screen(float v_depth)
{
  return drw_point_view_to_screen(vec3(0.0, 0.0, v_depth)).z;
}
float drw_depth_screen_to_view(float ss_depth)
{
  return drw_point_screen_to_view(vec3(0.0, 0.0, ss_depth)).z;
}

/** \} */

/////////////////////////////// Source file 16/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)

#ifndef DRAW_MODELMAT_CREATE_INFO
#  error Missing draw_modelmat additional create info on shader create info
#endif

#if defined(UNIFORM_RESOURCE_ID)
/* TODO(fclem): Legacy API. To remove. */
#  define resource_id drw_ResourceID
#  define DRW_RESOURCE_ID_VARYING_SET

#elif defined(GPU_VERTEX_SHADER)
#  if defined(UNIFORM_RESOURCE_ID_NEW)
#    define resource_id (drw_ResourceID >> DRW_VIEW_SHIFT)
#  else
#    define resource_id gpu_InstanceIndex
#  endif
#  define DRW_RESOURCE_ID_VARYING_SET drw_ResourceID_iface.resource_index = resource_id;

#elif defined(GPU_GEOMETRY_SHADER)
#  define resource_id drw_ResourceID_iface_in[0].resource_index

#elif defined(GPU_FRAGMENT_SHADER)
#  define resource_id drw_ResourceID_iface.resource_index
#endif

mat4x4 drw_modelmat()
{
  return drw_matrix_buf[resource_id].model;
}
mat4x4 drw_modelinv()
{
  return drw_matrix_buf[resource_id].model_inverse;
}

/**
 * Usually Normal matrix is `transpose(inverse(ViewMatrix * ModelMatrix))`.
 *
 * But since it is slow to multiply matrices we decompose it. Decomposing
 * inversion and transposition both invert the product order leaving us with
 * the same original order:
 * transpose(ViewMatrixInverse) * transpose(ModelMatrixInverse)
 *
 * Knowing that the view matrix is orthogonal, the transpose is also the inverse.
 * NOTE: This is only valid because we are only using the mat3 of the ViewMatrixInverse.
 * ViewMatrix * transpose(ModelMatrixInverse)
 */
mat3x3 drw_normat()
{
  return transpose(mat3x3(drw_modelinv()));
}
mat3x3 drw_norinv()
{
  return transpose(mat3x3(drw_modelmat()));
}

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 *
 * Space conversion helpers for normal vectors.
 * \{ */

vec3 drw_normal_object_to_world(vec3 lN)
{
  return (drw_normat() * lN);
}
vec3 drw_normal_world_to_object(vec3 N)
{
  return (drw_norinv() * N);
}

vec3 drw_normal_object_to_view(vec3 lN)
{
  return (mat3x3(drw_view.viewmat) * (drw_normat() * lN));
}
vec3 drw_normal_view_to_object(vec3 vN)
{
  return (drw_norinv() * (mat3x3(drw_view.viewinv) * vN));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 *
 * Space conversion helpers for points (coordinates).
 * \{ */

vec3 drw_point_object_to_world(vec3 lP)
{
  return (drw_modelmat() * vec4(lP, 1.0)).xyz;
}
vec3 drw_point_world_to_object(vec3 P)
{
  return (drw_modelinv() * vec4(P, 1.0)).xyz;
}

vec3 drw_point_object_to_view(vec3 lP)
{
  return (drw_view.viewmat * (drw_modelmat() * vec4(lP, 1.0))).xyz;
}
vec3 drw_point_view_to_object(vec3 vP)
{
  return (drw_modelinv() * (drw_view.viewinv * vec4(vP, 1.0))).xyz;
}

vec4 drw_point_object_to_homogenous(vec3 lP)
{
  return (drw_view.winmat * (drw_view.viewmat * (drw_modelmat() * vec4(lP, 1.0))));
}
vec3 drw_point_object_to_ndc(vec3 lP)
{
  return drw_perspective_divide(drw_point_object_to_homogenous(lP));
}

/** \} */

/////////////////////////////// Source file 17/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL
#define GPU_SHADER_MATH_BASE_LIB_GLSL

#define M_PI 3.14159265358979323846      /* pi */
#define M_TAU 6.28318530717958647692     /* tau = 2*pi */
#define M_PI_2 1.57079632679489661923    /* pi/2 */
#define M_PI_4 0.78539816339744830962    /* pi/4 */
#define M_SQRT2 1.41421356237309504880   /* sqrt(2) */
#define M_SQRT1_2 0.70710678118654752440 /* 1/sqrt(2) */
#define M_SQRT3 1.73205080756887729352   /* sqrt(3) */
#define M_SQRT1_3 0.57735026918962576450 /* 1/sqrt(3) */
#define M_1_PI 0.318309886183790671538   /* 1/pi */
#define M_E 2.7182818284590452354        /* e */
#define M_LOG2E 1.4426950408889634074    /* log_2 e */
#define M_LOG10E 0.43429448190325182765  /* log_10 e */
#define M_LN2 0.69314718055994530942     /* log_e 2 */
#define M_LN10 2.30258509299404568402    /* log_e 10 */

/* `powf` is really slow for raising to integer powers. */

float pow2f(float x)
{
  return x * x;
}
float pow3f(float x)
{
  return x * x * x;
}
float pow4f(float x)
{
  return pow2f(pow2f(x));
}
float pow5f(float x)
{
  return pow4f(x) * x;
}
float pow6f(float x)
{
  return pow2f(pow3f(x));
}
float pow7f(float x)
{
  return pow6f(x) * x;
}
float pow8f(float x)
{
  return pow2f(pow4f(x));
}

int square_i(int v)
{
  return v * v;
}
uint square_uint(uint v)
{
  return v * v;
}
float square(float v)
{
  return v * v;
}
vec2 square(vec2 v)
{
  return v * v;
}
vec3 square(vec3 v)
{
  return v * v;
}
vec4 square(vec4 v)
{
  return v * v;
}

int cube_i(int v)
{
  return v * v * v;
}
uint cube_uint(uint v)
{
  return v * v * v;
}
float cube_f(float v)
{
  return v * v * v;
}

float hypot(float x, float y)
{
  return sqrt(x * x + y * y);
}

float atan2(float y, float x)
{
  return atan(y, x);
}

/**
 * Safe `a` modulo `b`.
 * If `b` equal 0 the result will be 0.
 */
float safe_mod(float a, float b)
{
  return (b != 0.0) ? mod(a, b) : 0.0;
}

/**
 * Returns \a a if it is a multiple of \a b or the next multiple or \a b after \b a .
 * In other words, it is equivalent to `divide_ceil(a, b) * b`.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
int ceil_to_multiple(int a, int b)
{
  return ((a + b - 1) / b) * b;
}
uint ceil_to_multiple(uint a, uint b)
{
  return ((a + b - 1u) / b) * b;
}

/**
 * Integer division that returns the ceiling, instead of flooring like normal C division.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
int divide_ceil(int a, int b)
{
  return (a + b - 1) / b;
}
uint divide_ceil(uint a, uint b)
{
  return (a + b - 1u) / b;
}

/**
 * Component wise, use vector to replace min if it is smaller and max if bigger.
 */
void min_max(float value, inout float min_v, inout float max_v)
{
  min_v = min(value, min_v);
  max_v = max(value, max_v);
}

/**
 * Safe divide `a` by `b`.
 * If `b` equal 0 the result will be 0.
 */
float safe_divide(float a, float b)
{
  return (b != 0.0) ? (a / b) : 0.0;
}

/**
 * Safe reciprocal function. Returns `1/a`.
 * If `a` equal 0 the result will be 0.
 */
float safe_rcp(float a)
{
  return (a != 0.0) ? (1.0 / a) : 0.0;
}

/**
 * Safe square root function. Returns `sqrt(a)`.
 * If `a` is less or equal to 0 then the result will be 0.
 */
float safe_sqrt(float a)
{
  return sqrt(max(0.0, a));
}

/**
 * Safe `arccosine` function. Returns `acos(a)`.
 * If `a` is greater than 1, returns 0.
 * If `a` is less than -1, returns PI.
 */
float safe_acos(float a)
{
  if (a <= -1.0) {
    return M_PI;
  }
  else if (a >= 1.0) {
    return 0.0;
  }
  return acos(a);
}

/**
 * Return true if the difference between`a` and `b` is below the `epsilon` value.
 */
bool is_equal(float a, float b, const float epsilon)
{
  return abs(a - b) <= epsilon;
}

/** \} */

#endif /* GPU_SHADER_MATH_BASE_LIB_GLSL */

/////////////////////////////// Source file 18/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_VECTOR_LIB_GLSL
#  define GPU_SHADER_MATH_VECTOR_LIB_GLSL

/* Metal does not need prototypes. */
#  ifndef GPU_METAL

/**
 * Return true if all components is equal to zero.
 */
bool is_zero(vec2 vec);
bool is_zero(vec3 vec);
bool is_zero(vec4 vec);

/**
 * Return true if any component is equal to zero.
 */
bool is_any_zero(vec2 vec);
bool is_any_zero(vec3 vec);
bool is_any_zero(vec4 vec);

/**
 * Return true if the deference between`a` and `b` is below the `epsilon` value.
 * Epsilon value is scaled by magnitude of `a` before comparison.
 */
bool almost_equal_relative(vec2 a, vec2 b, const float epsilon_factor);
bool almost_equal_relative(vec3 a, vec3 b, const float epsilon_factor);
bool almost_equal_relative(vec4 a, vec4 b, const float epsilon_factor);

/**
 * Safe `a` modulo `b`.
 * If `b` equal 0 the result will be 0.
 */
vec2 safe_mod(vec2 a, vec2 b);
vec3 safe_mod(vec3 a, vec3 b);
vec4 safe_mod(vec4 a, vec4 b);
vec2 safe_mod(vec2 a, float b);
vec3 safe_mod(vec3 a, float b);
vec4 safe_mod(vec4 a, float b);

/**
 * Returns \a a if it is a multiple of \a b or the next multiple or \a b after \b a .
 * In other words, it is equivalent to `divide_ceil(a, b) * b`.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
ivec2 ceil_to_multiple(ivec2 a, ivec2 b);
ivec3 ceil_to_multiple(ivec3 a, ivec3 b);
ivec4 ceil_to_multiple(ivec4 a, ivec4 b);
uvec2 ceil_to_multiple(uvec2 a, uvec2 b);
uvec3 ceil_to_multiple(uvec3 a, uvec3 b);
uvec4 ceil_to_multiple(uvec4 a, uvec4 b);

/**
 * Integer division that returns the ceiling, instead of flooring like normal C division.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
ivec2 divide_ceil(ivec2 a, ivec2 b);
ivec3 divide_ceil(ivec3 a, ivec3 b);
ivec4 divide_ceil(ivec4 a, ivec4 b);
uvec2 divide_ceil(uvec2 a, uvec2 b);
uvec3 divide_ceil(uvec3 a, uvec3 b);
uvec4 divide_ceil(uvec4 a, uvec4 b);

/**
 * Component wise, use vector to replace min if it is smaller and max if bigger.
 */
void min_max(vec2 vector, inout vec2 min, inout vec2 max);
void min_max(vec3 vector, inout vec3 min, inout vec3 max);
void min_max(vec4 vector, inout vec4 min, inout vec4 max);

/**
 * Safe divide `a` by `b`.
 * If `b` equal 0 the result will be 0.
 */
vec2 safe_divide(vec2 a, vec2 b);
vec3 safe_divide(vec3 a, vec3 b);
vec4 safe_divide(vec4 a, vec4 b);
vec2 safe_divide(vec2 a, float b);
vec3 safe_divide(vec3 a, float b);
vec4 safe_divide(vec4 a, float b);

/**
 * Return the manhattan length of `a`.
 * This is also the sum of the absolute value of all components.
 */
float length_manhattan(vec2 a);
float length_manhattan(vec3 a);
float length_manhattan(vec4 a);

/**
 * Return the length squared of `a`.
 */
float length_squared(vec2 a);
float length_squared(vec3 a);
float length_squared(vec4 a);

/**
 * Return the manhattan distance between `a` and `b`.
 */
float distance_manhattan(vec2 a, vec2 b);
float distance_manhattan(vec3 a, vec3 b);
float distance_manhattan(vec4 a, vec4 b);

/**
 * Return the squared distance between `a` and `b`.
 */
float distance_squared(vec2 a, vec2 b);
float distance_squared(vec3 a, vec3 b);
float distance_squared(vec4 a, vec4 b);

/**
 * Return the projection of `p` onto `v_proj`.
 */
vec3 project(vec3 p, vec3 v_proj);

/**
 * Return normalized version of the `vector` and its length.
 */
vec2 normalize_and_get_length(vec2 vector, out float out_length);
vec3 normalize_and_get_length(vec3 vector, out float out_length);
vec4 normalize_and_get_length(vec4 vector, out float out_length);

/**
 * Return normalized version of the `vector` or a default normalized vector if `vector` is invalid.
 */
vec2 safe_normalize(vec2 vector);
vec3 safe_normalize(vec3 vector);
vec4 safe_normalize(vec4 vector);

/**
 * Safe reciprocal function. Returns `1/a`.
 * If `a` equal 0 the result will be 0.
 */
vec2 safe_rcp(vec2 a);
vec3 safe_rcp(vec3 a);
vec4 safe_rcp(vec4 a);

/**
 * Per component linear interpolation.
 */
vec2 interpolate(vec2 a, vec2 b, float t);
vec3 interpolate(vec3 a, vec3 b, float t);
vec4 interpolate(vec4 a, vec4 b, float t);

/**
 * Return half-way point between `a` and  `b`.
 */
vec2 midpoint(vec2 a, vec2 b);
vec3 midpoint(vec3 a, vec3 b);
vec4 midpoint(vec4 a, vec4 b);

/**
 * Return `vector` if `incident` and `reference` are pointing in the same direction.
 */
// vec2 faceforward(vec2 vector, vec2 incident, vec2 reference); /* Built-in GLSL. */

/**
 * Return the index of the component with the greatest absolute value.
 */
int dominant_axis(vec3 a);

/**
 * Calculates a perpendicular vector to \a v.
 * \note Returned vector can be in any perpendicular direction.
 * \note Returned vector might not the same length as \a v.
 */
vec3 orthogonal(vec3 v);
/**
 * Calculates a perpendicular vector to \a v.
 * \note Returned vector is always rotated 90 degrees counter clock wise.
 */
vec2 orthogonal(vec2 v);
ivec2 orthogonal(ivec2 v);

/**
 * Return true if the difference between`a` and `b` is below the `epsilon` value.
 */
bool is_equal(vec2 a, vec2 b, const float epsilon);
bool is_equal(vec3 a, vec3 b, const float epsilon);
bool is_equal(vec4 a, vec4 b, const float epsilon);

/**
 * Return the maximum component of a vector.
 */
float reduce_max(vec2 a);
float reduce_max(vec3 a);
float reduce_max(vec4 a);
int reduce_max(ivec2 a);
int reduce_max(ivec3 a);
int reduce_max(ivec4 a);

/**
 * Return the minimum component of a vector.
 */
float reduce_min(vec2 a);
float reduce_min(vec3 a);
float reduce_min(vec4 a);
int reduce_min(ivec2 a);
int reduce_min(ivec3 a);
int reduce_min(ivec4 a);

/**
 * Return the sum of the components of a vector.
 */
float reduce_add(vec2 a);
float reduce_add(vec3 a);
float reduce_add(vec4 a);
int reduce_add(ivec2 a);
int reduce_add(ivec3 a);
int reduce_add(ivec4 a);

/**
 * Return the average of the components of a vector.
 */
float average(vec2 a);
float average(vec3 a);
float average(vec4 a);

#  endif /* !GPU_METAL */

/* ---------------------------------------------------------------------- */
/** \name Implementation
 * \{ */

#  ifdef GPU_METAL /* Already defined in shader_defines.msl/glsl to move here. */
bool is_zero(vec2 vec)
{
  return all(equal(vec, vec2(0.0)));
}
bool is_zero(vec3 vec)
{
  return all(equal(vec, vec3(0.0)));
}
bool is_zero(vec4 vec)
{
  return all(equal(vec, vec4(0.0)));
}
#  endif /* GPU_METAL */

bool is_any_zero(vec2 vec)
{
  return any(equal(vec, vec2(0.0)));
}
bool is_any_zero(vec3 vec)
{
  return any(equal(vec, vec3(0.0)));
}
bool is_any_zero(vec4 vec)
{
  return any(equal(vec, vec4(0.0)));
}

bool almost_equal_relative(vec2 a, vec2 b, const float epsilon_factor)
{
  for (int i = 0; i < 2; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}
bool almost_equal_relative(vec3 a, vec3 b, const float epsilon_factor)
{
  for (int i = 0; i < 3; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}
bool almost_equal_relative(vec4 a, vec4 b, const float epsilon_factor)
{
  for (int i = 0; i < 4; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}

vec2 safe_mod(vec2 a, vec2 b)
{
  return select(vec2(0), mod(a, b), notEqual(b, vec2(0)));
}
vec3 safe_mod(vec3 a, vec3 b)
{
  return select(vec3(0), mod(a, b), notEqual(b, vec3(0)));
}
vec4 safe_mod(vec4 a, vec4 b)
{
  return select(vec4(0), mod(a, b), notEqual(b, vec4(0)));
}

vec2 safe_mod(vec2 a, float b)
{
  return (b != 0.0) ? mod(a, vec2(b)) : vec2(0);
}
vec3 safe_mod(vec3 a, float b)
{
  return (b != 0.0) ? mod(a, vec3(b)) : vec3(0);
}
vec4 safe_mod(vec4 a, float b)
{
  return (b != 0.0) ? mod(a, vec4(b)) : vec4(0);
}

ivec2 ceil_to_multiple(ivec2 a, ivec2 b)
{
  return ((a + b - 1) / b) * b;
}
ivec3 ceil_to_multiple(ivec3 a, ivec3 b)
{
  return ((a + b - 1) / b) * b;
}
ivec4 ceil_to_multiple(ivec4 a, ivec4 b)
{
  return ((a + b - 1) / b) * b;
}
uvec2 ceil_to_multiple(uvec2 a, uvec2 b)
{
  return ((a + b - 1u) / b) * b;
}
uvec3 ceil_to_multiple(uvec3 a, uvec3 b)
{
  return ((a + b - 1u) / b) * b;
}
uvec4 ceil_to_multiple(uvec4 a, uvec4 b)
{
  return ((a + b - 1u) / b) * b;
}

ivec2 divide_ceil(ivec2 a, ivec2 b)
{
  return (a + b - 1) / b;
}
ivec3 divide_ceil(ivec3 a, ivec3 b)
{
  return (a + b - 1) / b;
}
ivec4 divide_ceil(ivec4 a, ivec4 b)
{
  return (a + b - 1) / b;
}
uvec2 divide_ceil(uvec2 a, uvec2 b)
{
  return (a + b - 1u) / b;
}
uvec3 divide_ceil(uvec3 a, uvec3 b)
{
  return (a + b - 1u) / b;
}
uvec4 divide_ceil(uvec4 a, uvec4 b)
{
  return (a + b - 1u) / b;
}

void min_max(vec2 vector, inout vec2 min_v, inout vec2 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}
void min_max(vec3 vector, inout vec3 min_v, inout vec3 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}
void min_max(vec4 vector, inout vec4 min_v, inout vec4 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}

vec2 safe_divide(vec2 a, vec2 b)
{
  return select(vec2(0), a / b, notEqual(b, vec2(0)));
}
vec3 safe_divide(vec3 a, vec3 b)
{
  return select(vec3(0), a / b, notEqual(b, vec3(0)));
}
vec4 safe_divide(vec4 a, vec4 b)
{
  return select(vec4(0), a / b, notEqual(b, vec4(0)));
}

vec2 safe_divide(vec2 a, float b)
{
  return (b != 0.0) ? (a / b) : vec2(0);
}
vec3 safe_divide(vec3 a, float b)
{
  return (b != 0.0) ? (a / b) : vec3(0);
}
vec4 safe_divide(vec4 a, float b)
{
  return (b != 0.0) ? (a / b) : vec4(0);
}

float length_manhattan(vec2 a)
{
  return dot(abs(a), vec2(1));
}
float length_manhattan(vec3 a)
{
  return dot(abs(a), vec3(1));
}
float length_manhattan(vec4 a)
{
  return dot(abs(a), vec4(1));
}

float length_squared(vec2 a)
{
  return dot(a, a);
}
float length_squared(vec3 a)
{
  return dot(a, a);
}
float length_squared(vec4 a)
{
  return dot(a, a);
}

float distance_manhattan(vec2 a, vec2 b)
{
  return length_manhattan(a - b);
}
float distance_manhattan(vec3 a, vec3 b)
{
  return length_manhattan(a - b);
}
float distance_manhattan(vec4 a, vec4 b)
{
  return length_manhattan(a - b);
}

float distance_squared(vec2 a, vec2 b)
{
  return length_squared(a - b);
}
float distance_squared(vec3 a, vec3 b)
{
  return length_squared(a - b);
}
float distance_squared(vec4 a, vec4 b)
{
  return length_squared(a - b);
}

vec3 project(vec3 p, vec3 v_proj)
{
  if (is_zero(v_proj)) {
    return vec3(0);
  }
  return v_proj * (dot(p, v_proj) / dot(v_proj, v_proj));
}

vec2 normalize_and_get_length(vec2 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec2(0.0);
}
vec3 normalize_and_get_length(vec3 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec3(0.0);
}
vec4 normalize_and_get_length(vec4 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec4(0.0);
}

vec2 safe_normalize_and_get_length(vec2 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec2(1.0, 0.0);
}
vec3 safe_normalize_and_get_length(vec3 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec3(1.0, 0.0, 0.0);
}
vec4 safe_normalize_and_get_length(vec4 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec4(1.0, 0.0, 0.0, 0.0);
}

vec2 safe_normalize(vec2 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}
vec3 safe_normalize(vec3 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}
vec4 safe_normalize(vec4 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}

vec2 safe_rcp(vec2 a)
{
  return select(vec2(0.0), (1.0 / a), notEqual(a, vec2(0.0)));
}
vec3 safe_rcp(vec3 a)
{
  return select(vec3(0.0), (1.0 / a), notEqual(a, vec3(0.0)));
}
vec4 safe_rcp(vec4 a)
{
  return select(vec4(0.0), (1.0 / a), notEqual(a, vec4(0.0)));
}

vec2 interpolate(vec2 a, vec2 b, float t)
{
  return mix(a, b, t);
}
vec3 interpolate(vec3 a, vec3 b, float t)
{
  return mix(a, b, t);
}
vec4 interpolate(vec4 a, vec4 b, float t)
{
  return mix(a, b, t);
}

vec2 midpoint(vec2 a, vec2 b)
{
  return (a + b) * 0.5;
}
vec3 midpoint(vec3 a, vec3 b)
{
  return (a + b) * 0.5;
}
vec4 midpoint(vec4 a, vec4 b)
{
  return (a + b) * 0.5;
}

int dominant_axis(vec3 a)
{
  vec3 b = abs(a);
  return ((b.x > b.y) ? ((b.x > b.z) ? 0 : 2) : ((b.y > b.z) ? 1 : 2));
}

vec3 orthogonal(vec3 v)
{
  switch (dominant_axis(v)) {
    default:
    case 0:
      return vec3(-v.y - v.z, v.x, v.x);
    case 1:
      return vec3(v.y, -v.x - v.z, v.y);
    case 2:
      return vec3(v.z, v.z, -v.x - v.y);
  }
}

vec2 orthogonal(vec2 v)
{
  return vec2(-v.y, v.x);
}
ivec2 orthogonal(ivec2 v)
{
  return ivec2(-v.y, v.x);
}

bool is_equal(vec2 a, vec2 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec2(epsilon)));
}
bool is_equal(vec3 a, vec3 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec3(epsilon)));
}
bool is_equal(vec4 a, vec4 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec4(epsilon)));
}

float reduce_max(vec2 a)
{
  return max(a.x, a.y);
}
float reduce_max(vec3 a)
{
  return max(a.x, max(a.y, a.z));
}
float reduce_max(vec4 a)
{
  return max(max(a.x, a.y), max(a.z, a.w));
}
int reduce_max(ivec2 a)
{
  return max(a.x, a.y);
}
int reduce_max(ivec3 a)
{
  return max(a.x, max(a.y, a.z));
}
int reduce_max(ivec4 a)
{
  return max(max(a.x, a.y), max(a.z, a.w));
}

float reduce_min(vec2 a)
{
  return min(a.x, a.y);
}
float reduce_min(vec3 a)
{
  return min(a.x, min(a.y, a.z));
}
float reduce_min(vec4 a)
{
  return min(min(a.x, a.y), min(a.z, a.w));
}
int reduce_min(ivec2 a)
{
  return min(a.x, a.y);
}
int reduce_min(ivec3 a)
{
  return min(a.x, min(a.y, a.z));
}
int reduce_min(ivec4 a)
{
  return min(min(a.x, a.y), min(a.z, a.w));
}

float reduce_add(vec2 a)
{
  return a.x + a.y;
}
float reduce_add(vec3 a)
{
  return a.x + a.y + a.z;
}
float reduce_add(vec4 a)
{
  return a.x + a.y + a.z + a.w;
}
int reduce_add(ivec2 a)
{
  return a.x + a.y;
}
int reduce_add(ivec3 a)
{
  return a.x + a.y + a.z;
}
int reduce_add(ivec4 a)
{
  return a.x + a.y + a.z + a.w;
}

float average(vec2 a)
{
  return reduce_add(a) * (1.0 / 2.0);
}
float average(vec3 a)
{
  return reduce_add(a) * (1.0 / 3.0);
}
float average(vec4 a)
{
  return reduce_add(a) * (1.0 / 4.0);
}

#  define ASSERT_UNIT_EPSILON 0.0002

/* Checks are flipped so NAN doesn't assert because we're making sure the value was
 * normalized and in the case we don't want NAN to be raising asserts since there
 * is nothing to be done in that case. */
bool is_unit_scale(vec2 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}
bool is_unit_scale(vec3 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}
bool is_unit_scale(vec4 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}

/** \} */

#endif /* GPU_SHADER_MATH_VECTOR_LIB_GLSL */

/////////////////////////////// Source file 19/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_UTILDEFINES_GLSL
#define GPU_SHADER_UTILDEFINES_GLSL

#ifndef FLT_MAX
#  define FLT_MAX uintBitsToFloat(0x7F7FFFFFu)
#  define FLT_MIN uintBitsToFloat(0x00800000u)
#  define FLT_EPSILON 1.192092896e-07F
#  define SHRT_MAX 0x00007FFF
#  define INT_MAX 0x7FFFFFFF
#  define USHRT_MAX 0x0000FFFFu
#  define UINT_MAX 0xFFFFFFFFu
#endif
#define NAN_FLT uintBitsToFloat(0x7FC00000u)
#define FLT_11_MAX uintBitsToFloat(0x477E0000)
#define FLT_10_MAX uintBitsToFloat(0x477C0000)
#define FLT_11_11_10_MAX vec3(FLT_11_MAX, FLT_11_MAX, FLT_10_MAX)

#define UNPACK2(a) (a)[0], (a)[1]
#define UNPACK3(a) (a)[0], (a)[1], (a)[2]
#define UNPACK4(a) (a)[0], (a)[1], (a)[2], (a)[3]

/**
 * Clamp input into [0..1] range.
 */
#define saturate(a) clamp(a, 0.0, 1.0)

#define isfinite(a) (!isinf(a) && !isnan(a))

/* clang-format off */
#define in_range_inclusive(val, min_v, max_v) (all(greaterThanEqual(val, min_v)) && all(lessThanEqual(val, max_v)))
#define in_range_exclusive(val, min_v, max_v) (all(greaterThan(val, min_v)) && all(lessThan(val, max_v)))
#define in_texture_range(texel, tex) (all(greaterThanEqual(texel, ivec2(0))) && all(lessThan(texel, textureSize(tex, 0).xy)))
#define in_image_range(texel, tex) (all(greaterThanEqual(texel, ivec2(0))) && all(lessThan(texel, imageSize(tex).xy)))

#define weighted_sum(val0, val1, val2, val3, weights) ((val0 * weights[0] + val1 * weights[1] + val2 * weights[2] + val3 * weights[3]) * safe_rcp(weights[0] + weights[1] + weights[2] + weights[3]))
#define weighted_sum_array(val, weights) ((val[0] * weights[0] + val[1] * weights[1] + val[2] * weights[2] + val[3] * weights[3]) * safe_rcp(weights[0] + weights[1] + weights[2] + weights[3]))
/* clang-format on */

bool flag_test(uint flag, uint val)
{
  return (flag & val) != 0u;
}
bool flag_test(int flag, uint val)
{
  return flag_test(uint(flag), val);
}
bool flag_test(int flag, int val)
{
  return (flag & val) != 0;
}

void set_flag_from_test(inout uint value, bool test, uint flag)
{
  if (test) {
    value |= flag;
  }
  else {
    value &= ~flag;
  }
}
void set_flag_from_test(inout int value, bool test, int flag)
{
  if (test) {
    value |= flag;
  }
  else {
    value &= ~flag;
  }
}

/* Keep define to match C++ implementation. */
#define SET_FLAG_FROM_TEST(value, test, flag) set_flag_from_test(value, test, flag)

/**
 * Pack two 16-bit uint into one 32-bit uint.
 */
uint packUvec2x16(uvec2 data)
{
  data = (data & 0xFFFFu) << uvec2(0u, 16u);
  return data.x | data.y;
}
uvec2 unpackUvec2x16(uint data)
{
  return (uvec2(data) >> uvec2(0u, 16u)) & uvec2(0xFFFFu);
}

/**
 * Pack four 8-bit uint into one 32-bit uint.
 */
uint packUvec4x8(uvec4 data)
{
  data = (data & 0xFFu) << uvec4(0u, 8u, 16u, 24u);
  return data.x | data.y | data.z | data.w;
}
uvec4 unpackUvec4x8(uint data)
{
  return (uvec4(data) >> uvec4(0u, 8u, 16u, 24u)) & uvec4(0xFFu);
}

/**
 * Convert from float representation to ordered int allowing min/max atomic operation.
 * Based on: https://stackoverflow.com/a/31010352
 */
int floatBitsToOrderedInt(float value)
{
  /* Floats can be sorted using their bits interpreted as integers for positive values.
   * Negative values do not follow int's two's complement ordering which is reversed.
   * So we have to XOR all bits except the sign bits in order to reverse the ordering.
   * Note that this is highly hardware dependent, but there seems to be no case of GPU where the
   * ints ares not two's complement. */
  int int_value = floatBitsToInt(value);
  return (int_value < 0) ? (int_value ^ 0x7FFFFFFF) : int_value;
}
float orderedIntBitsToFloat(int int_value)
{
  return intBitsToFloat((int_value < 0) ? (int_value ^ 0x7FFFFFFF) : int_value);
}

/**
 * Ray offset to avoid self intersection.
 *
 * This can be used to compute a modified ray start position for rays leaving from a surface.
 * From:
 * "A Fast and Robust Method for Avoiding Self-Intersection"
 * Ray Tracing Gems, chapter 6.
 */
vec3 offset_ray(vec3 P, vec3 Ng)
{
  const float origin = 1.0 / 32.0;
  const float float_scale = 1.0 / 65536.0;
  const float int_scale = 256.0;

  ivec3 of_i = ivec3(int_scale * Ng);
  of_i = ivec3((P.x < 0.0) ? -of_i.x : of_i.x,
               (P.y < 0.0) ? -of_i.y : of_i.y,
               (P.z < 0.0) ? -of_i.z : of_i.z);
  vec3 P_i = intBitsToFloat(floatBitsToInt(P) + of_i);

  vec3 uf = P + float_scale * Ng;
  return vec3((abs(P.x) < origin) ? uf.x : P_i.x,
              (abs(P.y) < origin) ? uf.y : P_i.y,
              (abs(P.z) < origin) ? uf.z : P_i.z);
}

#endif /* GPU_SHADER_UTILDEFINES_GLSL */

/////////////////////////////// Source file 20/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_ROTATION_LIB_GLSL
#  define GPU_SHADER_MATH_ROTATION_LIB_GLSL

/* -------------------------------------------------------------------- */
/** \name Rotation Types
 * \{ */

struct Angle {
  /* Angle in radian. */
  float angle;

#  ifdef GPU_METAL
  Angle() = default;
  Angle(float angle_) : angle(angle_){};
#  endif
};

struct AxisAngle {
  vec3 axis;
  float angle;

#  ifdef GPU_METAL
  AxisAngle() = default;
  AxisAngle(vec3 axis_, float angle_) : axis(axis_), angle(angle_){};
#  endif
};

AxisAngle AxisAngle_identity()
{
  return AxisAngle(vec3(0, 1, 0), 0);
}

struct Quaternion {
  float x, y, z, w;
#  ifdef GPU_METAL
  Quaternion() = default;
  Quaternion(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_){};
#  endif
};

vec4 as_vec4(Quaternion quat)
{
  return vec4(quat.x, quat.y, quat.z, quat.w);
}

Quaternion Quaternion_identity()
{
  return Quaternion(1, 0, 0, 0);
}

struct EulerXYZ {
  float x, y, z;
#  ifdef GPU_METAL
  EulerXYZ() = default;
  EulerXYZ(float x_, float y_, float z_) : x(x_), y(y_), z(z_){};
#  endif
};

vec3 as_vec3(EulerXYZ eul)
{
  return vec3(eul.x, eul.y, eul.z);
}

EulerXYZ EulerXYZ_identity()
{
  return EulerXYZ(0, 0, 0);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Rotation Functions
 * \{ */

/**
 * Generic function for implementing slerp
 * (quaternions and spherical vector coords).
 *
 * \param t: factor in [0..1]
 * \param cosom: dot product from normalized vectors/quaternions.
 * \param r_w: calculated weights.
 */
vec2 interpolate_dot_slerp(float t, float cosom)
{
  vec2 w = vec2(1.0 - t, t);
  /* Within [-1..1] range, avoid aligned axis. */
  const float eps = 1e-4;
  if (abs(cosom) < 1.0 - eps) {
    float omega = acos(cosom);
    w = sin(w * omega) / sin(omega);
  }
  return w;
}

Quaternion interpolate(Quaternion a, Quaternion b, float t)
{
  vec4 quat = as_vec4(a);
  float cosom = dot(as_vec4(a), as_vec4(b));
  /* Rotate around shortest angle. */
  if (cosom < 0.0) {
    cosom = -cosom;
    quat = -quat;
  }
  vec2 w = interpolate_dot_slerp(t, cosom);
  quat = w.x * quat + w.y * as_vec4(b);
  return Quaternion(UNPACK4(quat));
}

Quaternion to_quaternion(EulerXYZ eul)
{
  float ti = eul.x * 0.5;
  float tj = eul.y * 0.5;
  float th = eul.z * 0.5;
  float ci = cos(ti);
  float cj = cos(tj);
  float ch = cos(th);
  float si = sin(ti);
  float sj = sin(tj);
  float sh = sin(th);
  float cc = ci * ch;
  float cs = ci * sh;
  float sc = si * ch;
  float ss = si * sh;

  Quaternion quat;
  quat.x = cj * cc + sj * ss;
  quat.y = cj * sc - sj * cs;
  quat.z = cj * ss + sj * cc;
  quat.w = cj * cs - sj * sc;
  return quat;
}

Quaternion to_axis_angle(AxisAngle axis_angle)
{
  float angle_cos = cos(axis_angle.angle);
  /** Using half angle identities: sin(angle / 2) = sqrt((1 - angle_cos) / 2) */
  float sine = sqrt(0.5 - angle_cos * 0.5);
  float cosine = sqrt(0.5 + angle_cos * 0.5);

  /* TODO(fclem): Optimize. */
  float angle_sin = sin(axis_angle.angle);
  if (angle_sin < 0.0) {
    sine = -sine;
  }

  Quaternion quat;
  quat.x = cosine;
  quat.y = axis_angle.axis.x * sine;
  quat.z = axis_angle.axis.y * sine;
  quat.w = axis_angle.axis.z * sine;
  return quat;
}

AxisAngle to_axis_angle(Quaternion quat)
{
  /* Calculate angle/2, and sin(angle/2). */
  float ha = acos(quat.x);
  float si = sin(ha);

  /* From half-angle to angle. */
  float angle = ha * 2;
  /* Prevent division by zero for axis conversion. */
  if (abs(si) < 0.0005) {
    si = 1.0;
  }

  vec3 axis = vec3(quat.y, quat.z, quat.w) / si;
  if (is_zero(axis)) {
    axis[1] = 1.0;
  }
  return AxisAngle(axis, angle);
}

AxisAngle to_axis_angle(EulerXYZ eul)
{
  /* Use quaternions as intermediate representation for now... */
  return to_axis_angle(to_quaternion(eul));
}

/** \} */

#endif /* GPU_SHADER_MATH_ROTATION_LIB_GLSL */

/////////////////////////////// Source file 21/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_rotation_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_MATRIX_LIB_GLSL
#  define GPU_SHADER_MATH_MATRIX_LIB_GLSL

/* -------------------------------------------------------------------- */
/** \name Static constructors
 * \{ */

mat2x2 mat2x2_diagonal(float v)
{
  return mat2x2(vec2(v, 0.0), vec2(0.0, v));
}
mat3x3 mat3x3_diagonal(float v)
{
  return mat3x3(vec3(v, 0.0, 0.0), vec3(0.0, v, 0.0), vec3(0.0, 0.0, v));
}
mat4x4 mat4x4_diagonal(float v)
{
  return mat4x4(vec4(v, 0.0, 0.0, 0.0),
                vec4(0.0, v, 0.0, 0.0),
                vec4(0.0, 0.0, v, 0.0),
                vec4(0.0, 0.0, 0.0, v));
}

mat2x2 mat2x2_all(float v)
{
  return mat2x2(vec2(v), vec2(v));
}
mat3x3 mat3x3_all(float v)
{
  return mat3x3(vec3(v), vec3(v), vec3(v));
}
mat4x4 mat4x4_all(float v)
{
  return mat4x4(vec4(v), vec4(v), vec4(v), vec4(v));
}

mat2x2 mat2x2_zero(float v)
{
  return mat2x2_all(0.0);
}
mat3x3 mat3x3_zero(float v)
{
  return mat3x3_all(0.0);
}
mat4x4 mat4x4_zero(float v)
{
  return mat4x4_all(0.0);
}

mat2x2 mat2x2_identity()
{
  return mat2x2_diagonal(1.0);
}
mat3x3 mat3x3_identity()
{
  return mat3x3_diagonal(1.0);
}
mat4x4 mat4x4_identity()
{
  return mat4x4_diagonal(1.0);
}

/** \} */

/* Metal does not need prototypes. */
#  ifndef GPU_METAL

/* -------------------------------------------------------------------- */
/** \name Matrix Operations
 * \{ */

/**
 * Returns the inverse of a square matrix or zero matrix on failure.
 * \a r_success is optional and set to true if the matrix was inverted successfully.
 */
mat2x2 invert(mat2x2 mat);
mat3x3 invert(mat3x3 mat);
mat4x4 invert(mat4x4 mat);

mat2x2 invert(mat2x2 mat, out bool r_success);
mat3x3 invert(mat3x3 mat, out bool r_success);
mat4x4 invert(mat4x4 mat, out bool r_success);

/**
 * Flip the matrix across its diagonal. Also flips dimensions for non square matrices.
 */
// mat3x3 transpose(mat3x3 mat); /* Built-In using GLSL language. */

/**
 * Normalize each column of the matrix individually.
 */
mat2x2 normalize(mat2x2 mat);
mat2x3 normalize(mat2x3 mat);
mat2x4 normalize(mat2x4 mat);
mat3x2 normalize(mat3x2 mat);
mat3x3 normalize(mat3x3 mat);
mat3x4 normalize(mat3x4 mat);
mat4x2 normalize(mat4x2 mat);
mat4x3 normalize(mat4x3 mat);
mat4x4 normalize(mat4x4 mat);

/**
 * Normalize each column of the matrix individually.
 * Return the length of each column vector.
 */
mat2x2 normalize_and_get_size(mat2x2 mat, out vec2 r_size);
mat2x3 normalize_and_get_size(mat2x3 mat, out vec2 r_size);
mat2x4 normalize_and_get_size(mat2x4 mat, out vec2 r_size);
mat3x2 normalize_and_get_size(mat3x2 mat, out vec3 r_size);
mat3x3 normalize_and_get_size(mat3x3 mat, out vec3 r_size);
mat3x4 normalize_and_get_size(mat3x4 mat, out vec3 r_size);
mat4x2 normalize_and_get_size(mat4x2 mat, out vec4 r_size);
mat4x3 normalize_and_get_size(mat4x3 mat, out vec4 r_size);
mat4x4 normalize_and_get_size(mat4x4 mat, out vec4 r_size);

/**
 * Returns the determinant of the matrix.
 * It can be interpreted as the signed volume (or area) of the unit cube after transformation.
 */
// float determinant(mat3x3 mat); /* Built-In using GLSL language. */

/**
 * Returns the adjoint of the matrix (also known as adjugate matrix).
 */
mat2x2 adjoint(mat2x2 mat);
mat3x3 adjoint(mat3x3 mat);
mat4x4 adjoint(mat4x4 mat);

/**
 * Equivalent to `mat * from_location(translation)` but with fewer operation.
 */
mat4x4 translate(mat4x4 mat, vec2 translation);
mat4x4 translate(mat4x4 mat, vec3 translation);

/**
 * Equivalent to `mat * from_rotation(rotation)` but with fewer operation.
 * Optimized for rotation on basis vector (i.e: AxisAngle({1, 0, 0}, 0.2)).
 */
mat3x3 rotate(mat3x3 mat, AxisAngle rotation);
mat3x3 rotate(mat3x3 mat, EulerXYZ rotation);
mat4x4 rotate(mat4x4 mat, AxisAngle rotation);
mat4x4 rotate(mat4x4 mat, EulerXYZ rotation);

/**
 * Equivalent to `mat * from_scale(scale)` but with fewer operation.
 */
mat3x3 scale(mat3x3 mat, vec2 scale);
mat3x3 scale(mat3x3 mat, vec3 scale);
mat4x4 scale(mat4x4 mat, vec2 scale);
mat4x4 scale(mat4x4 mat, vec3 scale);

/**
 * Interpolate each component linearly.
 */
// mat4x4 interpolate_linear(mat4x4 a, mat4x4 b, float t); /* TODO. */

/**
 * A polar-decomposition-based interpolation between matrix A and matrix B.
 */
// mat3x3 interpolate(mat3x3 a, mat3x3 b, float t); /* Not implemented. Too complex to port. */

/**
 * Naive interpolation implementation, faster than polar decomposition
 *
 * \note This code is about five times faster than the polar decomposition.
 * However, it gives un-expected results even with non-uniformly scaled matrices,
 * see #46418 for an example.
 *
 * \param A: Input matrix which is totally effective with `t = 0.0`.
 * \param B: Input matrix which is totally effective with `t = 1.0`.
 * \param t: Interpolation factor.
 */
mat3x3 interpolate_fast(mat3x3 a, mat3x3 b, float t);

/**
 * Naive transform matrix interpolation,
 * based on naive-decomposition-based interpolation from #interpolate_fast<T, 3, 3>.
 */
mat4x4 interpolate_fast(mat4x4 a, mat4x4 b, float t);

/**
 * Compute Moore-Penrose pseudo inverse of matrix.
 * Singular values below epsilon are ignored for stability (truncated SVD).
 */
// mat4x4 pseudo_invert(mat4x4 mat, float epsilon); /* Not implemented. Too complex to port. */

/** \} */

/* -------------------------------------------------------------------- */
/** \name Init helpers.
 * \{ */

/**
 * Create a translation only matrix. Matrix dimensions should be at least 4 col x 3 row.
 */
mat4x4 from_location(vec3 location);

/**
 * Create a matrix whose diagonal is defined by the given scale vector.
 */
mat2x2 from_scale(vec2 scale);
mat3x3 from_scale(vec3 scale);
mat4x4 from_scale(vec4 scale);

/**
 * Create a rotation only matrix.
 */
mat2x2 from_rotation(Angle rotation);
mat3x3 from_rotation(EulerXYZ rotation);
mat3x3 from_rotation(Quaternion rotation);
mat3x3 from_rotation(AxisAngle rotation);

/**
 * Create a transform matrix with rotation and scale applied in this order.
 */
mat3x3 from_rot_scale(EulerXYZ rotation, vec3 scale);

/**
 * Create a transform matrix with translation and rotation applied in this order.
 */
mat4x4 from_loc_rot(vec3 location, EulerXYZ rotation);

/**
 * Create a transform matrix with translation, rotation and scale applied in this order.
 */
mat4x4 from_loc_rot_scale(vec3 location, EulerXYZ rotation, vec3 scale);

/**
 * Create a rotation matrix from 2 basis vectors.
 * The matrix determinant is given to be positive and it can be converted to other rotation types.
 * \note `forward` and `up` must be normalized.
 */
// mat3x3 from_normalized_axis_data(vec3 forward, vec3 up); /* TODO. */

/**
 * Create a transform matrix with translation and rotation from 2 basis vectors and a translation.
 * \note `forward` and `up` must be normalized.
 */
// mat4x4 from_normalized_axis_data(vec3 location, vec3 forward, vec3 up); /* TODO. */

/**
 * Create a rotation matrix from only one \a up axis.
 * The other axes are chosen to always be orthogonal. The resulting matrix is a basis matrix.
 * \note `up` must be normalized.
 * \note This can be used to create a tangent basis from a normal vector.
 * \note The output of this function is not given to be same across blender version. Prefer using
 * `from_orthonormal_axes` for more stable output.
 */
mat3x3 from_up_axis(vec3 up);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Conversion function.
 * \{ */

/**
 * Extract euler rotation from transform matrix.
 * \return the rotation with the smallest values from the potential candidates.
 */
EulerXYZ to_euler(mat3x3 mat);
EulerXYZ to_euler(mat3x3 mat, const bool normalized);
EulerXYZ to_euler(mat4x4 mat);
EulerXYZ to_euler(mat4x4 mat, const bool normalized);

/**
 * Extract quaternion rotation from transform matrix.
 * \note normalized is set to false by default.
 */
Quaternion to_quaternion(mat3x3 mat);
Quaternion to_quaternion(mat3x3 mat, const bool normalized);
Quaternion to_quaternion(mat4x4 mat);
Quaternion to_quaternion(mat4x4 mat, const bool normalized);

/**
 * Extract the absolute 3d scale from a transform matrix.
 */
vec3 to_scale(mat3x3 mat);
vec3 to_scale(mat3x3 mat, const bool allow_negative_scale);
vec3 to_scale(mat4x4 mat);
vec3 to_scale(mat4x4 mat, const bool allow_negative_scale);

/**
 * Decompose a matrix into location, rotation, and scale components.
 * \tparam allow_negative_scale: if true, will compute determinant to know if matrix is negative.
 * Rotation and scale values will be flipped if it is negative.
 * This is a costly operation so it is disabled by default.
 */
void to_rot_scale(mat3x3 mat, out EulerXYZ r_rotation, out vec3 r_scale);
void to_rot_scale(mat3x3 mat, out Quaternion r_rotation, out vec3 r_scale);
void to_rot_scale(mat3x3 mat, out AxisAngle r_rotation, out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out EulerXYZ r_rotation, out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out AxisAngle r_rotation, out vec3 r_scale);

void to_rot_scale(mat3x3 mat,
                  out EulerXYZ r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_rot_scale(mat3x3 mat,
                  out Quaternion r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_rot_scale(mat3x3 mat,
                  out AxisAngle r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out EulerXYZ r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out AxisAngle r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform function.
 * \{ */

/**
 * Transform a 3d point using a 3x3 matrix (rotation & scale).
 */
vec3 transform_point(mat3x3 mat, vec3 point);

/**
 * Transform a 3d point using a 4x4 matrix (location & rotation & scale).
 */
vec3 transform_point(mat4x4 mat, vec3 point);

/**
 * Transform a 3d direction vector using a 3x3 matrix (rotation & scale).
 */
vec3 transform_direction(mat3x3 mat, vec3 direction);

/**
 * Transform a 3d direction vector using a 4x4 matrix (rotation & scale).
 */
vec3 transform_direction(mat4x4 mat, vec3 direction);

/**
 * Project a point using a matrix (location & rotation & scale & perspective divide).
 */
vec2 project_point(mat3x3 mat, vec2 point);
vec3 project_point(mat4x4 mat, vec3 point);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Projection Matrices.
 * \{ */

/**
 * \brief Create an orthographic projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * The resulting matrix can be used with either #project_point or #transform_point.
 */
mat4x4 projection_orthographic(
    float left, float right, float bottom, float top, float near_clip, float far_clip);

/**
 * \brief Create a perspective projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * `left`, `right`, `bottom`, `top` are frustum side distances at `z=near_clip`.
 * The resulting matrix can be used with #project_point.
 */
mat4x4 projection_perspective(
    float left, float right, float bottom, float top, float near_clip, float far_clip);

/**
 * \brief Create a perspective projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * Uses field of view angles instead of plane distances.
 * The resulting matrix can be used with #project_point.
 */
mat4x4 projection_perspective_fov(float angle_left,
                                  float angle_right,
                                  float angle_bottom,
                                  float angle_top,
                                  float near_clip,
                                  float far_clip);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Compare / Test
 * \{ */

/**
 * Returns true if all of the matrices components are strictly equal to 0.
 */
bool is_zero(mat3x3 a);
bool is_zero(mat4x4 a);

/**
 * Returns true if matrix has inverted handedness.
 *
 * \note It doesn't use determinant(mat4x4) as only the 3x3 components are needed
 * when the matrix is used as a transformation to represent location/scale/rotation.
 */
bool is_negative(mat3x3 mat);
bool is_negative(mat4x4 mat);

/**
 * Returns true if matrices are equal within the given epsilon.
 */
bool is_equal(mat2x2 a, mat2x2 b, float epsilon);
bool is_equal(mat3x3 a, mat3x3 b, float epsilon);
bool is_equal(mat4x4 a, mat4x4 b, float epsilon);

/**
 * Test if the X, Y and Z axes are perpendicular with each other.
 */
bool is_orthogonal(mat3x3 mat);
bool is_orthogonal(mat4x4 mat);

/**
 * Test if the X, Y and Z axes are perpendicular with each other and unit length.
 */
bool is_orthonormal(mat3x3 mat);
bool is_orthonormal(mat4x4 mat);

/**
 * Test if the X, Y and Z axes are perpendicular with each other and the same length.
 */
bool is_uniformly_scaled(mat3x3 mat);

/** \} */

#  endif /* GPU_METAL */

/* ---------------------------------------------------------------------- */
/** \name Implementation
 * \{ */

mat2x2 invert(mat2x2 mat)
{
  return inverse(mat);
}
mat3x3 invert(mat3x3 mat)
{
  return inverse(mat);
}
mat4x4 invert(mat4x4 mat)
{
  return inverse(mat);
}

mat2x2 invert(mat2x2 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat2x2(0.0);
}
mat3x3 invert(mat3x3 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat3x3(0.0);
}
mat4x4 invert(mat4x4 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat4x4(0.0);
}

#  if defined(GPU_OPENGL) || defined(GPU_METAL)
vec2 normalize(vec2 a)
{
  return a * inversesqrt(length_squared(a));
}
vec3 normalize(vec3 a)
{
  return a * inversesqrt(length_squared(a));
}
vec4 normalize(vec4 a)
{
  return a * inversesqrt(length_squared(a));
}
#  endif

mat2x2 normalize(mat2x2 mat)
{
  mat2x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  return ret;
}
mat2x3 normalize(mat2x3 mat)
{
  mat2x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  return ret;
}
mat2x4 normalize(mat2x4 mat)
{
  mat2x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  return ret;
}
mat3x2 normalize(mat3x2 mat)
{
  mat3x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  ret[2] = normalize(mat[2].xy);
  return ret;
}
mat3x3 normalize(mat3x3 mat)
{
  mat3x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  ret[2] = normalize(mat[2].xyz);
  return ret;
}
mat3x4 normalize(mat3x4 mat)
{
  mat3x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  ret[2] = normalize(mat[2].xyzw);
  return ret;
}
mat4x2 normalize(mat4x2 mat)
{
  mat4x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  ret[2] = normalize(mat[2].xy);
  ret[3] = normalize(mat[3].xy);
  return ret;
}
mat4x3 normalize(mat4x3 mat)
{
  mat4x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  ret[2] = normalize(mat[2].xyz);
  ret[3] = normalize(mat[3].xyz);
  return ret;
}
mat4x4 normalize(mat4x4 mat)
{
  mat4x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  ret[2] = normalize(mat[2].xyzw);
  ret[3] = normalize(mat[3].xyzw);
  return ret;
}

mat2x2 normalize_and_get_size(mat2x2 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat2x3 normalize_and_get_size(mat2x3 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat2x4 normalize_and_get_size(mat2x4 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat3x2 normalize_and_get_size(mat3x2 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat3x3 normalize_and_get_size(mat3x3 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat3x4 normalize_and_get_size(mat3x4 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat4x2 normalize_and_get_size(mat4x2 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}
mat4x3 normalize_and_get_size(mat4x3 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}
mat4x4 normalize_and_get_size(mat4x4 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}

mat2x2 adjoint(mat2x2 mat)
{
  mat2x2 adj = mat2x2(0.0);
  for (int c = 0; c < 2; c++) {
    for (int r = 0; r < 2; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      float tmp = 0.0;
      for (int m_c = 0; m_c < 2; m_c++) {
        for (int m_r = 0; m_r < 2; m_r++) {
          if (m_c != c && m_r != r) {
            tmp = mat[m_c][m_r];
          }
        }
      }
      float minor = tmp;
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}
mat3x3 adjoint(mat3x3 mat)
{
  mat3x3 adj = mat3x3(0.0);
  for (int c = 0; c < 3; c++) {
    for (int r = 0; r < 3; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      mat2x2 tmp = mat2x2(0.0);
      for (int m_c = 0; m_c < 3; m_c++) {
        for (int m_r = 0; m_r < 3; m_r++) {
          if (m_c != c && m_r != r) {
            int d_c = (m_c < c) ? m_c : (m_c - 1);
            int d_r = (m_r < r) ? m_r : (m_r - 1);
            tmp[d_c][d_r] = mat[m_c][m_r];
          }
        }
      }
      float minor = determinant(tmp);
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}
mat4x4 adjoint(mat4x4 mat)
{
  mat4x4 adj = mat4x4(0.0);
  for (int c = 0; c < 4; c++) {
    for (int r = 0; r < 4; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      mat3x3 tmp = mat3x3(0.0);
      for (int m_c = 0; m_c < 4; m_c++) {
        for (int m_r = 0; m_r < 4; m_r++) {
          if (m_c != c && m_r != r) {
            int d_c = (m_c < c) ? m_c : (m_c - 1);
            int d_r = (m_r < r) ? m_r : (m_r - 1);
            tmp[d_c][d_r] = mat[m_c][m_r];
          }
        }
      }
      float minor = determinant(tmp);
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}

mat4x4 translate(mat4x4 mat, vec3 translation)
{
  mat[3].xyz += translation[0] * mat[0].xyz;
  mat[3].xyz += translation[1] * mat[1].xyz;
  mat[3].xyz += translation[2] * mat[2].xyz;
  return mat;
}
mat4x4 translate(mat4x4 mat, vec2 translation)
{
  mat[3].xyz += translation[0] * mat[0].xyz;
  mat[3].xyz += translation[1] * mat[1].xyz;
  return mat;
}

mat3x3 rotate(mat3x3 mat, AxisAngle rotation)
{
  mat3x3 result;
  /* axis_vec is given to be normalized. */
  if (rotation.axis.x == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = mat[0][c];
      result[1][c] = angle_cos * mat[1][c] + angle_sin * mat[2][c];
      result[2][c] = -angle_sin * mat[1][c] + angle_cos * mat[2][c];
    }
  }
  else if (rotation.axis.y == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = angle_cos * mat[0][c] - angle_sin * mat[2][c];
      result[1][c] = mat[1][c];
      result[2][c] = angle_sin * mat[0][c] + angle_cos * mat[2][c];
    }
  }
  else if (rotation.axis.z == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = angle_cos * mat[0][c] + angle_sin * mat[1][c];
      result[1][c] = -angle_sin * mat[0][c] + angle_cos * mat[1][c];
      result[2][c] = mat[2][c];
    }
  }
  else {
    /* Un-optimized case. Arbitrary rotation. */
    result = mat * from_rotation(rotation);
  }
  return result;
}
mat3x3 rotate(mat3x3 mat, EulerXYZ rotation)
{
  AxisAngle axis_angle;
  if (rotation.y == 0.0 && rotation.z == 0.0) {
    axis_angle = AxisAngle(vec3(1.0, 0.0, 0.0), rotation.x);
  }
  else if (rotation.x == 0.0 && rotation.z == 0.0) {
    axis_angle = AxisAngle(vec3(0.0, 1.0, 0.0), rotation.y);
  }
  else if (rotation.x == 0.0 && rotation.y == 0.0) {
    axis_angle = AxisAngle(vec3(0.0, 0.0, 1.0), rotation.z);
  }
  else {
    /* Un-optimized case. Arbitrary rotation. */
    return mat * from_rotation(rotation);
  }
  return rotate(mat, axis_angle);
}

mat4x4 rotate(mat4x4 mat, AxisAngle rotation)
{
  mat4x4 result = mat4x4(rotate(mat3x3(mat), rotation));
  result[0][3] = mat[0][3];
  result[1][3] = mat[1][3];
  result[2][3] = mat[2][3];
  result[3][0] = mat[3][0];
  result[3][1] = mat[3][1];
  result[3][2] = mat[3][2];
  result[3][3] = mat[3][3];
  return result;
}
mat4x4 rotate(mat4x4 mat, EulerXYZ rotation)
{
  mat4x4 result = mat4x4(rotate(mat3x3(mat), rotation));
  result[0][3] = mat[0][3];
  result[1][3] = mat[1][3];
  result[2][3] = mat[2][3];
  result[3][0] = mat[3][0];
  result[3][1] = mat[3][1];
  result[3][2] = mat[3][2];
  result[3][3] = mat[3][3];
  return result;
}

mat3x3 scale(mat3x3 mat, vec2 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  return mat;
}
mat3x3 scale(mat3x3 mat, vec3 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  mat[2] *= scale[2];
  return mat;
}
mat4x4 scale(mat4x4 mat, vec2 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  return mat;
}
mat4x4 scale(mat4x4 mat, vec3 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  mat[2] *= scale[2];
  return mat;
}

mat4x4 from_location(vec3 location)
{
  mat4x4 ret = mat4x4(1.0);
  ret[3].xyz = location;
  return ret;
}

mat2x2 from_scale(vec2 scale)
{
  mat2x2 ret = mat2x2(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  return ret;
}
mat3x3 from_scale(vec3 scale)
{
  mat3x3 ret = mat3x3(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  ret[2][2] = scale[2];
  return ret;
}
mat4x4 from_scale(vec4 scale)
{
  mat4x4 ret = mat4x4(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  ret[2][2] = scale[2];
  ret[3][3] = scale[3];
  return ret;
}

mat2x2 from_rotation(Angle rotation)
{
  float c = cos(rotation.angle);
  float s = sin(rotation.angle);
  return mat2x2(c, -s, s, c);
}

mat3x3 from_rotation(EulerXYZ rotation)
{
  float ci = cos(rotation.x);
  float cj = cos(rotation.y);
  float ch = cos(rotation.z);
  float si = sin(rotation.x);
  float sj = sin(rotation.y);
  float sh = sin(rotation.z);
  float cc = ci * ch;
  float cs = ci * sh;
  float sc = si * ch;
  float ss = si * sh;

  mat3x3 mat;
  mat[0][0] = cj * ch;
  mat[1][0] = sj * sc - cs;
  mat[2][0] = sj * cc + ss;

  mat[0][1] = cj * sh;
  mat[1][1] = sj * ss + cc;
  mat[2][1] = sj * cs - sc;

  mat[0][2] = -sj;
  mat[1][2] = cj * si;
  mat[2][2] = cj * ci;
  return mat;
}

mat3x3 from_rotation(Quaternion rotation)
{
  /* NOTE: Should be double but support isn't native on most GPUs. */
  float q0 = M_SQRT2 * float(rotation.x);
  float q1 = M_SQRT2 * float(rotation.y);
  float q2 = M_SQRT2 * float(rotation.z);
  float q3 = M_SQRT2 * float(rotation.w);

  float qda = q0 * q1;
  float qdb = q0 * q2;
  float qdc = q0 * q3;
  float qaa = q1 * q1;
  float qab = q1 * q2;
  float qac = q1 * q3;
  float qbb = q2 * q2;
  float qbc = q2 * q3;
  float qcc = q3 * q3;

  mat3x3 mat;
  mat[0][0] = float(1.0 - qbb - qcc);
  mat[0][1] = float(qdc + qab);
  mat[0][2] = float(-qdb + qac);

  mat[1][0] = float(-qdc + qab);
  mat[1][1] = float(1.0 - qaa - qcc);
  mat[1][2] = float(qda + qbc);

  mat[2][0] = float(qdb + qac);
  mat[2][1] = float(-qda + qbc);
  mat[2][2] = float(1.0 - qaa - qbb);
  return mat;
}

mat3x3 from_rotation(AxisAngle rotation)
{
  float angle_sin = sin(rotation.angle);
  float angle_cos = cos(rotation.angle);
  vec3 axis = rotation.axis;

  float ico = (float(1) - angle_cos);
  vec3 nsi = axis * angle_sin;

  vec3 n012 = (axis * axis) * ico;
  float n_01 = (axis[0] * axis[1]) * ico;
  float n_02 = (axis[0] * axis[2]) * ico;
  float n_12 = (axis[1] * axis[2]) * ico;

  mat3 mat = from_scale(n012 + angle_cos);
  mat[0][1] = n_01 + nsi[2];
  mat[0][2] = n_02 - nsi[1];
  mat[1][0] = n_01 - nsi[2];
  mat[1][2] = n_12 + nsi[0];
  mat[2][0] = n_02 + nsi[1];
  mat[2][1] = n_12 - nsi[0];
  return mat;
}

mat3x3 from_rot_scale(EulerXYZ rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}
mat3x3 from_rot_scale(Quaternion rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}
mat3x3 from_rot_scale(AxisAngle rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}

mat4x4 from_loc_rot(vec3 location, EulerXYZ rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot(vec3 location, Quaternion rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot(vec3 location, AxisAngle rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}

mat4x4 from_loc_rot_scale(vec3 location, EulerXYZ rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot_scale(vec3 location, Quaternion rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot_scale(vec3 location, AxisAngle rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}

mat3x3 from_up_axis(vec3 up)
{
  /* Duff, Tom, et al. "Building an orthonormal basis, revisited." JCGT 6.1 (2017). */
  float z_sign = up.z >= 0.0 ? 1.0 : -1.0;
  float a = -1.0 / (z_sign + up.z);
  float b = up.x * up.y * a;

  mat3x3 basis;
  basis[0] = vec3(1.0 + z_sign * square(up.x) * a, z_sign * b, -z_sign * up.x);
  basis[1] = vec3(b, z_sign + square(up.y) * a, -up.y);
  basis[2] = up;
  return basis;
}

void detail_normalized_to_eul2(mat3 mat, out EulerXYZ eul1, out EulerXYZ eul2)
{
  float cy = hypot(mat[0][0], mat[0][1]);
  if (cy > 16.0f * FLT_EPSILON) {
    eul1.x = atan2(mat[1][2], mat[2][2]);
    eul1.y = atan2(-mat[0][2], cy);
    eul1.z = atan2(mat[0][1], mat[0][0]);

    eul2.x = atan2(-mat[1][2], -mat[2][2]);
    eul2.y = atan2(-mat[0][2], -cy);
    eul2.z = atan2(-mat[0][1], -mat[0][0]);
  }
  else {
    eul1.x = atan2(-mat[2][1], mat[1][1]);
    eul1.y = atan2(-mat[0][2], cy);
    eul1.z = 0.0;

    eul2 = eul1;
  }
}

EulerXYZ to_euler(mat3x3 mat)
{
  return to_euler(mat, true);
}
EulerXYZ to_euler(mat3x3 mat, const bool normalized)
{
  if (!normalized) {
    mat = normalize(mat);
  }
  EulerXYZ eul1, eul2;
  detail_normalized_to_eul2(mat, eul1, eul2);
  /* Return best, which is just the one with lowest values it in. */
  return (length_manhattan(as_vec3(eul1)) > length_manhattan(as_vec3(eul2))) ? eul2 : eul1;
}
EulerXYZ to_euler(mat4x4 mat)
{
  return to_euler(mat3(mat));
}
EulerXYZ to_euler(mat4x4 mat, const bool normalized)
{
  return to_euler(mat3(mat), normalized);
}

Quaternion normalized_to_quat_fast(mat3 mat)
{
  /* Caller must ensure matrices aren't negative for valid results, see: #24291, #94231. */
  Quaternion q;

  /* Method outlined by Mike Day, ref: https://math.stackexchange.com/a/3183435/220949
   * with an additional `sqrtf(..)` for higher precision result.
   * Removing the `sqrt` causes tests to fail unless the precision is set to 1e-6 or larger. */

  if (mat[2][2] < 0.0f) {
    if (mat[0][0] > mat[1][1]) {
      float trace = 1.0f + mat[0][0] - mat[1][1] - mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[1][2] < mat[2][1]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.y = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[1][2] - mat[2][1]) * s;
      q.z = (mat[0][1] + mat[1][0]) * s;
      q.w = (mat[2][0] + mat[0][2]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.z == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.y = 1.0f;
      }
    }
    else {
      float trace = 1.0f - mat[0][0] + mat[1][1] - mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[2][0] < mat[0][2]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.z = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[2][0] - mat[0][2]) * s;
      q.y = (mat[0][1] + mat[1][0]) * s;
      q.w = (mat[1][2] + mat[2][1]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.y == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.z = 1.0f;
      }
    }
  }
  else {
    if (mat[0][0] < -mat[1][1]) {
      float trace = 1.0f - mat[0][0] - mat[1][1] + mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[0][1] < mat[1][0]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.w = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[0][1] - mat[1][0]) * s;
      q.y = (mat[2][0] + mat[0][2]) * s;
      q.z = (mat[1][2] + mat[2][1]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.y == 0.0f && q.z == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.w = 1.0f;
      }
    }
    else {
      /* NOTE(@ideasman42): A zero matrix will fall through to this block,
       * needed so a zero scaled matrices to return a quaternion without rotation, see: #101848. */
      float trace = 1.0f + mat[0][0] + mat[1][1] + mat[2][2];
      float s = 2.0f * sqrt(trace);
      q.x = 0.25f * s;
      s = 1.0f / s;
      q.y = (mat[1][2] - mat[2][1]) * s;
      q.z = (mat[2][0] - mat[0][2]) * s;
      q.w = (mat[0][1] - mat[1][0]) * s;
      if ((trace == 1.0f) && (q.y == 0.0f && q.z == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.x = 1.0f;
      }
    }
  }
  return q;
}

Quaternion detail_normalized_to_quat_with_checks(mat3x3 mat)
{
  float det = determinant(mat);
  if (!isfinite(det)) {
    return Quaternion_identity();
  }
  else if (det < 0.0) {
    return normalized_to_quat_fast(-mat);
  }
  return normalized_to_quat_fast(mat);
}

Quaternion to_quaternion(mat3x3 mat)
{
  return detail_normalized_to_quat_with_checks(normalize(mat));
}
Quaternion to_quaternion(mat3x3 mat, const bool normalized)
{
  if (!normalized) {
    mat = normalize(mat);
  }
  return to_quaternion(mat);
}
Quaternion to_quaternion(mat4x4 mat)
{
  return to_quaternion(mat3(mat));
}
Quaternion to_quaternion(mat4x4 mat, const bool normalized)
{
  return to_quaternion(mat3(mat), normalized);
}

vec3 to_scale(mat3x3 mat)
{
  return vec3(length(mat[0]), length(mat[1]), length(mat[2]));
}
vec3 to_scale(mat3x3 mat, const bool allow_negative_scale)
{
  vec3 result = to_scale(mat);
  if (allow_negative_scale) {
    if (is_negative(mat)) {
      result = -result;
    }
  }
  return result;
}
vec3 to_scale(mat4x4 mat)
{
  return to_scale(mat3(mat));
}
vec3 to_scale(mat4x4 mat, const bool allow_negative_scale)
{
  return to_scale(mat3(mat), allow_negative_scale);
}

void to_rot_scale(mat3x3 mat, out EulerXYZ r_rotation, out vec3 r_scale)
{
  r_scale = to_scale(mat);
  r_rotation = to_euler(mat, true);
}
void to_rot_scale(mat3x3 mat,
                  out EulerXYZ r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale)
{
  mat3x3 normalized_mat = normalize_and_get_size(mat, r_scale);
  if (allow_negative_scale) {
    if (is_negative(normalized_mat)) {
      normalized_mat = -normalized_mat;
      r_scale = -r_scale;
    }
  }
  r_rotation = to_euler(mat, true);
}
void to_rot_scale(mat3x3 mat, out Quaternion r_rotation, out vec3 r_scale)
{
  r_scale = to_scale(mat);
  r_rotation = to_quaternion(mat, true);
}
void to_rot_scale(mat3x3 mat,
                  out Quaternion r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale)
{
  mat3x3 normalized_mat = normalize_and_get_size(mat, r_scale);
  if (allow_negative_scale) {
    if (is_negative(normalized_mat)) {
      normalized_mat = -normalized_mat;
      r_scale = -r_scale;
    }
  }
  r_rotation = to_quaternion(mat, true);
}

void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out EulerXYZ r_rotation, out vec3 r_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale);
}
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out EulerXYZ r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale, allow_negative_scale);
}
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out Quaternion r_rotation, out vec3 r_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale);
}
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale, allow_negative_scale);
}

vec3 transform_point(mat3x3 mat, vec3 point)
{
  return mat * point;
}

vec3 transform_point(mat4x4 mat, vec3 point)
{
  return (mat * vec4(point, 1.0)).xyz;
}

vec3 transform_direction(mat3x3 mat, vec3 direction)
{
  return mat * direction;
}

vec3 transform_direction(mat4x4 mat, vec3 direction)
{
  return mat3x3(mat) * direction;
}

vec2 project_point(mat3x3 mat, vec2 point)
{
  vec3 tmp = mat * vec3(point, 1.0);
  /* Absolute value to not flip the frustum upside down behind the camera. */
  return tmp.xy / abs(tmp.z);
}
vec3 project_point(mat4x4 mat, vec3 point)
{
  vec4 tmp = mat * vec4(point, 1.0);
  /* Absolute value to not flip the frustum upside down behind the camera. */
  return tmp.xyz / abs(tmp.w);
}

mat4x4 interpolate_fast(mat4x4 a, mat4x4 b, float t)
{
  vec3 a_loc, b_loc;
  vec3 a_scale, b_scale;
  Quaternion a_quat, b_quat;
  to_loc_rot_scale(a, a_loc, a_quat, a_scale);
  to_loc_rot_scale(b, b_loc, b_quat, b_scale);

  vec3 location = interpolate(a_loc, b_loc, t);
  vec3 scale = interpolate(a_scale, b_scale, t);
  Quaternion rotation = interpolate(a_quat, b_quat, t);
  return from_loc_rot_scale(location, rotation, scale);
}

mat4x4 projection_orthographic(
    float left, float right, float bottom, float top, float near_clip, float far_clip)
{
  float x_delta = right - left;
  float y_delta = top - bottom;
  float z_delta = far_clip - near_clip;

  mat4x4 mat = mat4x4(1.0);
  if (x_delta != 0.0 && y_delta != 0.0 && z_delta != 0.0) {
    mat[0][0] = 2.0 / x_delta;
    mat[3][0] = -(right + left) / x_delta;
    mat[1][1] = 2.0 / y_delta;
    mat[3][1] = -(top + bottom) / y_delta;
    mat[2][2] = -2.0 / z_delta; /* NOTE: negate Z. */
    mat[3][2] = -(far_clip + near_clip) / z_delta;
  }
  return mat;
}

mat4x4 projection_perspective(
    float left, float right, float bottom, float top, float near_clip, float far_clip)
{
  float x_delta = right - left;
  float y_delta = top - bottom;
  float z_delta = far_clip - near_clip;

  mat4x4 mat = mat4x4(1.0);
  if (x_delta != 0.0 && y_delta != 0.0 && z_delta != 0.0) {
    mat[0][0] = near_clip * 2.0 / x_delta;
    mat[1][1] = near_clip * 2.0 / y_delta;
    mat[2][0] = (right + left) / x_delta; /* NOTE: negate Z. */
    mat[2][1] = (top + bottom) / y_delta;
    mat[2][2] = -(far_clip + near_clip) / z_delta;
    mat[2][3] = -1.0;
    mat[3][2] = (-2.0 * near_clip * far_clip) / z_delta;
    mat[3][3] = 0.0;
  }
  return mat;
}

mat4x4 projection_perspective_fov(float angle_left,
                                  float angle_right,
                                  float angle_bottom,
                                  float angle_top,
                                  float near_clip,
                                  float far_clip)
{
  mat4x4 mat = projection_perspective(
      tan(angle_left), tan(angle_right), tan(angle_bottom), tan(angle_top), near_clip, far_clip);
  mat[0][0] /= near_clip;
  mat[1][1] /= near_clip;
  return mat;
}

bool is_zero(mat3x3 a)
{
  if (is_zero(a[0])) {
    if (is_zero(a[1])) {
      if (is_zero(a[2])) {
        return true;
      }
    }
  }
  return false;
}
bool is_zero(mat4x4 a)
{
  if (is_zero(a[0])) {
    if (is_zero(a[1])) {
      if (is_zero(a[2])) {
        if (is_zero(a[3])) {
          return true;
        }
      }
    }
  }
  return false;
}

bool is_negative(mat3x3 mat)
{
  return determinant(mat) < 0.0;
}
bool is_negative(mat4x4 mat)
{
  return is_negative(mat3x3(mat));
}

bool is_equal(mat2x2 a, mat2x2 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      return true;
    }
  }
  return false;
}
bool is_equal(mat3x3 a, mat3x3 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      if (is_equal(a[2], b[2], epsilon)) {
        return true;
      }
    }
  }
  return false;
}
bool is_equal(mat4x4 a, mat4x4 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      if (is_equal(a[2], b[2], epsilon)) {
        if (is_equal(a[3], b[3], epsilon)) {
          return true;
        }
      }
    }
  }
  return false;
}

bool is_orthogonal(mat3x3 mat)
{
  if (abs(dot(mat[0], mat[1])) > 1e-5) {
    return false;
  }
  if (abs(dot(mat[1], mat[2])) > 1e-5) {
    return false;
  }
  if (abs(dot(mat[2], mat[0])) > 1e-5) {
    return false;
  }
  return true;
}

bool is_orthonormal(mat3x3 mat)
{
  if (!is_orthogonal(mat)) {
    return false;
  }
  if (abs(length_squared(mat[0]) - 1.0) > 1e-5) {
    return false;
  }
  if (abs(length_squared(mat[1]) - 1.0) > 1e-5) {
    return false;
  }
  if (abs(length_squared(mat[2]) - 1.0) > 1e-5) {
    return false;
  }
  return true;
}

bool is_uniformly_scaled(mat3x3 mat)
{
  if (!is_orthogonal(mat)) {
    return false;
  }
  const float eps = 1e-7;
  float x = length_squared(mat[0]);
  float y = length_squared(mat[1]);
  float z = length_squared(mat[2]);
  return (abs(x - y) < eps) && abs(x - z) < eps;
}

bool is_orthogonal(mat4x4 mat)
{
  return is_orthogonal(mat3x3(mat));
}
bool is_orthonormal(mat4x4 mat)
{
  return is_orthonormal(mat3x3(mat));
}
bool is_uniformly_scaled(mat4x4 mat)
{
  return is_uniformly_scaled(mat3x3(mat));
}

/* Returns true if each individual columns are unit scaled. Mainly for assert usage. */
bool is_unit_scale(mat4x4 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      if (is_unit_scale(m[2])) {
        if (is_unit_scale(m[3])) {
          return true;
        }
      }
    }
  }
  return false;
}
bool is_unit_scale(mat3x3 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      if (is_unit_scale(m[2])) {
        return true;
      }
    }
  }
  return false;
}
bool is_unit_scale(mat2x2 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      return true;
    }
  }
  return false;
}

/** \} */

#endif /* GPU_SHADER_MATH_MATRIX_LIB_GLSL */

/////////////////////////////// Source file 22/////////////////////////////




vec3 calc_barycentric_distances(vec3 pos0, vec3 pos1, vec3 pos2)
{
  vec3 edge21 = pos2 - pos1;
  vec3 edge10 = pos1 - pos0;
  vec3 edge02 = pos0 - pos2;
  vec3 d21 = normalize(edge21);
  vec3 d10 = normalize(edge10);
  vec3 d02 = normalize(edge02);

  vec3 dists;
  float d = dot(d21, edge02);
  dists.x = sqrt(dot(edge02, edge02) - d * d);
  d = dot(d02, edge10);
  dists.y = sqrt(dot(edge10, edge10) - d * d);
  d = dot(d10, edge21);
  dists.z = sqrt(dot(edge21, edge21) - d * d);
  return dists;
}

vec2 calc_barycentric_co(int vertid)
{
  vec2 bary;
  bary.x = float((vertid % 3) == 0);
  bary.y = float((vertid % 3) == 1);
  return bary;
}

#ifdef HAIR_SHADER

/* Hairs uv and col attributes are passed by bufferTextures. */
#  define DEFINE_ATTR(type, attr) uniform samplerBuffer attr
#  define GET_ATTR(type, attr) hair_get_customdata_##type(attr)

#  define barycentric_get() hair_get_barycentric()
#  define barycentric_resolve(bary) hair_resolve_barycentric(bary)

vec3 orco_get(vec3 local_pos, mat4 modelmatinv, vec4 orco_madd[2], const samplerBuffer orco_samp)
{
  /* TODO: fix ORCO with modifiers. */
  vec3 orco = (modelmatinv * vec4(local_pos, 1.0)).xyz;
  return orco_madd[0].xyz + orco * orco_madd[1].xyz;
}

float hair_len_get(int id, const samplerBuffer len)
{
  return texelFetch(len, id).x;
}

vec4 tangent_get(const samplerBuffer attr, mat3 normalmat)
{
  /* Unsupported */
  return vec4(0.0);
}

#else /* MESH_SHADER */

#  define DEFINE_ATTR(type, attr) in type attr
#  define GET_ATTR(type, attr) attr

/* Calculated in geom shader later with calc_barycentric_co. */
#  define barycentric_get() vec2(0)
#  define barycentric_resolve(bary) bary

vec3 orco_get(vec3 local_pos, mat4 modelmatinv, vec4 orco_madd[2], vec4 orco)
{
  /* If the object does not have any deformation, the orco layer calculation is done on the fly
   * using the orco_madd factors.
   * We know when there is no orco layer when orco.w is 1.0 because it uses the generic vertex
   * attribute (which is [0,0,0,1]). */
  if (orco.w == 0.0) {
    return orco.xyz * 0.5 + 0.5;
  }
  else {
    return orco_madd[0].xyz + local_pos * orco_madd[1].xyz;
  }
}

float hair_len_get(int id, const float len)
{
  return len;
}

vec4 tangent_get(vec4 attr, mat3 normalmat)
{
  vec4 tangent;
  tangent.xyz = normalmat * attr.xyz;
  tangent.w = attr.w;
  float len_sqr = dot(tangent.xyz, tangent.xyz);
  /* Normalize only if vector is not null. */
  if (len_sqr > 0.0) {
    tangent.xyz *= inversesqrt(len_sqr);
  }
  return tangent;
}

#endif

/* Assumes GPU_VEC4 is color data. So converting to luminance like cycles. */
#define float_from_vec4(v) dot(v.rgb, vec3(0.2126, 0.7152, 0.0722))
#define float_from_vec3(v) ((v.r + v.g + v.b) * (1.0 / 3.0))
#define float_from_vec2(v) v.r

#define vec2_from_vec4(v) vec2(((v.r + v.g + v.b) * (1.0 / 3.0)), v.a)
#define vec2_from_vec3(v) vec2(((v.r + v.g + v.b) * (1.0 / 3.0)), 1.0)
#define vec2_from_float(v) vec2(v)

#define vec3_from_vec4(v) v.rgb
#define vec3_from_vec2(v) v.rrr
#define vec3_from_float(v) vec3(v)

#define vec4_from_vec3(v) vec4(v, 1.0)
#define vec4_from_vec2(v) v.rrrg
#define vec4_from_float(v) vec4(vec3(v), 1.0)

/* TODO: Move to shader_shared. */
#define RAY_TYPE_CAMERA 0
#define RAY_TYPE_SHADOW 1
#define RAY_TYPE_DIFFUSE 2
#define RAY_TYPE_GLOSSY 3

#ifdef GPU_FRAGMENT_SHADER
#  define FrontFacing gl_FrontFacing
#else
#  define FrontFacing true
#endif

/* Can't use enum here because not a header file. But would be great to do. */
#define ClosureType uint
#define CLOSURE_NONE_ID 0u
/* Diffuse */
#define CLOSURE_BSDF_DIFFUSE_ID 1u
#define CLOSURE_BSDF_OREN_NAYAR_ID 2u   /* TODO */
#define CLOSURE_BSDF_SHEEN_ID 4u        /* TODO */
#define CLOSURE_BSDF_DIFFUSE_TOON_ID 5u /* TODO */
#define CLOSURE_BSDF_TRANSLUCENT_ID 6u
/* Glossy */
#define CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID 7u
#define CLOSURE_BSDF_ASHIKHMIN_SHIRLEY_ID 8u /* TODO */
#define CLOSURE_BSDF_ASHIKHMIN_VELVET_ID 9u  /* TODO */
#define CLOSURE_BSDF_GLOSSY_TOON_ID 10u      /* TODO */
#define CLOSURE_BSDF_HAIR_REFLECTION_ID 11u  /* TODO */
/* Transmission */
#define CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID 12u
/* Glass */
#define CLOSURE_BSDF_HAIR_HUANG_ID 13u /* TODO */
/* BSSRDF */
#define CLOSURE_BSSRDF_BURLEY_ID 14u

struct ClosureUndetermined {
  vec3 color;
  float weight;
  vec3 N;
  ClosureType type;
  /* Additional data different for each closure type. */
  vec4 data;
};

ClosureUndetermined closure_new(ClosureType type)
{
  ClosureUndetermined cl;
  cl.type = type;
  return cl;
}

struct ClosureOcclusion {
  vec3 N;
};

struct ClosureDiffuse {
  float weight;
  vec3 color;
  vec3 N;
};

struct ClosureSubsurface {
  float weight;
  vec3 color;
  vec3 N;
  vec3 sss_radius;
};

struct ClosureTranslucent {
  float weight;
  vec3 color;
  vec3 N;
};

struct ClosureReflection {
  float weight;
  vec3 color;
  vec3 N;
  float roughness;
};

struct ClosureRefraction {
  float weight;
  vec3 color;
  vec3 N;
  float roughness;
  float ior;
};

struct ClosureHair {
  float weight;
  vec3 color;
  float offset;
  vec2 roughness;
  vec3 T;
};

struct ClosureVolumeScatter {
  float weight;
  vec3 scattering;
  float anisotropy;
};

struct ClosureVolumeAbsorption {
  float weight;
  vec3 absorption;
};

struct ClosureEmission {
  float weight;
  vec3 emission;
};

struct ClosureTransparency {
  float weight;
  vec3 transmittance;
  float holdout;
};

ClosureDiffuse to_closure_diffuse(ClosureUndetermined cl)
{
  ClosureDiffuse closure;
  closure.N = cl.N;
  closure.color = cl.color;
  return closure;
}

ClosureSubsurface to_closure_subsurface(ClosureUndetermined cl)
{
  ClosureSubsurface closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.sss_radius = cl.data.xyz;
  return closure;
}

ClosureTranslucent to_closure_translucent(ClosureUndetermined cl)
{
  ClosureTranslucent closure;
  closure.N = cl.N;
  closure.color = cl.color;
  return closure;
}

ClosureReflection to_closure_reflection(ClosureUndetermined cl)
{
  ClosureReflection closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.roughness = cl.data.x;
  return closure;
}

ClosureRefraction to_closure_refraction(ClosureUndetermined cl)
{
  ClosureRefraction closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.roughness = cl.data.x;
  closure.ior = cl.data.y;
  return closure;
}

struct GlobalData {
  /** World position. */
  vec3 P;
  /** Surface Normal. Normalized, overridden by bump displacement. */
  vec3 N;
  /** Raw interpolated normal (non-normalized) data. */
  vec3 Ni;
  /** Geometric Normal. */
  vec3 Ng;
  /** Curve Tangent Space. */
  vec3 curve_T, curve_B, curve_N;
  /** Barycentric coordinates. */
  vec2 barycentric_coords;
  vec3 barycentric_dists;
  /** Ray properties (approximation). */
  int ray_type;
  float ray_depth;
  float ray_length;
  /** Hair time along hair length. 0 at base 1 at tip. */
  float hair_time;
  /** Hair time along width of the hair. */
  float hair_time_width;
  /** Hair thickness in world space. */
  float hair_thickness;
  /** Index of the strand for per strand effects. */
  int hair_strand_id;
  /** Is hair. */
  bool is_strand;
};

GlobalData g_data;

#ifndef GPU_FRAGMENT_SHADER
/* Stubs. */
vec3 dF_impl(vec3 v)
{
  return vec3(0.0);
}

void dF_branch(float fn, out vec2 result)
{
  result = vec2(0.0);
}

void dF_branch_incomplete(float fn, out vec2 result)
{
  result = vec2(0.0);
}

#elif defined(GPU_FAST_DERIVATIVE) /* TODO(@fclem): User Option? */
/* Fast derivatives */
vec3 dF_impl(vec3 v)
{
  return vec3(0.0);
}

void dF_branch(float fn, out vec2 result)
{
  result.x = DFDX_SIGN * dFdx(fn);
  result.y = DFDY_SIGN * dFdy(fn);
}

#else
/* Precise derivatives */
int g_derivative_flag = 0;

vec3 dF_impl(vec3 v)
{
  if (g_derivative_flag > 0) {
    return DFDX_SIGN * dFdx(v);
  }
  else if (g_derivative_flag < 0) {
    return DFDY_SIGN * dFdy(v);
  }
  return vec3(0.0);
}

#  define dF_branch(fn, result) \
    if (true) { \
      g_derivative_flag = 1; \
      result.x = (fn); \
      g_derivative_flag = -1; \
      result.y = (fn); \
      g_derivative_flag = 0; \
      result -= vec2((fn)); \
    }

/* Used when the non-offset value is already computed elsewhere */
#  define dF_branch_incomplete(fn, result) \
    if (true) { \
      g_derivative_flag = 1; \
      result.x = (fn); \
      g_derivative_flag = -1; \
      result.y = (fn); \
      g_derivative_flag = 0; \
    }
#endif

/* TODO(fclem): Remove. */
#define CODEGEN_LIB

/////////////////////////////// Source file 23/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef COMMON_VIEW_LIB_GLSL
#define COMMON_VIEW_LIB_GLSL

#ifndef DRW_RESOURCE_CHUNK_LEN
#  error Missing draw_view additional create info on shader create info
#endif

/* Not supported anymore. TODO(fclem): Add back support. */
// #define IS_DEBUG_MOUSE_FRAGMENT (ivec2(gl_FragCoord) == drw_view.mouse_pixel)
#define IS_FIRST_INVOCATION (gl_GlobalInvocationID == uvec3(0))

#define cameraForward ViewMatrixInverse[2].xyz
#define cameraPos ViewMatrixInverse[3].xyz
vec3 cameraVec(vec3 P)
{
  return ((ProjectionMatrix[3][3] == 0.0) ? normalize(cameraPos - P) : cameraForward);
}
#define viewCameraVec(vP) ((ProjectionMatrix[3][3] == 0.0) ? normalize(-vP) : vec3(0.0, 0.0, 1.0))

#ifdef COMMON_GLOBALS_LIB
/* TODO move to overlay engine. */
float mul_project_m4_v3_zfac(vec3 co)
{
  vec3 vP = (ViewMatrix * vec4(co, 1.0)).xyz;
  return pixelFac * ((ProjectionMatrix[0][3] * vP.x) + (ProjectionMatrix[1][3] * vP.y) +
                     (ProjectionMatrix[2][3] * vP.z) + ProjectionMatrix[3][3]);
}
#endif

/* Not the right place but need to be common to all overlay's.
 * TODO: Split to an overlay lib. */
mat4 extract_matrix_packed_data(mat4 mat, out vec4 dataA, out vec4 dataB)
{
  const float div = 1.0 / 255.0;
  int a = int(mat[0][3]);
  int b = int(mat[1][3]);
  int c = int(mat[2][3]);
  int d = int(mat[3][3]);
  dataA = vec4(a & 0xFF, a >> 8, b & 0xFF, b >> 8) * div;
  dataB = vec4(c & 0xFF, c >> 8, d & 0xFF, d >> 8) * div;
  mat[0][3] = mat[1][3] = mat[2][3] = 0.0;
  mat[3][3] = 1.0;
  return mat;
}

/* Same here, Not the right place but need to be common to all overlay's.
 * TODO: Split to an overlay lib. */
/* edge_start and edge_pos needs to be in the range [0..sizeViewport]. */
vec4 pack_line_data(vec2 frag_co, vec2 edge_start, vec2 edge_pos)
{
  vec2 edge = edge_start - edge_pos;
  float len = length(edge);
  if (len > 0.0) {
    edge /= len;
    vec2 perp = vec2(-edge.y, edge.x);
    float dist = dot(perp, frag_co - edge_start);
    /* Add 0.1 to differentiate with cleared pixels. */
    return vec4(perp * 0.5 + 0.5, dist * 0.25 + 0.5 + 0.1, 1.0);
  }
  else {
    /* Default line if the origin is perfectly aligned with a pixel. */
    return vec4(1.0, 0.0, 0.5 + 0.1, 1.0);
  }
}

/* Temporary until we fully make the switch. */
#ifndef USE_GPU_SHADER_CREATE_INFO
uniform int drw_resourceChunk;
#endif /* !USE_GPU_SHADER_CREATE_INFO */

#ifdef GPU_VERTEX_SHADER

/* Temporary until we fully make the switch. */
#  ifndef USE_GPU_SHADER_CREATE_INFO

/* clang-format off */
#    if defined(IN_PLACE_INSTANCES) || defined(INSTANCED_ATTR) || defined(DRW_LEGACY_MODEL_MATRIX) || defined(GPU_DEPRECATED_AMD_DRIVER)
/* clang-format on */
/* When drawing instances of an object at the same position. */
#      define instanceId 0
#    else
#      define instanceId gl_InstanceID
#    endif

#    if defined(UNIFORM_RESOURCE_ID)
/* This is in the case we want to do a special instance drawcall for one object but still want to
 * have the right resourceId and all the correct UBO datas. */
uniform int drw_ResourceID;
#      define resource_id drw_ResourceID
#    else
#      define resource_id (gpu_BaseInstance + instanceId)
#    endif

/* Use this to declare and pass the value if
 * the fragment shader uses the resource_id. */
#    if defined(EEVEE_GENERATED_INTERFACE)
#      define RESOURCE_ID_VARYING
#      define PASS_RESOURCE_ID resourceIDFrag = resource_id;
#    elif defined(USE_GEOMETRY_SHADER)
#      define RESOURCE_ID_VARYING flat out int resourceIDGeom;
#      define PASS_RESOURCE_ID resourceIDGeom = resource_id;
#    else
#      define RESOURCE_ID_VARYING flat out int resourceIDFrag;
#      define PASS_RESOURCE_ID resourceIDFrag = resource_id;
#    endif

#  endif /* USE_GPU_SHADER_CREATE_INFO */

#endif /* GPU_VERTEX_SHADER */

/* Temporary until we fully make the switch. */
#ifdef USE_GPU_SHADER_CREATE_INFO
/* TODO(fclem): Rename PASS_RESOURCE_ID to DRW_RESOURCE_ID_VARYING_SET */
#  if defined(UNIFORM_RESOURCE_ID)
#    define resource_id drw_ResourceID
#    define PASS_RESOURCE_ID

#  elif defined(GPU_VERTEX_SHADER)
#    if defined(UNIFORM_RESOURCE_ID_NEW)
#      define resource_id (drw_ResourceID >> DRW_VIEW_SHIFT)
#    else
#      define resource_id gpu_InstanceIndex
#    endif
#    define PASS_RESOURCE_ID drw_ResourceID_iface.resource_index = resource_id;

#  elif defined(GPU_GEOMETRY_SHADER)
#    define resource_id drw_ResourceID_iface_in[0].resource_index
#    define PASS_RESOURCE_ID drw_ResourceID_iface_out.resource_index = resource_id;

#  elif defined(GPU_FRAGMENT_SHADER)
#    define resource_id drw_ResourceID_iface.resource_index
#  endif

/* TODO(fclem): Remove. */
#  define RESOURCE_ID_VARYING

#else
/* If used in a fragment / geometry shader, we pass
 * resource_id as varying. */
#  ifdef GPU_GEOMETRY_SHADER
/* TODO(fclem): Remove. This is getting ridiculous. */
#    if !defined(EEVEE_GENERATED_INTERFACE)
#      define RESOURCE_ID_VARYING \
        flat out int resourceIDFrag; \
        flat in int resourceIDGeom[];
#    else
#      define RESOURCE_ID_VARYING
#    endif

#    define resource_id resourceIDGeom
#    define PASS_RESOURCE_ID resourceIDFrag = resource_id[0];
#  endif

#  if defined(GPU_FRAGMENT_SHADER)
#    if !defined(EEVEE_GENERATED_INTERFACE)
flat in int resourceIDFrag;
#    endif
#    define resource_id resourceIDFrag
#  endif
#endif

/* Breaking this across multiple lines causes issues for some older GLSL compilers. */
/* clang-format off */
#if !defined(GPU_INTEL) && !defined(GPU_DEPRECATED_AMD_DRIVER) && (!defined(OS_MAC) || defined(GPU_METAL)) && !defined(INSTANCED_ATTR) && !defined(DRW_LEGACY_MODEL_MATRIX)
/* clang-format on */

/* Temporary until we fully make the switch. */
#  ifndef DRW_SHADER_SHARED_H

struct ObjectMatrices {
  mat4 model;
  mat4 model_inverse;
};
#  endif /* !DRW_SHADER_SHARED_H */

#  ifndef USE_GPU_SHADER_CREATE_INFO
layout(std140) uniform modelBlock
{
  ObjectMatrices drw_matrices[DRW_RESOURCE_CHUNK_LEN];
};

#    define ModelMatrix (drw_matrices[resource_id].model)
#    define ModelMatrixInverse (drw_matrices[resource_id].model_inverse)
#  endif /* USE_GPU_SHADER_CREATE_INFO */

#else /* GPU_INTEL */

/* Temporary until we fully make the switch. */
#  ifndef USE_GPU_SHADER_CREATE_INFO
/* Intel GPU seems to suffer performance impact when the model matrix is in UBO storage.
 * So for now we just force using the legacy path. */
/* Note that this is also a workaround of a problem on OSX (AMD or NVIDIA)
 * and older AMD driver on windows. */
uniform mat4 ModelMatrix;
uniform mat4 ModelMatrixInverse;
#  endif /* !USE_GPU_SHADER_CREATE_INFO */

#endif

/* Temporary until we fully make the switch. */
#ifndef USE_GPU_SHADER_CREATE_INFO
#  define resource_handle (drw_resourceChunk * DRW_RESOURCE_CHUNK_LEN + resource_id)
#endif

/** Transform shortcuts. */
/* Rule of thumb: Try to reuse world positions and normals because converting through view-space
 * will always be decomposed in at least 2 matrix operation. */

/**
 * Some clarification:
 * Usually Normal matrix is transpose(inverse(ViewMatrix * ModelMatrix))
 *
 * But since it is slow to multiply matrices we decompose it. Decomposing
 * inversion and transposition both invert the product order leaving us with
 * the same original order:
 * transpose(ViewMatrixInverse) * transpose(ModelMatrixInverse)
 *
 * Knowing that the view matrix is orthogonal, the transpose is also the inverse.
 * NOTE: This is only valid because we are only using the mat3 of the ViewMatrixInverse.
 * ViewMatrix * transpose(ModelMatrixInverse)
 */
#define NormalMatrix transpose(mat3(ModelMatrixInverse))
#define NormalMatrixInverse transpose(mat3(ModelMatrix))

#define normal_object_to_view(n) (mat3(ViewMatrix) * (NormalMatrix * n))
#define normal_object_to_world(n) (NormalMatrix * n)
#define normal_world_to_object(n) (NormalMatrixInverse * n)
#define normal_world_to_view(n) (mat3(ViewMatrix) * n)
#define normal_view_to_world(n) (mat3(ViewMatrixInverse) * n)

#define point_object_to_ndc(p) \
  (ProjectionMatrix * (ViewMatrix * vec4((ModelMatrix * vec4(p, 1.0)).xyz, 1.0)))
#define point_object_to_view(p) ((ViewMatrix * vec4((ModelMatrix * vec4(p, 1.0)).xyz, 1.0)).xyz)
#define point_object_to_world(p) ((ModelMatrix * vec4(p, 1.0)).xyz)
#define point_view_to_object(p) ((ModelMatrixInverse * (ViewMatrixInverse * vec4(p, 1.0))).xyz)
#define point_world_to_object(p) ((ModelMatrixInverse * vec4(p, 1.0)).xyz)

vec4 point_view_to_ndc(vec3 p)
{
  return ProjectionMatrix * vec4(p, 1.0);
}

vec3 point_view_to_world(vec3 p)
{
  return (ViewMatrixInverse * vec4(p, 1.0)).xyz;
}

vec4 point_world_to_ndc(vec3 p)
{
  return ProjectionMatrix * (ViewMatrix * vec4(p, 1.0));
}

vec3 point_world_to_view(vec3 p)
{
  return (ViewMatrix * vec4(p, 1.0)).xyz;
}

/* View-space Z is used to adjust for perspective projection.
 * Homogenous W is used to convert from NDC to homogenous space.
 * Offset is in view-space, so positive values are closer to the camera. */
float get_homogenous_z_offset(float vs_z, float hs_w, float vs_offset)
{
  if (vs_offset == 0.0) {
    /* Don't calculate homogenous offset if view-space offset is zero. */
    return 0.0;
  }
  else if (ProjectionMatrix[3][3] == 0.0) {
    /* Clamp offset to half of Z to avoid floating point precision errors. */
    vs_offset = min(vs_offset, vs_z * -0.5);
    /* From "Projection Matrix Tricks" by Eric Lengyel:
     * http://www.terathon.com/gdc07_lengyel.pdf (p. 24 Depth Modification) */
    return ProjectionMatrix[3][2] * (vs_offset / (vs_z * (vs_z + vs_offset))) * hs_w;
  }
  else {
    return ProjectionMatrix[2][2] * vs_offset * hs_w;
  }
}

/* Due to some shader compiler bug, we somewhat need to access gl_VertexID
 * to make vertex shaders work. even if it's actually dead code. */
#if defined(GPU_INTEL) && defined(GPU_OPENGL)
#  define GPU_INTEL_VERTEX_SHADER_WORKAROUND gl_Position.x = float(gl_VertexID);
#else
#  define GPU_INTEL_VERTEX_SHADER_WORKAROUND
#endif

#define DRW_BASE_SELECTED (1 << 1)
#define DRW_BASE_FROM_DUPLI (1 << 2)
#define DRW_BASE_FROM_SET (1 << 3)
#define DRW_BASE_ACTIVE (1 << 4)

/* Wire Color Types, matching eV3DShadingColorType. */
#define V3D_SHADING_SINGLE_COLOR 2
#define V3D_SHADING_OBJECT_COLOR 4
#define V3D_SHADING_RANDOM_COLOR 1

/* ---- Opengl Depth conversion ---- */

float linear_depth(bool is_persp, float z, float zf, float zn)
{
  if (is_persp) {
    return (zn * zf) / (z * (zn - zf) + zf);
  }
  else {
    return (z * 2.0 - 1.0) * zf;
  }
}

float buffer_depth(bool is_persp, float z, float zf, float zn)
{
  if (is_persp) {
    return (zf * (zn - z)) / (z * (zn - zf));
  }
  else {
    return (z / (zf * 2.0)) + 0.5;
  }
}

float get_view_z_from_depth(float depth)
{
  float d = 2.0 * depth - 1.0;
  if (ProjectionMatrix[3][3] == 0.0) {
    d = -ProjectionMatrix[3][2] / (d + ProjectionMatrix[2][2]);
  }
  else {
    d = (d - ProjectionMatrix[3][2]) / ProjectionMatrix[2][2];
  }
  return d;
}

float get_depth_from_view_z(float z)
{
  float d;
  if (ProjectionMatrix[3][3] == 0.0) {
    d = (-ProjectionMatrix[3][2] / z) - ProjectionMatrix[2][2];
  }
  else {
    d = ProjectionMatrix[2][2] * z + ProjectionMatrix[3][2];
  }
  return d * 0.5 + 0.5;
}

vec2 get_uvs_from_view(vec3 view)
{
  vec4 ndc = ProjectionMatrix * vec4(view, 1.0);
  return (ndc.xy / ndc.w) * 0.5 + 0.5;
}

vec3 get_view_space_from_depth(vec2 uvcoords, float depth)
{
  vec3 ndc = vec3(uvcoords, depth) * 2.0 - 1.0;
  vec4 p = ProjectionMatrixInverse * vec4(ndc, 1.0);
  return p.xyz / p.w;
}

vec3 get_world_space_from_depth(vec2 uvcoords, float depth)
{
  return (ViewMatrixInverse * vec4(get_view_space_from_depth(uvcoords, depth), 1.0)).xyz;
}

vec3 get_view_vector_from_screen_uv(vec2 uvcoords)
{
  if (ProjectionMatrix[3][3] == 0.0) {
    vec2 ndc = vec2(uvcoords * 2.0 - 1.0);
    /* This is the manual inversion of the ProjectionMatrix. */
    vec3 vV = vec3((-ndc - ProjectionMatrix[2].xy) /
                       vec2(ProjectionMatrix[0][0], ProjectionMatrix[1][1]),
                   -ProjectionMatrix[2][2] - ProjectionMatrix[3][2]);
    return normalize(vV);
  }
  /* Orthographic case. */
  return vec3(0.0, 0.0, 1.0);
}

#endif /* COMMON_VIEW_LIB_GLSL */

/////////////////////////////// Source file 24/////////////////////////////




/* NOTE: To be used with UNIFORM_RESOURCE_ID and INSTANCED_ATTR as define. */
#pragma BLENDER_REQUIRE(common_view_lib.glsl)
#ifdef POINTCLOUD_SHADER
#  define COMMON_POINTCLOUD_LIB

#  ifndef DRW_POINTCLOUD_INFO
#    error Ensure createInfo includes draw_pointcloud.
#  endif

int pointcloud_get_point_id()
{
#  ifdef GPU_VERTEX_SHADER
  return gl_VertexID / 32;
#  endif
  return 0;
}

mat3 pointcloud_get_facing_matrix(vec3 p)
{
  mat3 facing_mat;
  facing_mat[2] = cameraVec(p);
  facing_mat[1] = normalize(cross(ViewMatrixInverse[0].xyz, facing_mat[2]));
  facing_mat[0] = cross(facing_mat[1], facing_mat[2]);
  return facing_mat;
}

/* Returns world center position and radius. */
void pointcloud_get_pos_and_radius(out vec3 outpos, out float outradius)
{
  int id = pointcloud_get_point_id();
  vec4 pos_rad = texelFetch(ptcloud_pos_rad_tx, id);
  outpos = point_object_to_world(pos_rad.xyz);
  outradius = dot(abs(mat3(ModelMatrix) * pos_rad.www), vec3(1.0 / 3.0));
}

/* Return world position and normal. */
void pointcloud_get_pos_nor_radius(out vec3 outpos, out vec3 outnor, out float outradius)
{
  vec3 p;
  float radius;
  pointcloud_get_pos_and_radius(p, radius);

  mat3 facing_mat = pointcloud_get_facing_matrix(p);

  int vert_id = 0;
#  ifdef GPU_VERTEX_SHADER
  /* NOTE: Avoid modulo by non-power-of-two in shader. See Index buffer setup. */
  vert_id = gl_VertexID % 32;
#  endif

  vec3 pos_inst = vec3(0.0);

  switch (vert_id) {
    case 0:
      pos_inst.z = 1.0;
      break;
    case 1:
      pos_inst.x = 1.0;
      break;
    case 2:
      pos_inst.y = 1.0;
      break;
    case 3:
      pos_inst.x = -1.0;
      break;
    case 4:
      pos_inst.y = -1.0;
      break;
  }

  /* TODO(fclem): remove multiplication here. Here only for keeping the size correct for now. */
  radius *= 0.01;
  outnor = facing_mat * pos_inst;
  outpos = p + outnor * radius;
  outradius = radius;
}

/* Return world position and normal. */
void pointcloud_get_pos_and_nor(out vec3 outpos, out vec3 outnor)
{
  vec3 nor, pos;
  float radius;
  pointcloud_get_pos_nor_radius(pos, nor, radius);
  outpos = pos;
  outnor = nor;
}

vec3 pointcloud_get_pos()
{
  vec3 outpos, outnor;
  pointcloud_get_pos_and_nor(outpos, outnor);
  return outpos;
}

float pointcloud_get_customdata_float(const samplerBuffer cd_buf)
{
  int id = pointcloud_get_point_id();
  return texelFetch(cd_buf, id).r;
}

vec2 pointcloud_get_customdata_vec2(const samplerBuffer cd_buf)
{
  int id = pointcloud_get_point_id();
  return texelFetch(cd_buf, id).rg;
}

vec3 pointcloud_get_customdata_vec3(const samplerBuffer cd_buf)
{
  int id = pointcloud_get_point_id();
  return texelFetch(cd_buf, id).rgb;
}

vec4 pointcloud_get_customdata_vec4(const samplerBuffer cd_buf)
{
  int id = pointcloud_get_point_id();
  return texelFetch(cd_buf, id).rgba;
}

vec2 pointcloud_get_barycentric(void)
{
  /* TODO: To be implemented. */
  return vec2(0.0);
}
#endif

/////////////////////////////// Source file 25/////////////////////////////




/**
 * Library to create hairs dynamically from control points.
 * This is less bandwidth intensive than fetching the vertex attributes
 * but does more ALU work per vertex. This also reduces the amount
 * of data the CPU has to precompute and transfer for each update.
 */

/* Avoid including hair functionality for shaders and materials which do not require hair.
 * Required to prevent compilation failure for missing shader inputs and uniforms when hair library
 * is included via other libraries. These are only specified in the ShaderCreateInfo when needed.
 */
#ifdef HAIR_SHADER
#  define COMMON_HAIR_LIB

/* TODO(fclem): Keep documentation but remove the uniform declaration. */
#  ifndef USE_GPU_SHADER_CREATE_INFO

/**
 * hairStrandsRes: Number of points per hair strand.
 * 2 - no subdivision
 * 3+ - 1 or more interpolated points per hair.
 */
uniform int hairStrandsRes = 8;

/**
 * hairThicknessRes : Subdivide around the hair.
 * 1 - Wire Hair: Only one pixel thick, independent of view distance.
 * 2 - Poly-strip Hair: Correct width, flat if camera is parallel.
 * 3+ - Cylinder Hair: Massive calculation but potentially perfect. Still need proper support.
 */
uniform int hairThicknessRes = 1;

/* Hair thickness shape. */
uniform float hairRadRoot = 0.01;
uniform float hairRadTip = 0.0;
uniform float hairRadShape = 0.5;
uniform bool hairCloseTip = true;

uniform mat4 hairDupliMatrix;

/* Strand batch offset when used in compute shaders. */
uniform int hairStrandOffset = 0;

/* -- Per control points -- */
uniform samplerBuffer hairPointBuffer; /* RGBA32F */

/* -- Per strands data -- */
uniform usamplerBuffer hairStrandBuffer;    /* R32UI */
uniform usamplerBuffer hairStrandSegBuffer; /* R16UI */

/* Not used, use one buffer per uv layer */
// uniform samplerBuffer hairUVBuffer; /* RG32F */
// uniform samplerBuffer hairColBuffer; /* RGBA16 linear color */
#  else
#    ifndef DRW_HAIR_INFO
#      error Ensure createInfo includes draw_hair for general use or eevee_legacy_hair_lib for EEVEE.
#    endif
#  endif /* !USE_GPU_SHADER_CREATE_INFO */

#  define point_position xyz
#  define point_time w /* Position along the hair length */

/* -- Subdivision stage -- */
/**
 * We use a transform feedback or compute shader to preprocess the strands and add more subdivision
 * to it. For the moment these are simple smooth interpolation but one could hope to see the full
 * children particle modifiers being evaluated at this stage.
 *
 * If no more subdivision is needed, we can skip this step.
 */

#  ifdef GPU_VERTEX_SHADER
float hair_get_local_time()
{
  return float(gl_VertexID % hairStrandsRes) / float(hairStrandsRes - 1);
}

int hair_get_id()
{
  return gl_VertexID / hairStrandsRes;
}
#  endif

#  ifdef GPU_COMPUTE_SHADER
float hair_get_local_time()
{
  return float(gl_GlobalInvocationID.y) / float(hairStrandsRes - 1);
}

int hair_get_id()
{
  return int(gl_GlobalInvocationID.x) + hairStrandOffset;
}
#  endif

#  ifdef HAIR_PHASE_SUBDIV
int hair_get_base_id(float local_time, int strand_segments, out float interp_time)
{
  float time_per_strand_seg = 1.0 / float(strand_segments);

  float ratio = local_time / time_per_strand_seg;
  interp_time = fract(ratio);

  return int(ratio);
}

void hair_get_interp_attrs(
    out vec4 data0, out vec4 data1, out vec4 data2, out vec4 data3, out float interp_time)
{
  float local_time = hair_get_local_time();

  int hair_id = hair_get_id();
  int strand_offset = int(texelFetch(hairStrandBuffer, hair_id).x);
  int strand_segments = int(texelFetch(hairStrandSegBuffer, hair_id).x);

  int id = hair_get_base_id(local_time, strand_segments, interp_time);

  int ofs_id = id + strand_offset;

  data0 = texelFetch(hairPointBuffer, ofs_id - 1);
  data1 = texelFetch(hairPointBuffer, ofs_id);
  data2 = texelFetch(hairPointBuffer, ofs_id + 1);
  data3 = texelFetch(hairPointBuffer, ofs_id + 2);

  if (id <= 0) {
    /* root points. Need to reconstruct previous data. */
    data0 = data1 * 2.0 - data2;
  }
  if (id + 1 >= strand_segments) {
    /* tip points. Need to reconstruct next data. */
    data3 = data2 * 2.0 - data1;
  }
}
#  endif

/* -- Drawing stage -- */
/**
 * For final drawing, the vertex index and the number of vertex per segment
 */

#  if !defined(HAIR_PHASE_SUBDIV) && defined(GPU_VERTEX_SHADER)

int hair_get_strand_id(void)
{
  return gl_VertexID / (hairStrandsRes * hairThicknessRes);
}

int hair_get_base_id(void)
{
  return gl_VertexID / hairThicknessRes;
}

/* Copied from cycles. */
float hair_shaperadius(float shape, float root, float tip, float time)
{
  float radius = 1.0 - time;

  if (shape < 0.0) {
    radius = pow(radius, 1.0 + shape);
  }
  else {
    radius = pow(radius, 1.0 / (1.0 - shape));
  }

  if (hairCloseTip && (time > 0.99)) {
    return 0.0;
  }

  return (radius * (root - tip)) + tip;
}

#    if defined(OS_MAC) && defined(GPU_OPENGL)
in float dummy;
#    endif

void hair_get_center_pos_tan_binor_time(bool is_persp,
                                        mat4 invmodel_mat,
                                        vec3 camera_pos,
                                        vec3 camera_z,
                                        out vec3 wpos,
                                        out vec3 wtan,
                                        out vec3 wbinor,
                                        out float time,
                                        out float thickness)
{
  int id = hair_get_base_id();
  vec4 data = texelFetch(hairPointBuffer, id);
  wpos = data.point_position;
  time = data.point_time;

#    if defined(OS_MAC) && defined(GPU_OPENGL)
  /* Generate a dummy read to avoid the driver bug with shaders having no
   * vertex reads on macOS (#60171) */
  wpos.y += dummy * 0.0;
#    endif

  if (time == 0.0) {
    /* Hair root */
    wtan = texelFetch(hairPointBuffer, id + 1).point_position - wpos;
  }
  else {
    wtan = wpos - texelFetch(hairPointBuffer, id - 1).point_position;
  }

  mat4 obmat = hairDupliMatrix;
  wpos = (obmat * vec4(wpos, 1.0)).xyz;
  wtan = -normalize(mat3(obmat) * wtan);

  vec3 camera_vec = (is_persp) ? camera_pos - wpos : camera_z;
  wbinor = normalize(cross(camera_vec, wtan));

  thickness = hair_shaperadius(hairRadShape, hairRadRoot, hairRadTip, time);
}

void hair_get_pos_tan_binor_time(bool is_persp,
                                 mat4 invmodel_mat,
                                 vec3 camera_pos,
                                 vec3 camera_z,
                                 out vec3 wpos,
                                 out vec3 wtan,
                                 out vec3 wbinor,
                                 out float time,
                                 out float thickness,
                                 out float thick_time)
{
  hair_get_center_pos_tan_binor_time(
      is_persp, invmodel_mat, camera_pos, camera_z, wpos, wtan, wbinor, time, thickness);
  if (hairThicknessRes > 1) {
    thick_time = float(gl_VertexID % hairThicknessRes) / float(hairThicknessRes - 1);
    thick_time = thickness * (thick_time * 2.0 - 1.0);
    /* Take object scale into account.
     * NOTE: This only works fine with uniform scaling. */
    float scale = 1.0 / length(mat3(invmodel_mat) * wbinor);
    wpos += wbinor * thick_time * scale;
  }
  else {
    /* NOTE: Ensures 'hairThickTime' is initialized -
     * avoids undefined behavior on certain macOS configurations. */
    thick_time = 0.0;
  }
}

float hair_get_customdata_float(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).r;
}

vec2 hair_get_customdata_vec2(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rg;
}

vec3 hair_get_customdata_vec3(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rgb;
}

vec4 hair_get_customdata_vec4(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rgba;
}

vec3 hair_get_strand_pos(void)
{
  int id = hair_get_strand_id() * hairStrandsRes;
  return texelFetch(hairPointBuffer, id).point_position;
}

vec2 hair_get_barycentric(void)
{
  /* To match cycles without breaking into individual segment we encode if we need to invert
   * the first component into the second component. We invert if the barycentricTexCo.y
   * is NOT 0.0 or 1.0. */
  int id = hair_get_base_id();
  return vec2(float((id % 2) == 1), float(((id % 4) % 3) > 0));
}

#  endif

/* To be fed the result of hair_get_barycentric from vertex shader. */
vec2 hair_resolve_barycentric(vec2 vert_barycentric)
{
  if (fract(vert_barycentric.y) != 0.0) {
    return vec2(vert_barycentric.x, 0.0);
  }
  else {
    return vec2(1.0 - vert_barycentric.x, 0.0);
  }
}

/* Hair interpolation functions. */
vec4 hair_get_weights_cardinal(float t)
{
  float t2 = t * t;
  float t3 = t2 * t;
#  if defined(CARDINAL)
  float fc = 0.71;
#  else /* defined(CATMULL_ROM) */
  float fc = 0.5;
#  endif

  vec4 weights;
  /* GLSL Optimized version of key_curve_position_weights() */
  float fct = t * fc;
  float fct2 = t2 * fc;
  float fct3 = t3 * fc;
  weights.x = (fct2 * 2.0 - fct3) - fct;
  weights.y = (t3 * 2.0 - fct3) + (-t2 * 3.0 + fct2) + 1.0;
  weights.z = (-t3 * 2.0 + fct3) + (t2 * 3.0 - (2.0 * fct2)) + fct;
  weights.w = fct3 - fct2;
  return weights;
}

/* TODO(fclem): This one is buggy, find why. (it's not the optimization!!) */
vec4 hair_get_weights_bspline(float t)
{
  float t2 = t * t;
  float t3 = t2 * t;

  vec4 weights;
  /* GLSL Optimized version of key_curve_position_weights() */
  weights.xz = vec2(-0.16666666, -0.5) * t3 + (0.5 * t2 + 0.5 * vec2(-t, t) + 0.16666666);
  weights.y = (0.5 * t3 - t2 + 0.66666666);
  weights.w = (0.16666666 * t3);
  return weights;
}

vec4 hair_interp_data(vec4 v0, vec4 v1, vec4 v2, vec4 v3, vec4 w)
{
  return v0 * w.x + v1 * w.y + v2 * w.z + v3 * w.w;
}
#endif

/////////////////////////////// Source file 26/////////////////////////////




#pragma BLENDER_REQUIRE(draw_model_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
/* MAT_GEOM_POINT_CLOUD */
#pragma BLENDER_REQUIRE(common_pointcloud_lib.glsl)
/* MAT_GEOM_CURVES */
#pragma BLENDER_REQUIRE(common_hair_lib.glsl) /* TODO rename to curve. */

#define EEVEE_ATTRIBUTE_LIB

/* All attributes are loaded in order. This allow us to use a global counter to retrieve the
 * correct grid xform. */
/* TODO(fclem): This is very dangerous as it requires a reset for each time `attrib_load` is
 * called. Instead, the right attribute index should be passed to attr_load_* functions. */
int g_attr_id = 0;

/* Point clouds and curves are not compatible with volume grids.
 * They will fallback to their own attributes loading. */
#if defined(MAT_VOLUME) && !defined(MAT_GEOM_CURVES) && !defined(MAT_GEOM_POINT_CLOUD)
#  if defined(OBINFO_LIB) && !defined(MAT_GEOM_WORLD)
#    define GRID_ATTRIBUTES
#  endif

/* -------------------------------------------------------------------- */
/** \name Volume
 *
 * Volume objects loads attributes from "grids" in the form of 3D textures.
 * Per grid transform order is following loading order.
 * \{ */

#  ifdef GRID_ATTRIBUTES
vec3 g_lP = vec3(0.0);
#  else
vec3 g_wP = vec3(0.0);
#  endif

vec3 grid_coordinates()
{
#  ifdef GRID_ATTRIBUTES
  vec3 co = (drw_volume.grids_xform[g_attr_id] * vec4(g_lP, 1.0)).xyz;
#  else
  /* Only for test shaders. All the runtime shaders require `draw_object_infos` and
   * `draw_volume_infos`. */
  vec3 co = vec3(0.0);
#  endif
  g_attr_id += 1;
  return co;
}

vec3 attr_load_orco(sampler3D tex)
{
  g_attr_id += 1;
#  ifdef GRID_ATTRIBUTES
  return OrcoTexCoFactors[0].xyz + g_lP * OrcoTexCoFactors[1].xyz;
#  else
  return g_wP;
#  endif
}
vec4 attr_load_tangent(sampler3D tex)
{
  g_attr_id += 1;
  return vec4(0);
}
vec4 attr_load_vec4(sampler3D tex)
{
  return texture(tex, grid_coordinates());
}
vec3 attr_load_vec3(sampler3D tex)
{
  return texture(tex, grid_coordinates()).rgb;
}
vec2 attr_load_vec2(sampler3D tex)
{
  return texture(tex, grid_coordinates()).rg;
}
float attr_load_float(sampler3D tex)
{
  return texture(tex, grid_coordinates()).r;
}
vec4 attr_load_color(sampler3D tex)
{
  return texture(tex, grid_coordinates());
}
vec3 attr_load_uv(sampler3D attr)
{
  g_attr_id += 1;
  return vec3(0);
}

/** \} */

#elif defined(MAT_GEOM_MESH)

/* -------------------------------------------------------------------- */
/** \name Mesh
 *
 * Mesh objects attributes are loaded using vertex input attributes.
 * \{ */

#  ifdef OBINFO_LIB
vec3 attr_load_orco(vec4 orco)
{
#    ifdef GPU_VERTEX_SHADER
  /* We know when there is no orco layer when orco.w is 1.0 because it uses the generic vertex
   * attribute (which is [0,0,0,1]). */
  if (orco.w == 1.0) {
    /* If the object does not have any deformation, the orco layer calculation is done on the fly
     * using the orco_madd factors. */
    return OrcoTexCoFactors[0].xyz + pos * OrcoTexCoFactors[1].xyz;
  }
#    endif
  return orco.xyz * 0.5 + 0.5;
}
#  endif
vec4 attr_load_tangent(vec4 tangent)
{
  tangent.xyz = safe_normalize(drw_normal_object_to_world(tangent.xyz));
  return tangent;
}
vec4 attr_load_vec4(vec4 attr)
{
  return attr;
}
vec3 attr_load_vec3(vec3 attr)
{
  return attr;
}
vec2 attr_load_vec2(vec2 attr)
{
  return attr;
}
float attr_load_float(float attr)
{
  return attr;
}
vec4 attr_load_color(vec4 attr)
{
  return attr;
}
vec3 attr_load_uv(vec3 attr)
{
  return attr;
}

/** \} */

#elif defined(MAT_GEOM_POINT_CLOUD)

/* -------------------------------------------------------------------- */
/** \name Point Cloud
 *
 * Point Cloud objects loads attributes from buffers through sampler buffers.
 * \{ */

#  ifdef OBINFO_LIB
vec3 attr_load_orco(vec4 orco)
{
  vec3 P = pointcloud_get_pos();
  vec3 lP = transform_point(ModelMatrixInverse, P);
  return OrcoTexCoFactors[0].xyz + lP * OrcoTexCoFactors[1].xyz;
}
#  endif

vec4 attr_load_tangent(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec4(cd_buf);
}
vec3 attr_load_uv(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec3(cd_buf);
}
vec4 attr_load_color(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec4(cd_buf);
}
vec4 attr_load_vec4(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec4(cd_buf);
}
vec3 attr_load_vec3(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec3(cd_buf);
}
vec2 attr_load_vec2(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_vec2(cd_buf);
}
float attr_load_float(samplerBuffer cd_buf)
{
  return pointcloud_get_customdata_float(cd_buf);
}

/** \} */

#elif defined(MAT_GEOM_GPENCIL)

/* -------------------------------------------------------------------- */
/** \name Grease Pencil
 *
 * Grease Pencil objects have one uv and one color attribute layer.
 * \{ */

/* Globals to feed the load functions. */
vec2 g_uvs;
vec4 g_color;

#  ifdef OBINFO_LIB
vec3 attr_load_orco(vec4 orco)
{
  vec3 lP = point_world_to_object(interp.P);
  return OrcoTexCoFactors[0].xyz + lP * OrcoTexCoFactors[1].xyz;
}
#  endif
vec4 attr_load_tangent(vec4 tangent)
{
  return vec4(0.0, 0.0, 0.0, 1.0);
}
vec3 attr_load_uv(vec3 dummy)
{
  return vec3(g_uvs, 0.0);
}
vec4 attr_load_color(vec4 dummy)
{
  return g_color;
}
vec4 attr_load_vec4(vec4 attr)
{
  return vec4(0.0);
}
vec3 attr_load_vec3(vec3 attr)
{
  return vec3(0.0);
}
vec2 attr_load_vec2(vec2 attr)
{
  return vec2(0.0);
}
float attr_load_float(float attr)
{
  return 0.0;
}

/** \} */

#elif defined(MAT_GEOM_CURVES) && defined(GPU_VERTEX_SHADER)

/* -------------------------------------------------------------------- */
/** \name Curve
 *
 * Curve objects loads attributes from buffers through sampler buffers.
 * Per attribute scope follows loading order.
 * \{ */

#  ifdef OBINFO_LIB
vec3 attr_load_orco(vec4 orco)
{
  vec3 P = hair_get_strand_pos();
  vec3 lP = transform_point(ModelMatrixInverse, P);
  return OrcoTexCoFactors[0].xyz + lP * OrcoTexCoFactors[1].xyz;
}
#  endif

int g_curves_attr_id = 0;

/* Return the index to use for looking up the attribute value in the sampler
 * based on the attribute scope (point or spline). */
int curves_attribute_element_id()
{
  int id = curve_interp_flat.strand_id;
  if (drw_curves.is_point_attribute[g_curves_attr_id][0] != 0u) {
#  ifdef COMMON_HAIR_LIB
    id = hair_get_base_id();
#  endif
  }

  g_curves_attr_id += 1;
  return id;
}

vec4 attr_load_tangent(samplerBuffer cd_buf)
{
  /* Not supported for the moment. */
  return vec4(0.0, 0.0, 0.0, 1.0);
}
vec3 attr_load_uv(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curve_interp_flat.strand_id).rgb;
}
vec4 attr_load_color(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curve_interp_flat.strand_id).rgba;
}
vec4 attr_load_vec4(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curves_attribute_element_id()).rgba;
}
vec3 attr_load_vec3(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curves_attribute_element_id()).rgb;
}
vec2 attr_load_vec2(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curves_attribute_element_id()).rg;
}
float attr_load_float(samplerBuffer cd_buf)
{
  return texelFetch(cd_buf, curves_attribute_element_id()).r;
}

/** \} */

#elif defined(MAT_GEOM_WORLD)

/* -------------------------------------------------------------------- */
/** \name World
 *
 * World has no attributes other than orco.
 * \{ */

vec3 attr_load_orco(vec4 orco)
{
  return -g_data.N;
}
vec4 attr_load_tangent(vec4 tangent)
{
  return vec4(0);
}
vec4 attr_load_vec4(vec4 attr)
{
  return vec4(0);
}
vec3 attr_load_vec3(vec3 attr)
{
  return vec3(0);
}
vec2 attr_load_vec2(vec2 attr)
{
  return vec2(0);
}
float attr_load_float(float attr)
{
  return 0.0;
}
vec4 attr_load_color(vec4 attr)
{
  return vec4(0);
}
vec3 attr_load_uv(vec3 attr)
{
  return vec3(0);
}

/** \} */

#endif

/////////////////////////////// Source file 27/////////////////////////////




void output_renderpass_color(int id, vec4 color)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  if (id >= 0) {
    ivec2 texel = ivec2(gl_FragCoord.xy);
    imageStore(rp_color_img, ivec3(texel, id), color);
  }
#endif
}

void output_renderpass_value(int id, float value)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  if (id >= 0) {
    ivec2 texel = ivec2(gl_FragCoord.xy);
    imageStore(rp_value_img, ivec3(texel, id), vec4(value));
  }
#endif
}

void clear_aovs()
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.color_len; i++) {
    output_renderpass_color(uniform_buf.render_pass.color_len + i, vec4(0));
  }
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.value_len; i++) {
    output_renderpass_value(uniform_buf.render_pass.value_len + i, 0.0);
  }
#endif
}

void output_aov(vec4 color, float value, uint hash)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.color_len; i++) {
    if (uniform_buf.render_pass.aovs.hash_color[i].x == hash) {
      imageStore(rp_color_img,
                 ivec3(ivec2(gl_FragCoord.xy), uniform_buf.render_pass.color_len + i),
                 color);
      return;
    }
  }
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.value_len; i++) {
    if (uniform_buf.render_pass.aovs.hash_value[i].x == hash) {
      imageStore(rp_value_img,
                 ivec3(ivec2(gl_FragCoord.xy), uniform_buf.render_pass.value_len + i),
                 vec4(value));
      return;
    }
  }
#endif
}

/////////////////////////////// Source file 28/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_renderpass_lib.glsl)

#define filmScalingFactor float(uniform_buf.film.scaling_factor)

vec3 g_emission;
vec3 g_transmittance;
float g_holdout;

vec3 g_volume_scattering;
float g_volume_anisotropy;
vec3 g_volume_absorption;

/* The Closure type is never used. Use float as dummy type. */
#define Closure float
#define CLOSURE_DEFAULT 0.0

/* Maximum number of picked closure. */
#ifndef CLOSURE_BIN_COUNT
#  define CLOSURE_BIN_COUNT 1
#endif
/* Sampled closure parameters. */
ClosureUndetermined g_closure_bins[CLOSURE_BIN_COUNT];
/* Random number per sampled closure type. */
float g_closure_rand[CLOSURE_BIN_COUNT];

ClosureUndetermined g_closure_get(int i)
{
  switch (i) {
    default:
    case 0:
      return g_closure_bins[0];
#if CLOSURE_BIN_COUNT > 1
    case 1:
      return g_closure_bins[1];
#endif
#if CLOSURE_BIN_COUNT > 2
    case 2:
      return g_closure_bins[2];
#endif
  }
}

ClosureUndetermined g_closure_get_resolved(int i, float weight_fac)
{
  ClosureUndetermined cl = g_closure_get(i);
  cl.color *= cl.weight * weight_fac;
  return cl;
}

ClosureType closure_type_get(ClosureDiffuse cl)
{
  return CLOSURE_BSDF_DIFFUSE_ID;
}

ClosureType closure_type_get(ClosureTranslucent cl)
{
  return CLOSURE_BSDF_TRANSLUCENT_ID;
}

ClosureType closure_type_get(ClosureReflection cl)
{
  return CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID;
}

ClosureType closure_type_get(ClosureRefraction cl)
{
  return CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID;
}

ClosureType closure_type_get(ClosureSubsurface cl)
{
  return CLOSURE_BSSRDF_BURLEY_ID;
}

/**
 * Returns true if the closure is to be selected based on the input weight.
 */
bool closure_select_check(float weight, inout float total_weight, inout float r)
{
  if (weight < 1e-5) {
    return false;
  }
  total_weight += weight;
  float x = weight / total_weight;
  bool chosen = (r < x);
  /* Assuming that if r is in the interval [0,x] or [x,1], it's still uniformly distributed within
   * that interval, so remapping to [0,1] again to explore this space of probability. */
  r = (chosen) ? (r / x) : ((r - x) / (1.0 - x));
  return chosen;
}

/**
 * Assign `candidate` to `destination` based on a random value and the respective weights.
 */
void closure_select(inout ClosureUndetermined destination,
                    inout float random,
                    ClosureUndetermined candidate)
{
  if (closure_select_check(candidate.weight, destination.weight, random)) {
    float tmp = destination.weight;
    destination = candidate;
    destination.weight = tmp;
  }
}

void closure_weights_reset(float closure_rand)
{
  g_closure_rand[0] = closure_rand;
  g_closure_bins[0].weight = 0.0;
#if CLOSURE_BIN_COUNT > 1
  g_closure_rand[1] = closure_rand;
  g_closure_bins[1].weight = 0.0;
#endif
#if CLOSURE_BIN_COUNT > 2
  g_closure_rand[2] = closure_rand;
  g_closure_bins[2].weight = 0.0;
#endif

  g_volume_scattering = vec3(0.0);
  g_volume_anisotropy = 0.0;
  g_volume_absorption = vec3(0.0);

  g_emission = vec3(0.0);
  g_transmittance = vec3(0.0);
  g_volume_scattering = vec3(0.0);
  g_volume_absorption = vec3(0.0);
  g_holdout = 0.0;
}

#define closure_base_copy(cl, in_cl) \
  cl.weight = in_cl.weight; \
  cl.color = in_cl.color; \
  cl.N = in_cl.N; \
  cl.type = closure_type_get(in_cl);

/* Single BSDFs. */
Closure closure_eval(ClosureDiffuse diffuse)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, diffuse);
#if (CLOSURE_BIN_COUNT > 1) && defined(MAT_TRANSLUCENT) && !defined(MAT_CLEARCOAT)
  /* Use second slot so we can have diffuse + translucent without noise. */
  closure_select(g_closure_bins[1], g_closure_rand[1], cl);
#else
  /* Either is single closure or use same bin as transmission bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
#endif
  return Closure(0);
}

Closure closure_eval(ClosureSubsurface diffuse)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, diffuse);
  cl.data.rgb = diffuse.sss_radius;
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

Closure closure_eval(ClosureTranslucent translucent)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, translucent);
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

/* Alternate between two bins on a per closure basis.
 * Allow clearcoat layer without noise.
 * Choosing the bin with the least weight can choose a
 * different bin for the same closure and
 * produce issue with ray-tracing denoiser.
 * Always start with the second bin, this one doesn't
 * overlap with other closure. */
bool g_closure_reflection_bin = true;
#define CHOOSE_MIN_WEIGHT_CLOSURE_BIN(a, b) \
  if (g_closure_reflection_bin) { \
    closure_select(g_closure_bins[b], g_closure_rand[b], cl); \
  } \
  else { \
    closure_select(g_closure_bins[a], g_closure_rand[a], cl); \
  } \
  g_closure_reflection_bin = !g_closure_reflection_bin;

Closure closure_eval(ClosureReflection reflection)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, reflection);
  cl.data.r = reflection.roughness;

#ifdef MAT_CLEARCOAT
#  if CLOSURE_BIN_COUNT == 2
  /* Multiple reflection closures. */
  CHOOSE_MIN_WEIGHT_CLOSURE_BIN(0, 1);
#  elif CLOSURE_BIN_COUNT == 3
  /* Multiple reflection closures and one other closure. */
  CHOOSE_MIN_WEIGHT_CLOSURE_BIN(1, 2);
#  else
#    error Clearcoat should always have at least 2 bins
#  endif
#else
#  if CLOSURE_BIN_COUNT == 1
  /* Only one reflection closure is present in the whole tree. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
#  elif CLOSURE_BIN_COUNT == 2
  /* Only one reflection and one other closure. */
  closure_select(g_closure_bins[1], g_closure_rand[1], cl);
#  elif CLOSURE_BIN_COUNT == 3
  /* Only one reflection and two other closures. */
  closure_select(g_closure_bins[2], g_closure_rand[2], cl);
#  endif
#endif

#undef CHOOSE_MIN_WEIGHT_CLOSURE_BIN

  return Closure(0);
}

Closure closure_eval(ClosureRefraction refraction)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, refraction);
  cl.data.r = refraction.roughness;
  cl.data.g = refraction.ior;
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

Closure closure_eval(ClosureEmission emission)
{
  g_emission += emission.emission * emission.weight;
  return Closure(0);
}

Closure closure_eval(ClosureTransparency transparency)
{
  g_transmittance += transparency.transmittance * transparency.weight;
  g_holdout += transparency.holdout * transparency.weight;
  return Closure(0);
}

Closure closure_eval(ClosureVolumeScatter volume_scatter)
{
  g_volume_scattering += volume_scatter.scattering * volume_scatter.weight;
  g_volume_anisotropy += volume_scatter.anisotropy * volume_scatter.weight;
  return Closure(0);
}

Closure closure_eval(ClosureVolumeAbsorption volume_absorption)
{
  g_volume_absorption += volume_absorption.absorption * volume_absorption.weight;
  return Closure(0);
}

Closure closure_eval(ClosureHair hair)
{
  /* TODO */
  return Closure(0);
}

/* Glass BSDF. */
Closure closure_eval(ClosureReflection reflection, ClosureRefraction refraction)
{
  closure_eval(reflection);
  closure_eval(refraction);
  return Closure(0);
}

/* Dielectric BSDF. */
Closure closure_eval(ClosureDiffuse diffuse, ClosureReflection reflection)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  return Closure(0);
}

/* Coat BSDF. */
Closure closure_eval(ClosureReflection reflection, ClosureReflection coat)
{
  closure_eval(reflection);
  closure_eval(coat);
  return Closure(0);
}

/* Volume BSDF. */
Closure closure_eval(ClosureVolumeScatter volume_scatter,
                     ClosureVolumeAbsorption volume_absorption,
                     ClosureEmission emission)
{
  closure_eval(volume_scatter);
  closure_eval(volume_absorption);
  closure_eval(emission);
  return Closure(0);
}

/* Specular BSDF. */
Closure closure_eval(ClosureDiffuse diffuse, ClosureReflection reflection, ClosureReflection coat)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  closure_eval(coat);
  return Closure(0);
}

/* Principled BSDF. */
Closure closure_eval(ClosureDiffuse diffuse,
                     ClosureReflection reflection,
                     ClosureReflection coat,
                     ClosureRefraction refraction)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  closure_eval(coat);
  closure_eval(refraction);
  return Closure(0);
}

/* NOP since we are sampling closures. */
Closure closure_add(Closure cl1, Closure cl2)
{
  return Closure(0);
}
Closure closure_mix(Closure cl1, Closure cl2, float fac)
{
  return Closure(0);
}

float ambient_occlusion_eval(vec3 normal,
                             float max_distance,
                             const float inverted,
                             const float sample_count)
{
  /* Avoid multi-line pre-processor conditionals.
   * Some drivers don't handle them correctly. */
  // clang-format off
#if defined(GPU_FRAGMENT_SHADER) && defined(MAT_AMBIENT_OCCLUSION) && !defined(MAT_DEPTH) && !defined(MAT_SHADOW)
  // clang-format on
#  if 0 /* TODO(fclem): Finish inverted horizon scan. */
  /* TODO(fclem): Replace eevee_ambient_occlusion_lib by eevee_horizon_scan_eval_lib when this is
   * finished. */
  vec3 vP = drw_point_world_to_view(g_data.P);
  vec3 vN = drw_normal_world_to_view(normal);

  ivec2 texel = ivec2(gl_FragCoord.xy);
  vec2 noise;
  noise.x = interlieved_gradient_noise(vec2(texel), 3.0, 0.0);
  noise.y = utility_tx_fetch(utility_tx, vec2(texel), UTIL_BLUE_NOISE_LAYER).r;
  noise = fract(noise + sampling_rng_2D_get(SAMPLING_AO_U));

  ClosureOcclusion occlusion;
  occlusion.N = (inverted != 0.0) ? -vN : vN;

  HorizonScanContext ctx;
  ctx.occlusion = occlusion;

  horizon_scan_eval(vP,
                    ctx,
                    noise,
                    uniform_buf.ao.pixel_size,
                    max_distance,
                    uniform_buf.ao.thickness,
                    uniform_buf.ao.angle_bias,
                    10,
                    inverted != 0.0);

  return saturate(ctx.occlusion_result.r);
#  else
  vec3 vP = drw_point_world_to_view(g_data.P);
  ivec2 texel = ivec2(gl_FragCoord.xy);
  OcclusionData data = ambient_occlusion_search(
      vP, hiz_tx, texel, max_distance, inverted, sample_count);

  vec3 V = drw_world_incident_vector(g_data.P);
  vec3 N = normal;
  vec3 Ng = g_data.Ng;

  float unused_error, visibility;
  vec3 unused;
  ambient_occlusion_eval(data, texel, V, N, Ng, inverted, visibility, unused_error, unused);
  return visibility;
#  endif
#else
  return 1.0;
#endif
}

#ifndef GPU_METAL
void attrib_load();
Closure nodetree_surface(float closure_rand);
Closure nodetree_volume();
vec3 nodetree_displacement();
float nodetree_thickness();
vec4 closure_to_rgba(Closure cl);
#endif

/* Simplified form of F_eta(eta, 1.0). */
float F0_from_ior(float eta)
{
  float A = (eta - 1.0) / (eta + 1.0);
  return A * A;
}

/* Return the fresnel color from a precomputed LUT value (from brdf_lut). */
vec3 F_brdf_single_scatter(vec3 f0, vec3 f90, vec2 lut)
{
  return f0 * lut.x + f90 * lut.y;
}

/* Multi-scattering brdf approximation from
 * "A Multiple-Scattering Microfacet Model for Real-Time Image-based Lighting"
 * https://jcgt.org/published/0008/01/03/paper.pdf by Carmelo J. Fdez-Agüera. */
vec3 F_brdf_multi_scatter(vec3 f0, vec3 f90, vec2 lut)
{
  vec3 FssEss = F_brdf_single_scatter(f0, f90, lut);

  float Ess = lut.x + lut.y;
  float Ems = 1.0 - Ess;
  vec3 Favg = f0 + (f90 - f0) / 21.0;

  /* The original paper uses `FssEss * radiance + Fms*Ems * irradiance`, but
   * "A Journey Through Implementing Multi-scattering BRDFs and Area Lights" by Steve McAuley
   * suggests to use `FssEss * radiance + Fms*Ems * radiance` which results in comparable quality.
   * We handle `radiance` outside of this function, so the result simplifies to:
   * `FssEss + Fms*Ems = FssEss * (1 + Ems*Favg / (1 - Ems*Favg)) = FssEss / (1 - Ems*Favg)`.
   * This is a simple albedo scaling very similar to the approach used by Cycles:
   * "Practical multiple scattering compensation for microfacet model". */
  return FssEss / (1.0 - Ems * Favg);
}

vec2 brdf_lut(float cos_theta, float roughness)
{
#ifdef EEVEE_UTILITY_TX
  return utility_tx_sample_lut(utility_tx, cos_theta, roughness, UTIL_BSDF_LAYER).rg;
#else
  return vec2(1.0, 0.0);
#endif
}

void brdf_f82_tint_lut(vec3 F0,
                       vec3 F82,
                       float cos_theta,
                       float roughness,
                       bool do_multiscatter,
                       out vec3 reflectance)
{
#ifdef EEVEE_UTILITY_TX
  vec3 split_sum = utility_tx_sample_lut(utility_tx, cos_theta, roughness, UTIL_BSDF_LAYER).rgb;
#else
  vec3 split_sum = vec3(1.0, 0.0, 0.0);
#endif

  reflectance = do_multiscatter ? F_brdf_multi_scatter(F0, vec3(1.0), split_sum.xy) :
                                  F_brdf_single_scatter(F0, vec3(1.0), split_sum.xy);

  /* Precompute the F82 term factor for the Fresnel model.
   * In the classic F82 model, the F82 input directly determines the value of the Fresnel
   * model at ~82°, similar to F0 and F90.
   * With F82-Tint, on the other hand, the value at 82° is the value of the classic Schlick
   * model multiplied by the tint input.
   * Therefore, the factor follows by setting `F82Tint(cosI) = FSchlick(cosI) - b*cosI*(1-cosI)^6`
   * and `F82Tint(acos(1/7)) = FSchlick(acos(1/7)) * f82_tint` and solving for `b`. */
  const float f = 6.0 / 7.0;
  const float f5 = (f * f) * (f * f) * f;
  const float f6 = (f * f) * (f * f) * (f * f);
  vec3 F_schlick = mix(F0, vec3(1.0), f5);
  vec3 b = F_schlick * (7.0 / f6) * (1.0 - F82);
  reflectance -= b * split_sum.z;
}

/* Return texture coordinates to sample BSDF LUT. */
vec3 lut_coords_bsdf(float cos_theta, float roughness, float ior)
{
  /* IOR is the sine of the critical angle. */
  float critical_cos = sqrt(1.0 - ior * ior);

  vec3 coords;
  coords.x = square(ior);
  coords.y = cos_theta;
  coords.y -= critical_cos;
  coords.y /= (coords.y > 0.0) ? (1.0 - critical_cos) : critical_cos;
  coords.y = coords.y * 0.5 + 0.5;
  coords.z = roughness;

  return saturate(coords);
}

/* Return texture coordinates to sample Surface LUT. */
vec3 lut_coords_btdf(float cos_theta, float roughness, float ior)
{
  return vec3(sqrt((ior - 1.0) / (ior + 1.0)), sqrt(1.0 - cos_theta), roughness);
}

/* Computes the reflectance and transmittance based on the tint (`f0`, `f90`, `transmission_tint`)
 * and the BSDF LUT. */
void bsdf_lut(vec3 F0,
              vec3 F90,
              vec3 transmission_tint,
              float cos_theta,
              float roughness,
              float ior,
              bool do_multiscatter,
              out vec3 reflectance,
              out vec3 transmittance)
{
#ifdef EEVEE_UTILITY_TX
  if (ior == 1.0) {
    reflectance = vec3(0.0);
    transmittance = transmission_tint;
    return;
  }

  vec2 split_sum;
  float transmission_factor;

  if (ior > 1.0) {
    split_sum = brdf_lut(cos_theta, roughness);
    vec3 coords = lut_coords_btdf(cos_theta, roughness, ior);
    transmission_factor = utility_tx_sample_bsdf_lut(utility_tx, coords.xy, coords.z).a;
    /* Gradually increase `f90` from 0 to 1 when IOR is in the range of [1.0, 1.33], to avoid harsh
     * transition at `IOR == 1`. */
    if (all(equal(F90, vec3(1.0)))) {
      F90 = vec3(saturate(2.33 / 0.33 * (ior - 1.0) / (ior + 1.0)));
    }
  }
  else {
    vec3 coords = lut_coords_bsdf(cos_theta, roughness, ior);
    vec3 bsdf = utility_tx_sample_bsdf_lut(utility_tx, coords.xy, coords.z).rgb;
    split_sum = bsdf.rg;
    transmission_factor = bsdf.b;
  }

  reflectance = F_brdf_single_scatter(F0, F90, split_sum);
  transmittance = (vec3(1.0) - F0) * transmission_factor * transmission_tint;

  if (do_multiscatter) {
    float real_F0 = F0_from_ior(ior);
    float Ess = real_F0 * split_sum.x + split_sum.y + (1.0 - real_F0) * transmission_factor;
    float Ems = 1.0 - Ess;
    /* Assume that the transmissive tint makes up most of the overall color if it's not zero. */
    vec3 Favg = all(equal(transmission_tint, vec3(0.0))) ? F0 + (F90 - F0) / 21.0 :
                                                           transmission_tint;

    vec3 scale = 1.0 / (1.0 - Ems * Favg);
    reflectance *= scale;
    transmittance *= scale;
  }
#else
  reflectance = vec3(0.0);
  transmittance = vec3(0.0);
#endif
  return;
}

/* Computes the reflectance and transmittance based on the BSDF LUT. */
vec2 bsdf_lut(float cos_theta, float roughness, float ior, bool do_multiscatter)
{
  float F0 = F0_from_ior(ior);
  vec3 color = vec3(1.0);
  vec3 reflectance, transmittance;
  bsdf_lut(vec3(F0),
           color,
           color,
           cos_theta,
           roughness,
           ior,
           do_multiscatter,
           reflectance,
           transmittance);
  return vec2(reflectance.r, transmittance.r);
}

#ifdef EEVEE_MATERIAL_STUBS
#  define attrib_load()
#  define nodetree_displacement() vec3(0.0)
#  define nodetree_surface(closure_rand) Closure(0)
#  define nodetree_volume() Closure(0)
#  define nodetree_thickness() 0.1
#endif

#ifdef GPU_VERTEX_SHADER
#  define closure_to_rgba(a) vec4(0.0)
#endif

/* -------------------------------------------------------------------- */
/** \name Fragment Displacement
 *
 * Displacement happening in the fragment shader.
 * Can be used in conjunction with a per vertex displacement.
 *
 * \{ */

#ifdef MAT_DISPLACEMENT_BUMP
/* Return new shading normal. */
vec3 displacement_bump()
{
#  if defined(GPU_FRAGMENT_SHADER) && !defined(MAT_GEOM_CURVES)
  vec2 dHd;
  dF_branch(dot(nodetree_displacement(), g_data.N + dF_impl(g_data.N)), dHd);

  vec3 dPdx = dFdx(g_data.P);
  vec3 dPdy = dFdy(g_data.P);

  /* Get surface tangents from normal. */
  vec3 Rx = cross(dPdy, g_data.N);
  vec3 Ry = cross(g_data.N, dPdx);

  /* Compute surface gradient and determinant. */
  float det = dot(dPdx, Rx);

  vec3 surfgrad = dHd.x * Rx + dHd.y * Ry;

  float facing = FrontFacing ? 1.0 : -1.0;
  return normalize(abs(det) * g_data.N - facing * sign(det) * surfgrad);
#  else
  return g_data.N;
#  endif
}
#endif

void fragment_displacement()
{
#ifdef MAT_DISPLACEMENT_BUMP
  g_data.N = displacement_bump();
#endif
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Coordinate implementations
 *
 * Callbacks for the texture coordinate node.
 *
 * \{ */

vec3 coordinate_camera(vec3 P)
{
  vec3 vP;
  if (false /* Probe. */) {
    /* Unsupported. It would make the probe camera-dependent. */
    vP = P;
  }
  else {
#ifdef MAT_GEOM_WORLD
    vP = drw_normal_world_to_view(P);
#else
    vP = drw_point_world_to_view(P);
#endif
  }
  vP.z = -vP.z;
  return vP;
}

vec3 coordinate_screen(vec3 P)
{
  vec3 window = vec3(0.0);
  if (false /* Probe. */) {
    /* Unsupported. It would make the probe camera-dependent. */
    window.xy = vec2(0.5);
  }
  else {
#ifdef MAT_GEOM_WORLD
    window.xy = drw_point_view_to_screen(interp.P).xy;
#else
    /* TODO(fclem): Actual camera transform. */
    window.xy = drw_point_world_to_screen(P).xy;
#endif
    window.xy = window.xy * uniform_buf.camera.uv_scale + uniform_buf.camera.uv_bias;
  }
  return window;
}

vec3 coordinate_reflect(vec3 P, vec3 N)
{
#ifdef MAT_GEOM_WORLD
  return N;
#else
  return -reflect(drw_world_incident_vector(P), N);
#endif
}

vec3 coordinate_incoming(vec3 P)
{
#ifdef MAT_GEOM_WORLD
  return -P;
#else
  return drw_world_incident_vector(P);
#endif
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Mixed render resolution
 *
 * Callbacks image texture sampling.
 *
 * \{ */

float film_scaling_factor_get()
{
  return float(uniform_buf.film.scaling_factor);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volume Attribute post
 *
 * TODO(@fclem): These implementation details should concern the DRWManager and not be a fix on
 * the engine side. But as of now, the engines are responsible for loading the attributes.
 *
 * \{ */

/* Point clouds and curves are not compatible with volume grids.
 * They will fallback to their own attributes loading. */
#if defined(MAT_VOLUME) && !defined(MAT_GEOM_CURVES) && !defined(MAT_GEOM_POINT_CLOUD)
#  if defined(OBINFO_LIB) && !defined(MAT_GEOM_WORLD)
/* We could just check for GRID_ATTRIBUTES but this avoids for header dependency. */
#    define GRID_ATTRIBUTES_LOAD_POST
#  endif
#endif

float attr_load_temperature_post(float attr)
{
#ifdef GRID_ATTRIBUTES_LOAD_POST
  /* Bring the into standard range without having to modify the grid values */
  attr = (attr > 0.01) ? (attr * drw_volume.temperature_mul + drw_volume.temperature_bias) : 0.0;
#endif
  return attr;
}
vec4 attr_load_color_post(vec4 attr)
{
#ifdef GRID_ATTRIBUTES_LOAD_POST
  /* Density is premultiplied for interpolation, divide it out here. */
  attr.rgb *= safe_rcp(attr.a);
  attr.rgb *= drw_volume.color_mul.rgb;
  attr.a = 1.0;
#endif
  return attr;
}

#undef GRID_ATTRIBUTES_LOAD_POST

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Attributes
 *
 * TODO(@fclem): These implementation details should concern the DRWManager and not be a fix on
 * the engine side. But as of now, the engines are responsible for loading the attributes.
 *
 * \{ */

vec4 attr_load_uniform(vec4 attr, const uint attr_hash)
{
#if defined(OBATTR_LIB)
  uint index = floatBitsToUint(ObjectAttributeStart);
  for (uint i = 0; i < floatBitsToUint(ObjectAttributeLen); i++, index++) {
    if (drw_attrs[index].hash_code == attr_hash) {
      return vec4(drw_attrs[index].data_x,
                  drw_attrs[index].data_y,
                  drw_attrs[index].data_z,
                  drw_attrs[index].data_w);
    }
  }
  return vec4(0.0);
#else
  return attr;
#endif
}

/** \} */

/////////////////////////////// Source file 29/////////////////////////////




/**
 * Sampling data accessors and random number generators.
 * Also contains some sample mapping functions.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Sampling data.
 *
 * Return a random values from Low Discrepancy Sequence in [0..1) range.
 * This value is uniform (constant) for the whole scene sample.
 * You might want to couple it with a noise function.
 * \{ */

#ifdef EEVEE_SAMPLING_DATA

float sampling_rng_1D_get(const eSamplingDimension dimension)
{
  return sampling_buf.dimensions[dimension];
}

vec2 sampling_rng_2D_get(const eSamplingDimension dimension)
{
  return vec2(sampling_buf.dimensions[dimension], sampling_buf.dimensions[dimension + 1u]);
}

vec3 sampling_rng_3D_get(const eSamplingDimension dimension)
{
  return vec3(sampling_buf.dimensions[dimension],
              sampling_buf.dimensions[dimension + 1u],
              sampling_buf.dimensions[dimension + 2u]);
}

#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Random Number Generators.
 * \{ */

/* Interleaved gradient noise by Jorge Jimenez
 * http://www.iryoku.com/next-generation-post-processing-in-call-of-duty-advanced-warfare
 * Seeding found by Epic Game. */
float interlieved_gradient_noise(vec2 pixel, float seed, float offset)
{
  pixel += seed * (vec2(47, 17) * 0.695);
  return fract(offset + 52.9829189 * fract(0.06711056 * pixel.x + 0.00583715 * pixel.y));
}

vec2 interlieved_gradient_noise(vec2 pixel, vec2 seed, vec2 offset)
{
  return vec2(interlieved_gradient_noise(pixel, seed.x, offset.x),
              interlieved_gradient_noise(pixel, seed.y, offset.y));
}

vec3 interlieved_gradient_noise(vec2 pixel, vec3 seed, vec3 offset)
{
  return vec3(interlieved_gradient_noise(pixel, seed.x, offset.x),
              interlieved_gradient_noise(pixel, seed.y, offset.y),
              interlieved_gradient_noise(pixel, seed.z, offset.z));
}

/* From: http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html */
float van_der_corput_radical_inverse(uint bits)
{
#if 0 /* Reference */
  bits = (bits << 16u) | (bits >> 16u);
  bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
  bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
  bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
  bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
#else
  bits = bitfieldReverse(bits);
#endif
  /* Same as dividing by 0x100000000. */
  return float(bits) * 2.3283064365386963e-10;
}

vec2 hammersley_2d(float i, float sample_count)
{
  vec2 rand;
  rand.x = i / sample_count;
  rand.y = van_der_corput_radical_inverse(uint(i));
  return rand;
}

vec2 hammersley_2d(uint i, uint sample_count)
{
  vec2 rand;
  rand.x = float(i) / float(sample_count);
  rand.y = van_der_corput_radical_inverse(i);
  return rand;
}

vec2 hammersley_2d(int i, int sample_count)
{
  return hammersley_2d(uint(i), uint(sample_count));
}

/* Not random but still useful. sample_count should be an even. */
vec2 regular_grid_2d(int i, int sample_count)
{
  int sample_per_dim = int(sqrt(float(sample_count)));
  return (vec2(i % sample_per_dim, i / sample_per_dim) + 0.5) / float(sample_per_dim);
}

/* PCG */

/* https://www.pcg-random.org/ */
uint pcg_uint(uint u)
{
  uint state = u * 747796405u + 2891336453u;
  uint word = ((state >> ((state >> 28u) + 4u)) ^ state) * 277803737u;
  return (word >> 22u) ^ word;
}

float pcg(float v)
{
  return pcg_uint(floatBitsToUint(v)) / float(0xffffffffU);
}

float pcg(vec2 v)
{
  /* Nested pcg (faster and better quality that pcg2d). */
  uvec2 u = floatBitsToUint(v);
  return pcg_uint(pcg_uint(u.x) + u.y) / float(0xffffffffU);
}

/* http://www.jcgt.org/published/0009/03/02/ */
vec3 pcg3d(vec3 v)
{
  uvec3 u = floatBitsToUint(v);

  u = u * 1664525u + 1013904223u;

  u.x += u.y * u.z;
  u.y += u.z * u.x;
  u.z += u.x * u.y;

  u ^= u >> 16u;

  u.x += u.y * u.z;
  u.y += u.z * u.x;
  u.z += u.x * u.y;

  return vec3(u) / float(0xffffffffU);
}

/* http://www.jcgt.org/published/0009/03/02/ */
vec4 pcg4d(vec4 v)
{
  uvec4 u = floatBitsToUint(v);

  u = u * 1664525u + 1013904223u;

  u.x += u.y * u.w;
  u.y += u.z * u.x;
  u.z += u.x * u.y;
  u.w += u.y * u.z;

  u ^= u >> 16u;

  u.x += u.y * u.w;
  u.y += u.z * u.x;
  u.z += u.x * u.y;
  u.w += u.y * u.z;

  return vec4(u) / float(0xffffffffU);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Distribution mapping.
 *
 * Functions mapping input random numbers to sampling shapes (i.e: hemisphere).
 * \{ */

/* Given 1 random number in [0..1] range, return a random unit circle sample. */
vec2 sample_circle(float rand)
{
  float phi = (rand - 0.5) * M_TAU;
  float cos_phi = cos(phi);
  float sin_phi = sqrt(1.0 - square(cos_phi)) * sign(phi);
  return vec2(cos_phi, sin_phi);
}

/* Given 2 random number in [0..1] range, return a random unit disk sample. */
vec2 sample_disk(vec2 rand)
{
  return sample_circle(rand.y) * sqrt(rand.x);
}

/* This transform a 2d random sample (in [0..1] range) to a sample located on a cylinder of the
 * same range. This is because the sampling functions expect such a random sample which is
 * normally precomputed. */
vec3 sample_cylinder(vec2 rand)
{
  return vec3(rand.x, sample_circle(rand.y));
}

vec3 sample_sphere(vec2 rand)
{
  float cos_theta = rand.x * 2.0 - 1.0;
  float sin_theta = safe_sqrt(1.0 - cos_theta * cos_theta);
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/**
 * Uniform hemisphere distribution.
 * \a rand is 2 random float in the [0..1] range.
 * Returns point on a Z positive hemisphere of radius 1 and centered on the origin.
 */
vec3 sample_hemisphere(vec2 rand)
{
  float cos_theta = rand.x;
  float sin_theta = safe_sqrt(1.0 - square(cos_theta));
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/**
 * Uniform cone distribution.
 * \a rand is 2 random float in the [0..1] range.
 * \a cos_angle is the cosine of the half angle.
 * Returns point on a Z positive hemisphere of radius 1 and centered on the origin.
 */
vec3 sample_uniform_cone(vec2 rand, float cos_angle)
{
  float cos_theta = mix(cos_angle, 1.0, rand.x);
  float sin_theta = safe_sqrt(1.0 - square(cos_theta));
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/** \} */

/////////////////////////////// Source file 30/////////////////////////////




#pragma BLENDER_REQUIRE(draw_model_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_nodetree_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)

#if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER) && defined(MAT_GEOM_MESH)
vec3 barycentric_distances_get()
{
#  if defined(GPU_METAL)
  /* Calculate Barycentric distances from available parameters in Metal. */
  float wp_delta = length(dfdx(interp.P)) + length(dfdy(interp.P));
  float bc_delta = length(dfdx(gpu_BaryCoord)) + length(dfdy(gpu_BaryCoord));
  float rate_of_change = wp_delta / bc_delta;
  vec3 dists;
  dists.x = rate_of_change * (1.0 - gpu_BaryCoord.x);
  dists.y = rate_of_change * (1.0 - gpu_BaryCoord.y);
  dists.z = rate_of_change * (1.0 - gpu_BaryCoord.z);
#  else
  /* NOTE: No need to undo perspective divide since it has not been applied. */
  vec3 pos0 = (ProjectionMatrixInverse * gpu_position_at_vertex(0)).xyz;
  vec3 pos1 = (ProjectionMatrixInverse * gpu_position_at_vertex(1)).xyz;
  vec3 pos2 = (ProjectionMatrixInverse * gpu_position_at_vertex(2)).xyz;
  vec3 edge21 = pos2 - pos1;
  vec3 edge10 = pos1 - pos0;
  vec3 edge02 = pos0 - pos2;
  vec3 d21 = safe_normalize(edge21);
  vec3 d10 = safe_normalize(edge10);
  vec3 d02 = safe_normalize(edge02);
  vec3 dists;
  float d = dot(d21, edge02);
  dists.x = sqrt(dot(edge02, edge02) - d * d);
  d = dot(d02, edge10);
  dists.y = sqrt(dot(edge10, edge10) - d * d);
  d = dot(d10, edge21);
  dists.z = sqrt(dot(edge21, edge21) - d * d);
#  endif
  return dists;
}
#endif

void init_globals_mesh()
{
#if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER) && defined(MAT_GEOM_MESH)
  g_data.barycentric_coords = gpu_BaryCoord.xy;
  g_data.barycentric_dists = barycentric_distances_get();
#endif
}

void init_globals_curves()
{
#if defined(MAT_GEOM_CURVES)
  /* Shade as a cylinder. */
  float cos_theta = curve_interp.time_width / curve_interp.thickness;
#  if defined(GPU_FRAGMENT_SHADER)
  if (hairThicknessRes == 1) {
#    ifdef EEVEE_UTILITY_TX
    /* Random cosine normal distribution on the hair surface. */
    float noise = utility_tx_fetch(utility_tx, gl_FragCoord.xy, UTIL_BLUE_NOISE_LAYER).x;
#      ifdef EEVEE_SAMPLING_DATA
    /* Needs to check for SAMPLING_DATA, otherwise surfel shader validation fails. */
    noise = fract(noise + sampling_rng_1D_get(SAMPLING_CURVES_U));
#      endif
    cos_theta = noise * 2.0 - 1.0;
#    endif
  }
#  endif
  float sin_theta = sqrt(max(0.0, 1.0 - cos_theta * cos_theta));
  g_data.N = g_data.Ni = normalize(interp.N * sin_theta + curve_interp.binormal * cos_theta);

  /* Costly, but follows cycles per pixel tangent space (not following curve shape). */
  vec3 V = drw_world_incident_vector(g_data.P);
  g_data.curve_T = -curve_interp.tangent;
  g_data.curve_B = cross(V, g_data.curve_T);
  g_data.curve_N = safe_normalize(cross(g_data.curve_T, g_data.curve_B));

  g_data.is_strand = true;
  g_data.hair_time = curve_interp.time;
  g_data.hair_thickness = curve_interp.thickness;
  g_data.hair_strand_id = curve_interp_flat.strand_id;
#  if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER)
  g_data.barycentric_coords = hair_resolve_barycentric(curve_interp.barycentric_coords);
#  endif
#endif
}

void init_globals_gpencil()
{
  /* Undo back-face flip as the grease-pencil normal is already pointing towards the camera. */
  g_data.N = g_data.Ni = interp.N;
}

void init_globals()
{
  /* Default values. */
  g_data.P = interp.P;
  g_data.Ni = interp.N;
  g_data.N = safe_normalize(interp.N);
  g_data.Ng = g_data.N;
  g_data.is_strand = false;
  g_data.hair_time = 0.0;
  g_data.hair_thickness = 0.0;
  g_data.hair_strand_id = 0;
#if defined(MAT_SHADOW)
  g_data.ray_type = RAY_TYPE_SHADOW;
#elif defined(MAT_CAPTURE)
  g_data.ray_type = RAY_TYPE_DIFFUSE;
#else
  if (uniform_buf.pipeline.is_probe_reflection) {
    g_data.ray_type = RAY_TYPE_GLOSSY;
  }
  else {
    g_data.ray_type = RAY_TYPE_CAMERA;
  }
#endif
  g_data.ray_depth = 0.0;
  g_data.ray_length = distance(g_data.P, drw_view_position());
  g_data.barycentric_coords = vec2(0.0);
  g_data.barycentric_dists = vec3(0.0);

#ifdef GPU_FRAGMENT_SHADER
  g_data.N = (FrontFacing) ? g_data.N : -g_data.N;
  g_data.Ni = (FrontFacing) ? g_data.Ni : -g_data.Ni;
  g_data.Ng = safe_normalize(cross(dFdx(g_data.P), dFdy(g_data.P)));
#endif

#if defined(MAT_GEOM_MESH)
  init_globals_mesh();
#elif defined(MAT_GEOM_CURVES)
  init_globals_curves();
#elif defined(MAT_GEOM_GPENCIL)
  init_globals_gpencil();
#endif
}

/* Avoid some compiler issue with non set interface parameters. */
void init_interface()
{
#ifdef GPU_VERTEX_SHADER
  interp.P = vec3(0.0);
  interp.N = vec3(0.0);
  drw_ResourceID_iface.resource_index = resource_id;
#endif
}

#if defined(GPU_VERTEX_SHADER) && defined(MAT_SHADOW)
void shadow_viewport_layer_set(int view_id, int lod)
{
#  ifdef SHADOW_UPDATE_ATOMIC_RASTER
  shadow_iface.shadow_view_id = view_id;
#  else
  /* We still render to a layered frame-buffer in the case of Metal + Tile Based Renderer.
   * Since it needs correct depth buffering, each view needs to not overlap each others.
   * It doesn't matter much for other platform, so we use that as a way to pass the view id. */
  gpu_Layer = view_id;
#  endif
  gpu_ViewportIndex = lod;
}
#endif

#if defined(GPU_FRAGMENT_SHADER) && defined(MAT_SHADOW)
int shadow_view_id_get()
{
#  ifdef SHADOW_UPDATE_ATOMIC_RASTER
  return shadow_iface.shadow_view_id;
#  else
  return gpu_Layer;
#  endif
}
#endif

/////////////////////////////// Source file 31/////////////////////////////




/**
 * Camera projection / uv functions and utils.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Panoramic Projections
 *
 * Adapted from Cycles to match EEVEE's coordinate system.
 * \{ */

vec2 camera_equirectangular_from_direction(CameraData cam, vec3 dir)
{
  float phi = atan(-dir.z, dir.x);
  float theta = acos(dir.y / length(dir));
  return (vec2(phi, theta) - cam.equirect_bias) * cam.equirect_scale_inv;
}

vec3 camera_equirectangular_to_direction(CameraData cam, vec2 uv)
{
  uv = uv * cam.equirect_scale + cam.equirect_bias;
  float phi = uv.x;
  float theta = uv.y;
  float sin_theta = sin(theta);
  return vec3(sin_theta * cos(phi), cos(theta), -sin_theta * sin(phi));
}

vec2 camera_fisheye_from_direction(CameraData cam, vec3 dir)
{
  float r = atan(length(dir.xy), -dir.z) / cam.fisheye_fov;
  float phi = atan(dir.y, dir.x);
  vec2 uv = r * vec2(cos(phi), sin(phi)) + 0.5;
  return (uv - cam.uv_bias) / cam.uv_scale;
}

vec3 camera_fisheye_to_direction(CameraData cam, vec2 uv)
{
  uv = uv * cam.uv_scale + cam.uv_bias;
  uv = (uv - 0.5) * 2.0;
  float r = length(uv);
  if (r > 1.0) {
    return vec3(0.0);
  }
  float phi = safe_acos(uv.x * safe_rcp(r));
  float theta = r * cam.fisheye_fov * 0.5;
  if (uv.y < 0.0) {
    phi = -phi;
  }
  return vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), -cos(theta));
}

vec2 camera_mirror_ball_from_direction(CameraData cam, vec3 dir)
{
  dir = normalize(dir);
  dir.z -= 1.0;
  dir *= safe_rcp(2.0 * safe_sqrt(-0.5 * dir.z));
  vec2 uv = 0.5 * dir.xy + 0.5;
  return (uv - cam.uv_bias) / cam.uv_scale;
}

vec3 camera_mirror_ball_to_direction(CameraData cam, vec2 uv)
{
  uv = uv * cam.uv_scale + cam.uv_bias;
  vec3 dir;
  dir.xy = uv * 2.0 - 1.0;
  if (length_squared(dir.xy) > 1.0) {
    return vec3(0.0);
  }
  dir.z = -safe_sqrt(1.0 - square(dir.x) - square(dir.y));
  const vec3 I = vec3(0.0, 0.0, 1.0);
  return reflect(I, dir);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Regular projections
 * \{ */

vec3 camera_view_from_uv(mat4 projmat, vec2 uv)
{
  return project_point(projmat, vec3(uv * 2.0 - 1.0, 0.0));
}

vec2 camera_uv_from_view(mat4 projmat, bool is_persp, vec3 vV)
{
  vec4 tmp = projmat * vec4(vV, 1.0);
  if (is_persp && tmp.w <= 0.0) {
    /* Return invalid coordinates for points behind the camera.
     * This can happen with panoramic projections. */
    return vec2(-1.0);
  }
  return (tmp.xy / tmp.w) * 0.5 + 0.5;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name General functions handling all projections
 * \{ */

vec3 camera_view_from_uv(CameraData cam, vec2 uv)
{
  vec3 vV;
  switch (cam.type) {
    default:
    case CAMERA_ORTHO:
    case CAMERA_PERSP:
      return camera_view_from_uv(cam.wininv, uv);
    case CAMERA_PANO_EQUIRECT:
      vV = camera_equirectangular_to_direction(cam, uv);
      break;
    case CAMERA_PANO_EQUIDISTANT:
      // ATTR_FALLTHROUGH;
    case CAMERA_PANO_EQUISOLID:
      vV = camera_fisheye_to_direction(cam, uv);
      break;
    case CAMERA_PANO_MIRROR:
      vV = camera_mirror_ball_to_direction(cam, uv);
      break;
  }
  return vV;
}

vec2 camera_uv_from_view(CameraData cam, vec3 vV)
{
  switch (cam.type) {
    default:
    case CAMERA_ORTHO:
      return camera_uv_from_view(cam.winmat, false, vV);
    case CAMERA_PERSP:
      return camera_uv_from_view(cam.winmat, true, vV);
    case CAMERA_PANO_EQUIRECT:
      return camera_equirectangular_from_direction(cam, vV);
    case CAMERA_PANO_EQUISOLID:
      // ATTR_FALLTHROUGH;
    case CAMERA_PANO_EQUIDISTANT:
      return camera_fisheye_from_direction(cam, vV);
    case CAMERA_PANO_MIRROR:
      return camera_mirror_ball_from_direction(cam, vV);
  }
}

vec2 camera_uv_from_world(CameraData cam, vec3 P)
{
  vec3 vV = transform_direction(cam.viewmat, normalize(P));
  return camera_uv_from_view(cam, vV);
}

/** \} */

/////////////////////////////// Source file 32/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)
#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_camera_lib.glsl)

vec4 velocity_pack(vec4 data)
{
  return data * 0.01;
}

vec4 velocity_unpack(vec4 data)
{
  return data * 100.0;
}

#ifdef VELOCITY_CAMERA

/**
 * Given a triple of position, compute the previous and next motion vectors.
 * Returns uv space motion vectors in pairs (motion_prev.xy, motion_next.xy).
 */
vec4 velocity_surface(vec3 P_prv, vec3 P, vec3 P_nxt)
{
  /* NOTE: We don't use the drw_view.persmat to avoid adding the TAA jitter to the velocity. */
  vec2 prev_uv = project_point(camera_prev.persmat, P_prv).xy;
  vec2 curr_uv = project_point(camera_curr.persmat, P).xy;
  vec2 next_uv = project_point(camera_next.persmat, P_nxt).xy;
  /* Fix issue with perspective division. */
  if (any(isnan(prev_uv))) {
    prev_uv = curr_uv;
  }
  if (any(isnan(next_uv))) {
    next_uv = curr_uv;
  }
  /* NOTE: We output both vectors in the same direction so we can reuse the same vector
   * with RGRG swizzle in viewport. */
  vec4 motion = vec4(prev_uv - curr_uv, curr_uv - next_uv);
  /* Convert NDC velocity to UV velocity */
  motion *= 0.5;

  return motion;
}

/**
 * Given a view space view vector \a vV, compute the previous and next motion vectors for
 * background pixels.
 * Returns uv space motion vectors in pairs (motion_prev.xy, motion_next.xy).
 */
vec4 velocity_background(vec3 vV)
{
  /* Only transform direction to avoid losing precision. */
  vec3 V = transform_direction(camera_curr.viewinv, vV);
  /* NOTE: We don't use the drw_view.winmat to avoid adding the TAA jitter to the velocity. */
  vec2 prev_uv = project_point(camera_prev.winmat, V).xy;
  vec2 curr_uv = project_point(camera_curr.winmat, V).xy;
  vec2 next_uv = project_point(camera_next.winmat, V).xy;
  /* NOTE: We output both vectors in the same direction so we can reuse the same vector
   * with RGRG swizzle in viewport. */
  vec4 motion = vec4(prev_uv - curr_uv, curr_uv - next_uv);
  /* Convert NDC velocity to UV velocity */
  motion *= 0.5;

  return motion;
}

vec4 velocity_resolve(vec4 vector, vec2 uv, float depth)
{
  if (vector.x == VELOCITY_INVALID) {
    bool is_background = (depth == 1.0);
    if (is_background) {
      /* NOTE: Use viewCameraVec to avoid imprecision if camera is far from origin. */
      vec3 vV = drw_view_incident_vector(drw_point_screen_to_view(vec3(uv, 1.0)));
      return velocity_background(vV);
    }
    else {
      /* Static geometry. No translation in world space. */
      vec3 P = drw_point_screen_to_world(vec3(uv, depth));
      return velocity_surface(P, P, P);
    }
  }
  return velocity_unpack(vector);
}

/**
 * Load and resolve correct velocity as some pixels might still not have correct
 * motion data for performance reasons.
 * Returns motion vector in render UV space.
 */
vec4 velocity_resolve(sampler2D vector_tx, ivec2 texel, float depth)
{
  vec2 uv = (vec2(texel) + 0.5) / vec2(textureSize(vector_tx, 0).xy);
  vec4 vector = texelFetch(vector_tx, texel, 0);
  return velocity_resolve(vector, uv, depth);
}

#endif

#ifdef MAT_VELOCITY

/**
 * Given a triple of position, compute the previous and next motion vectors.
 * Returns a tuple of world space motion deltas.
 */
void velocity_local_pos_get(vec3 lP, int vert_id, out vec3 lP_prev, out vec3 lP_next)
{
  VelocityIndex vel = velocity_indirection_buf[resource_id];
  lP_next = lP_prev = lP;
  if (vel.geo.do_deform) {
    if (vel.geo.ofs[STEP_PREVIOUS] != -1) {
      lP_prev = velocity_geo_prev_buf[vel.geo.ofs[STEP_PREVIOUS] + vert_id].xyz;
    }
    if (vel.geo.ofs[STEP_NEXT] != -1) {
      lP_next = velocity_geo_next_buf[vel.geo.ofs[STEP_NEXT] + vert_id].xyz;
    }
  }
}

/**
 * Given a triple of position, compute the previous and next motion vectors.
 * Returns a tuple of world space motion deltas.
 */
void velocity_vertex(
    vec3 lP_prev, vec3 lP, vec3 lP_next, out vec3 motion_prev, out vec3 motion_next)
{
  VelocityIndex vel = velocity_indirection_buf[resource_id];
  mat4 obmat_prev = velocity_obj_prev_buf[vel.obj.ofs[STEP_PREVIOUS]];
  mat4 obmat_next = velocity_obj_next_buf[vel.obj.ofs[STEP_NEXT]];
  vec3 P_prev = transform_point(obmat_prev, lP_prev);
  vec3 P_next = transform_point(obmat_next, lP_next);
  vec3 P = transform_point(ModelMatrix, lP);
  motion_prev = P_prev - P;
  motion_next = P_next - P;
}

#endif

/////////////////////////////// Source file 33/////////////////////////////




#pragma BLENDER_REQUIRE(draw_model_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_attributes_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_nodetree_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_surf_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_velocity_lib.glsl)

void main()
{
  DRW_VIEW_FROM_RESOURCE_ID;
#ifdef MAT_SHADOW
  shadow_viewport_layer_set(int(drw_view_id), int(viewport_index_buf[drw_view_id]));
#endif

  init_interface();

  interp.P = drw_point_object_to_world(pos);
  interp.N = normalize(drw_normal_object_to_world(nor));
#ifdef MAT_VELOCITY
  vec3 prv, nxt;
  velocity_local_pos_get(pos, gl_VertexID, prv, nxt);
  /* FIXME(fclem): Evaluating before displacement avoid displacement being treated as motion but
   * ignores motion from animated displacement. Supporting animated displacement motion vectors
   * would require evaluating the nodetree multiple time with different nodetree UBOs evaluated at
   * different times, but also with different attributes (maybe we could assume static attribute at
   * least). */
  velocity_vertex(prv, pos, nxt, motion.prev, motion.next);
#endif

  init_globals();
  attrib_load();

  interp.P += nodetree_displacement();

#ifdef MAT_CLIP_PLANE
  clip_interp.clip_distance = dot(clip_plane.plane, vec4(interp.P, 1.0));
#endif

  gl_Position = drw_point_world_to_homogenous(interp.P);
}

/////////////////////////////// Source file 34/////////////////////////////




void node_emission(vec4 color, float strength, float weight, out Closure result)
{
  color = max(color, vec4(0.0));
  strength = max(strength, 0.0);

  ClosureEmission emission_data;
  emission_data.weight = weight;
  emission_data.emission = color.rgb * strength;

  result = closure_eval(emission_data);
}

/////////////////////////////// Source file 35/////////////////////////////




/* Requires all common matrices declared. */

void normal_transform_object_to_world(vec3 vin, out vec3 vout)
{
  /* Expansion of NormalMatrix. */
  vout = vin * mat3(ModelMatrixInverse);
}

void normal_transform_world_to_object(vec3 vin, out vec3 vout)
{
  /* Expansion of NormalMatrixInverse. */
  vout = vin * mat3(ModelMatrix);
}

void direction_transform_object_to_world(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrix) * vin;
}

void direction_transform_object_to_view(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrix) * vin;
  vout = mat3x3(ViewMatrix) * vout;
}

void direction_transform_view_to_world(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrixInverse) * vin;
}

void direction_transform_view_to_object(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrixInverse) * vin;
  vout = mat3x3(ModelMatrixInverse) * vout;
}

void direction_transform_world_to_view(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrix) * vin;
}

void direction_transform_world_to_object(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrixInverse) * vin;
}

void point_transform_object_to_world(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrix * vec4(vin, 1.0)).xyz;
}

void point_transform_object_to_view(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrix * (ModelMatrix * vec4(vin, 1.0))).xyz;
}

void point_transform_view_to_world(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrixInverse * vec4(vin, 1.0)).xyz;
}

void point_transform_view_to_object(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrixInverse * (ViewMatrixInverse * vec4(vin, 1.0))).xyz;
}

void point_transform_world_to_view(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrix * vec4(vin, 1.0)).xyz;
}

void point_transform_world_to_object(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrixInverse * vec4(vin, 1.0)).xyz;
}

/////////////////////////////// Source file 36/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_material_transform_utils.glsl)

void node_output_material_surface(Closure surface, out Closure out_surface)
{
  out_surface = surface;
}

void node_output_material_volume(Closure volume, out Closure out_volume)
{
  out_volume = volume;
}

void node_output_material_displacement(vec3 displacement, out vec3 out_displacement)
{
  out_displacement = displacement;
}

void node_output_material_thickness(float thickness, out float out_thickness)
{
  vec3 thickness_vec;
  direction_transform_object_to_world(vec3(max(thickness, 0.0)), thickness_vec);
  thickness_vec = abs(thickness_vec);
  /* Contrary to displacement we need to output a scalar quantity.
   * We arbitrarily choose to output the axis with the minimum extent since it is the axis along
   * which the object is usually viewed at. */
  out_thickness = min(min(thickness_vec.x, thickness_vec.y), thickness_vec.z);
}

/////////////////////////////// Source file 37/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_FAST_LIB_GLSL
#define GPU_SHADER_MATH_FAST_LIB_GLSL

/* [Drobot2014a] Low Level Optimizations for GCN. */
float sqrt_fast(float v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}
vec2 sqrt_fast(vec2 v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}

/* [Eberly2014] GPGPU Programming for Games and Science. */
float acos_fast(float v)
{
  float res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  return (v >= 0) ? res : M_PI - res;
}
vec2 acos_fast(vec2 v)
{
  vec2 res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  v.x = (v.x >= 0) ? res.x : M_PI - res.x;
  v.y = (v.y >= 0) ? res.y : M_PI - res.y;
  return v;
}

#endif /* GPU_SHADER_MATH_FAST_LIB_GLSL */

/////////////////////////////// Source file 38/////////////////////////////




/* Float Math */

/* WORKAROUND: To be removed once we port all code to use `gpu_shader_math_base_lib.glsl`. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

float safe_divide(float a, float b)
{
  return (b != 0.0) ? a / b : 0.0;
}

#endif

/* fmod function compatible with OSL (copy from OSL/dual.h) */
float compatible_fmod(float a, float b)
{
  if (b != 0.0) {
    int N = int(a / b);
    return a - N * b;
  }
  return 0.0;
}

vec2 compatible_fmod(vec2 a, float b)
{
  return vec2(compatible_fmod(a.x, b), compatible_fmod(a.y, b));
}

vec3 compatible_fmod(vec3 a, float b)
{
  return vec3(compatible_fmod(a.x, b), compatible_fmod(a.y, b), compatible_fmod(a.z, b));
}

vec4 compatible_fmod(vec4 a, float b)
{
  return vec4(compatible_fmod(a.x, b),
              compatible_fmod(a.y, b),
              compatible_fmod(a.z, b),
              compatible_fmod(a.w, b));
}

float compatible_pow(float x, float y)
{
  if (y == 0.0) { /* x^0 -> 1, including 0^0 */
    return 1.0;
  }

  /* GLSL pow doesn't accept negative x. */
  if (x < 0.0) {
    if (mod(-y, 2.0) == 0.0) {
      return pow(-x, y);
    }
    else {
      return -pow(-x, y);
    }
  }
  else if (x == 0.0) {
    return 0.0;
  }

  return pow(x, y);
}

/* A version of pow that returns a fallback value if the computation is undefined. From the spec:
 * The result is undefined if x < 0 or if x = 0 and y is less than or equal 0. */
float fallback_pow(float x, float y, float fallback)
{
  if (x < 0.0 || (x == 0.0 && y <= 0.0)) {
    return fallback;
  }

  return pow(x, y);
}

float wrap(float a, float b, float c)
{
  float range = b - c;
  return (range != 0.0) ? a - (range * floor((a - c) / range)) : c;
}

vec3 wrap(vec3 a, vec3 b, vec3 c)
{
  return vec3(wrap(a.x, b.x, c.x), wrap(a.y, b.y, c.y), wrap(a.z, b.z, c.z));
}

/* WORKAROUND: To be removed once we port all code to use gpu_shader_math_base_lib.glsl. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

float hypot(float x, float y)
{
  return sqrt(x * x + y * y);
}

#endif

int floor_to_int(float x)
{
  return int(floor(x));
}

int quick_floor(float x)
{
  return int(x) - ((x < 0) ? 1 : 0);
}

/* Vector Math */

/* WORKAROUND: To be removed once we port all code to use gpu_shader_math_base_lib.glsl. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

vec2 safe_divide(vec2 a, vec2 b)
{
  return vec2(safe_divide(a.x, b.x), safe_divide(a.y, b.y));
}

vec3 safe_divide(vec3 a, vec3 b)
{
  return vec3(safe_divide(a.x, b.x), safe_divide(a.y, b.y), safe_divide(a.z, b.z));
}

vec4 safe_divide(vec4 a, vec4 b)
{
  return vec4(
      safe_divide(a.x, b.x), safe_divide(a.y, b.y), safe_divide(a.z, b.z), safe_divide(a.w, b.w));
}

vec2 safe_divide(vec2 a, float b)
{
  return (b != 0.0) ? a / b : vec2(0.0);
}

vec3 safe_divide(vec3 a, float b)
{
  return (b != 0.0) ? a / b : vec3(0.0);
}

vec4 safe_divide(vec4 a, float b)
{
  return (b != 0.0) ? a / b : vec4(0.0);
}

#endif

vec3 compatible_fmod(vec3 a, vec3 b)
{
  return vec3(compatible_fmod(a.x, b.x), compatible_fmod(a.y, b.y), compatible_fmod(a.z, b.z));
}

void invert_z(vec3 v, out vec3 outv)
{
  v.z = -v.z;
  outv = v;
}

void vector_normalize(vec3 normal, out vec3 outnormal)
{
  outnormal = normalize(normal);
}

void vector_copy(vec3 normal, out vec3 outnormal)
{
  outnormal = normal;
}

vec3 fallback_pow(vec3 a, float b, vec3 fallback)
{
  return vec3(fallback_pow(a.x, b, fallback.x),
              fallback_pow(a.y, b, fallback.y),
              fallback_pow(a.z, b, fallback.z));
}

/* Matrix Math */

/* Return a 2D rotation matrix with the angle that the input 2D vector makes with the x axis. */
mat2 vector_to_rotation_matrix(vec2 vector)
{
  vec2 normalized_vector = normalize(vector);
  float cos_angle = normalized_vector.x;
  float sin_angle = normalized_vector.y;
  return mat2(cos_angle, sin_angle, -sin_angle, cos_angle);
}

mat3 euler_to_mat3(vec3 euler)
{
  float cx = cos(euler.x);
  float cy = cos(euler.y);
  float cz = cos(euler.z);
  float sx = sin(euler.x);
  float sy = sin(euler.y);
  float sz = sin(euler.z);

  mat3 mat;
  mat[0][0] = cy * cz;
  mat[0][1] = cy * sz;
  mat[0][2] = -sy;

  mat[1][0] = sy * sx * cz - cx * sz;
  mat[1][1] = sy * sx * sz + cx * cz;
  mat[1][2] = cy * sx;

  mat[2][0] = sy * cx * cz + sx * sz;
  mat[2][1] = sy * cx * sz - sx * cz;
  mat[2][2] = cy * cx;
  return mat;
}

/////////////////////////////// Source file 39/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_common_math_utils.glsl)

void math_add(float a, float b, float c, out float result)
{
  result = a + b;
}

void math_subtract(float a, float b, float c, out float result)
{
  result = a - b;
}

void math_multiply(float a, float b, float c, out float result)
{
  result = a * b;
}

void math_divide(float a, float b, float c, out float result)
{
  result = safe_divide(a, b);
}

void math_power(float a, float b, float c, out float result)
{
  if (a >= 0.0) {
    result = compatible_pow(a, b);
  }
  else {
    float fraction = mod(abs(b), 1.0);
    if (fraction > 0.999 || fraction < 0.001) {
      result = compatible_pow(a, floor(b + 0.5));
    }
    else {
      result = 0.0;
    }
  }
}

void math_logarithm(float a, float b, float c, out float result)
{
  result = (a > 0.0 && b > 0.0) ? log2(a) / log2(b) : 0.0;
}

void math_sqrt(float a, float b, float c, out float result)
{
  result = (a > 0.0) ? sqrt(a) : 0.0;
}

void math_inversesqrt(float a, float b, float c, out float result)
{
  result = inversesqrt(a);
}

void math_absolute(float a, float b, float c, out float result)
{
  result = abs(a);
}

void math_radians(float a, float b, float c, out float result)
{
  result = radians(a);
}

void math_degrees(float a, float b, float c, out float result)
{
  result = degrees(a);
}

void math_minimum(float a, float b, float c, out float result)
{
  result = min(a, b);
}

void math_maximum(float a, float b, float c, out float result)
{
  result = max(a, b);
}

void math_less_than(float a, float b, float c, out float result)
{
  result = (a < b) ? 1.0 : 0.0;
}

void math_greater_than(float a, float b, float c, out float result)
{
  result = (a > b) ? 1.0 : 0.0;
}

void math_round(float a, float b, float c, out float result)
{
  result = floor(a + 0.5);
}

void math_floor(float a, float b, float c, out float result)
{
  result = floor(a);
}

void math_ceil(float a, float b, float c, out float result)
{
  result = ceil(a);
}

void math_fraction(float a, float b, float c, out float result)
{
  result = a - floor(a);
}

void math_modulo(float a, float b, float c, out float result)
{
  result = compatible_fmod(a, b);
}

void math_floored_modulo(float a, float b, float c, out float result)
{
  result = (b != 0.0) ? a - floor(a / b) * b : 0.0;
}

void math_trunc(float a, float b, float c, out float result)
{
  result = trunc(a);
}

void math_snap(float a, float b, float c, out float result)
{
  result = floor(safe_divide(a, b)) * b;
}

void math_pingpong(float a, float b, float c, out float result)
{
  result = (b != 0.0) ? abs(fract((a - b) / (b * 2.0)) * b * 2.0 - b) : 0.0;
}

/* Adapted from GODOT-engine math_funcs.h. */
void math_wrap(float a, float b, float c, out float result)
{
  result = wrap(a, b, c);
}

void math_sine(float a, float b, float c, out float result)
{
  result = sin(a);
}

void math_cosine(float a, float b, float c, out float result)
{
  result = cos(a);
}

void math_tangent(float a, float b, float c, out float result)
{
  result = tan(a);
}

void math_sinh(float a, float b, float c, out float result)
{
  result = sinh(a);
}

void math_cosh(float a, float b, float c, out float result)
{
  result = cosh(a);
}

void math_tanh(float a, float b, float c, out float result)
{
  result = tanh(a);
}

void math_arcsine(float a, float b, float c, out float result)
{
  result = (a <= 1.0 && a >= -1.0) ? asin(a) : 0.0;
}

void math_arccosine(float a, float b, float c, out float result)
{
  result = (a <= 1.0 && a >= -1.0) ? acos(a) : 0.0;
}

void math_arctangent(float a, float b, float c, out float result)
{
  result = atan(a);
}

void math_arctan2(float a, float b, float c, out float result)
{
  result = atan(a, b);
}

void math_sign(float a, float b, float c, out float result)
{
  result = sign(a);
}

void math_exponent(float a, float b, float c, out float result)
{
  result = exp(a);
}

void math_compare(float a, float b, float c, out float result)
{
  result = (abs(a - b) <= max(c, 1e-5)) ? 1.0 : 0.0;
}

void math_multiply_add(float a, float b, float c, out float result)
{
  result = a * b + c;
}

/* See: https://www.iquilezles.org/www/articles/smin/smin.htm. */
void math_smoothmin(float a, float b, float c, out float result)
{
  if (c != 0.0) {
    float h = max(c - abs(a - b), 0.0) / c;
    result = min(a, b) - h * h * h * c * (1.0 / 6.0);
  }
  else {
    result = min(a, b);
  }
}

void math_smoothmax(float a, float b, float c, out float result)
{
  math_smoothmin(-a, -b, c, result);
  result = -result;
}

/* TODO(fclem): Fix dependency hell one EEVEE legacy is removed. */
float math_reduce_max(vec3 a)
{
  return max(a.x, max(a.y, a.z));
}

float math_average(vec3 a)
{
  return (a.x + a.y + a.z) * (1.0 / 3.0);
}

/////////////////////////////// Source file 40/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_common_math.glsl)

vec3 tint_from_color(vec3 color)
{
  float lum = dot(color, vec3(0.3, 0.6, 0.1));  /* luminance approx. */
  return (lum > 0.0) ? color / lum : vec3(1.0); /* normalize lum. to isolate hue+sat */
}

float principled_sheen(float NV, float rough)
{
  /* Empirical approximation (manual curve fitting) to the sheen_weight albedo. Can be refined. */
  float den = 35.6694f * rough * rough - 24.4269f * rough * NV - 0.1405f * NV * NV +
              6.1211f * rough + 0.28105f * NV - 0.1405f;
  float num = 58.5299f * rough * rough - 85.0941f * rough * NV + 9.8955f * NV * NV +
              1.9250f * rough + 74.2268f * NV - 0.2246f;
  return saturate(den / num);
}

float ior_from_F0(float F0)
{
  float f = sqrt(clamp(F0, 0.0, 0.99));
  return (-f - 1.0) / (f - 1.0);
}

void node_bsdf_principled(vec4 base_color,
                          float metallic,
                          float roughness,
                          float ior,
                          float alpha,
                          vec3 N,
                          float weight,
                          float subsurface_weight,
                          vec3 subsurface_radius,
                          float subsurface_scale,
                          float subsurface_ior,
                          float subsurface_anisotropy,
                          float specular_ior_level,
                          vec4 specular_tint,
                          float anisotropic,
                          float anisotropic_rotation,
                          vec3 T,
                          float transmission_weight,
                          float coat_weight,
                          float coat_roughness,
                          float coat_ior,
                          vec4 coat_tint,
                          vec3 CN,
                          float sheen_weight,
                          float sheen_roughness,
                          vec4 sheen_tint,
                          vec4 emission,
                          float emission_strength,
                          const float do_diffuse,
                          const float do_coat,
                          const float do_refraction,
                          const float do_multiscatter,
                          const float do_sss,
                          out Closure result)
{
  /* Match cycles. */
  metallic = saturate(metallic);
  roughness = saturate(roughness);
  ior = max(ior, 1e-5);
  alpha = saturate(alpha);
  subsurface_weight = saturate(subsurface_weight);
  /* Not used by EEVEE */
  /* subsurface_anisotropy = clamp(subsurface_anisotropy, 0.0, 0.9); */
  /* subsurface_ior = clamp(subsurface_ior, 1.01, 3.8); */
  specular_ior_level = max(specular_ior_level, 0.0);
  specular_tint = max(specular_tint, vec4(0.0));
  /* Not used by EEVEE */
  /* anisotropic = saturate(anisotropic); */
  transmission_weight = saturate(transmission_weight);
  coat_weight = max(coat_weight, 0.0);
  coat_roughness = saturate(coat_roughness);
  coat_ior = max(coat_ior, 1.0);
  coat_tint = max(coat_tint, vec4(0.0));
  sheen_weight = max(sheen_weight, 0.0);
  sheen_roughness = saturate(sheen_roughness);
  sheen_tint = max(sheen_tint, vec4(0.0));

  base_color = max(base_color, vec4(0.0));
  vec4 clamped_base_color = min(base_color, vec4(1.0));

  N = safe_normalize(N);
  CN = safe_normalize(CN);
  vec3 V = coordinate_incoming(g_data.P);
  float NV = dot(N, V);

  ClosureTransparency transparency_data;
  transparency_data.weight = weight;
  transparency_data.transmittance = vec3(1.0 - alpha);
  transparency_data.holdout = 0.0;
  weight *= alpha;

  /* First layer: Sheen */
  vec3 sheen_data_color = vec3(0.0);
  if (sheen_weight > 0.0) {
    /* TODO: Maybe sheen_weight should be specular. */
    vec3 sheen_color = sheen_weight * sheen_tint.rgb * principled_sheen(NV, sheen_roughness);
    sheen_data_color = weight * sheen_color;
    /* Attenuate lower layers */
    weight *= max((1.0 - math_reduce_max(sheen_color)), 0.0);
  }

  /* Second layer: Coat */
  ClosureReflection coat_data;
  coat_data.N = CN;
  coat_data.roughness = coat_roughness;
  coat_data.color = vec3(1.0);

  if (coat_weight > 0.0) {
    float coat_NV = dot(coat_data.N, V);
    float reflectance = bsdf_lut(coat_NV, coat_data.roughness, coat_ior, false).x;
    coat_data.weight = weight * coat_weight * reflectance;
    /* Attenuate lower layers */
    weight *= max((1.0 - reflectance * coat_weight), 0.0);

    if (!all(equal(coat_tint.rgb, vec3(1.0)))) {
      float coat_neta = 1.0 / coat_ior;
      float NT = sqrt_fast(1.0 - coat_neta * coat_neta * (1 - NV * NV));
      /* Tint lower layers. */
      coat_tint.rgb = mix(vec3(1.0), pow(coat_tint.rgb, vec3(1.0 / NT)), saturate(coat_weight));
    }
  }
  else {
    coat_tint.rgb = vec3(1.0);
    coat_data.weight = 0.0;
  }

  /* Attenuated by sheen and coat. */
  ClosureEmission emission_data;
  emission_data.weight = weight;
  emission_data.emission = coat_tint.rgb * emission.rgb * emission_strength;

  /* Metallic component */
  ClosureReflection reflection_data;
  reflection_data.N = N;
  reflection_data.roughness = roughness;
  vec3 reflection_tint = specular_tint.rgb;
  if (metallic > 0.0) {
    vec3 F0 = clamped_base_color.rgb;
    vec3 F82 = min(reflection_tint, vec3(1.0));
    vec3 metallic_brdf;
    brdf_f82_tint_lut(F0, F82, NV, roughness, do_multiscatter != 0.0, metallic_brdf);
    reflection_data.color = weight * metallic * metallic_brdf;
    /* Attenuate lower layers */
    weight *= max((1.0 - metallic), 0.0);
  }
  else {
    reflection_data.color = vec3(0.0);
  }

  /* Transmission component */
  ClosureRefraction refraction_data;
  refraction_data.N = N;
  refraction_data.roughness = roughness;
  refraction_data.ior = ior;
  if (transmission_weight > 0.0) {
    vec3 F0 = vec3(F0_from_ior(ior)) * reflection_tint;
    vec3 F90 = vec3(1.0);
    vec3 reflectance, transmittance;
    bsdf_lut(F0,
             F90,
             clamped_base_color.rgb,
             NV,
             roughness,
             ior,
             do_multiscatter != 0.0,
             reflectance,
             transmittance);

    reflection_data.color += weight * transmission_weight * reflectance;

    refraction_data.weight = weight * transmission_weight;
    refraction_data.color = transmittance * coat_tint.rgb;
    /* Attenuate lower layers */
    weight *= max((1.0 - transmission_weight), 0.0);
  }
  else {
    refraction_data.weight = 0.0;
    refraction_data.color = vec3(0.0);
  }

  /* Specular component */
  if (true) {
    float eta = ior;
    float f0 = F0_from_ior(eta);
    if (specular_ior_level != 0.5) {
      f0 *= 2.0 * specular_ior_level;
      eta = ior_from_F0(f0);
      if (ior < 1.0) {
        eta = 1.0 / eta;
      }
    }

    vec3 F0 = vec3(f0) * reflection_tint;
    F0 = clamp(F0, vec3(0.0), vec3(1.0));
    vec3 F90 = vec3(1.0);
    vec3 reflectance, unused;
    bsdf_lut(F0, F90, vec3(0.0), NV, roughness, eta, do_multiscatter != 0.0, reflectance, unused);

    reflection_data.color += weight * reflectance;
    /* Adjust the weight of picking the closure. */
    reflection_data.color *= coat_tint.rgb;
    reflection_data.weight = math_average(reflection_data.color);
    reflection_data.color *= safe_rcp(reflection_data.weight);

    /* Attenuate lower layers */
    weight *= max((1.0 - math_reduce_max(reflectance)), 0.0);
  }

#ifdef GPU_SHADER_EEVEE_LEGACY_DEFINES
  /* EEVEE Legacy evaluates the subsurface as a diffuse closure.
   * So this has no performance penalty. However, using a separate closure for subsurface
   * (just like for EEVEE-Next) would induce a huge performance hit. */
  ClosureSubsurface diffuse_data;
  /* Flag subsurface as disabled by default. */
  diffuse_data.sss_radius.b = -1.0;
#else
  ClosureDiffuse diffuse_data;
#endif
  diffuse_data.N = N;

  subsurface_radius = max(subsurface_radius * subsurface_scale, vec3(0.0));

  /* Subsurface component */
  if (subsurface_weight > 0.0) {
#ifdef GPU_SHADER_EEVEE_LEGACY_DEFINES
    /* For Legacy EEVEE, Subsurface is just part of the diffuse. Just evaluate the mixed color. */
    /* Subsurface Scattering materials behave unpredictably with values greater than 1.0 in
     * Cycles. So it's clamped there and we clamp here for consistency with Cycles. */
    base_color = mix(base_color, clamped_base_color, subsurface_weight);
    if (do_sss != 0.0) {
      diffuse_data.sss_radius = subsurface_weight * subsurface_radius;
    }
#else
    ClosureSubsurface sss_data;
    sss_data.N = N;
    sss_data.sss_radius = subsurface_radius;
    /* Subsurface Scattering materials behave unpredictably with values greater than 1.0 in
     * Cycles. So it's clamped there and we clamp here for consistency with Cycles. */
    sss_data.color = (subsurface_weight * weight) * clamped_base_color.rgb * coat_tint.rgb;
    /* Add energy of the sheen layer until we have proper sheen BSDF. */
    sss_data.color += sheen_data_color;

    sss_data.weight = math_average(sss_data.color);
    sss_data.color *= safe_rcp(sss_data.weight);
    result = closure_eval(sss_data);

    /* Attenuate lower layers */
    weight *= max((1.0 - subsurface_weight), 0.0);
#endif
  }

  /* Diffuse component */
  if (true) {
    diffuse_data.color = weight * base_color.rgb * coat_tint.rgb;
    /* Add energy of the sheen layer until we have proper sheen BSDF. */
    diffuse_data.color += sheen_data_color;

    diffuse_data.weight = math_average(diffuse_data.color);
    diffuse_data.color *= safe_rcp(diffuse_data.weight);
  }

  /* Ref. #98190: Defines are optimizations for old compilers.
   * Might become unnecessary with EEVEE-Next. */
  if (do_diffuse == 0.0 && do_refraction == 0.0 && do_coat != 0.0) {
#ifdef PRINCIPLED_COAT
    /* Metallic & Coat case. */
    result = closure_eval(reflection_data, coat_data);
#endif
  }
  else if (do_diffuse == 0.0 && do_refraction == 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_METALLIC
    /* Metallic case. */
    result = closure_eval(reflection_data);
#endif
  }
  else if (do_diffuse != 0.0 && do_refraction == 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_DIELECTRIC
    /* Dielectric case. */
    result = closure_eval(diffuse_data, reflection_data);
#endif
  }
  else if (do_diffuse == 0.0 && do_refraction != 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_GLASS
    /* Glass case. */
    result = closure_eval(reflection_data, refraction_data);
#endif
  }
  else {
#ifdef PRINCIPLED_ANY
    /* Un-optimized case. */
    result = closure_eval(diffuse_data, reflection_data, coat_data, refraction_data);
#endif
  }
  Closure emission_cl = closure_eval(emission_data);
  Closure transparency_cl = closure_eval(transparency_data);
  result = closure_add(result, emission_cl);
  result = closure_add(result, transparency_cl);
}

/////////////////////////////// Source file 41/////////////////////////////




void set_value(float val, out float outval)
{
  outval = val;
}

void set_rgb(vec3 col, out vec3 outcol)
{
  outcol = col;
}

void set_rgba(vec4 col, out vec4 outcol)
{
  outcol = col;
}

void set_value_zero(out float outval)
{
  outval = 0.0;
}

void set_value_one(out float outval)
{
  outval = 1.0;
}

void set_rgb_zero(out vec3 outval)
{
  outval = vec3(0.0);
}

void set_rgb_one(out vec3 outval)
{
  outval = vec3(1.0);
}

void set_rgba_zero(out vec4 outval)
{
  outval = vec4(0.0);
}

void set_rgba_one(out vec4 outval)
{
  outval = vec4(1.0);
}

/////////////////////////////// Source file 42/////////////////////////////




void node_shader_to_rgba(Closure cl, out vec4 outcol, out float outalpha)
{
  outcol = closure_to_rgba(cl);
  outalpha = outcol.a;
}

/////////////////////////////// Source file 43/////////////////////////////




void world_normals_get(out vec3 N)
{
  N = g_data.N;
}

/////////////////////////////// Source file 44/////////////////////////////
void attrib_load()
{
}

vec3 nodetree_displacement()
{
return vec3(0);
}
