# pip install glfw PyOpenGL
import glfw
import OpenGL
from OpenGL.GL import *
from OpenGL.extensions import hasGLExtension
from OpenGL.GL.KHR.parallel_shader_compile import glMaxShaderCompilerThreadsKHR, GL_COMPLETION_STATUS_KHR

import random, time, os

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--thread_count', type=int, default=16)
parser.add_argument('--shader_count', type=int, default=64)
args = parser.parse_args()
THREAD_COUNT = args.thread_count
SHADER_COUNT = args.shader_count

def gl_buffer(type, size, data=None):
    types = {
        GL_BYTE : GLbyte,
        GL_UNSIGNED_BYTE : GLubyte,
        GL_SHORT : GLshort,
        GL_UNSIGNED_SHORT : GLushort,
        GL_INT : GLint,
        GL_UNSIGNED_INT : GLuint,
        #GL_HALF_FLOAT : GLhalfARB,
        GL_HALF_FLOAT : GLfloat,
        GL_FLOAT : GLfloat,
        GL_DOUBLE : GLdouble,
        GL_BOOL : GLboolean,
    }
    gl_type = (types[type] * size)
    if data:
        try:
            return gl_type(*data)
        except:
            return gl_type(data)
    else:
        return gl_type()

class Shader:

    def __init__(self, vertex, fragment):
        start_time = time.time()

        status = gl_buffer(GL_INT,1)
        info_log = gl_buffer(GL_BYTE, 1024)
        self.program = glCreateProgram()
        self.error = ""

        def compile_shader (source, shader_type):
            shader = glCreateShader(shader_type)
            glShaderSource(shader, source)
            glCompileShader(shader)
            glGetShaderiv(shader, GL_COMPILE_STATUS, status)
            if status[0] == GL_FALSE:
                info_log = glGetShaderInfoLog(shader)
                self.error += 'SHADER COMPILER ERROR :\n' + buffer_to_string(info_log)
            return shader

        self.vertex_shader = compile_shader(vertex, GL_VERTEX_SHADER)
        self.fragment_shader = compile_shader(fragment, GL_FRAGMENT_SHADER)

        glAttachShader(self.program, self.vertex_shader)
        glAttachShader(self.program, self.fragment_shader)
        glLinkProgram(self.program)

        self.is_ready_ = False

        if THREAD_COUNT == 0:
            glGetProgramiv(self.program, GL_LINK_STATUS, status)
            if status[0] == GL_FALSE:
                info_log = glGetProgramInfoLog(program)
                self.error += 'SHADER LINKER ERROR :\n' + buffer_to_string(info_log)
            self.is_ready_ = True

        end_time = time.time()
        print("COMPILE TIME: ", end_time - start_time)
    
    def is_ready(self):
        if self.is_ready_:
            return True
        else:
            status = gl_buffer(GL_INT,1)
            glGetProgramiv(self.program, GL_COMPLETION_STATUS_KHR, status)
            self.is_ready_ = status[0] == GL_TRUE
            # return self.is_ready_
            # Requesting the link status makes compilation faster (?!?!?)
            if status[0] == GL_TRUE:
                glGetProgramiv(self.program, GL_LINK_STATUS, status)
                return True
            return False

    def __del__(self):
        glDeleteShader(self.vertex_shader)
        glDeleteShader(self.fragment_shader)
        glDeleteProgram(self.program)


def main():
    glfw.ERROR_REPORTING = True
    glfw.init()

    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    
    window = glfw.create_window(256, 256, 'Test', None, None)
    glfw.make_context_current(window)
    #glfw.hide_window(window)

    if THREAD_COUNT > 0:
        glMaxShaderCompilerThreadsKHR(THREAD_COUNT)

    vert_src = ""
    with open("test_vert.glsl", 'r', encoding='utf8') as f:
        vert_src = f.read()
    frag_src = ""
    with open("test_frag.glsl", 'r', encoding='utf8') as f:
        frag_src = f.read()

    shaders = []

    start_time = time.time()

    for i in range(SHADER_COUNT):
        shaders.append(Shader(
            vert_src.replace("g_data", "g_data"+str(random.randint(0,100000))),
            frag_src.replace("g_data", "g_data"+str(random.randint(0,100000)))))

    end_time = time.time()
    print("FULL SYNC COMPILE TIME: ", end_time - start_time)

    while True:
        is_ready = True
        for shader in shaders:
            if not shader.is_ready():
                is_ready = False
                break
        if is_ready:
            break
        time.sleep(0.1)
       
    end_time = time.time()
    print("FULL ASYNC COMPILE TIME: ", end_time - start_time)

    for shader in shaders:
        del shader
    del shaders
    glfw.terminate()

main()
