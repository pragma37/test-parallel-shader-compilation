/////////////////////////////// Source file 0/////////////////////////////
#version 430
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_viewport_layer_array: enable
#define gpu_Layer gl_Layer
#define gpu_ViewportIndex gl_ViewportIndex
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_EmitVertex EmitVertex
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1
#define DFDY_SIGN 1




/** Type aliases. */
/** IMPORTANT: Be wary of size and alignment matching for types that are present
 * in C++ shared code. */

/* Boolean in GLSL are 32bit in interface structs. */
#define bool32_t bool
#define bool2 bvec2
#define bool3 bvec3
#define bool4 bvec4

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define int2 ivec2
#define int3 ivec3
#define int4 ivec4
#define uint2 uvec2
#define uint3 uvec3
#define uint4 uvec4
/* GLSL already follows the packed alignment / size rules for vec3. */
#define packed_float3 float3
#define packed_int3 int3
#define packed_uint3 uint3

#define float2x2 mat2x2
#define float3x2 mat3x2
#define float4x2 mat4x2
#define float2x3 mat2x3
#define float3x3 mat3x3
#define float4x3 mat4x3
#define float2x4 mat2x4
#define float3x4 mat3x4
#define float4x4 mat4x4

/* Small types are unavailable in GLSL (or badly supported), promote them to bigger type. */
#define char int
#define char2 int2
#define char3 int3
#define char4 int4
#define short int
#define short2 int2
#define short3 int3
#define short4 int4
#define uchar uint
#define uchar2 uint2
#define uchar3 uint3
#define uchar4 uint4
#define ushort uint
#define ushort2 uint2
#define ushort3 uint3
#define ushort4 uint4
#define half float
#define half2 float2
#define half3 float3
#define half4 float4

/* Aliases for supported fixed width types. */
#define int32_t int
#define uint32_t uint

/* Fast store variant macro. In GLSL this is the same as imageStore, but assumes no bounds
 * checking. */
#define imageStoreFast imageStore
#define imageLoadFast imageLoad

/* Texture format tokens -- Type explicitness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

#define usampler2DArrayAtomic usampler2DArray
#define usampler2DAtomic usampler2D
#define usampler3DAtomic usampler3D
#define isampler2DArrayAtomic isampler2DArray
#define isampler2DAtomic isampler2D
#define isampler3DAtomic isampler3D

/* Pass through functions. */
#define imageFence(image)

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}

/////////////////////////////// Source file 1/////////////////////////////

/////////////////////////////// Source file 2/////////////////////////////
#define GPU_SHADER

/////////////////////////////// Source file 3/////////////////////////////
#define GPU_NVIDIA

/////////////////////////////// Source file 4/////////////////////////////
#define OS_WIN

/////////////////////////////// Source file 5/////////////////////////////
#define GPU_OPENGL

/////////////////////////////// Source file 6/////////////////////////////
#define GPU_FRAGMENT_SHADER

/////////////////////////////// Source file 7/////////////////////////////
#define PRINCIPLED_DIELECTRIC 
#define MAT_DIFFUSE 
#define MAT_REFLECTION 
#define CLOSURE_BIN_COUNT 2
#define GBUFFER_LAYER_MAX 2
#define LIGHT_CLOSURE_EVAL_COUNT 2
#define MAT_RENDER_PASS_SUPPORT 
#define MAT_GEOM_MESH 
#define DRAW_MODELMAT_CREATE_INFO 
#define drw_ModelMatrixInverse drw_matrix_buf[resource_id].model_inverse
#define drw_ModelMatrix drw_matrix_buf[resource_id].model
#define ModelMatrixInverse drw_ModelMatrixInverse
#define ModelMatrix drw_ModelMatrix
#define UNIFORM_RESOURCE_ID_NEW 
#define drw_ResourceID resource_id_buf[gpu_BaseInstance + gl_InstanceID]
#define DRAW_VIEW_CREATE_INFO 
#define drw_view drw_view_[drw_view_id]
#define MAT_DEFERRED 
#define GBUFFER_WRITE 
#define EEVEE_UTILITY_TX 
#define EEVEE_SAMPLING_DATA 
#define SPHERE_PROBE 
#define IRRADIANCE_GRID_SAMPLING 
#define SHADOW_READ_ATOMIC 
#define USE_GPU_SHADER_CREATE_INFO

/////////////////////////////// Source file 8/////////////////////////////




/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool32_t a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
/* Silence macros when compiling for shaders. */
#  define BLI_STATIC_ASSERT(cond, msg)
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define ENUM_OPERATORS(a, b)
/* Incompatible keywords. */
#  define static
#  define inline
/* Math function renaming. */
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  include  BLI_math_matrix_types.hh 
#  include  BLI_math_vector_types.hh 

using bool32_t = int32_t;
// using bool2 = blender::int2; /* Size is not consistent across backend. */
// using bool3 = blender::int3; /* Size is not consistent across backend. */
// using bool4 = blender::int4; /* Size is not consistent across backend. */

using blender::float2;
using blender::float4;
using blender::int2;
using blender::int4;
using blender::uint2;
using blender::uint4;
/** IMPORTANT: Do not use in shared struct. Use packed_(float/int/uint)3 instead.
 * Here for static functions usage only. */
using blender::float3;
using blender::int3;
using blender::uint3;
/** Packed types are needed for MSL which have different alignment rules for float3. */
using packed_float3 = blender::float3;
using packed_int3 = blender::int3;
using packed_uint3 = blender::uint3;

using blender::float2x2;
// using blender::float3x2; /* Does not follow alignment rules of GPU. */
using blender::float4x2;
// using blender::float2x3; /* Does not follow alignment rules of GPU. */
// using blender::float3x3; /* Does not follow alignment rules of GPU. */
// using blender::float4x3; /* Does not follow alignment rules of GPU. */
using blender::float2x4;
using blender::float3x4;
using blender::float4x4;

#endif

/////////////////////////////// Source file 9/////////////////////////////
struct NodeTree {
vec4 u3;
vec4 u16;
vec4 u24;
vec4 u28;
vec4 u29;
vec3 u11;
float crypto_hash;
float u4;
float u5;
float u6;
float u7;
float u9;
float u10;
float u12;
float u13;
float u14;
float u15;
float u17;
float u18;
float u20;
float u21;
float u22;
float u23;
float u26;
float u27;
float u30;
float u41;
float u42;
};


/////////////////////////////// Source file 10/////////////////////////////




/** \file
 * \ingroup eevee
 *
 * List of defines that are shared with the GPUShaderCreateInfos. We do this to avoid
 * dragging larger headers into the createInfo pipeline which would cause problems.
 */

#ifndef GPU_SHADER
#  pragma once
#endif

#ifndef SQUARE
#  define SQUARE(x) ((x) * (x))
#endif

/* Look Up Tables. */
#define LUT_WORKGROUP_SIZE 16

/* Hierarchical Z down-sampling. */
#define HIZ_MIP_COUNT 7
/* NOTE: The shader is written to update 5 mipmaps using LDS. */
#define HIZ_GROUP_SIZE 32

/* Avoid too much overhead caused by resizing the light buffers too many time. */
#define LIGHT_CHUNK 256

#define CULLING_SELECT_GROUP_SIZE 256
#define CULLING_SORT_GROUP_SIZE 256
#define CULLING_ZBIN_GROUP_SIZE 1024
#define CULLING_TILE_GROUP_SIZE 256

/* Reflection Probes. */
#define SPHERE_PROBE_REMAP_GROUP_SIZE 32
#define SPHERE_PROBE_GROUP_SIZE 16
#define SPHERE_PROBE_SELECT_GROUP_SIZE 64
#define SPHERE_PROBE_MIPMAP_LEVELS 5
#define SPHERE_PROBE_SH_GROUP_SIZE 256
#define SPHERE_PROBE_SH_SAMPLES_PER_GROUP 64
/* Must be power of two for correct partitioning. */
#define SPHERE_PROBE_ATLAS_MAX_SUBDIV 10
#define SPHERE_PROBE_ATLAS_RES (1 << SPHERE_PROBE_ATLAS_MAX_SUBDIV)
/* Maximum number of thread-groups dispatched for remapping a probe to octahedral mapping. */
#define SPHERE_PROBE_MAX_HARMONIC SQUARE(SPHERE_PROBE_ATLAS_RES / SPHERE_PROBE_REMAP_GROUP_SIZE)
/* Start and end value for mixing sphere probe and volume probes. */
#define SPHERE_PROBE_MIX_START_ROUGHNESS 0.7
#define SPHERE_PROBE_MIX_END_ROUGHNESS 0.9
/* Roughness of the last mip map for sphere probes. */
#define SPHERE_PROBE_MIP_MAX_ROUGHNESS 0.7
/**
 * Limited by the UBO size limit `(16384 bytes / sizeof(SphereProbeData))`.
 */
#define SPHERE_PROBE_MAX 128

/**
 * Limited by the performance impact it can cause.
 * Limited by the max layer count supported by a hardware (256).
 * Limited by the UBO size limit `(16384 bytes / sizeof(PlanarProbeData))`.
 */
#define PLANAR_PROBE_MAX 16

/**
 * IMPORTANT: Some data packing are tweaked for these values.
 * Be sure to update them accordingly.
 * SHADOW_TILEMAP_RES max is 32 because of the shared bitmaps used for LOD tagging.
 * It is also limited by the maximum thread group size (1024).
 */
#if 0
/* Useful for debugging the tile-copy version of the shadow rendering without making debugging
 * tools unresponsive. */
#  define SHADOW_TILEMAP_RES 4
#  define SHADOW_TILEMAP_LOD 2 /* LOG2(SHADOW_TILEMAP_RES) */
#else
#  define SHADOW_TILEMAP_RES 32
#  define SHADOW_TILEMAP_LOD 5 /* LOG2(SHADOW_TILEMAP_RES) */
#endif
#define SHADOW_TILEMAP_LOD0_LEN ((SHADOW_TILEMAP_RES / 1) * (SHADOW_TILEMAP_RES / 1))
#define SHADOW_TILEMAP_LOD1_LEN ((SHADOW_TILEMAP_RES / 2) * (SHADOW_TILEMAP_RES / 2))
#define SHADOW_TILEMAP_LOD2_LEN ((SHADOW_TILEMAP_RES / 4) * (SHADOW_TILEMAP_RES / 4))
#define SHADOW_TILEMAP_LOD3_LEN ((SHADOW_TILEMAP_RES / 8) * (SHADOW_TILEMAP_RES / 8))
#define SHADOW_TILEMAP_LOD4_LEN ((SHADOW_TILEMAP_RES / 16) * (SHADOW_TILEMAP_RES / 16))
#define SHADOW_TILEMAP_LOD5_LEN ((SHADOW_TILEMAP_RES / 32) * (SHADOW_TILEMAP_RES / 32))
#define SHADOW_TILEMAP_PER_ROW 64
#define SHADOW_TILEDATA_PER_TILEMAP \
  (SHADOW_TILEMAP_LOD0_LEN + SHADOW_TILEMAP_LOD1_LEN + SHADOW_TILEMAP_LOD2_LEN + \
   SHADOW_TILEMAP_LOD3_LEN + SHADOW_TILEMAP_LOD4_LEN + SHADOW_TILEMAP_LOD5_LEN)
/* Maximum number of relative LOD distance we can store. */
#define SHADOW_TILEMAP_MAX_CLIPMAP_LOD 8
#if 0
/* Useful for debugging the tile-copy version of the shadow rendering without making debugging
 * tools unresponsive. */
#  define SHADOW_PAGE_CLEAR_GROUP_SIZE 8
#  define SHADOW_PAGE_RES 8
#  define SHADOW_PAGE_LOD 3 /* LOG2(SHADOW_PAGE_RES) */
#else
#  define SHADOW_PAGE_CLEAR_GROUP_SIZE 32
#  define SHADOW_PAGE_RES 256
#  define SHADOW_PAGE_LOD 8 /* LOG2(SHADOW_PAGE_RES) */
#endif
/* For testing only. */
// #define SHADOW_FORCE_LOD0
#define SHADOW_MAP_MAX_RES (SHADOW_PAGE_RES * SHADOW_TILEMAP_RES)
#define SHADOW_DEPTH_SCAN_GROUP_SIZE 8
#define SHADOW_AABB_TAG_GROUP_SIZE 64
#define SHADOW_MAX_TILEMAP 4096
#define SHADOW_MAX_TILE (SHADOW_MAX_TILEMAP * SHADOW_TILEDATA_PER_TILEMAP)
#define SHADOW_MAX_PAGE 4096
#define SHADOW_BOUNDS_GROUP_SIZE 64
#define SHADOW_CLIPMAP_GROUP_SIZE 64
#define SHADOW_VIEW_MAX 64 /* Must match DRW_VIEW_MAX. */
#define SHADOW_RENDER_MAP_SIZE (SHADOW_VIEW_MAX * SHADOW_TILEMAP_LOD0_LEN)
#define SHADOW_ATOMIC 1
#define SHADOW_PAGE_PER_ROW 4
#define SHADOW_PAGE_PER_COL 4
#define SHADOW_PAGE_PER_LAYER (SHADOW_PAGE_PER_ROW * SHADOW_PAGE_PER_COL)
#define SHADOW_MAX_STEP 16
#define SHADOW_MAX_RAY 4
#define SHADOW_ROG_ID 0

/* Deferred Lighting. */
#define DEFERRED_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define DEFERRED_GBUFFER_ROG_ID 0

/* Ray-tracing. */
#define RAYTRACE_GROUP_SIZE 8
/* Keep this as a define to avoid shader variations. */
#define RAYTRACE_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define RAYTRACE_RAYTIME_FORMAT GPU_R32F
#define RAYTRACE_VARIANCE_FORMAT GPU_R16F
#define RAYTRACE_TILEMASK_FORMAT GPU_R8UI

/* Sub-Surface Scattering. */
#define SUBSURFACE_GROUP_SIZE RAYTRACE_GROUP_SIZE
#define SUBSURFACE_RADIANCE_FORMAT GPU_R11F_G11F_B10F
#define SUBSURFACE_OBJECT_ID_FORMAT GPU_R16UI

/* Film. */
#define FILM_GROUP_SIZE 16

/* Motion Blur. */
#define MOTION_BLUR_GROUP_SIZE 32
#define MOTION_BLUR_DILATE_GROUP_SIZE 512

/* Irradiance Cache. */
/** Maximum number of entities inside the cache. */
#define IRRADIANCE_GRID_MAX 64

/* Depth Of Field. */
#define DOF_TILES_SIZE 8
#define DOF_TILES_FLATTEN_GROUP_SIZE DOF_TILES_SIZE
#define DOF_TILES_DILATE_GROUP_SIZE 8
#define DOF_BOKEH_LUT_SIZE 32
#define DOF_MAX_SLIGHT_FOCUS_RADIUS 5
#define DOF_SLIGHT_FOCUS_SAMPLE_MAX 16
#define DOF_MIP_COUNT 4
#define DOF_REDUCE_GROUP_SIZE (1 << (DOF_MIP_COUNT - 1))
#define DOF_DEFAULT_GROUP_SIZE 32
#define DOF_STABILIZE_GROUP_SIZE 16
#define DOF_FILTER_GROUP_SIZE 8
#define DOF_GATHER_GROUP_SIZE DOF_TILES_SIZE
#define DOF_RESOLVE_GROUP_SIZE (DOF_TILES_SIZE * 2)

/* Ambient Occlusion. */
#define AMBIENT_OCCLUSION_PASS_TILE_SIZE 16

/* IrradianceBake. */
#define SURFEL_GROUP_SIZE 256
#define SURFEL_LIST_GROUP_SIZE 256
#define IRRADIANCE_GRID_GROUP_SIZE 4 /* In each dimension, so 4x4x4 workgroup size. */
#define IRRADIANCE_GRID_BRICK_SIZE 4 /* In each dimension, so 4x4x4 brick size. */
#define IRRADIANCE_BOUNDS_GROUP_SIZE 64

/* Volumes. */
#define VOLUME_GROUP_SIZE 4
#define VOLUME_INTEGRATION_GROUP_SIZE 8
#define VOLUME_HIT_DEPTH_MAX 16

/* Velocity. */
#define VERTEX_COPY_GROUP_SIZE 64

/* Resource bindings. */

/* Textures. */
/* Used anywhere. (Starts at index 2, since 0 and 1 are used by draw_gpencil) */
#define RBUFS_UTILITY_TEX_SLOT 2
#define HIZ_TEX_SLOT 3
/* Only during surface shading (forward and deferred eval). */
#define SHADOW_TILEMAPS_TEX_SLOT 4
#define SHADOW_ATLAS_TEX_SLOT 5
#define VOLUME_PROBE_TEX_SLOT 6
#define SPHERE_PROBE_TEX_SLOT 7
#define VOLUME_SCATTERING_TEX_SLOT 8
#define VOLUME_TRANSMITTANCE_TEX_SLOT 9
/* Currently only used by ray-tracing, but might become used by forward too. */
#define PLANAR_PROBE_DEPTH_TEX_SLOT 10
#define PLANAR_PROBE_RADIANCE_TEX_SLOT 11

/* Images. */
#define RBUFS_COLOR_SLOT 0
#define RBUFS_VALUE_SLOT 1
#define RBUFS_CRYPTOMATTE_SLOT 2
#define GBUF_CLOSURE_SLOT 3
#define GBUF_NORMAL_SLOT 4
#define GBUF_HEADER_SLOT 5
/* Volume properties pass do not write to `rbufs`. Reuse the same bind points. */
#define VOLUME_PROP_SCATTERING_IMG_SLOT 0
#define VOLUME_PROP_EXTINCTION_IMG_SLOT 1
#define VOLUME_PROP_EMISSION_IMG_SLOT 2
#define VOLUME_PROP_PHASE_IMG_SLOT 3
#define VOLUME_OCCUPANCY_SLOT 4
/* Only during volume pre-pass. */
#define VOLUME_HIT_DEPTH_SLOT 0
#define VOLUME_HIT_COUNT_SLOT 1
/* Only during shadow rendering. */
#define SHADOW_ATLAS_IMG_SLOT 4

/* Uniform Buffers. */
/* Slot 0 is GPU_NODE_TREE_UBO_SLOT. */
#define UNIFORM_BUF_SLOT 1
/* Only during surface shading (forward and deferred eval). */
#define IRRADIANCE_GRID_BUF_SLOT 2
#define SPHERE_PROBE_BUF_SLOT 3
#define PLANAR_PROBE_BUF_SLOT 4
/* Only during pre-pass. */
#define VELOCITY_CAMERA_PREV_BUF 2
#define VELOCITY_CAMERA_CURR_BUF 3
#define VELOCITY_CAMERA_NEXT_BUF 4
#define CLIP_PLANE_BUF 5

/* Storage Buffers. */
#define LIGHT_CULL_BUF_SLOT 0
#define LIGHT_BUF_SLOT 1
#define LIGHT_ZBIN_BUF_SLOT 2
#define LIGHT_TILE_BUF_SLOT 3
#define IRRADIANCE_BRICK_BUF_SLOT 4
#define SAMPLING_BUF_SLOT 6
#define CRYPTOMATTE_BUF_SLOT 7
/* Only during surface capture. */
#define SURFEL_BUF_SLOT 4
/* Only during surface capture. */
#define CAPTURE_BUF_SLOT 5
/* Only during shadow rendering. */
#define SHADOW_RENDER_MAP_BUF_SLOT 3
#define SHADOW_PAGE_INFO_SLOT 4
#define SHADOW_VIEWPORT_INDEX_BUF_SLOT 5

/* Only during pre-pass. */
#define VELOCITY_OBJ_PREV_BUF_SLOT 0
#define VELOCITY_OBJ_NEXT_BUF_SLOT 1
#define VELOCITY_GEO_PREV_BUF_SLOT 2
#define VELOCITY_GEO_NEXT_BUF_SLOT 3
#define VELOCITY_INDIRECTION_BUF_SLOT 4

/* Treat closure as singular if the roughness is below this threshold. */
#define BSDF_ROUGHNESS_THRESHOLD 2e-2

/////////////////////////////// Source file 11/////////////////////////////




/**
 * Shared structures, enums & defines between C++ and GLSL.
 * Can also include some math functions but they need to be simple enough to be valid in both
 * language.
 */

#ifndef USE_GPU_SHADER_CREATE_INFO
#  pragma once

#  include  BLI_math_bits.h 
#  include  BLI_memory_utils.hh 

#  include  DRW_gpu_wrapper.hh 

#  include  draw_manager.hh 
#  include  draw_pass.hh 

#  include  eevee_defines.hh 

#  include  GPU_shader_shared.hh 

namespace blender::eevee {

class ShadowDirectional;
class ShadowPunctual;

using namespace draw;

constexpr GPUSamplerState no_filter = GPUSamplerState::default_sampler();
constexpr GPUSamplerState with_filter = {GPU_SAMPLER_FILTERING_LINEAR};
#endif

/* __cplusplus is true when compiling with MSL, so ensure we are not inside a shader. */
#ifdef GPU_SHADER
#  define IS_CPP 0
#else
#  define IS_CPP 1
#endif

#define UBO_MIN_MAX_SUPPORTED_SIZE 1 << 14

/* -------------------------------------------------------------------- */
/** \name Debug Mode
 * \{ */

/** These are just to make more sense of G.debug_value's values. Reserved range is 1-30. */
#define  eDebugMode  uint
const uint 
  DEBUG_NONE = 0u,
  /**
   * Gradient showing light evaluation hot-spots.
   */
  DEBUG_LIGHT_CULLING = 1u,
  /**
   * Show incorrectly down-sample tiles in red.
   */
  DEBUG_HIZ_VALIDATION = 2u,
  /**
   * Display IrradianceCache surfels.
   */
  DEBUG_IRRADIANCE_CACHE_SURFELS_NORMAL = 3u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_IRRADIANCE = 4u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_VISIBILITY = 5u,
  DEBUG_IRRADIANCE_CACHE_SURFELS_CLUSTER = 6u,
  /**
   * Display IrradianceCache virtual offset.
   */
  DEBUG_IRRADIANCE_CACHE_VIRTUAL_OFFSET = 7u,
  DEBUG_IRRADIANCE_CACHE_VALIDITY = 8u,
  /**
   * Show tiles depending on their status.
   */
  DEBUG_SHADOW_TILEMAPS = 10u,
  /**
   * Show content of shadow map. Used to verify projection code.
   */
  DEBUG_SHADOW_VALUES = 11u,
  /**
   * Show random color for each tile. Verify allocation and LOD assignment.
   */
  DEBUG_SHADOW_TILE_RANDOM_COLOR = 12u,
  /**
   * Show random color for each tile. Verify distribution and LOD transitions.
   */
  DEBUG_SHADOW_TILEMAP_RANDOM_COLOR = 13u,
  /**
   * Show storage cost of each pixel in the gbuffer.
   */
  DEBUG_GBUFFER_STORAGE = 14u,
  /**
   * Show evaluation cost of each pixel.
   */
  DEBUG_GBUFFER_EVALUATION = 15u;

/** \} */

/* -------------------------------------------------------------------- */
/** \name Look-Up Table Generation
 * \{ */

#define  PrecomputeType  uint
const uint 
  LUT_GGX_BRDF_SPLIT_SUM = 0u,
  LUT_GGX_BTDF_IOR_GT_ONE = 1u,
  LUT_GGX_BSDF_SPLIT_SUM = 2u,
  LUT_BURLEY_SSS_PROFILE = 3u,
  LUT_RANDOM_WALK_SSS_PROFILE = 4u;

/** \} */

/* -------------------------------------------------------------------- */
/** \name Sampling
 * \{ */

#define  eSamplingDimension  uint
const uint 
  SAMPLING_FILTER_U = 0u,
  SAMPLING_FILTER_V = 1u,
  SAMPLING_LENS_U = 2u,
  SAMPLING_LENS_V = 3u,
  SAMPLING_TIME = 4u,
  SAMPLING_SHADOW_U = 5u,
  SAMPLING_SHADOW_V = 6u,
  SAMPLING_SHADOW_W = 7u,
  SAMPLING_SHADOW_X = 8u,
  SAMPLING_SHADOW_Y = 9u,
  SAMPLING_CLOSURE = 10u,
  SAMPLING_LIGHTPROBE = 11u,
  SAMPLING_TRANSPARENCY = 12u,
  SAMPLING_SSS_U = 13u,
  SAMPLING_SSS_V = 14u,
  SAMPLING_RAYTRACE_U = 15u,
  SAMPLING_RAYTRACE_V = 16u,
  SAMPLING_RAYTRACE_W = 17u,
  SAMPLING_RAYTRACE_X = 18u,
  SAMPLING_AO_U = 19u,
  SAMPLING_AO_V = 20u,
  SAMPLING_CURVES_U = 21u,
  SAMPLING_VOLUME_U = 22u,
  SAMPLING_VOLUME_V = 23u,
  SAMPLING_VOLUME_W = 24u
;

/**
 * IMPORTANT: Make sure the array can contain all sampling dimensions.
 * Also note that it needs to be multiple of 4.
 */
#define SAMPLING_DIMENSION_COUNT 28

/* NOTE(@fclem): Needs to be used in #StorageBuffer because of arrays of scalar. */
struct SamplingData {
  /** Array containing random values from Low Discrepancy Sequence in [0..1) range. */
  float dimensions[SAMPLING_DIMENSION_COUNT];
};
BLI_STATIC_ASSERT_ALIGN(SamplingData, 16)

/* Returns total sample count in a web pattern of the given size. */
static inline int sampling_web_sample_count_get(int web_density, int in_ring_count)
{
  return ((in_ring_count * in_ring_count + in_ring_count) / 2) * web_density + 1;
}

/* Returns lowest possible ring count that contains at least sample_count samples. */
static inline int sampling_web_ring_count_get(int web_density, int sample_count)
{
  /* Inversion of web_sample_count_get(). */
  float x = 2.0f * (float(sample_count) - 1.0f) / float(web_density);
  /* Solving polynomial. We only search positive solution. */
  float discriminant = 1.0f + 4.0f * x;
  return int(ceilf(0.5f * (sqrtf(discriminant) - 1.0f)));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Camera
 * \{ */

#define  eCameraType  uint
const uint 
  CAMERA_PERSP = 0u,
  CAMERA_ORTHO = 1u,
  CAMERA_PANO_EQUIRECT = 2u,
  CAMERA_PANO_EQUISOLID = 3u,
  CAMERA_PANO_EQUIDISTANT = 4u,
  CAMERA_PANO_MIRROR = 5u
;

static inline bool is_panoramic(eCameraType type)
{
  return type > CAMERA_ORTHO;
}

struct CameraData {
  /* View Matrices of the camera, not from any view! */
  float4x4 persmat;
  float4x4 persinv;
  float4x4 viewmat;
  float4x4 viewinv;
  float4x4 winmat;
  float4x4 wininv;
  /** Camera UV scale and bias. */
  float2 uv_scale;
  float2 uv_bias;
  /** Panorama parameters. */
  float2 equirect_scale;
  float2 equirect_scale_inv;
  float2 equirect_bias;
  float fisheye_fov;
  float fisheye_lens;
  /** Clipping distances. */
  float clip_near;
  float clip_far;
  eCameraType type;
  /** World space distance between view corners at unit distance from camera. */
  float screen_diagonal_length;
  float _pad0;
  float _pad1;
  float _pad2;

  bool32_t initialized;

#ifdef __cplusplus
  /* Small constructor to allow detecting new buffers. */
  CameraData() : initialized(false){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(CameraData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Film
 * \{ */

#define FILM_PRECOMP_SAMPLE_MAX 16

#define  eFilmWeightLayerIndex  uint
const uint 
  FILM_WEIGHT_LAYER_ACCUMULATION = 0u,
  FILM_WEIGHT_LAYER_DISTANCE = 1u;

#define  ePassStorageType  uint
const uint 
  PASS_STORAGE_COLOR = 0u,
  PASS_STORAGE_VALUE = 1u,
  PASS_STORAGE_CRYPTOMATTE = 2u;

#define  PassCategory  uint
const uint 
  PASS_CATEGORY_DATA = 1u << 0,
  PASS_CATEGORY_COLOR_1 = 1u << 1,
  PASS_CATEGORY_COLOR_2 = 1u << 2,
  PASS_CATEGORY_COLOR_3 = 1u << 3,
  PASS_CATEGORY_AOV = 1u << 4,
  PASS_CATEGORY_CRYPTOMATTE = 1u << 5;
ENUM_OPERATORS(PassCategory, PASS_CATEGORY_CRYPTOMATTE)

struct FilmSample {
  int2 texel;
  float weight;
  /** Used for accumulation. */
  float weight_sum_inv;
};
BLI_STATIC_ASSERT_ALIGN(FilmSample, 16)

struct FilmData {
  /** Size of the film in pixels. */
  int2 extent;
  /** Offset to convert from Display space to Film space, in pixels. */
  int2 offset;
  /** Size of the render buffers including overscan when rendering the main views, in pixels. */
  int2 render_extent;
  /**
   * Sub-pixel offset applied to the window matrix.
   * NOTE: In final film pixel unit.
   * NOTE: Positive values makes the view translate in the negative axes direction.
   * NOTE: The origin is the center of the lower left film pixel of the area covered by a render
   * pixel if using scaled resolution rendering.
   */
  float2 subpixel_offset;
  /** Scaling factor to convert texel to uvs. */
  float2 extent_inv;
  /**
   * Number of border pixels on all sides inside the render_extent that do not contribute to the
   * final image.
   */
  int overscan;
  /** Is true if history is valid and can be sampled. Bypass history to resets accumulation. */
  bool32_t use_history;
  /** Controlled by user in lookdev mode or by render settings. */
  float background_opacity;
  /** Output counts per type. */
  int color_len, value_len;
  /** Index in color_accum_img or value_accum_img of each pass. -1 if pass is not enabled. */
  int mist_id;
  int normal_id;
  int position_id;
  int vector_id;
  int diffuse_light_id;
  int diffuse_color_id;
  int specular_light_id;
  int specular_color_id;
  int volume_light_id;
  int emission_id;
  int environment_id;
  int shadow_id;
  int ambient_occlusion_id;
  int transparent_id;
  /** Not indexed but still not -1 if enabled. */
  int depth_id;
  int combined_id;
  /** Id of the render-pass to be displayed. -1 for combined. */
  int display_id;
  /** Storage type of the render-pass to be displayed. */
  ePassStorageType display_storage_type;
  /** True if we bypass the accumulation and directly output the accumulation buffer. */
  bool32_t display_only;
  /** Start of AOVs and number of aov. */
  int aov_color_id, aov_color_len;
  int aov_value_id, aov_value_len;
  /** Start of cryptomatte per layer (-1 if pass is not enabled). */
  int cryptomatte_object_id;
  int cryptomatte_asset_id;
  int cryptomatte_material_id;
  /** Max number of samples stored per layer (is even number). */
  int cryptomatte_samples_len;
  /** Settings to render mist pass */
  float mist_scale, mist_bias, mist_exponent;
  /** Scene exposure used for better noise reduction. */
  float exposure_scale;
  /** Scaling factor for scaled resolution rendering. */
  int scaling_factor;
  /** Film pixel filter radius. */
  float filter_radius;
  /** Precomputed samples. First in the table is the closest one. The rest is unordered. */
  int samples_len;
  /** Sum of the weights of all samples in the sample table. */
  float samples_weight_total;
  int _pad1;
  int _pad2;
  FilmSample samples[FILM_PRECOMP_SAMPLE_MAX];
};
BLI_STATIC_ASSERT_ALIGN(FilmData, 16)

static inline float film_filter_weight(float filter_radius, float sample_distance_sqr)
{
#if 1 /* Faster */
  /* Gaussian fitted to Blackman-Harris. */
  float r = sample_distance_sqr / (filter_radius * filter_radius);
  const float sigma = 0.284;
  const float fac = -0.5 / (sigma * sigma);
  float weight = expf(fac * r);
#else
  /* Blackman-Harris filter. */
  float r = M_TAU * saturate(0.5 + sqrtf(sample_distance_sqr) / (2.0 * filter_radius));
  float weight = 0.35875 - 0.48829 * cosf(r) + 0.14128 * cosf(2.0 * r) - 0.01168 * cosf(3.0 * r);
#endif
  return weight;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name RenderBuffers
 * \{ */

/* Theoretical max is 128 as we are using texture array and VRAM usage.
 * However, the output_aov() function perform a linear search inside all the hashes.
 * If we find a way to avoid this we could bump this number up. */
#define AOV_MAX 16

struct AOVsInfoData {
  /* Use uint4 to workaround std140 packing rules.
   * Only the x value is used. */
  uint4 hash_value[AOV_MAX];
  uint4 hash_color[AOV_MAX];
  /* Length of used data. */
  int color_len;
  int value_len;
  /** Id of the AOV to be displayed (from the start of the AOV array). -1 for combined. */
  int display_id;
  /** True if the AOV to be displayed is from the value accumulation buffer. */
  bool32_t display_is_value;
};
BLI_STATIC_ASSERT_ALIGN(AOVsInfoData, 16)

struct RenderBuffersInfoData {
  AOVsInfoData aovs;
  /* Color. */
  int color_len;
  int normal_id;
  int position_id;
  int diffuse_light_id;
  int diffuse_color_id;
  int specular_light_id;
  int specular_color_id;
  int volume_light_id;
  int emission_id;
  int environment_id;
  int transparent_id;
  /* Value */
  int value_len;
  int shadow_id;
  int ambient_occlusion_id;
  int _pad0, _pad1;
};
BLI_STATIC_ASSERT_ALIGN(RenderBuffersInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name VelocityModule
 * \{ */

#define VELOCITY_INVALID 512.0

#define  eVelocityStep  uint
const uint 
  STEP_PREVIOUS = 0,
  STEP_NEXT = 1,
  STEP_CURRENT = 2;

struct VelocityObjectIndex {
  /** Offset inside #VelocityObjectBuf for each time-step. Indexed using eVelocityStep. */
  packed_int3 ofs;
  /** Temporary index to copy this to the #VelocityIndexBuf. */
  uint resource_id;

#ifdef __cplusplus
  VelocityObjectIndex() : ofs(-1, -1, -1), resource_id(-1){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(VelocityObjectIndex, 16)

struct VelocityGeometryIndex {
  /** Offset inside #VelocityGeometryBuf for each time-step. Indexed using eVelocityStep. */
  packed_int3 ofs;
  /** If true, compute deformation motion blur. */
  bool32_t do_deform;
  /**
   * Length of data inside #VelocityGeometryBuf for each time-step.
   * Indexed using eVelocityStep.
   */
  packed_int3 len;

  int _pad0;

#ifdef __cplusplus
  VelocityGeometryIndex() : ofs(-1, -1, -1), do_deform(false), len(-1, -1, -1), _pad0(1){};
#endif
};
BLI_STATIC_ASSERT_ALIGN(VelocityGeometryIndex, 16)

struct VelocityIndex {
  VelocityObjectIndex obj;
  VelocityGeometryIndex geo;
};
BLI_STATIC_ASSERT_ALIGN(VelocityGeometryIndex, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Motion Blur
 * \{ */

#define MOTION_BLUR_TILE_SIZE 32
#define MOTION_BLUR_MAX_TILE 512 /* 16384 / MOTION_BLUR_TILE_SIZE */
struct MotionBlurData {
  /** As the name suggests. Used to avoid a division in the sampling. */
  float2 target_size_inv;
  /** Viewport motion scaling factor. Make blur relative to frame time not render time. */
  float2 motion_scale;
  /** Depth scaling factor. Avoid blurring background behind moving objects. */
  float depth_scale;

  float _pad0, _pad1, _pad2;
};
BLI_STATIC_ASSERT_ALIGN(MotionBlurData, 16)

/* For some reasons some GLSL compilers do not like this struct.
 * So we declare it as a uint array instead and do indexing ourselves. */
#ifdef __cplusplus
struct MotionBlurTileIndirection {
  /**
   * Stores indirection to the tile with the highest velocity covering each tile.
   * This is stored using velocity in the MSB to be able to use atomicMax operations.
   */
  uint prev[MOTION_BLUR_MAX_TILE][MOTION_BLUR_MAX_TILE];
  uint next[MOTION_BLUR_MAX_TILE][MOTION_BLUR_MAX_TILE];
};
BLI_STATIC_ASSERT_ALIGN(MotionBlurTileIndirection, 16)
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volumes
 * \{ */

struct VolumesInfoData {
  /* During object voxelization, we need to use an infinite projection matrix to avoid clipping
   * faces. But they cannot be used for recovering the view position from froxel position as they
   * are not invertible. We store the finite projection matrix and use it for this purpose. */
  float4x4 winmat_finite;
  float4x4 wininv_finite;
  /* Copies of the matrices above but without jittering. Used for re-projection. */
  float4x4 wininv_stable;
  float4x4 winmat_stable;
  /* Previous render sample copy of winmat_stable. */
  float4x4 history_winmat_stable;
  /* Transform from current view space to previous render sample view space. */
  float4x4 curr_view_to_past_view;
  /* Size of the froxel grid texture. */
  packed_int3 tex_size;
  /* Maximum light intensity during volume lighting evaluation. */
  float light_clamp;
  /* Inverse of size of the froxel grid. */
  packed_float3 inv_tex_size;
  /* Maximum light intensity during volume lighting evaluation. */
  float shadow_steps;
  /* 2D scaling factor to make froxel squared. */
  float2 coord_scale;
  /* Extent and inverse extent of the main shading view (render extent, not film extent). */
  float2 main_view_extent;
  float2 main_view_extent_inv;
  /* Size in main view pixels of one froxel in XY. */
  int tile_size;
  /* Hi-Z LOD to use during volume shadow tagging. */
  int tile_size_lod;
  /* Depth to froxel mapping. */
  float depth_near;
  float depth_far;
  float depth_distribution;
  /* Previous render sample copy of the depth mapping parameters. */
  float history_depth_near;
  float history_depth_far;
  float history_depth_distribution;
  /* Amount of history to blend during the scatter phase. */
  float history_opacity;

  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(VolumesInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Depth of field
 * \{ */

/* 5% error threshold. */
#define DOF_FAST_GATHER_COC_ERROR 0.05
#define DOF_GATHER_RING_COUNT 5
#define DOF_DILATE_RING_COUNT 3

struct DepthOfFieldData {
  /** Size of the render targets for gather & scatter passes. */
  int2 extent;
  /** Size of a pixel in uv space (1.0 / extent). */
  float2 texel_size;
  /** Scale factor for anisotropic bokeh. */
  float2 bokeh_anisotropic_scale;
  float2 bokeh_anisotropic_scale_inv;
  /* Correction factor to align main target pixels with the filtered mipmap chain texture. */
  float2 gather_uv_fac;
  /** Scatter parameters. */
  float scatter_coc_threshold;
  float scatter_color_threshold;
  float scatter_neighbor_max_color;
  int scatter_sprite_per_row;
  /** Number of side the bokeh shape has. */
  float bokeh_blades;
  /** Rotation of the bokeh shape. */
  float bokeh_rotation;
  /** Multiplier and bias to apply to linear depth to Circle of confusion (CoC). */
  float coc_mul, coc_bias;
  /** Maximum absolute allowed Circle of confusion (CoC). Min of computed max and user max. */
  float coc_abs_max;
  /** Copy of camera type. */
  eCameraType camera_type;
  /** Weights of spatial filtering in stabilize pass. Not array to avoid alignment restriction. */
  float4 filter_samples_weight;
  float filter_center_weight;
  /** Max number of sprite in the scatter pass for each ground. */
  int scatter_max_rect;

  int _pad0, _pad1;
};
BLI_STATIC_ASSERT_ALIGN(DepthOfFieldData, 16)

struct ScatterRect {
  /** Color and CoC of the 4 pixels the scatter sprite represents. */
  float4 color_and_coc[4];
  /** Rect center position in half pixel space. */
  float2 offset;
  /** Rect half extent in half pixel space. */
  float2 half_extent;
};
BLI_STATIC_ASSERT_ALIGN(ScatterRect, 16)

/** WORKAROUND(@fclem): This is because this file is included before common_math_lib.glsl. */
#ifndef M_PI
#  define EEVEE_PI
#  define M_PI 3.14159265358979323846 /* pi */
#endif

static inline float coc_radius_from_camera_depth(DepthOfFieldData dof, float depth)
{
  depth = (dof.camera_type != CAMERA_ORTHO) ? 1.0f / depth : depth;
  return dof.coc_mul * depth + dof.coc_bias;
}

static inline float regular_polygon_side_length(float sides_count)
{
  return 2.0f * sinf(M_PI / sides_count);
}

/* Returns intersection ratio between the radius edge at theta and the regular polygon edge.
 * Start first corners at theta == 0. */
static inline float circle_to_polygon_radius(float sides_count, float theta)
{
  /* From Graphics Gems from CryENGINE 3 (SIGGRAPH 2013) by Tiago Sousa (slide 36). */
  float side_angle = (2.0f * M_PI) / sides_count;
  return cosf(side_angle * 0.5f) /
         cosf(theta - side_angle * floorf((sides_count * theta + M_PI) / (2.0f * M_PI)));
}

/* Remap input angle to have homogenous spacing of points along a polygon edge.
 * Expects theta to be in [0..2pi] range. */
static inline float circle_to_polygon_angle(float sides_count, float theta)
{
  float side_angle = (2.0f * M_PI) / sides_count;
  float halfside_angle = side_angle * 0.5f;
  float side = floorf(theta / side_angle);
  /* Length of segment from center to the middle of polygon side. */
  float adjacent = circle_to_polygon_radius(sides_count, 0.0f);

  /* This is the relative position of the sample on the polygon half side. */
  float local_theta = theta - side * side_angle;
  float ratio = (local_theta - halfside_angle) / halfside_angle;

  float halfside_len = regular_polygon_side_length(sides_count) * 0.5f;
  float opposite = ratio * halfside_len;

  /* NOTE: atan(y_over_x) has output range [-M_PI_2..M_PI_2]. */
  float final_local_theta = atanf(opposite / adjacent);

  return side * side_angle + final_local_theta;
}

#ifdef EEVEE_PI
#  undef M_PI
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light Culling
 * \{ */

/* Number of items we can cull. Limited by how we store CullingZBin. */
#define CULLING_MAX_ITEM 65536
/* Fine grained subdivision in the Z direction. Limited by the LDS in z-binning compute shader. */
#define CULLING_ZBIN_COUNT 4096
/* Max tile map resolution per axes. */
#define CULLING_TILE_RES 16

struct LightCullingData {
  /** Scale applied to tile pixel coordinates to get target UV coordinate. */
  float2 tile_to_uv_fac;
  /** Scale and bias applied to linear Z to get zbin. */
  float zbin_scale;
  float zbin_bias;
  /** Valid item count in the source data array. */
  uint items_count;
  /** Items that are processed by the 2.5D culling. */
  uint local_lights_len;
  /** Items that are **NOT** processed by the 2.5D culling (i.e: Sun Lights). */
  uint sun_lights_len;
  /** Number of items that passes the first culling test. (local lights only) */
  uint visible_count;
  /** Extent of one square tile in pixels. */
  float tile_size;
  /** Number of tiles on the X/Y axis. */
  uint tile_x_len;
  uint tile_y_len;
  /** Number of word per tile. Depends on the maximum number of lights. */
  uint tile_word_len;
};
BLI_STATIC_ASSERT_ALIGN(LightCullingData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Lights
 * \{ */

#define LIGHT_NO_SHADOW -1

#define  eLightType  uint
const uint 
  LIGHT_SUN = 0u,
  LIGHT_SUN_ORTHO = 1u,
  /* Point light. */
  LIGHT_OMNI_SPHERE = 10u,
  LIGHT_OMNI_DISK = 11u,
  /* Spot light. */
  LIGHT_SPOT_SPHERE = 12u,
  LIGHT_SPOT_DISK = 13u,
  /* Area light. */
  LIGHT_RECT = 20u,
  LIGHT_ELLIPSE = 21u
;

#define  LightingType  uint
const uint 
  LIGHT_DIFFUSE = 0u,
  LIGHT_SPECULAR = 1u,
  LIGHT_TRANSMISSION = 2u,
  LIGHT_VOLUME = 3u;

static inline bool is_area_light(eLightType type)
{
  return type >= LIGHT_RECT;
}

static inline bool is_point_light(eLightType type)
{
  return type >= LIGHT_OMNI_SPHERE && type <= LIGHT_SPOT_DISK;
}

static inline bool is_spot_light(eLightType type)
{
  return type == LIGHT_SPOT_SPHERE || type == LIGHT_SPOT_DISK;
}

static inline bool is_sphere_light(eLightType type)
{
  return type == LIGHT_SPOT_SPHERE || type == LIGHT_OMNI_SPHERE;
}

static inline bool is_oriented_disk_light(eLightType type)
{
  return type == LIGHT_SPOT_DISK || type == LIGHT_OMNI_DISK;
}

static inline bool is_sun_light(eLightType type)
{
  return type < LIGHT_OMNI_SPHERE;
}

static inline bool is_local_light(eLightType type)
{
  return type >= LIGHT_OMNI_SPHERE;
}

/* Using define because GLSL doesn't have inheritance, and encapsulation forces us to add some
 * unneeded padding. */
#define LOCAL_LIGHT_COMMON \
  /** Special radius factor for point lighting (volume). */ \
  float radius_squared; \
  /** Maximum influence radius. Used for culling. Equal to clip far distance. */ \
  float influence_radius_max; \
  /** Influence radius (inverted and squared) adjusted for Surface / Volume power. */ \
  float influence_radius_invsqr_surface; \
  float influence_radius_invsqr_volume; \
  /** --- Shadow Data --- */ \
  /** Other parts of the perspective matrix. Assumes symmetric frustum. */ \
  float clip_side; \
  /** Number of allocated tilemap for this local light. */ \
  int tilemaps_count; \
  /** Scaling factor to the light shape for shadow ray casting. */ \
  float shadow_scale; \
  /** Shift to apply to the light origin to get the shadow projection origin. */ \
  float shadow_projection_shift;

/* Untyped local light data. Gets reinterpreted to LightSpotData and LightAreaData.
 * Allow access to local light common data without casting. */
struct LightLocalData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  float _pad1;
  float _pad2;

  float2 _pad3;
  float _pad4;
  float _pad5;
};
BLI_STATIC_ASSERT_ALIGN(LightLocalData, 16)

/* Despite the name, is also used for omni light. */
struct LightSpotData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  /** Sphere light radius. */
  float radius;
  /** Scale and bias to spot equation parameter. Used for adjusting the falloff. */
  float spot_mul;

  /** Inverse spot size (in X and Y axes). */
  float2 spot_size_inv;
  /** Spot angle tangent. */
  float spot_tan;
  float spot_bias;
};
BLI_STATIC_ASSERT(sizeof(LightSpotData) == sizeof(LightLocalData),  Data size must match )

struct LightAreaData {
  LOCAL_LIGHT_COMMON

  /** Padding reserved for when shadow_projection_shift will become a vec3. */
  float _pad0_reserved;
  float _pad1_reserved;
  float _pad2;
  float _pad3;

  /** Shape size. */
  float2 size;
  float _pad5;
  float _pad6;
};
BLI_STATIC_ASSERT(sizeof(LightAreaData) == sizeof(LightLocalData),  Data size must match )

struct LightSunData {
  float radius;
  float _pad0;
  float _pad1;
  float _pad2;

  float _pad3;
  float _pad4;
  /** --- Shadow Data --- */
  /** Offset of the LOD min in LOD min tile units. Split positive and negative for bit-shift. */
  int2 clipmap_base_offset_neg;

  int2 clipmap_base_offset_pos;
  /** Angle covered by the light shape for shadow ray casting. */
  float shadow_angle;
  /** Trace distance around the shading point. */
  float shadow_trace_distance;

  /** Offset to convert from world units to tile space of the clipmap_lod_max. */
  float2 clipmap_origin;
  /** Clip-map LOD range to avoid sampling outside of valid range. */
  int clipmap_lod_min;
  int clipmap_lod_max;
};
BLI_STATIC_ASSERT(sizeof(LightSunData) == sizeof(LightLocalData),  Data size must match )

/* Enable when debugging. This is quite costly. */
#define SAFE_UNION_ACCESS 0

#if IS_CPP
/* C++ always uses union. */
#  define USE_LIGHT_UNION 1
#elif defined(GPU_BACKEND_METAL) && !SAFE_UNION_ACCESS
/* Metal supports union, but force usage of the getters if SAFE_UNION_ACCESS is enabled. */
#  define USE_LIGHT_UNION 1
#else
/* Use getter functions on GPU if not supported or if SAFE_UNION_ACCESS is enabled. */
#  define USE_LIGHT_UNION 0
#endif

struct LightData {
  /** Normalized object to world matrix. */
  /* TODO(fclem): Use float4x3. */
  float4x4 object_mat;
  /** Aliases for axes. */
#ifndef USE_GPU_SHADER_CREATE_INFO
#  define _right object_mat[0]
#  define _up object_mat[1]
#  define _back object_mat[2]
#  define _position object_mat[3]
#else
#  define _right object_mat[0].xyz
#  define _up object_mat[1].xyz
#  define _back object_mat[2].xyz
#  define _position object_mat[3].xyz
#endif

  /** Power depending on shader type. Referenced by LightingType. */
  float4 power;
  /** Light Color. */
  packed_float3 color;
  /** Light Type. */
  eLightType type;

  /** --- Shadow Data --- */
  /** Near clip distances. Float stored as orderedIntBitsToFloat for atomic operations. */
  int clip_near;
  int clip_far;
  /** Index of the first tile-map. Set to LIGHT_NO_SHADOW if light is not casting shadow. */
  int tilemap_index;
  /* Radius in pixels for shadow filtering. */
  float pcf_radius;

  /* Shadow Map resolution bias. */
  float lod_bias;
  float _pad0;
  float _pad1;
  float _pad2;

#if USE_LIGHT_UNION
  union {
    LightLocalData local;
    LightSpotData spot;
    LightAreaData area;
    LightSunData sun;
  };
#else
  /* Use `light_*_data_get(light)` to access typed data. */
  LightLocalData do_not_access_directly;
#endif
};
BLI_STATIC_ASSERT_ALIGN(LightData, 16)

#ifdef GPU_SHADER
#  define CHECK_TYPE_PAIR(a, b)
#  define CHECK_TYPE(a, b)
#  define FLOAT_AS_INT floatBitsToInt
#  define TYPECAST_NOOP

#else /* C++ */
#  define FLOAT_AS_INT float_as_int
#  define TYPECAST_NOOP
#endif

/* In addition to the static asserts that verify correct member assignment, also verify access on
 * the GPU so that only lights of a certain type can read for the appropriate union member.
 * Return cross platform garbage data as some platform can return cleared memory if we early exit.
 */
#if SAFE_UNION_ACCESS
#  ifdef GPU_SHADER
/* Should result in a beautiful zebra pattern on invalid load. */
#    if defined(GPU_FRAGMENT_SHADER)
#      define GARBAGE_VALUE sin(gl_FragCoord.x + gl_FragCoord.y)
#    elif defined(GPU_COMPUTE_SHADER)
#      define GARBAGE_VALUE \
        sin(float(gl_GlobalInvocationID.x + gl_GlobalInvocationID.y + gl_GlobalInvocationID.z))
#    else
#      define GARBAGE_VALUE sin(float(gl_VertexID))
#    endif

/* Can be set to zero if zebra creates out-of-bound accesses and crashes. At least avoid UB. */
// #    define GARBAGE_VALUE 0.0

#  else /* C++ */
#    define GARBAGE_VALUE 0.0f
#  endif

#  define SAFE_BEGIN(data_type, check) \
    data_type data; \
    bool _validity_check = check; \
    float _garbage = GARBAGE_VALUE;

/* Assign garbage value if the light type check fails. */
#  define SAFE_ASSIGN_LIGHT_TYPE_CHECK(_type, _value) \
    (_validity_check ? (_value) : _type(_garbage))
#else
#  define SAFE_BEGIN(data_type, check) data_type data;
#  define SAFE_ASSIGN_LIGHT_TYPE_CHECK(_type, _value) _value
#endif

#if USE_LIGHT_UNION
#  define DATA_MEMBER local
#else
#  define DATA_MEMBER do_not_access_directly
#endif

#define ERROR_OFS(a, b)  Offset of   STRINGIFY(a)   mismatch offset of   STRINGIFY(b)

/* This is a dangerous process, make sure to static assert every assignment. */
#define SAFE_ASSIGN(a, reinterpret_fn, in_type, b) \
  CHECK_TYPE_PAIR(data.a, reinterpret_fn(light.DATA_MEMBER.b)); \
  data.a = reinterpret_fn(SAFE_ASSIGN_LIGHT_TYPE_CHECK(in_type, light.DATA_MEMBER.b)); \
  BLI_STATIC_ASSERT(offsetof(decltype(data), a) == offsetof(LightLocalData, b), ERROR_OFS(a, b))

#define SAFE_ASSIGN_FLOAT(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, float, b);
#define SAFE_ASSIGN_FLOAT2(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, float2, b);
#define SAFE_ASSIGN_INT(a, b) SAFE_ASSIGN(a, TYPECAST_NOOP, int, b);
#define SAFE_ASSIGN_FLOAT_AS_INT(a, b) SAFE_ASSIGN(a, FLOAT_AS_INT, float, b);
#define SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(a, b, c) \
  SAFE_ASSIGN_FLOAT_AS_INT(a.x, b); \
  SAFE_ASSIGN_FLOAT_AS_INT(a.y, c);

#if !USE_LIGHT_UNION || IS_CPP

/* These functions are not meant to be used in C++ code. They are only defined on the C++ side for
 * static assertions. Hide them. */
#  if IS_CPP
namespace do_not_use {
#  endif

static inline LightSpotData light_local_data_get(LightData light)
{
  SAFE_BEGIN(LightSpotData, is_local_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  return data;
}

static inline LightSpotData light_spot_data_get(LightData light)
{
  SAFE_BEGIN(LightSpotData, is_spot_light(light.type) || is_point_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  SAFE_ASSIGN_FLOAT(radius, _pad1)
  SAFE_ASSIGN_FLOAT(spot_mul, _pad2)
  SAFE_ASSIGN_FLOAT2(spot_size_inv, _pad3)
  SAFE_ASSIGN_FLOAT(spot_tan, _pad4)
  SAFE_ASSIGN_FLOAT(spot_bias, _pad5)
  return data;
}

static inline LightAreaData light_area_data_get(LightData light)
{
  SAFE_BEGIN(LightAreaData, is_area_light(light.type))
  SAFE_ASSIGN_FLOAT(radius_squared, radius_squared)
  SAFE_ASSIGN_FLOAT(influence_radius_max, influence_radius_max)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_surface, influence_radius_invsqr_surface)
  SAFE_ASSIGN_FLOAT(influence_radius_invsqr_volume, influence_radius_invsqr_volume)
  SAFE_ASSIGN_FLOAT(clip_side, clip_side)
  SAFE_ASSIGN_FLOAT(shadow_scale, shadow_scale)
  SAFE_ASSIGN_FLOAT(shadow_projection_shift, shadow_projection_shift)
  SAFE_ASSIGN_INT(tilemaps_count, tilemaps_count)
  SAFE_ASSIGN_FLOAT2(size, _pad3)
  return data;
}

static inline LightSunData light_sun_data_get(LightData light)
{
  SAFE_BEGIN(LightSunData, is_sun_light(light.type))
  SAFE_ASSIGN_FLOAT(radius, radius_squared)
  SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(clipmap_base_offset_neg, shadow_scale, shadow_projection_shift)
  SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE(clipmap_base_offset_pos, _pad0_reserved, _pad1_reserved)
  SAFE_ASSIGN_FLOAT(shadow_angle, _pad1)
  SAFE_ASSIGN_FLOAT(shadow_trace_distance, _pad2)
  SAFE_ASSIGN_FLOAT2(clipmap_origin, _pad3)
  SAFE_ASSIGN_FLOAT_AS_INT(clipmap_lod_min, _pad4)
  SAFE_ASSIGN_FLOAT_AS_INT(clipmap_lod_max, _pad5)
  return data;
}

#  if IS_CPP
}  // namespace do_not_use
#  endif

#endif

#if USE_LIGHT_UNION
#  define light_local_data_get(light) light.local
#  define light_spot_data_get(light) light.spot
#  define light_area_data_get(light) light.area
#  define light_sun_data_get(light) light.sun
#endif

#undef DATA_MEMBER
#undef GARBAGE_VALUE
#undef FLOAT_AS_INT
#undef TYPECAST_NOOP
#undef SAFE_BEGIN
#undef SAFE_ASSIGN_LIGHT_TYPE_CHECK
#undef ERROR_OFS
#undef SAFE_ASSIGN
#undef SAFE_ASSIGN_FLOAT
#undef SAFE_ASSIGN_FLOAT2
#undef SAFE_ASSIGN_INT
#undef SAFE_ASSIGN_FLOAT_AS_INT
#undef SAFE_ASSIGN_FLOAT_AS_INT2_COMBINE

static inline int light_tilemap_max_get(LightData light)
{
  /* This is not something we need in performance critical code. */
  if (is_sun_light(light.type)) {
    return light.tilemap_index +
           (light_sun_data_get(light).clipmap_lod_max - light_sun_data_get(light).clipmap_lod_min);
  }
  return light.tilemap_index + light_local_data_get(light).tilemaps_count - 1;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Shadows
 *
 * Shadow data for either a directional shadow or a punctual shadow.
 *
 * A punctual shadow is composed of 1, 5 or 6 shadow regions.
 * Regions are sorted in this order -Z, +X, -X, +Y, -Y, +Z.
 * Face index is computed from light's object space coordinates.
 *
 * A directional light shadow is composed of multiple clip-maps with each level
 * covering twice as much area as the previous one.
 * \{ */

#define  eShadowProjectionType  uint
const uint 
  SHADOW_PROJECTION_CUBEFACE = 0u,
  SHADOW_PROJECTION_CLIPMAP = 1u,
  SHADOW_PROJECTION_CASCADE = 2u;

static inline int2 shadow_cascade_grid_offset(int2 base_offset, int level_relative)
{
  return (base_offset * level_relative) / (1 << 16);
}

/**
 * Small descriptor used for the tile update phase. Updated by CPU & uploaded to GPU each redraw.
 */
struct ShadowTileMapData {
  /** Cached, used for rendering. */
  float4x4 viewmat;
  /** Precomputed matrix, not used for rendering but for tagging. */
  float4x4 winmat;
  /** Punctual : Corners of the frustum. (vec3 padded to vec4) */
  float4 corners[4];
  /** Integer offset of the center of the 16x16 tiles from the origin of the tile space. */
  int2 grid_offset;
  /** Shift between previous and current grid_offset. Allows update tagging. */
  int2 grid_shift;
  /** True for punctual lights. */
  eShadowProjectionType projection_type;
  /** Multiple of SHADOW_TILEDATA_PER_TILEMAP. Offset inside the tile buffer. */
  int tiles_index;
  /** Index of persistent data in the persistent data buffer. */
  int clip_data_index;
  /** Bias LOD to tag for usage to lower the amount of tile used. */
  float lod_bias;
  int _pad0;
  int _pad1;
  int _pad2;
  /** Near and far clip distances for punctual. */
  float clip_near;
  float clip_far;
  /** Half of the tilemap size in world units. Used to compute window matrix. */
  float half_size;
  /** Offset in local space to the tilemap center in world units. Used for directional winmat. */
  float2 center_offset;
};
BLI_STATIC_ASSERT_ALIGN(ShadowTileMapData, 16)

/**
 * Per tilemap data persistent on GPU.
 */
struct ShadowTileMapClip {
  /** Clip distances that were used to render the pages. */
  float clip_near_stored;
  float clip_far_stored;
  /** Near and far clip distances for directional. Float stored as int for atomic operations. */
  /** NOTE: These are positive just like camera parameters. */
  int clip_near;
  int clip_far;
};
BLI_STATIC_ASSERT_ALIGN(ShadowTileMapClip, 16)

struct ShadowPagesInfoData {
  /** Number of free pages in the free page buffer. */
  int page_free_count;
  /** Number of page allocations needed for this cycle. */
  int page_alloc_count;
  /** Index of the next cache page in the cached page buffer. */
  uint page_cached_next;
  /** Index of the first page in the buffer since the last defragment. */
  uint page_cached_start;
  /** Index of the last page in the buffer since the last defragment. */
  uint page_cached_end;

  int _pad0;
  int _pad1;
  int _pad2;
};
BLI_STATIC_ASSERT_ALIGN(ShadowPagesInfoData, 16)

struct ShadowStatistics {
  /** Statistics that are read back to CPU after a few frame (to avoid stall). */
  int page_used_count;
  int page_update_count;
  int page_allocated_count;
  int page_rendered_count;
  int view_needed_count;
  int _pad0;
  int _pad1;
  int _pad2;
};
BLI_STATIC_ASSERT_ALIGN(ShadowStatistics, 16)

/** Decoded tile data structure. */
struct ShadowTileData {
  /** Page inside the virtual shadow map atlas. */
  uint3 page;
  /** Page index inside pages_cached_buf. Only valid if `is_cached` is true. */
  uint cache_index;
  /** If the tile is needed for rendering. */
  bool is_used;
  /** True if an update is needed. This persists even if the tile gets unused. */
  bool do_update;
  /** True if the tile owns the page (mutually exclusive with `is_cached`). */
  bool is_allocated;
  /** True if the tile has been staged for rendering. This will remove the `do_update` flag. */
  bool is_rendered;
  /** True if the tile is inside the pages_cached_buf (mutually exclusive with `is_allocated`). */
  bool is_cached;
};
/** \note Stored packed as a uint. */
#define ShadowTileDataPacked uint

#define  eShadowFlag  uint
const uint 
  SHADOW_NO_DATA = 0u,
  SHADOW_IS_CACHED = (1u << 27u),
  SHADOW_IS_ALLOCATED = (1u << 28u),
  SHADOW_DO_UPDATE = (1u << 29u),
  SHADOW_IS_RENDERED = (1u << 30u),
  SHADOW_IS_USED = (1u << 31u)
;

/* NOTE: Trust the input to be in valid range (max is [3,3,255]).
 * If it is in valid range, it should pack to 12bits so that `shadow_tile_pack()` can use it.
 * But sometime this is used to encode invalid pages uint3(-1) and it needs to output uint(-1). */
static inline uint shadow_page_pack(uint3 page)
{
  return (page.x << 0u) | (page.y << 2u) | (page.z << 4u);
}
static inline uint3 shadow_page_unpack(uint data)
{
  uint3 page;
  BLI_STATIC_ASSERT(SHADOW_PAGE_PER_ROW <= 4 && SHADOW_PAGE_PER_COL <= 4,  Update page packing )
  page.x = (data >> 0u) & 3u;
  page.y = (data >> 2u) & 3u;
  BLI_STATIC_ASSERT(SHADOW_MAX_PAGE <= 4096,  Update page packing )
  page.z = (data >> 4u) & 255u;
  return page;
}

static inline ShadowTileData shadow_tile_unpack(ShadowTileDataPacked data)
{
  ShadowTileData tile;
  tile.page = shadow_page_unpack(data);
  /* -- 12 bits -- */
  /* Unused bits. */
  /* -- 15 bits -- */
  BLI_STATIC_ASSERT(SHADOW_MAX_PAGE <= 4096,  Update page packing )
  tile.cache_index = (data >> 15u) & 4095u;
  /* -- 27 bits -- */
  tile.is_used = (data & SHADOW_IS_USED) != 0;
  tile.is_cached = (data & SHADOW_IS_CACHED) != 0;
  tile.is_allocated = (data & SHADOW_IS_ALLOCATED) != 0;
  tile.is_rendered = (data & SHADOW_IS_RENDERED) != 0;
  tile.do_update = (data & SHADOW_DO_UPDATE) != 0;
  return tile;
}

static inline ShadowTileDataPacked shadow_tile_pack(ShadowTileData tile)
{
  uint data;
  /* NOTE: Page might be set to invalid values for tracking invalid usages.
   * So we have to mask the result. */
  data = shadow_page_pack(tile.page) & uint(SHADOW_MAX_PAGE - 1);
  data |= (tile.cache_index & 4095u) << 15u;
  data |= (tile.is_used ? uint(SHADOW_IS_USED) : 0);
  data |= (tile.is_allocated ? uint(SHADOW_IS_ALLOCATED) : 0);
  data |= (tile.is_cached ? uint(SHADOW_IS_CACHED) : 0);
  data |= (tile.is_rendered ? uint(SHADOW_IS_RENDERED) : 0);
  data |= (tile.do_update ? uint(SHADOW_DO_UPDATE) : 0);
  return data;
}

/**
 * Decoded tile data structure.
 * Similar to ShadowTileData, this one is only used for rendering and packed into `tilemap_tx`.
 * This allow to reuse some bits for other purpose.
 */
struct ShadowSamplingTile {
  /** Page inside the virtual shadow map atlas. */
  uint3 page;
  /** LOD pointed to LOD 0 tile page. */
  uint lod;
  /** Offset to the texel position to align with the LOD page start. (directional only). */
  uint2 lod_offset;
  /** If the tile is needed for rendering. */
  bool is_valid;
};
/** \note Stored packed as a uint. */
#define ShadowSamplingTilePacked uint

/* NOTE: Trust the input to be in valid range [0, (1 << SHADOW_TILEMAP_MAX_CLIPMAP_LOD) - 1].
 * Maximum LOD level index we can store is SHADOW_TILEMAP_MAX_CLIPMAP_LOD,
 * so we need SHADOW_TILEMAP_MAX_CLIPMAP_LOD bits to store the offset in each dimension.
 * Result fits into SHADOW_TILEMAP_MAX_CLIPMAP_LOD * 2 bits. */
static inline uint shadow_lod_offset_pack(uint2 ofs)
{
  BLI_STATIC_ASSERT(SHADOW_TILEMAP_MAX_CLIPMAP_LOD <= 8,  Update page packing )
  return ofs.x | (ofs.y << SHADOW_TILEMAP_MAX_CLIPMAP_LOD);
}
static inline uint2 shadow_lod_offset_unpack(uint data)
{
  return (uint2(data) >> uint2(0, SHADOW_TILEMAP_MAX_CLIPMAP_LOD)) &
         uint2((1 << SHADOW_TILEMAP_MAX_CLIPMAP_LOD) - 1);
}

static inline ShadowSamplingTile shadow_sampling_tile_unpack(ShadowSamplingTilePacked data)
{
  ShadowSamplingTile tile;
  tile.page = shadow_page_unpack(data);
  /* -- 12 bits -- */
  /* Max value is actually SHADOW_TILEMAP_MAX_CLIPMAP_LOD but we mask the bits. */
  tile.lod = (data >> 12u) & 15u;
  /* -- 16 bits -- */
  tile.lod_offset = shadow_lod_offset_unpack(data >> 16u);
  /* -- 32 bits -- */
  tile.is_valid = data != 0u;
#ifndef GPU_SHADER
  /* Make tests pass on CPU but it is not required for proper rendering. */
  if (tile.lod == 0) {
    tile.lod_offset.x = 0;
  }
#endif
  return tile;
}

static inline ShadowSamplingTilePacked shadow_sampling_tile_pack(ShadowSamplingTile tile)
{
  if (!tile.is_valid) {
    return 0u;
  }
  /* Tag a valid tile of LOD0 valid by setting their offset to 1.
   * This doesn't change the sampling and allows to use of all bits for data.
   * This makes sure no valid packed tile is 0u. */
  if (tile.lod == 0) {
    tile.lod_offset.x = 1;
  }
  uint data = shadow_page_pack(tile.page);
  /* Max value is actually SHADOW_TILEMAP_MAX_CLIPMAP_LOD but we mask the bits. */
  data |= (tile.lod & 15u) << 12u;
  data |= shadow_lod_offset_pack(tile.lod_offset) << 16u;
  return data;
}

static inline ShadowSamplingTile shadow_sampling_tile_create(ShadowTileData tile_data, uint lod)
{
  ShadowSamplingTile tile;
  tile.page = tile_data.page;
  tile.lod = lod;
  tile.lod_offset = uint2(0, 0); /* Computed during tilemap amend phase. */
  /* At this point, it should be the case that all given tiles that have been tagged as used are
   * ready for sampling. Otherwise tile_data should be SHADOW_NO_DATA. */
  tile.is_valid = tile_data.is_used;
  return tile;
}

struct ShadowSceneData {
  /* Number of shadow rays to shoot for each light. */
  int ray_count;
  /* Number of shadow samples to take for each shadow ray. */
  int step_count;
  /* Bias the shading point by using the normal to avoid self intersection. */
  float normal_bias;
  /* Ratio between tile-map pixel world  radius  and film pixel world  radius . */
  float tilemap_projection_ratio;
};
BLI_STATIC_ASSERT_ALIGN(ShadowSceneData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light-probe Sphere
 * \{ */

struct ReflectionProbeLowFreqLight {
  packed_float3 direction;
  float ambient;
};
BLI_STATIC_ASSERT_ALIGN(ReflectionProbeLowFreqLight, 16)

#define  LightProbeShape  uint
const uint 
  SHAPE_ELIPSOID = 0u,
  SHAPE_CUBOID = 1u;

/* Sampling coordinates using UV space. */
struct SphereProbeUvArea {
  /* Offset in UV space to the start of the sampling space of the octahedron map. */
  float2 offset;
  /* Scaling of the squared UV space of the octahedron map. */
  float scale;
  /* Layer of the atlas where the octahedron map is stored. */
  float layer;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeUvArea, 16)

/* Pixel read/write coordinates using pixel space. */
struct SphereProbePixelArea {
  /* Offset in pixel space to the start of the writing space of the octahedron map.
   * Note that the writing space is not the same as the sampling space as we have borders. */
  int2 offset;
  /* Size of the area in pixel that is covered by this probe mip-map. */
  int extent;
  /* Layer of the atlas where the octahedron map is stored. */
  int layer;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbePixelArea, 16)

/** Mapping data to locate a reflection probe in texture. */
struct SphereProbeData {
  /** Transform to probe local position with non-uniform scaling. */
  float3x4 world_to_probe_transposed;

  packed_float3 location;
  /** Shape of the parallax projection. */
  float parallax_distance;
  LightProbeShape parallax_shape;
  LightProbeShape influence_shape;
  /** Influence factor based on the distance to the parallax shape. */
  float influence_scale;
  float influence_bias;

  SphereProbeUvArea atlas_coord;

  /**
   * Irradiance at the probe location encoded as spherical harmonics.
   * Only contain the average luminance. Used for cube-map normalization.
   */
  ReflectionProbeLowFreqLight low_freq_light;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeData, 16)

/** Viewport Display Pass. */
struct SphereProbeDisplayData {
  int probe_index;
  float display_size;
  float _pad0;
  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeDisplayData, 16)

/* Used for sphere probe spherical harmonics extraction. Output one for each thread-group
 * and do a sum afterward. Reduces bandwidth usage. */
struct SphereProbeHarmonic {
  float4 L0_M0;
  float4 L1_Mn1;
  float4 L1_M0;
  float4 L1_Mp1;
};
BLI_STATIC_ASSERT_ALIGN(SphereProbeHarmonic, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volume Probe Cache
 * \{ */

struct SurfelRadiance {
  /* Actually stores radiance and world (sky) visibility. Stored normalized. */
  float4 front;
  float4 back;
  /* Accumulated weights per face. */
  float front_weight;
  float back_weight;
  float _pad0;
  float _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SurfelRadiance, 16)

struct Surfel {
  /** World position of the surfel. */
  packed_float3 position;
  /** Previous surfel index in the ray link-list. Only valid after sorting. */
  int prev;
  /** World orientation of the surface. */
  packed_float3 normal;
  /** Next surfel index in the ray link-list. */
  int next;
  /** Surface albedo to apply to incoming radiance. */
  packed_float3 albedo_front;
  /** Distance along the ray direction for sorting. */
  float ray_distance;
  /** Surface albedo to apply to incoming radiance. */
  packed_float3 albedo_back;
  /** Cluster this surfel is assigned to. */
  int cluster_id;
  /** True if the light can bounce or be emitted by the surfel back face. */
  bool32_t double_sided;
  int _pad0;
  int _pad1;
  int _pad2;
  /** Surface radiance: Emission + Direct Lighting. */
  SurfelRadiance radiance_direct;
  /** Surface radiance: Indirect Lighting. Double buffered to avoid race conditions. */
  SurfelRadiance radiance_indirect[2];
};
BLI_STATIC_ASSERT_ALIGN(Surfel, 16)

struct CaptureInfoData {
  /** Grid size without padding. */
  packed_int3 irradiance_grid_size;
  /** True if the surface shader needs to write the surfel data. */
  bool32_t do_surfel_output;
  /** True if the surface shader needs to increment the surfel_len. */
  bool32_t do_surfel_count;
  /** Number of surfels inside the surfel buffer or the needed len. */
  uint surfel_len;
  /** Total number of a ray for light transportation. */
  float sample_count;
  /** 0 based sample index. */
  float sample_index;
  /** Transform of the light-probe object. */
  float4x4 irradiance_grid_local_to_world;
  /** Transform of the light-probe object. */
  float4x4 irradiance_grid_world_to_local;
  /** Transform vectors from world space to local space. Does not have location component. */
  /** TODO(fclem): This could be a float3x4 or a float3x3 if padded correctly. */
  float4x4 irradiance_grid_world_to_local_rotation;
  /** Scene bounds. Stored as min & max and as int for atomic operations. */
  int scene_bound_x_min;
  int scene_bound_y_min;
  int scene_bound_z_min;
  int scene_bound_x_max;
  int scene_bound_y_max;
  int scene_bound_z_max;
  /* Max intensity a ray can have. */
  float clamp_direct;
  float clamp_indirect;
  float _pad1;
  float _pad2;
  /** Minimum distance between a grid sample and a surface. Used to compute virtual offset. */
  float min_distance_to_surface;
  /** Maximum world scale offset an irradiance grid sample can be baked with. */
  float max_virtual_offset;
  /** Radius of surfels. */
  float surfel_radius;
  /** Capture options. */
  bool32_t capture_world_direct;
  bool32_t capture_world_indirect;
  bool32_t capture_visibility_direct;
  bool32_t capture_visibility_indirect;
  bool32_t capture_indirect;
  bool32_t capture_emission;
  int _pad0;
  /* World light probe atlas coordinate. */
  SphereProbeUvArea world_atlas_coord;
};
BLI_STATIC_ASSERT_ALIGN(CaptureInfoData, 16)

struct SurfelListInfoData {
  /** Size of the grid used to project the surfels into linked lists. */
  int2 ray_grid_size;
  /** Maximum number of list. Is equal to `ray_grid_size.x * ray_grid_size.y`. */
  int list_max;

  int _pad0;
};
BLI_STATIC_ASSERT_ALIGN(SurfelListInfoData, 16)

struct VolumeProbeData {
  /** World to non-normalized local grid space [0..size-1]. Stored transposed for compactness. */
  float3x4 world_to_grid_transposed;
  /** Number of bricks for this grid. */
  packed_int3 grid_size_padded;
  /** Index in brick descriptor list of the first brick of this grid. */
  int brick_offset;
  /** Biases to apply to the shading point in order to sample a valid probe. */
  float normal_bias;
  float view_bias;
  float facing_bias;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(VolumeProbeData, 16)

struct IrradianceBrick {
  /* Offset in pixel to the start of the data inside the atlas texture. */
  uint2 atlas_coord;
};
/** \note Stored packed as a uint. */
#define IrradianceBrickPacked uint

static inline IrradianceBrickPacked irradiance_brick_pack(IrradianceBrick brick)
{
  uint2 data = (uint2(brick.atlas_coord) & 0xFFFFu) << uint2(0u, 16u);
  IrradianceBrickPacked brick_packed = data.x | data.y;
  return brick_packed;
}

static inline IrradianceBrick irradiance_brick_unpack(IrradianceBrickPacked brick_packed)
{
  IrradianceBrick brick;
  brick.atlas_coord = (uint2(brick_packed) >> uint2(0u, 16u)) & uint2(0xFFFFu);
  return brick;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Hierarchical-Z Buffer
 * \{ */

struct HiZData {
  /** Scale factor to remove HiZBuffer padding. */
  float2 uv_scale;

  float2 _pad0;
};
BLI_STATIC_ASSERT_ALIGN(HiZData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light Clamping
 * \{ */

struct ClampData {
  float surface_direct;
  float surface_indirect;
  float volume_direct;
  float volume_indirect;
};
BLI_STATIC_ASSERT_ALIGN(ClampData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Ray-Tracing
 * \{ */

#define  eClosureBits  uint
const uint 
  CLOSURE_NONE = 0u,
  CLOSURE_DIFFUSE = (1u << 0u),
  CLOSURE_SSS = (1u << 1u),
  CLOSURE_REFLECTION = (1u << 2u),
  CLOSURE_REFRACTION = (1u << 3u),
  CLOSURE_TRANSLUCENT = (1u << 4u),
  CLOSURE_TRANSPARENCY = (1u << 8u),
  CLOSURE_EMISSION = (1u << 9u),
  CLOSURE_HOLDOUT = (1u << 10u),
  CLOSURE_VOLUME = (1u << 11u),
  CLOSURE_AMBIENT_OCCLUSION = (1u << 12u),
  CLOSURE_SHADER_TO_RGBA = (1u << 13u),
  CLOSURE_CLEARCOAT = (1u << 14u);

#define  GBufferMode  uint
const uint 
  /** None mode for pixels not rendered. */
  GBUF_NONE = 0u,

  GBUF_DIFFUSE = 1u,
  GBUF_TRANSLUCENT = 2u,
  GBUF_REFLECTION = 3u,
  GBUF_REFRACTION = 4u,
  GBUF_SUBSURFACE = 5u,

  /** Used for surfaces that have no lit closure and just encode a normal layer. */
  GBUF_UNLIT = 11u,

  /** Parameter Optimized. Packs one closure into less layer. */
  GBUF_REFLECTION_COLORLESS = 12u,
  GBUF_REFRACTION_COLORLESS = 13u,

  /** Special configurations. Packs multiple closures into less layer. */
  /* TODO(@fclem): This is isn't currently working due to monolithic nature of the evaluation. */
  GBUF_METAL_CLEARCOAT = 15u;

struct RayTraceData {
  /** ViewProjection matrix used to render the previous frame. */
  float4x4 history_persmat;
  /** ViewProjection matrix used to render the radiance texture. */
  float4x4 radiance_persmat;
  /** Input resolution. */
  int2 full_resolution;
  /** Inverse of input resolution to get screen UVs. */
  float2 full_resolution_inv;
  /** Scale and bias to go from ray-trace resolution to input resolution. */
  int2 resolution_bias;
  int resolution_scale;
  /** View space thickness the objects. */
  float thickness;
  /** Scale and bias to go from horizon-trace resolution to input resolution. */
  int2 horizon_resolution_bias;
  int horizon_resolution_scale;
  /** Determine how fast the sample steps are getting bigger. */
  float quality;
  /** Maximum roughness for which we will trace a ray. */
  float roughness_mask_scale;
  float roughness_mask_bias;
  /** If set to true will bypass spatial denoising. */
  bool32_t skip_denoise;
  /** If set to false will bypass tracing for refractive closures. */
  bool32_t trace_refraction;
  /** Closure being ray-traced. */
  int closure_index;
  int _pad0;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(RayTraceData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Ambient Occlusion
 * \{ */

struct AOData {
  float2 pixel_size;
  float distance;
  float quality;

  float thickness;
  float angle_bias;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(AOData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Subsurface
 * \{ */

#define SSS_SAMPLE_MAX 64
#define SSS_BURLEY_TRUNCATE 16.0
#define SSS_BURLEY_TRUNCATE_CDF 0.9963790093708328
#define SSS_TRANSMIT_LUT_SIZE 64.0
#define SSS_TRANSMIT_LUT_RADIUS 2.0
#define SSS_TRANSMIT_LUT_SCALE ((SSS_TRANSMIT_LUT_SIZE - 1.0) / float(SSS_TRANSMIT_LUT_SIZE))
#define SSS_TRANSMIT_LUT_BIAS (0.5 / float(SSS_TRANSMIT_LUT_SIZE))
#define SSS_TRANSMIT_LUT_STEP_RES 64.0

struct SubsurfaceData {
  /** xy: 2D sample position [-1..1], zw: sample_bounds. */
  /* NOTE(fclem) Using float4 for alignment. */
  float4 samples[SSS_SAMPLE_MAX];
  /** Sample index after which samples are not randomly rotated anymore. */
  int jitter_threshold;
  /** Number of samples precomputed in the set. */
  int sample_len;
  int _pad0;
  int _pad1;
};
BLI_STATIC_ASSERT_ALIGN(SubsurfaceData, 16)

static inline float3 burley_setup(float3 radius, float3 albedo)
{
  /* TODO(fclem): Avoid constant duplication. */
  const float m_1_pi = 0.318309886183790671538;

  float3 A = albedo;
  /* Diffuse surface transmission, equation (6). */
  float3 s = 1.9 - A + 3.5 * ((A - 0.8) * (A - 0.8));
  /* Mean free path length adapted to fit ancient Cubic and Gaussian models. */
  float3 l = 0.25 * m_1_pi * radius;

  return l / s;
}

static inline float3 burley_eval(float3 d, float r)
{
  /* Slide 33. */
  float3 exp_r_3_d;
  /* TODO(fclem): Vectorize. */
  exp_r_3_d.x = expf(-r / (3.0 * d.x));
  exp_r_3_d.y = expf(-r / (3.0 * d.y));
  exp_r_3_d.z = expf(-r / (3.0 * d.z));
  float3 exp_r_d = exp_r_3_d * exp_r_3_d * exp_r_3_d;
  /* NOTE:
   * - Surface albedo is applied at the end.
   * - This is normalized diffuse model, so the equation is multiplied
   *   by 2*pi, which also matches `cdf()`.
   */
  return (exp_r_d + exp_r_3_d) / (4.0 * d);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Light-probe Planar Data
 * \{ */

struct PlanarProbeData {
  /** Matrices used to render the planar capture. */
  float4x4 viewmat;
  float4x4 winmat;
  /** Transform world to local position with influence distance as Z scale. */
  float3x4 world_to_object_transposed;
  /** World space plane normal. */
  packed_float3 normal;
  /** Layer in the planar capture textures used by this probe. */
  int layer_id;
};
BLI_STATIC_ASSERT_ALIGN(PlanarProbeData, 16)

struct ClipPlaneData {
  /** World space clip plane equation. Used to render planar light-probes. */
  float4 plane;
};
BLI_STATIC_ASSERT_ALIGN(ClipPlaneData, 16)

/** Viewport Display Pass. */
struct PlanarProbeDisplayData {
  float4x4 plane_to_world;
  int probe_index;
  float _pad0;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(PlanarProbeDisplayData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pipeline Data
 * \{ */

struct PipelineInfoData {
  float alpha_hash_scale;
  bool32_t is_probe_reflection;
  float _pad1;
  float _pad2;
};
BLI_STATIC_ASSERT_ALIGN(PipelineInfoData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Data
 * \{ */

/* Combines data from several modules to avoid wasting binding slots. */
struct UniformData {
  AOData ao;
  CameraData camera;
  ClampData clamp;
  FilmData film;
  HiZData hiz;
  RayTraceData raytrace;
  RenderBuffersInfoData render_pass;
  ShadowSceneData shadow;
  SubsurfaceData subsurface;
  VolumesInfoData volumes;
  PipelineInfoData pipeline;
};
BLI_STATIC_ASSERT_ALIGN(UniformData, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Utility Texture
 * \{ */

#define UTIL_TEX_SIZE 64
#define UTIL_BTDF_LAYER_COUNT 16
/* Scale and bias to avoid interpolation of the border pixel.
 * Remap UVs to the border pixels centers. */
#define UTIL_TEX_UV_SCALE ((UTIL_TEX_SIZE - 1.0f) / UTIL_TEX_SIZE)
#define UTIL_TEX_UV_BIAS (0.5f / UTIL_TEX_SIZE)

#define UTIL_BLUE_NOISE_LAYER 0
#define UTIL_SSS_TRANSMITTANCE_PROFILE_LAYER 1
#define UTIL_LTC_MAT_LAYER 2
#define UTIL_BSDF_LAYER 3
#define UTIL_BTDF_LAYER 4
#define UTIL_DISK_INTEGRAL_LAYER UTIL_SSS_TRANSMITTANCE_PROFILE_LAYER
#define UTIL_DISK_INTEGRAL_COMP 3

#ifdef GPU_SHADER

#  if defined(GPU_FRAGMENT_SHADER)
#    define UTIL_TEXEL vec2(gl_FragCoord.xy)
#  elif defined(GPU_COMPUTE_SHADER)
#    define UTIL_TEXEL vec2(gl_GlobalInvocationID.xy)
#  else
#    define UTIL_TEXEL vec2(gl_VertexID, 0)
#  endif

/* Fetch texel. Wrapping if above range. */
float4 utility_tx_fetch(sampler2DArray util_tx, float2 texel, float layer)
{
  return texelFetch(util_tx, int3(int2(texel) % UTIL_TEX_SIZE, layer), 0);
}

/* Sample at uv position. Filtered & Wrapping enabled. */
float4 utility_tx_sample(sampler2DArray util_tx, float2 uv, float layer)
{
  return textureLod(util_tx, float3(uv, layer), 0.0);
}

/* Sample at uv position but with scale and bias so that uv space bounds lie on texel centers. */
float4 utility_tx_sample_lut(sampler2DArray util_tx, float2 uv, float layer)
{
  /* Scale and bias coordinates, for correct filtered lookup. */
  uv = uv * UTIL_TEX_UV_SCALE + UTIL_TEX_UV_BIAS;
  return textureLod(util_tx, float3(uv, layer), 0.0);
}

/* Sample GGX BSDF LUT. */
float4 utility_tx_sample_bsdf_lut(sampler2DArray util_tx, float2 uv, float layer)
{
  /* Scale and bias coordinates, for correct filtered lookup. */
  uv = uv * UTIL_TEX_UV_SCALE + UTIL_TEX_UV_BIAS;
  layer = layer * UTIL_BTDF_LAYER_COUNT + UTIL_BTDF_LAYER;

  float layer_floored;
  float interp = modf(layer, layer_floored);

  float4 tex_low = textureLod(util_tx, float3(uv, layer_floored), 0.0);
  float4 tex_high = textureLod(util_tx, float3(uv, layer_floored + 1.0), 0.0);

  /* Manual trilinear interpolation. */
  return mix(tex_low, tex_high, interp);
}

/* Sample LTC or BSDF LUTs with `cos_theta` and `roughness` as inputs. */
float4 utility_tx_sample_lut(sampler2DArray util_tx, float cos_theta, float roughness, float layer)
{
  /* LUTs are parameterized by `sqrt(1.0 - cos_theta)` for more precision near grazing incidence.
   */
  vec2 coords = vec2(roughness, sqrt(clamp(1.0 - cos_theta, 0.0, 1.0)));
  return utility_tx_sample_lut(util_tx, coords, layer);
}

#endif

/** \} */

#if IS_CPP

using AOVsInfoDataBuf = draw::StorageBuffer<AOVsInfoData>;
using CameraDataBuf = draw::UniformBuffer<CameraData>;
using ClosureTileBuf = draw::StorageArrayBuffer<uint, 1024, true>;
using DepthOfFieldDataBuf = draw::UniformBuffer<DepthOfFieldData>;
using DepthOfFieldScatterListBuf = draw::StorageArrayBuffer<ScatterRect, 16, true>;
using DrawIndirectBuf = draw::StorageBuffer<DrawCommand, true>;
using DispatchIndirectBuf = draw::StorageBuffer<DispatchCommand>;
using UniformDataBuf = draw::UniformBuffer<UniformData>;
using VolumeProbeDataBuf = draw::UniformArrayBuffer<VolumeProbeData, IRRADIANCE_GRID_MAX>;
using IrradianceBrickBuf = draw::StorageVectorBuffer<IrradianceBrickPacked, 16>;
using LightCullingDataBuf = draw::StorageBuffer<LightCullingData>;
using LightCullingKeyBuf = draw::StorageArrayBuffer<uint, LIGHT_CHUNK, true>;
using LightCullingTileBuf = draw::StorageArrayBuffer<uint, LIGHT_CHUNK, true>;
using LightCullingZbinBuf = draw::StorageArrayBuffer<uint, CULLING_ZBIN_COUNT, true>;
using LightCullingZdistBuf = draw::StorageArrayBuffer<float, LIGHT_CHUNK, true>;
using LightDataBuf = draw::StorageArrayBuffer<LightData, LIGHT_CHUNK>;
using MotionBlurDataBuf = draw::UniformBuffer<MotionBlurData>;
using MotionBlurTileIndirectionBuf = draw::StorageBuffer<MotionBlurTileIndirection, true>;
using RayTraceTileBuf = draw::StorageArrayBuffer<uint, 1024, true>;
using SubsurfaceTileBuf = RayTraceTileBuf;
using SphereProbeDataBuf = draw::UniformArrayBuffer<SphereProbeData, SPHERE_PROBE_MAX>;
using SphereProbeDisplayDataBuf = draw::StorageArrayBuffer<SphereProbeDisplayData>;
using PlanarProbeDataBuf = draw::UniformArrayBuffer<PlanarProbeData, PLANAR_PROBE_MAX>;
using PlanarProbeDisplayDataBuf = draw::StorageArrayBuffer<PlanarProbeDisplayData>;
using SamplingDataBuf = draw::StorageBuffer<SamplingData>;
using ShadowStatisticsBuf = draw::StorageBuffer<ShadowStatistics>;
using ShadowPagesInfoDataBuf = draw::StorageBuffer<ShadowPagesInfoData>;
using ShadowPageHeapBuf = draw::StorageVectorBuffer<uint, SHADOW_MAX_PAGE>;
using ShadowPageCacheBuf = draw::StorageArrayBuffer<uint2, SHADOW_MAX_PAGE, true>;
using ShadowTileMapDataBuf = draw::StorageVectorBuffer<ShadowTileMapData, SHADOW_MAX_TILEMAP>;
using ShadowTileMapClipBuf = draw::StorageArrayBuffer<ShadowTileMapClip, SHADOW_MAX_TILEMAP, true>;
using ShadowTileDataBuf = draw::StorageArrayBuffer<ShadowTileDataPacked, SHADOW_MAX_TILE, true>;
using SurfelBuf = draw::StorageArrayBuffer<Surfel, 64>;
using SurfelRadianceBuf = draw::StorageArrayBuffer<SurfelRadiance, 64>;
using CaptureInfoBuf = draw::StorageBuffer<CaptureInfoData>;
using SurfelListInfoBuf = draw::StorageBuffer<SurfelListInfoData>;
using VelocityGeometryBuf = draw::StorageArrayBuffer<float4, 16, true>;
using VelocityIndexBuf = draw::StorageArrayBuffer<VelocityIndex, 16>;
using VelocityObjectBuf = draw::StorageArrayBuffer<float4x4, 16>;
using CryptomatteObjectBuf = draw::StorageArrayBuffer<float2, 16>;
using ClipPlaneBuf = draw::UniformBuffer<ClipPlaneData>;
}  // namespace blender::eevee
#endif

/////////////////////////////// Source file 12/////////////////////////////




#ifndef GPU_SHADER
#  pragma once

#  include  GPU_shader.hh 
#  include  GPU_shader_shared_utils.hh 
#  include  draw_defines.hh 

struct ViewCullingData;
struct ViewMatrices;
struct ObjectMatrices;
struct ObjectInfos;
struct ObjectBounds;
struct VolumeInfos;
struct CurvesInfos;
struct ObjectAttribute;
struct LayerAttribute;
struct DrawCommand;
struct DispatchCommand;
struct DRWDebugPrintBuffer;
struct DRWDebugVert;
struct DRWDebugDrawBuffer;
struct FrustumCorners;
struct FrustumPlanes;

/* __cplusplus is true when compiling with MSL. */
#  if defined(__cplusplus) && !defined(GPU_SHADER)
/* C++ only forward declarations. */
struct Object;
struct Scene;
struct ViewLayer;
struct GPUUniformAttr;
struct GPULayerAttr;

namespace blender::draw {

struct ObjectRef;

}  // namespace blender::draw

#  endif
#endif

#define DRW_SHADER_SHARED_H

#define DRW_RESOURCE_CHUNK_LEN 512

/* Define the maximum number of grid we allow in a volume UBO. */
#define DRW_GRID_PER_VOLUME_MAX 16

/* Define the maximum number of attribute we allow in a curves UBO.
 * This should be kept in sync with `GPU_ATTR_MAX` */
#define DRW_ATTRIBUTE_PER_CURVES_MAX 15

/* -------------------------------------------------------------------- */
/** \name Views
 * \{ */

#ifndef DRW_VIEW_LEN
/* Single-view case (default). */
#  define drw_view_id 0
#  define DRW_VIEW_LEN 1
#  define DRW_VIEW_SHIFT 0
#  define DRW_VIEW_FROM_RESOURCE_ID
#else

/* Multi-view case. */
/** This should be already defined at shaderCreateInfo level. */
// #  define DRW_VIEW_LEN 64
/** Global that needs to be set correctly in each shader stage. */
uint drw_view_id = 0;
/**
 * In order to reduce the memory requirements, the view id is merged with resource id to avoid
 * doubling the memory required only for view indexing.
 */
/** \note This is simply log2(DRW_VIEW_LEN) but making sure it is optimized out. */
#  define DRW_VIEW_SHIFT \
    ((DRW_VIEW_LEN > 32) ? 6 : \
     (DRW_VIEW_LEN > 16) ? 5 : \
     (DRW_VIEW_LEN > 8)  ? 4 : \
     (DRW_VIEW_LEN > 4)  ? 3 : \
     (DRW_VIEW_LEN > 2)  ? 2 : \
                           1)
#  define DRW_VIEW_MASK ~(0xFFFFFFFFu << DRW_VIEW_SHIFT)
#  define DRW_VIEW_FROM_RESOURCE_ID drw_view_id = (uint(drw_ResourceID) & DRW_VIEW_MASK)
#endif

struct FrustumCorners {
  float4 corners[8];
};
BLI_STATIC_ASSERT_ALIGN(FrustumCorners, 16)

struct FrustumPlanes {
  /* [0] left
   * [1] right
   * [2] bottom
   * [3] top
   * [4] near
   * [5] far */
  float4 planes[6];
};
BLI_STATIC_ASSERT_ALIGN(FrustumPlanes, 16)

struct ViewCullingData {
  /** \note vec3 array padded to vec4. */
  /** Frustum corners. */
  FrustumCorners frustum_corners;
  FrustumPlanes frustum_planes;
  float4 bound_sphere;
};
BLI_STATIC_ASSERT_ALIGN(ViewCullingData, 16)

struct ViewMatrices {
  float4x4 viewmat;
  float4x4 viewinv;
  float4x4 winmat;
  float4x4 wininv;
};
BLI_STATIC_ASSERT_ALIGN(ViewMatrices, 16)

/* Do not override old definitions if the shader uses this header but not shader info. */
#ifdef USE_GPU_SHADER_CREATE_INFO
/* TODO(@fclem): Mass rename. */
#  define ViewMatrix drw_view.viewmat
#  define ViewMatrixInverse drw_view.viewinv
#  define ProjectionMatrix drw_view.winmat
#  define ProjectionMatrixInverse drw_view.wininv
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug draw shapes
 * \{ */

struct ObjectMatrices {
  float4x4 model;
  float4x4 model_inverse;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync(const Object &object);
  void sync(const float4x4 &model_matrix);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectMatrices, 16)

#define  eObjectInfoFlag  uint
const uint 
  OBJECT_SELECTED = (1u << 0u),
  OBJECT_FROM_DUPLI = (1u << 1u),
  OBJECT_FROM_SET = (1u << 2u),
  OBJECT_ACTIVE = (1u << 3u),
  OBJECT_NEGATIVE_SCALE = (1u << 4u),
  /* Avoid skipped info to change culling. */
  OBJECT_NO_INFO = ~OBJECT_NEGATIVE_SCALE
;

struct ObjectInfos {
#if defined(GPU_SHADER) && !defined(DRAW_FINALIZE_SHADER)
  /* TODO Rename to struct member for GLSL too. */
  float4 orco_mul_bias[2];
  float4 ob_color;
  float4 infos;
#else
  /** Uploaded as center + size. Converted to mul+bias to local coord. */
  packed_float3 orco_add;
  uint object_attrs_offset;
  packed_float3 orco_mul;
  uint object_attrs_len;

  float4 ob_color;
  uint index;
  uint _pad2;
  float random;
  eObjectInfoFlag flag;
#endif

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync();
  void sync(const blender::draw::ObjectRef ref, bool is_active_object);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectInfos, 16)

struct ObjectBounds {
  /**
   * Uploaded as vertex (0, 4, 3, 1) of the bbox in local space, matching XYZ axis order.
   * Then processed by GPU and stored as (0, 4-0, 3-0, 1-0) in world space for faster culling.
   */
  float4 bounding_corners[4];
  /** Bounding sphere derived from the bounding corner. Computed on GPU. */
  float4 bounding_sphere;
  /** Radius of the inscribed sphere derived from the bounding corner. Computed on GPU. */
#define _inner_sphere_radius bounding_corners[3].w

#if !defined(GPU_SHADER) && defined(__cplusplus)
  void sync();
  void sync(const Object &ob, float inflate_bounds = 0.0f);
  void sync(const float3 &center, const float3 &size);
#endif
};
BLI_STATIC_ASSERT_ALIGN(ObjectBounds, 16)

inline bool drw_bounds_are_valid(ObjectBounds bounds)
{
  return bounds.bounding_sphere.w >= 0.0f;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Object attributes
 * \{ */

struct VolumeInfos {
  /** Object to grid-space. */
  float4x4 grids_xform[DRW_GRID_PER_VOLUME_MAX];
  /** \note vec4 for alignment. Only float3 needed. */
  float4 color_mul;
  float density_scale;
  float temperature_mul;
  float temperature_bias;
  float _pad;
};
BLI_STATIC_ASSERT_ALIGN(VolumeInfos, 16)

struct CurvesInfos {
  /** Per attribute scope, follows loading order.
   * \note uint as bool in GLSL is 4 bytes.
   * \note GLSL pad arrays of scalar to 16 bytes (std140). */
  uint4 is_point_attribute[DRW_ATTRIBUTE_PER_CURVES_MAX];
};
BLI_STATIC_ASSERT_ALIGN(CurvesInfos, 16)

#pragma pack(push, 4)
struct ObjectAttribute {
  /* Workaround the padding cost from alignment requirements.
   * (see GL spec : 7.6.2.2 Standard Uniform Block Layout) */
  float data_x, data_y, data_z, data_w;
  uint hash_code;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  bool sync(const blender::draw::ObjectRef &ref, const GPUUniformAttr &attr);
#endif
};
#pragma pack(pop)
/** \note we only align to 4 bytes and fetch data manually so make sure
 * C++ compiler gives us the same size. */
BLI_STATIC_ASSERT_ALIGN(ObjectAttribute, 20)

#pragma pack(push, 4)
struct LayerAttribute {
  float4 data;
  uint hash_code;
  uint buffer_length; /* Only in the first record. */
  uint _pad1, _pad2;

#if !defined(GPU_SHADER) && defined(__cplusplus)
  bool sync(const Scene *scene, const ViewLayer *layer, const GPULayerAttr &attr);
#endif
};
#pragma pack(pop)
BLI_STATIC_ASSERT_ALIGN(LayerAttribute, 32)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Indirect commands structures.
 * \{ */

struct DrawCommand {
  /* TODO(fclem): Rename */
  uint vertex_len;
  uint instance_len;
  uint vertex_first;
#if defined(GPU_SHADER)
  uint base_index;
  /** \note base_index is i_first for non-indexed draw-calls. */
#  define _instance_first_array base_index
#else
  union {
    uint base_index;
    /* Use this instead of instance_first_indexed for non indexed draw calls. */
    uint instance_first_array;
  };
#endif

  uint instance_first_indexed;

  uint _pad0, _pad1, _pad2;
};
BLI_STATIC_ASSERT_ALIGN(DrawCommand, 16)

struct DispatchCommand {
  uint num_groups_x;
  uint num_groups_y;
  uint num_groups_z;
  uint _pad0;
};
BLI_STATIC_ASSERT_ALIGN(DispatchCommand, 16)

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug print
 * \{ */

/* Take the header (DrawCommand) into account. */
#define DRW_DEBUG_PRINT_MAX (8 * 1024) - 4
/** \note Cannot be more than 255 (because of column encoding). */
#define DRW_DEBUG_PRINT_WORD_WRAP_COLUMN 120u

/* The debug print buffer is laid-out as the following struct.
 * But we use plain array in shader code instead because of driver issues. */
struct DRWDebugPrintBuffer {
  DrawCommand command;
  /** Each character is encoded as 3 `uchar` with char_index, row and column position. */
  uint char_array[DRW_DEBUG_PRINT_MAX];
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugPrintBuffer, 16)

/* Use number of char as vertex count. Equivalent to `DRWDebugPrintBuffer.command.v_count`. */
#define drw_debug_print_cursor drw_debug_print_buf[0]
/* Reuse first instance as row index as we don't use instancing. Equivalent to
 * `DRWDebugPrintBuffer.command.i_first`. */
#define drw_debug_print_row_shared drw_debug_print_buf[3]
/**
 * Offset to the first data. Equal to: `sizeof(DrawCommand) / sizeof(uint)`.
 * This is needed because we bind the whole buffer as a `uint` array.
 */
#define drw_debug_print_offset 8

/** \} */

/* -------------------------------------------------------------------- */
/** \name Debug draw shapes
 * \{ */

struct DRWDebugVert {
  /* This is a weird layout, but needed to be able to use DRWDebugVert as
   * a DrawCommand and avoid alignment issues. See drw_debug_verts_buf[] definition. */
  uint pos0;
  uint pos1;
  uint pos2;
  /* Named vert_color to avoid global namespace collision with uniform color. */
  uint vert_color;
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugVert, 16)

inline DRWDebugVert debug_vert_make(uint in_pos0, uint in_pos1, uint in_pos2, uint in_vert_color)
{
  DRWDebugVert debug_vert;
  debug_vert.pos0 = in_pos0;
  debug_vert.pos1 = in_pos1;
  debug_vert.pos2 = in_pos2;
  debug_vert.vert_color = in_vert_color;
  return debug_vert;
}

/* Take the header (DrawCommand) into account. */
#define DRW_DEBUG_DRAW_VERT_MAX (64 * 8192) - 1

/* The debug draw buffer is laid-out as the following struct.
 * But we use plain array in shader code instead because of driver issues. */
struct DRWDebugDrawBuffer {
  DrawCommand command;
  DRWDebugVert verts[DRW_DEBUG_DRAW_VERT_MAX];
};
BLI_STATIC_ASSERT_ALIGN(DRWDebugPrintBuffer, 16)

/* Equivalent to `DRWDebugDrawBuffer.command.v_count`. */
#define drw_debug_draw_v_count drw_debug_verts_buf[0].pos0
/**
 * Offset to the first data. Equal to: `sizeof(DrawCommand) / sizeof(DRWDebugVert)`.
 * This is needed because we bind the whole buffer as a `DRWDebugVert` array.
 */
#define drw_debug_draw_offset 2

/** \} */

/////////////////////////////// Source file 13/////////////////////////////

/* Pass Resources. */
layout(binding = 0, rgba16f) uniform restrict writeonly image2DArray rp_color_img;
layout(binding = 1, r16f) uniform restrict writeonly image2DArray rp_value_img;
layout(binding = 1, std140) uniform uniform_buf { UniformData _uniform_buf; };
layout(binding = 7, std430) restrict readonly buffer cryptomatte_object_buf { vec2 _cryptomatte_object_buf[]; };
layout(binding = 2, rgba32f) uniform restrict writeonly image2D rp_cryptomatte_img;
layout(binding = 10, std430) restrict readonly buffer drw_matrix_buf { ObjectMatrices _drw_matrix_buf[]; };
layout(binding = 11, std430) restrict readonly buffer resource_id_buf { int _resource_id_buf[]; };
layout(binding = 11, std140) uniform drw_view_ { ViewMatrices _drw_view_[DRW_VIEW_LEN]; };
layout(binding = 3, rgb10_a2) uniform restrict writeonly image2DArray out_gbuf_closure_img;
layout(binding = 4, rg16) uniform restrict writeonly image2DArray out_gbuf_normal_img;
layout(binding = 2) uniform sampler2DArray utility_tx;
layout(binding = 6, std430) restrict readonly buffer sampling_buf { SamplingData _sampling_buf; };
layout(binding = 3) uniform sampler2D hiz_tx;
layout(binding = 0, std430) restrict readonly buffer light_cull_buf { LightCullingData _light_cull_buf; };
layout(binding = 1, std430) restrict readonly buffer light_buf { LightData _light_buf[]; };
layout(binding = 2, std430) restrict readonly buffer light_zbin_buf { uint _light_zbin_buf[]; };
layout(binding = 3, std430) restrict readonly buffer light_tile_buf { uint _light_tile_buf[]; };
layout(binding = 3, std140) uniform reflection_probe_buf { SphereProbeData _reflection_probe_buf[SPHERE_PROBE_MAX]; };
layout(binding = 7) uniform sampler2DArray reflection_probes_tx;
layout(binding = 2, std140) uniform grids_infos_buf { VolumeProbeData _grids_infos_buf[IRRADIANCE_GRID_MAX]; };
layout(binding = 4, std430) restrict readonly buffer bricks_infos_buf { uint _bricks_infos_buf[]; };
layout(binding = 6) uniform sampler3D irradiance_atlas_tx;
layout(binding = 5) uniform usampler2DArray shadow_atlas_tx;
layout(binding = 4) uniform usampler2D shadow_tilemaps_tx;
#define uniform_buf (_uniform_buf)
#define cryptomatte_object_buf (_cryptomatte_object_buf)
#define drw_matrix_buf (_drw_matrix_buf)
#define resource_id_buf (_resource_id_buf)
#define drw_view_ (_drw_view_)
#define sampling_buf (_sampling_buf)
#define light_cull_buf (_light_cull_buf)
#define light_buf (_light_buf)
#define light_zbin_buf (_light_zbin_buf)
#define light_tile_buf (_light_tile_buf)
#define reflection_probe_buf (_reflection_probe_buf)
#define grids_infos_buf (_grids_infos_buf)
#define bricks_infos_buf (_bricks_infos_buf)

/* Batch Resources. */
layout(binding = 0, std140) uniform node_tree { NodeTree _node_tree; };
#define node_tree (_node_tree)

/* Push Constants. */


/////////////////////////////// Source file 14/////////////////////////////

/* Interfaces. */
in eevee_surf_iface{
  smooth vec3 P;
  smooth vec3 N;
}
interp;
in draw_resource_id_iface{
  flat int resource_index;
}
drw_ResourceID_iface;
layout(early_fragment_tests) in;
layout(depth_unchanged) out float gl_FragDepth;

/* Sub-pass Inputs. */

/* Outputs. */
layout(location = 0) out vec4 out_radiance;
layout(location = 1) out uint out_gbuf_header;
layout(location = 2) out vec2 out_gbuf_normal;
layout(location = 3) out vec4 out_gbuf_closure1;
layout(location = 4) out vec4 out_gbuf_closure2;


/////////////////////////////// Source file 15/////////////////////////////




#ifndef DRAW_VIEW_CREATE_INFO
#  error Missing draw_view additional create info on shader create info
#endif

/* Returns true if the current view has a perspective projection matrix. */
bool drw_view_is_perspective()
{
  return drw_view.winmat[3][3] == 0.0;
}

/* Returns the view forward vector, going towards the viewer. */
vec3 drw_view_forward()
{
  return drw_view.viewinv[2].xyz;
}

/* Returns the view origin. */
vec3 drw_view_position()
{
  return drw_view.viewinv[3].xyz;
}

/* Returns the projection matrix far clip distance. */
float drw_view_far()
{
  if (drw_view_is_perspective()) {
    return -drw_view.winmat[3][2] / (drw_view.winmat[2][2] + 1.0);
  }
  return -(drw_view.winmat[3][2] - 1.0) / drw_view.winmat[2][2];
}

/* Returns the projection matrix near clip distance. */
float drw_view_near()
{
  if (drw_view_is_perspective()) {
    return -drw_view.winmat[3][2] / (drw_view.winmat[2][2] - 1.0);
  }
  return -(drw_view.winmat[3][2] + 1.0) / drw_view.winmat[2][2];
}

/**
 * Returns the world incident vector `V` (going towards the viewer)
 * from the world position `P` and the current view.
 */
vec3 drw_world_incident_vector(vec3 P)
{
  return drw_view_is_perspective() ? normalize(drw_view_position() - P) : drw_view_forward();
}

/**
 * Returns the view incident vector `vV` (going towards the viewer)
 * from the view position `vP` and the current view.
 */
vec3 drw_view_incident_vector(vec3 vP)
{
  return drw_view_is_perspective() ? normalize(-vP) : vec3(0.0, 0.0, 1.0);
}

/**
 * Transform position on screen UV space [0..1] to Normalized Device Coordinate space [-1..1].
 */
vec3 drw_screen_to_ndc(vec3 ss_P)
{
  return ss_P * 2.0 - 1.0;
}
vec2 drw_screen_to_ndc(vec2 ss_P)
{
  return ss_P * 2.0 - 1.0;
}
float drw_screen_to_ndc(float ss_P)
{
  return ss_P * 2.0 - 1.0;
}

/**
 * Transform position in Normalized Device Coordinate [-1..1] to screen UV space [0..1].
 */
vec3 drw_ndc_to_screen(vec3 ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}
vec2 drw_ndc_to_screen(vec2 ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}
float drw_ndc_to_screen(float ndc_P)
{
  return ndc_P * 0.5 + 0.5;
}

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 * \{ */

vec3 drw_normal_view_to_world(vec3 vN)
{
  return (mat3x3(drw_view.viewinv) * vN);
}

vec3 drw_normal_world_to_view(vec3 N)
{
  return (mat3x3(drw_view.viewmat) * N);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Position
 * \{ */

vec3 drw_perspective_divide(vec4 hs_P)
{
  return hs_P.xyz / hs_P.w;
}

vec3 drw_point_view_to_world(vec3 vP)
{
  return (drw_view.viewinv * vec4(vP, 1.0)).xyz;
}
vec4 drw_point_view_to_homogenous(vec3 vP)
{
  return (drw_view.winmat * vec4(vP, 1.0));
}
vec3 drw_point_view_to_ndc(vec3 vP)
{
  return drw_perspective_divide(drw_point_view_to_homogenous(vP));
}

vec3 drw_point_world_to_view(vec3 P)
{
  return (drw_view.viewmat * vec4(P, 1.0)).xyz;
}
vec4 drw_point_world_to_homogenous(vec3 P)
{
  return (drw_view.winmat * (drw_view.viewmat * vec4(P, 1.0)));
}
vec3 drw_point_world_to_ndc(vec3 P)
{
  return drw_perspective_divide(drw_point_world_to_homogenous(P));
}

vec3 drw_point_ndc_to_view(vec3 ssP)
{
  return drw_perspective_divide(drw_view.wininv * vec4(ssP, 1.0));
}
vec3 drw_point_ndc_to_world(vec3 ssP)
{
  return drw_point_view_to_world(drw_point_ndc_to_view(ssP));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Screen Positions
 * \{ */

vec3 drw_point_view_to_screen(vec3 vP)
{
  return drw_ndc_to_screen(drw_point_view_to_ndc(vP));
}
vec3 drw_point_world_to_screen(vec3 vP)
{
  return drw_ndc_to_screen(drw_point_world_to_ndc(vP));
}

vec3 drw_point_screen_to_view(vec3 ssP)
{
  return drw_point_ndc_to_view(drw_screen_to_ndc(ssP));
}
vec3 drw_point_screen_to_world(vec3 ssP)
{
  return drw_point_view_to_world(drw_point_screen_to_view(ssP));
}

float drw_depth_view_to_screen(float v_depth)
{
  return drw_point_view_to_screen(vec3(0.0, 0.0, v_depth)).z;
}
float drw_depth_screen_to_view(float ss_depth)
{
  return drw_point_screen_to_view(vec3(0.0, 0.0, ss_depth)).z;
}

/** \} */

/////////////////////////////// Source file 16/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL
#define GPU_SHADER_MATH_BASE_LIB_GLSL

#define M_PI 3.14159265358979323846      /* pi */
#define M_TAU 6.28318530717958647692     /* tau = 2*pi */
#define M_PI_2 1.57079632679489661923    /* pi/2 */
#define M_PI_4 0.78539816339744830962    /* pi/4 */
#define M_SQRT2 1.41421356237309504880   /* sqrt(2) */
#define M_SQRT1_2 0.70710678118654752440 /* 1/sqrt(2) */
#define M_SQRT3 1.73205080756887729352   /* sqrt(3) */
#define M_SQRT1_3 0.57735026918962576450 /* 1/sqrt(3) */
#define M_1_PI 0.318309886183790671538   /* 1/pi */
#define M_E 2.7182818284590452354        /* e */
#define M_LOG2E 1.4426950408889634074    /* log_2 e */
#define M_LOG10E 0.43429448190325182765  /* log_10 e */
#define M_LN2 0.69314718055994530942     /* log_e 2 */
#define M_LN10 2.30258509299404568402    /* log_e 10 */

/* `powf` is really slow for raising to integer powers. */

float pow2f(float x)
{
  return x * x;
}
float pow3f(float x)
{
  return x * x * x;
}
float pow4f(float x)
{
  return pow2f(pow2f(x));
}
float pow5f(float x)
{
  return pow4f(x) * x;
}
float pow6f(float x)
{
  return pow2f(pow3f(x));
}
float pow7f(float x)
{
  return pow6f(x) * x;
}
float pow8f(float x)
{
  return pow2f(pow4f(x));
}

int square_i(int v)
{
  return v * v;
}
uint square_uint(uint v)
{
  return v * v;
}
float square(float v)
{
  return v * v;
}
vec2 square(vec2 v)
{
  return v * v;
}
vec3 square(vec3 v)
{
  return v * v;
}
vec4 square(vec4 v)
{
  return v * v;
}

int cube_i(int v)
{
  return v * v * v;
}
uint cube_uint(uint v)
{
  return v * v * v;
}
float cube_f(float v)
{
  return v * v * v;
}

float hypot(float x, float y)
{
  return sqrt(x * x + y * y);
}

float atan2(float y, float x)
{
  return atan(y, x);
}

/**
 * Safe `a` modulo `b`.
 * If `b` equal 0 the result will be 0.
 */
float safe_mod(float a, float b)
{
  return (b != 0.0) ? mod(a, b) : 0.0;
}

/**
 * Returns \a a if it is a multiple of \a b or the next multiple or \a b after \b a .
 * In other words, it is equivalent to `divide_ceil(a, b) * b`.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
int ceil_to_multiple(int a, int b)
{
  return ((a + b - 1) / b) * b;
}
uint ceil_to_multiple(uint a, uint b)
{
  return ((a + b - 1u) / b) * b;
}

/**
 * Integer division that returns the ceiling, instead of flooring like normal C division.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
int divide_ceil(int a, int b)
{
  return (a + b - 1) / b;
}
uint divide_ceil(uint a, uint b)
{
  return (a + b - 1u) / b;
}

/**
 * Component wise, use vector to replace min if it is smaller and max if bigger.
 */
void min_max(float value, inout float min_v, inout float max_v)
{
  min_v = min(value, min_v);
  max_v = max(value, max_v);
}

/**
 * Safe divide `a` by `b`.
 * If `b` equal 0 the result will be 0.
 */
float safe_divide(float a, float b)
{
  return (b != 0.0) ? (a / b) : 0.0;
}

/**
 * Safe reciprocal function. Returns `1/a`.
 * If `a` equal 0 the result will be 0.
 */
float safe_rcp(float a)
{
  return (a != 0.0) ? (1.0 / a) : 0.0;
}

/**
 * Safe square root function. Returns `sqrt(a)`.
 * If `a` is less or equal to 0 then the result will be 0.
 */
float safe_sqrt(float a)
{
  return sqrt(max(0.0, a));
}

/**
 * Safe `arccosine` function. Returns `acos(a)`.
 * If `a` is greater than 1, returns 0.
 * If `a` is less than -1, returns PI.
 */
float safe_acos(float a)
{
  if (a <= -1.0) {
    return M_PI;
  }
  else if (a >= 1.0) {
    return 0.0;
  }
  return acos(a);
}

/**
 * Return true if the difference between`a` and `b` is below the `epsilon` value.
 */
bool is_equal(float a, float b, const float epsilon)
{
  return abs(a - b) <= epsilon;
}

/** \} */

#endif /* GPU_SHADER_MATH_BASE_LIB_GLSL */

/////////////////////////////// Source file 17/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_VECTOR_LIB_GLSL
#  define GPU_SHADER_MATH_VECTOR_LIB_GLSL

/* Metal does not need prototypes. */
#  ifndef GPU_METAL

/**
 * Return true if all components is equal to zero.
 */
bool is_zero(vec2 vec);
bool is_zero(vec3 vec);
bool is_zero(vec4 vec);

/**
 * Return true if any component is equal to zero.
 */
bool is_any_zero(vec2 vec);
bool is_any_zero(vec3 vec);
bool is_any_zero(vec4 vec);

/**
 * Return true if the deference between`a` and `b` is below the `epsilon` value.
 * Epsilon value is scaled by magnitude of `a` before comparison.
 */
bool almost_equal_relative(vec2 a, vec2 b, const float epsilon_factor);
bool almost_equal_relative(vec3 a, vec3 b, const float epsilon_factor);
bool almost_equal_relative(vec4 a, vec4 b, const float epsilon_factor);

/**
 * Safe `a` modulo `b`.
 * If `b` equal 0 the result will be 0.
 */
vec2 safe_mod(vec2 a, vec2 b);
vec3 safe_mod(vec3 a, vec3 b);
vec4 safe_mod(vec4 a, vec4 b);
vec2 safe_mod(vec2 a, float b);
vec3 safe_mod(vec3 a, float b);
vec4 safe_mod(vec4 a, float b);

/**
 * Returns \a a if it is a multiple of \a b or the next multiple or \a b after \b a .
 * In other words, it is equivalent to `divide_ceil(a, b) * b`.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
ivec2 ceil_to_multiple(ivec2 a, ivec2 b);
ivec3 ceil_to_multiple(ivec3 a, ivec3 b);
ivec4 ceil_to_multiple(ivec4 a, ivec4 b);
uvec2 ceil_to_multiple(uvec2 a, uvec2 b);
uvec3 ceil_to_multiple(uvec3 a, uvec3 b);
uvec4 ceil_to_multiple(uvec4 a, uvec4 b);

/**
 * Integer division that returns the ceiling, instead of flooring like normal C division.
 * It is undefined if \a a is negative or \b b is not strictly positive.
 */
ivec2 divide_ceil(ivec2 a, ivec2 b);
ivec3 divide_ceil(ivec3 a, ivec3 b);
ivec4 divide_ceil(ivec4 a, ivec4 b);
uvec2 divide_ceil(uvec2 a, uvec2 b);
uvec3 divide_ceil(uvec3 a, uvec3 b);
uvec4 divide_ceil(uvec4 a, uvec4 b);

/**
 * Component wise, use vector to replace min if it is smaller and max if bigger.
 */
void min_max(vec2 vector, inout vec2 min, inout vec2 max);
void min_max(vec3 vector, inout vec3 min, inout vec3 max);
void min_max(vec4 vector, inout vec4 min, inout vec4 max);

/**
 * Safe divide `a` by `b`.
 * If `b` equal 0 the result will be 0.
 */
vec2 safe_divide(vec2 a, vec2 b);
vec3 safe_divide(vec3 a, vec3 b);
vec4 safe_divide(vec4 a, vec4 b);
vec2 safe_divide(vec2 a, float b);
vec3 safe_divide(vec3 a, float b);
vec4 safe_divide(vec4 a, float b);

/**
 * Return the manhattan length of `a`.
 * This is also the sum of the absolute value of all components.
 */
float length_manhattan(vec2 a);
float length_manhattan(vec3 a);
float length_manhattan(vec4 a);

/**
 * Return the length squared of `a`.
 */
float length_squared(vec2 a);
float length_squared(vec3 a);
float length_squared(vec4 a);

/**
 * Return the manhattan distance between `a` and `b`.
 */
float distance_manhattan(vec2 a, vec2 b);
float distance_manhattan(vec3 a, vec3 b);
float distance_manhattan(vec4 a, vec4 b);

/**
 * Return the squared distance between `a` and `b`.
 */
float distance_squared(vec2 a, vec2 b);
float distance_squared(vec3 a, vec3 b);
float distance_squared(vec4 a, vec4 b);

/**
 * Return the projection of `p` onto `v_proj`.
 */
vec3 project(vec3 p, vec3 v_proj);

/**
 * Return normalized version of the `vector` and its length.
 */
vec2 normalize_and_get_length(vec2 vector, out float out_length);
vec3 normalize_and_get_length(vec3 vector, out float out_length);
vec4 normalize_and_get_length(vec4 vector, out float out_length);

/**
 * Return normalized version of the `vector` or a default normalized vector if `vector` is invalid.
 */
vec2 safe_normalize(vec2 vector);
vec3 safe_normalize(vec3 vector);
vec4 safe_normalize(vec4 vector);

/**
 * Safe reciprocal function. Returns `1/a`.
 * If `a` equal 0 the result will be 0.
 */
vec2 safe_rcp(vec2 a);
vec3 safe_rcp(vec3 a);
vec4 safe_rcp(vec4 a);

/**
 * Per component linear interpolation.
 */
vec2 interpolate(vec2 a, vec2 b, float t);
vec3 interpolate(vec3 a, vec3 b, float t);
vec4 interpolate(vec4 a, vec4 b, float t);

/**
 * Return half-way point between `a` and  `b`.
 */
vec2 midpoint(vec2 a, vec2 b);
vec3 midpoint(vec3 a, vec3 b);
vec4 midpoint(vec4 a, vec4 b);

/**
 * Return `vector` if `incident` and `reference` are pointing in the same direction.
 */
// vec2 faceforward(vec2 vector, vec2 incident, vec2 reference); /* Built-in GLSL. */

/**
 * Return the index of the component with the greatest absolute value.
 */
int dominant_axis(vec3 a);

/**
 * Calculates a perpendicular vector to \a v.
 * \note Returned vector can be in any perpendicular direction.
 * \note Returned vector might not the same length as \a v.
 */
vec3 orthogonal(vec3 v);
/**
 * Calculates a perpendicular vector to \a v.
 * \note Returned vector is always rotated 90 degrees counter clock wise.
 */
vec2 orthogonal(vec2 v);
ivec2 orthogonal(ivec2 v);

/**
 * Return true if the difference between`a` and `b` is below the `epsilon` value.
 */
bool is_equal(vec2 a, vec2 b, const float epsilon);
bool is_equal(vec3 a, vec3 b, const float epsilon);
bool is_equal(vec4 a, vec4 b, const float epsilon);

/**
 * Return the maximum component of a vector.
 */
float reduce_max(vec2 a);
float reduce_max(vec3 a);
float reduce_max(vec4 a);
int reduce_max(ivec2 a);
int reduce_max(ivec3 a);
int reduce_max(ivec4 a);

/**
 * Return the minimum component of a vector.
 */
float reduce_min(vec2 a);
float reduce_min(vec3 a);
float reduce_min(vec4 a);
int reduce_min(ivec2 a);
int reduce_min(ivec3 a);
int reduce_min(ivec4 a);

/**
 * Return the sum of the components of a vector.
 */
float reduce_add(vec2 a);
float reduce_add(vec3 a);
float reduce_add(vec4 a);
int reduce_add(ivec2 a);
int reduce_add(ivec3 a);
int reduce_add(ivec4 a);

/**
 * Return the average of the components of a vector.
 */
float average(vec2 a);
float average(vec3 a);
float average(vec4 a);

#  endif /* !GPU_METAL */

/* ---------------------------------------------------------------------- */
/** \name Implementation
 * \{ */

#  ifdef GPU_METAL /* Already defined in shader_defines.msl/glsl to move here. */
bool is_zero(vec2 vec)
{
  return all(equal(vec, vec2(0.0)));
}
bool is_zero(vec3 vec)
{
  return all(equal(vec, vec3(0.0)));
}
bool is_zero(vec4 vec)
{
  return all(equal(vec, vec4(0.0)));
}
#  endif /* GPU_METAL */

bool is_any_zero(vec2 vec)
{
  return any(equal(vec, vec2(0.0)));
}
bool is_any_zero(vec3 vec)
{
  return any(equal(vec, vec3(0.0)));
}
bool is_any_zero(vec4 vec)
{
  return any(equal(vec, vec4(0.0)));
}

bool almost_equal_relative(vec2 a, vec2 b, const float epsilon_factor)
{
  for (int i = 0; i < 2; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}
bool almost_equal_relative(vec3 a, vec3 b, const float epsilon_factor)
{
  for (int i = 0; i < 3; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}
bool almost_equal_relative(vec4 a, vec4 b, const float epsilon_factor)
{
  for (int i = 0; i < 4; i++) {
    if (abs(a[i] - b[i]) > epsilon_factor * abs(a[i])) {
      return false;
    }
  }
  return true;
}

vec2 safe_mod(vec2 a, vec2 b)
{
  return select(vec2(0), mod(a, b), notEqual(b, vec2(0)));
}
vec3 safe_mod(vec3 a, vec3 b)
{
  return select(vec3(0), mod(a, b), notEqual(b, vec3(0)));
}
vec4 safe_mod(vec4 a, vec4 b)
{
  return select(vec4(0), mod(a, b), notEqual(b, vec4(0)));
}

vec2 safe_mod(vec2 a, float b)
{
  return (b != 0.0) ? mod(a, vec2(b)) : vec2(0);
}
vec3 safe_mod(vec3 a, float b)
{
  return (b != 0.0) ? mod(a, vec3(b)) : vec3(0);
}
vec4 safe_mod(vec4 a, float b)
{
  return (b != 0.0) ? mod(a, vec4(b)) : vec4(0);
}

ivec2 ceil_to_multiple(ivec2 a, ivec2 b)
{
  return ((a + b - 1) / b) * b;
}
ivec3 ceil_to_multiple(ivec3 a, ivec3 b)
{
  return ((a + b - 1) / b) * b;
}
ivec4 ceil_to_multiple(ivec4 a, ivec4 b)
{
  return ((a + b - 1) / b) * b;
}
uvec2 ceil_to_multiple(uvec2 a, uvec2 b)
{
  return ((a + b - 1u) / b) * b;
}
uvec3 ceil_to_multiple(uvec3 a, uvec3 b)
{
  return ((a + b - 1u) / b) * b;
}
uvec4 ceil_to_multiple(uvec4 a, uvec4 b)
{
  return ((a + b - 1u) / b) * b;
}

ivec2 divide_ceil(ivec2 a, ivec2 b)
{
  return (a + b - 1) / b;
}
ivec3 divide_ceil(ivec3 a, ivec3 b)
{
  return (a + b - 1) / b;
}
ivec4 divide_ceil(ivec4 a, ivec4 b)
{
  return (a + b - 1) / b;
}
uvec2 divide_ceil(uvec2 a, uvec2 b)
{
  return (a + b - 1u) / b;
}
uvec3 divide_ceil(uvec3 a, uvec3 b)
{
  return (a + b - 1u) / b;
}
uvec4 divide_ceil(uvec4 a, uvec4 b)
{
  return (a + b - 1u) / b;
}

void min_max(vec2 vector, inout vec2 min_v, inout vec2 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}
void min_max(vec3 vector, inout vec3 min_v, inout vec3 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}
void min_max(vec4 vector, inout vec4 min_v, inout vec4 max_v)
{
  min_v = min(vector, min_v);
  max_v = max(vector, max_v);
}

vec2 safe_divide(vec2 a, vec2 b)
{
  return select(vec2(0), a / b, notEqual(b, vec2(0)));
}
vec3 safe_divide(vec3 a, vec3 b)
{
  return select(vec3(0), a / b, notEqual(b, vec3(0)));
}
vec4 safe_divide(vec4 a, vec4 b)
{
  return select(vec4(0), a / b, notEqual(b, vec4(0)));
}

vec2 safe_divide(vec2 a, float b)
{
  return (b != 0.0) ? (a / b) : vec2(0);
}
vec3 safe_divide(vec3 a, float b)
{
  return (b != 0.0) ? (a / b) : vec3(0);
}
vec4 safe_divide(vec4 a, float b)
{
  return (b != 0.0) ? (a / b) : vec4(0);
}

float length_manhattan(vec2 a)
{
  return dot(abs(a), vec2(1));
}
float length_manhattan(vec3 a)
{
  return dot(abs(a), vec3(1));
}
float length_manhattan(vec4 a)
{
  return dot(abs(a), vec4(1));
}

float length_squared(vec2 a)
{
  return dot(a, a);
}
float length_squared(vec3 a)
{
  return dot(a, a);
}
float length_squared(vec4 a)
{
  return dot(a, a);
}

float distance_manhattan(vec2 a, vec2 b)
{
  return length_manhattan(a - b);
}
float distance_manhattan(vec3 a, vec3 b)
{
  return length_manhattan(a - b);
}
float distance_manhattan(vec4 a, vec4 b)
{
  return length_manhattan(a - b);
}

float distance_squared(vec2 a, vec2 b)
{
  return length_squared(a - b);
}
float distance_squared(vec3 a, vec3 b)
{
  return length_squared(a - b);
}
float distance_squared(vec4 a, vec4 b)
{
  return length_squared(a - b);
}

vec3 project(vec3 p, vec3 v_proj)
{
  if (is_zero(v_proj)) {
    return vec3(0);
  }
  return v_proj * (dot(p, v_proj) / dot(v_proj, v_proj));
}

vec2 normalize_and_get_length(vec2 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec2(0.0);
}
vec3 normalize_and_get_length(vec3 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec3(0.0);
}
vec4 normalize_and_get_length(vec4 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec4(0.0);
}

vec2 safe_normalize_and_get_length(vec2 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec2(1.0, 0.0);
}
vec3 safe_normalize_and_get_length(vec3 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec3(1.0, 0.0, 0.0);
}
vec4 safe_normalize_and_get_length(vec4 vector, out float out_length)
{
  out_length = length_squared(vector);
  const float threshold = 1e-35f;
  if (out_length > threshold) {
    out_length = sqrt(out_length);
    return vector / out_length;
  }
  /* Either the vector is small or one of its values contained `nan`. */
  out_length = 0.0;
  return vec4(1.0, 0.0, 0.0, 0.0);
}

vec2 safe_normalize(vec2 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}
vec3 safe_normalize(vec3 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}
vec4 safe_normalize(vec4 vector)
{
  float unused_length;
  return safe_normalize_and_get_length(vector, unused_length);
}

vec2 safe_rcp(vec2 a)
{
  return select(vec2(0.0), (1.0 / a), notEqual(a, vec2(0.0)));
}
vec3 safe_rcp(vec3 a)
{
  return select(vec3(0.0), (1.0 / a), notEqual(a, vec3(0.0)));
}
vec4 safe_rcp(vec4 a)
{
  return select(vec4(0.0), (1.0 / a), notEqual(a, vec4(0.0)));
}

vec2 interpolate(vec2 a, vec2 b, float t)
{
  return mix(a, b, t);
}
vec3 interpolate(vec3 a, vec3 b, float t)
{
  return mix(a, b, t);
}
vec4 interpolate(vec4 a, vec4 b, float t)
{
  return mix(a, b, t);
}

vec2 midpoint(vec2 a, vec2 b)
{
  return (a + b) * 0.5;
}
vec3 midpoint(vec3 a, vec3 b)
{
  return (a + b) * 0.5;
}
vec4 midpoint(vec4 a, vec4 b)
{
  return (a + b) * 0.5;
}

int dominant_axis(vec3 a)
{
  vec3 b = abs(a);
  return ((b.x > b.y) ? ((b.x > b.z) ? 0 : 2) : ((b.y > b.z) ? 1 : 2));
}

vec3 orthogonal(vec3 v)
{
  switch (dominant_axis(v)) {
    default:
    case 0:
      return vec3(-v.y - v.z, v.x, v.x);
    case 1:
      return vec3(v.y, -v.x - v.z, v.y);
    case 2:
      return vec3(v.z, v.z, -v.x - v.y);
  }
}

vec2 orthogonal(vec2 v)
{
  return vec2(-v.y, v.x);
}
ivec2 orthogonal(ivec2 v)
{
  return ivec2(-v.y, v.x);
}

bool is_equal(vec2 a, vec2 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec2(epsilon)));
}
bool is_equal(vec3 a, vec3 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec3(epsilon)));
}
bool is_equal(vec4 a, vec4 b, const float epsilon)
{
  return all(lessThanEqual(abs(a - b), vec4(epsilon)));
}

float reduce_max(vec2 a)
{
  return max(a.x, a.y);
}
float reduce_max(vec3 a)
{
  return max(a.x, max(a.y, a.z));
}
float reduce_max(vec4 a)
{
  return max(max(a.x, a.y), max(a.z, a.w));
}
int reduce_max(ivec2 a)
{
  return max(a.x, a.y);
}
int reduce_max(ivec3 a)
{
  return max(a.x, max(a.y, a.z));
}
int reduce_max(ivec4 a)
{
  return max(max(a.x, a.y), max(a.z, a.w));
}

float reduce_min(vec2 a)
{
  return min(a.x, a.y);
}
float reduce_min(vec3 a)
{
  return min(a.x, min(a.y, a.z));
}
float reduce_min(vec4 a)
{
  return min(min(a.x, a.y), min(a.z, a.w));
}
int reduce_min(ivec2 a)
{
  return min(a.x, a.y);
}
int reduce_min(ivec3 a)
{
  return min(a.x, min(a.y, a.z));
}
int reduce_min(ivec4 a)
{
  return min(min(a.x, a.y), min(a.z, a.w));
}

float reduce_add(vec2 a)
{
  return a.x + a.y;
}
float reduce_add(vec3 a)
{
  return a.x + a.y + a.z;
}
float reduce_add(vec4 a)
{
  return a.x + a.y + a.z + a.w;
}
int reduce_add(ivec2 a)
{
  return a.x + a.y;
}
int reduce_add(ivec3 a)
{
  return a.x + a.y + a.z;
}
int reduce_add(ivec4 a)
{
  return a.x + a.y + a.z + a.w;
}

float average(vec2 a)
{
  return reduce_add(a) * (1.0 / 2.0);
}
float average(vec3 a)
{
  return reduce_add(a) * (1.0 / 3.0);
}
float average(vec4 a)
{
  return reduce_add(a) * (1.0 / 4.0);
}

#  define ASSERT_UNIT_EPSILON 0.0002

/* Checks are flipped so NAN doesn't assert because we're making sure the value was
 * normalized and in the case we don't want NAN to be raising asserts since there
 * is nothing to be done in that case. */
bool is_unit_scale(vec2 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}
bool is_unit_scale(vec3 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}
bool is_unit_scale(vec4 v)
{
  float test_unit = length_squared(v);
  return (!(abs(test_unit - 1.0) >= ASSERT_UNIT_EPSILON) ||
          !(abs(test_unit) >= ASSERT_UNIT_EPSILON));
}

/** \} */

#endif /* GPU_SHADER_MATH_VECTOR_LIB_GLSL */

/////////////////////////////// Source file 18/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_UTILDEFINES_GLSL
#define GPU_SHADER_UTILDEFINES_GLSL

#ifndef FLT_MAX
#  define FLT_MAX uintBitsToFloat(0x7F7FFFFFu)
#  define FLT_MIN uintBitsToFloat(0x00800000u)
#  define FLT_EPSILON 1.192092896e-07F
#  define SHRT_MAX 0x00007FFF
#  define INT_MAX 0x7FFFFFFF
#  define USHRT_MAX 0x0000FFFFu
#  define UINT_MAX 0xFFFFFFFFu
#endif
#define NAN_FLT uintBitsToFloat(0x7FC00000u)
#define FLT_11_MAX uintBitsToFloat(0x477E0000)
#define FLT_10_MAX uintBitsToFloat(0x477C0000)
#define FLT_11_11_10_MAX vec3(FLT_11_MAX, FLT_11_MAX, FLT_10_MAX)

#define UNPACK2(a) (a)[0], (a)[1]
#define UNPACK3(a) (a)[0], (a)[1], (a)[2]
#define UNPACK4(a) (a)[0], (a)[1], (a)[2], (a)[3]

/**
 * Clamp input into [0..1] range.
 */
#define saturate(a) clamp(a, 0.0, 1.0)

#define isfinite(a) (!isinf(a) && !isnan(a))

/* clang-format off */
#define in_range_inclusive(val, min_v, max_v) (all(greaterThanEqual(val, min_v)) && all(lessThanEqual(val, max_v)))
#define in_range_exclusive(val, min_v, max_v) (all(greaterThan(val, min_v)) && all(lessThan(val, max_v)))
#define in_texture_range(texel, tex) (all(greaterThanEqual(texel, ivec2(0))) && all(lessThan(texel, textureSize(tex, 0).xy)))
#define in_image_range(texel, tex) (all(greaterThanEqual(texel, ivec2(0))) && all(lessThan(texel, imageSize(tex).xy)))

#define weighted_sum(val0, val1, val2, val3, weights) ((val0 * weights[0] + val1 * weights[1] + val2 * weights[2] + val3 * weights[3]) * safe_rcp(weights[0] + weights[1] + weights[2] + weights[3]))
#define weighted_sum_array(val, weights) ((val[0] * weights[0] + val[1] * weights[1] + val[2] * weights[2] + val[3] * weights[3]) * safe_rcp(weights[0] + weights[1] + weights[2] + weights[3]))
/* clang-format on */

bool flag_test(uint flag, uint val)
{
  return (flag & val) != 0u;
}
bool flag_test(int flag, uint val)
{
  return flag_test(uint(flag), val);
}
bool flag_test(int flag, int val)
{
  return (flag & val) != 0;
}

void set_flag_from_test(inout uint value, bool test, uint flag)
{
  if (test) {
    value |= flag;
  }
  else {
    value &= ~flag;
  }
}
void set_flag_from_test(inout int value, bool test, int flag)
{
  if (test) {
    value |= flag;
  }
  else {
    value &= ~flag;
  }
}

/* Keep define to match C++ implementation. */
#define SET_FLAG_FROM_TEST(value, test, flag) set_flag_from_test(value, test, flag)

/**
 * Pack two 16-bit uint into one 32-bit uint.
 */
uint packUvec2x16(uvec2 data)
{
  data = (data & 0xFFFFu) << uvec2(0u, 16u);
  return data.x | data.y;
}
uvec2 unpackUvec2x16(uint data)
{
  return (uvec2(data) >> uvec2(0u, 16u)) & uvec2(0xFFFFu);
}

/**
 * Pack four 8-bit uint into one 32-bit uint.
 */
uint packUvec4x8(uvec4 data)
{
  data = (data & 0xFFu) << uvec4(0u, 8u, 16u, 24u);
  return data.x | data.y | data.z | data.w;
}
uvec4 unpackUvec4x8(uint data)
{
  return (uvec4(data) >> uvec4(0u, 8u, 16u, 24u)) & uvec4(0xFFu);
}

/**
 * Convert from float representation to ordered int allowing min/max atomic operation.
 * Based on: https://stackoverflow.com/a/31010352
 */
int floatBitsToOrderedInt(float value)
{
  /* Floats can be sorted using their bits interpreted as integers for positive values.
   * Negative values do not follow int's two's complement ordering which is reversed.
   * So we have to XOR all bits except the sign bits in order to reverse the ordering.
   * Note that this is highly hardware dependent, but there seems to be no case of GPU where the
   * ints ares not two's complement. */
  int int_value = floatBitsToInt(value);
  return (int_value < 0) ? (int_value ^ 0x7FFFFFFF) : int_value;
}
float orderedIntBitsToFloat(int int_value)
{
  return intBitsToFloat((int_value < 0) ? (int_value ^ 0x7FFFFFFF) : int_value);
}

/**
 * Ray offset to avoid self intersection.
 *
 * This can be used to compute a modified ray start position for rays leaving from a surface.
 * From:
 * "A Fast and Robust Method for Avoiding Self-Intersection"
 * Ray Tracing Gems, chapter 6.
 */
vec3 offset_ray(vec3 P, vec3 Ng)
{
  const float origin = 1.0 / 32.0;
  const float float_scale = 1.0 / 65536.0;
  const float int_scale = 256.0;

  ivec3 of_i = ivec3(int_scale * Ng);
  of_i = ivec3((P.x < 0.0) ? -of_i.x : of_i.x,
               (P.y < 0.0) ? -of_i.y : of_i.y,
               (P.z < 0.0) ? -of_i.z : of_i.z);
  vec3 P_i = intBitsToFloat(floatBitsToInt(P) + of_i);

  vec3 uf = P + float_scale * Ng;
  return vec3((abs(P.x) < origin) ? uf.x : P_i.x,
              (abs(P.y) < origin) ? uf.y : P_i.y,
              (abs(P.z) < origin) ? uf.z : P_i.z);
}

#endif /* GPU_SHADER_UTILDEFINES_GLSL */

/////////////////////////////// Source file 19/////////////////////////////




vec3 calc_barycentric_distances(vec3 pos0, vec3 pos1, vec3 pos2)
{
  vec3 edge21 = pos2 - pos1;
  vec3 edge10 = pos1 - pos0;
  vec3 edge02 = pos0 - pos2;
  vec3 d21 = normalize(edge21);
  vec3 d10 = normalize(edge10);
  vec3 d02 = normalize(edge02);

  vec3 dists;
  float d = dot(d21, edge02);
  dists.x = sqrt(dot(edge02, edge02) - d * d);
  d = dot(d02, edge10);
  dists.y = sqrt(dot(edge10, edge10) - d * d);
  d = dot(d10, edge21);
  dists.z = sqrt(dot(edge21, edge21) - d * d);
  return dists;
}

vec2 calc_barycentric_co(int vertid)
{
  vec2 bary;
  bary.x = float((vertid % 3) == 0);
  bary.y = float((vertid % 3) == 1);
  return bary;
}

#ifdef HAIR_SHADER

/* Hairs uv and col attributes are passed by bufferTextures. */
#  define DEFINE_ATTR(type, attr) uniform samplerBuffer attr
#  define GET_ATTR(type, attr) hair_get_customdata_##type(attr)

#  define barycentric_get() hair_get_barycentric()
#  define barycentric_resolve(bary) hair_resolve_barycentric(bary)

vec3 orco_get(vec3 local_pos, mat4 modelmatinv, vec4 orco_madd[2], const samplerBuffer orco_samp)
{
  /* TODO: fix ORCO with modifiers. */
  vec3 orco = (modelmatinv * vec4(local_pos, 1.0)).xyz;
  return orco_madd[0].xyz + orco * orco_madd[1].xyz;
}

float hair_len_get(int id, const samplerBuffer len)
{
  return texelFetch(len, id).x;
}

vec4 tangent_get(const samplerBuffer attr, mat3 normalmat)
{
  /* Unsupported */
  return vec4(0.0);
}

#else /* MESH_SHADER */

#  define DEFINE_ATTR(type, attr) in type attr
#  define GET_ATTR(type, attr) attr

/* Calculated in geom shader later with calc_barycentric_co. */
#  define barycentric_get() vec2(0)
#  define barycentric_resolve(bary) bary

vec3 orco_get(vec3 local_pos, mat4 modelmatinv, vec4 orco_madd[2], vec4 orco)
{
  /* If the object does not have any deformation, the orco layer calculation is done on the fly
   * using the orco_madd factors.
   * We know when there is no orco layer when orco.w is 1.0 because it uses the generic vertex
   * attribute (which is [0,0,0,1]). */
  if (orco.w == 0.0) {
    return orco.xyz * 0.5 + 0.5;
  }
  else {
    return orco_madd[0].xyz + local_pos * orco_madd[1].xyz;
  }
}

float hair_len_get(int id, const float len)
{
  return len;
}

vec4 tangent_get(vec4 attr, mat3 normalmat)
{
  vec4 tangent;
  tangent.xyz = normalmat * attr.xyz;
  tangent.w = attr.w;
  float len_sqr = dot(tangent.xyz, tangent.xyz);
  /* Normalize only if vector is not null. */
  if (len_sqr > 0.0) {
    tangent.xyz *= inversesqrt(len_sqr);
  }
  return tangent;
}

#endif

/* Assumes GPU_VEC4 is color data. So converting to luminance like cycles. */
#define float_from_vec4(v) dot(v.rgb, vec3(0.2126, 0.7152, 0.0722))
#define float_from_vec3(v) ((v.r + v.g + v.b) * (1.0 / 3.0))
#define float_from_vec2(v) v.r

#define vec2_from_vec4(v) vec2(((v.r + v.g + v.b) * (1.0 / 3.0)), v.a)
#define vec2_from_vec3(v) vec2(((v.r + v.g + v.b) * (1.0 / 3.0)), 1.0)
#define vec2_from_float(v) vec2(v)

#define vec3_from_vec4(v) v.rgb
#define vec3_from_vec2(v) v.rrr
#define vec3_from_float(v) vec3(v)

#define vec4_from_vec3(v) vec4(v, 1.0)
#define vec4_from_vec2(v) v.rrrg
#define vec4_from_float(v) vec4(vec3(v), 1.0)

/* TODO: Move to shader_shared. */
#define RAY_TYPE_CAMERA 0
#define RAY_TYPE_SHADOW 1
#define RAY_TYPE_DIFFUSE 2
#define RAY_TYPE_GLOSSY 3

#ifdef GPU_FRAGMENT_SHADER
#  define FrontFacing gl_FrontFacing
#else
#  define FrontFacing true
#endif

/* Can't use enum here because not a header file. But would be great to do. */
#define ClosureType uint
#define CLOSURE_NONE_ID 0u
/* Diffuse */
#define CLOSURE_BSDF_DIFFUSE_ID 1u
#define CLOSURE_BSDF_OREN_NAYAR_ID 2u   /* TODO */
#define CLOSURE_BSDF_SHEEN_ID 4u        /* TODO */
#define CLOSURE_BSDF_DIFFUSE_TOON_ID 5u /* TODO */
#define CLOSURE_BSDF_TRANSLUCENT_ID 6u
/* Glossy */
#define CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID 7u
#define CLOSURE_BSDF_ASHIKHMIN_SHIRLEY_ID 8u /* TODO */
#define CLOSURE_BSDF_ASHIKHMIN_VELVET_ID 9u  /* TODO */
#define CLOSURE_BSDF_GLOSSY_TOON_ID 10u      /* TODO */
#define CLOSURE_BSDF_HAIR_REFLECTION_ID 11u  /* TODO */
/* Transmission */
#define CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID 12u
/* Glass */
#define CLOSURE_BSDF_HAIR_HUANG_ID 13u /* TODO */
/* BSSRDF */
#define CLOSURE_BSSRDF_BURLEY_ID 14u

struct ClosureUndetermined {
  vec3 color;
  float weight;
  vec3 N;
  ClosureType type;
  /* Additional data different for each closure type. */
  vec4 data;
};

ClosureUndetermined closure_new(ClosureType type)
{
  ClosureUndetermined cl;
  cl.type = type;
  return cl;
}

struct ClosureOcclusion {
  vec3 N;
};

struct ClosureDiffuse {
  float weight;
  vec3 color;
  vec3 N;
};

struct ClosureSubsurface {
  float weight;
  vec3 color;
  vec3 N;
  vec3 sss_radius;
};

struct ClosureTranslucent {
  float weight;
  vec3 color;
  vec3 N;
};

struct ClosureReflection {
  float weight;
  vec3 color;
  vec3 N;
  float roughness;
};

struct ClosureRefraction {
  float weight;
  vec3 color;
  vec3 N;
  float roughness;
  float ior;
};

struct ClosureHair {
  float weight;
  vec3 color;
  float offset;
  vec2 roughness;
  vec3 T;
};

struct ClosureVolumeScatter {
  float weight;
  vec3 scattering;
  float anisotropy;
};

struct ClosureVolumeAbsorption {
  float weight;
  vec3 absorption;
};

struct ClosureEmission {
  float weight;
  vec3 emission;
};

struct ClosureTransparency {
  float weight;
  vec3 transmittance;
  float holdout;
};

ClosureDiffuse to_closure_diffuse(ClosureUndetermined cl)
{
  ClosureDiffuse closure;
  closure.N = cl.N;
  closure.color = cl.color;
  return closure;
}

ClosureSubsurface to_closure_subsurface(ClosureUndetermined cl)
{
  ClosureSubsurface closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.sss_radius = cl.data.xyz;
  return closure;
}

ClosureTranslucent to_closure_translucent(ClosureUndetermined cl)
{
  ClosureTranslucent closure;
  closure.N = cl.N;
  closure.color = cl.color;
  return closure;
}

ClosureReflection to_closure_reflection(ClosureUndetermined cl)
{
  ClosureReflection closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.roughness = cl.data.x;
  return closure;
}

ClosureRefraction to_closure_refraction(ClosureUndetermined cl)
{
  ClosureRefraction closure;
  closure.N = cl.N;
  closure.color = cl.color;
  closure.roughness = cl.data.x;
  closure.ior = cl.data.y;
  return closure;
}

struct GlobalData {
  /** World position. */
  vec3 P;
  /** Surface Normal. Normalized, overridden by bump displacement. */
  vec3 N;
  /** Raw interpolated normal (non-normalized) data. */
  vec3 Ni;
  /** Geometric Normal. */
  vec3 Ng;
  /** Curve Tangent Space. */
  vec3 curve_T, curve_B, curve_N;
  /** Barycentric coordinates. */
  vec2 barycentric_coords;
  vec3 barycentric_dists;
  /** Ray properties (approximation). */
  int ray_type;
  float ray_depth;
  float ray_length;
  /** Hair time along hair length. 0 at base 1 at tip. */
  float hair_time;
  /** Hair time along width of the hair. */
  float hair_time_width;
  /** Hair thickness in world space. */
  float hair_thickness;
  /** Index of the strand for per strand effects. */
  int hair_strand_id;
  /** Is hair. */
  bool is_strand;
};

GlobalData g_data;

#ifndef GPU_FRAGMENT_SHADER
/* Stubs. */
vec3 dF_impl(vec3 v)
{
  return vec3(0.0);
}

void dF_branch(float fn, out vec2 result)
{
  result = vec2(0.0);
}

void dF_branch_incomplete(float fn, out vec2 result)
{
  result = vec2(0.0);
}

#elif defined(GPU_FAST_DERIVATIVE) /* TODO(@fclem): User Option? */
/* Fast derivatives */
vec3 dF_impl(vec3 v)
{
  return vec3(0.0);
}

void dF_branch(float fn, out vec2 result)
{
  result.x = DFDX_SIGN * dFdx(fn);
  result.y = DFDY_SIGN * dFdy(fn);
}

#else
/* Precise derivatives */
int g_derivative_flag = 0;

vec3 dF_impl(vec3 v)
{
  if (g_derivative_flag > 0) {
    return DFDX_SIGN * dFdx(v);
  }
  else if (g_derivative_flag < 0) {
    return DFDY_SIGN * dFdy(v);
  }
  return vec3(0.0);
}

#  define dF_branch(fn, result) \
    if (true) { \
      g_derivative_flag = 1; \
      result.x = (fn); \
      g_derivative_flag = -1; \
      result.y = (fn); \
      g_derivative_flag = 0; \
      result -= vec2((fn)); \
    }

/* Used when the non-offset value is already computed elsewhere */
#  define dF_branch_incomplete(fn, result) \
    if (true) { \
      g_derivative_flag = 1; \
      result.x = (fn); \
      g_derivative_flag = -1; \
      result.y = (fn); \
      g_derivative_flag = 0; \
    }
#endif

/* TODO(fclem): Remove. */
#define CODEGEN_LIB

/////////////////////////////// Source file 20/////////////////////////////




/**
 * G-buffer: Packing and unpacking of G-buffer data.
 *
 * See #GBuffer for a breakdown of the G-buffer layout.
 *
 * There is two way of indexing closure data from the GBuffer:
 * - per "bin": same closure indices as during the material evaluation pass.
 *              Can have none-closures.
 * - per "layer": gbuffer internal storage order. Tightly packed, will only have none-closures at
 *                the end of the array.
 *
 * Indexing per bin is better to avoid parameter discontinuity for a given closure
 * (i.e: for denoising), whereas indexing per layer is better for iterating through the closure
 * without dealing with none-closures.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Types
 *
 * \{ */

/* Note: Only specialized for the gbuffer pass. */
#ifndef GBUFFER_LAYER_MAX
#  define GBUFFER_LAYER_MAX 3
#endif
#define GBUFFER_NORMAL_MAX (GBUFFER_LAYER_MAX + /* Additional data */ 1)
#define GBUFFER_DATA_MAX (GBUFFER_LAYER_MAX * 2)
#define GBUFFER_HEADER_BITS_PER_LAYER 4
/* Note: Reserve the last 4 bits for the normal layers ids. */
#define GBUFFER_NORMAL_BITS_SHIFT 12

struct GBufferData {
  ClosureUndetermined closure[GBUFFER_LAYER_MAX];
  /* Additional object information if any closure needs it. */
  float thickness;
  uint object_id;
  /* First world normal stored in the gbuffer. Only valid if `has_any_surface` is true. */
  vec3 surface_N;
};

/* Result of Packing the GBuffer. */
struct GBufferWriter {
  /* Packed GBuffer data in layer indexing. */
  vec4 data[GBUFFER_DATA_MAX];
  /* Packed normal data. Redundant normals are omitted. */
  vec2 N[GBUFFER_NORMAL_MAX];
  /* Header containing which closures are encoded and which normals are used. */
  uint header;
  /** Only used for book-keeping. Not actually written. Can be derived from header. */
  /* Number of bins written in the header. Counts empty bins. */
  int bins_len;
  /* Number of data written in the data array. */
  int data_len;
  /* Number of normal written in the normal array. */
  int normal_len;
};

/* Result of loading the GBuffer. */
struct GBufferReader {
  ClosureUndetermined closures[GBUFFER_LAYER_MAX];
  /* First world normal stored in the gbuffer. Only valid if `has_any_surface` is true. */
  vec3 surface_N;
  /* Additional object information if any closure needs it. */
  float thickness;
  uint object_id;

  uint header;
  /* Number of valid closure encoded in the gbuffer. */
  int closure_count;
  /* Only used for book-keeping when reading. */
  int data_len;
  /* Only used for debugging and testing. */
  int normal_len;
  /* Texel of the gbuffer being read. */
  ivec2 texel;
};

ClosureType gbuffer_mode_to_closure_type(uint mode)
{
  switch (mode) {
    case GBUF_DIFFUSE:
      return ClosureType(CLOSURE_BSDF_DIFFUSE_ID);
    case GBUF_TRANSLUCENT:
      return ClosureType(CLOSURE_BSDF_TRANSLUCENT_ID);
    case GBUF_SUBSURFACE:
      return ClosureType(CLOSURE_BSSRDF_BURLEY_ID);
    case GBUF_REFLECTION_COLORLESS:
    case GBUF_REFLECTION:
      return ClosureType(CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID);
    case GBUF_REFRACTION_COLORLESS:
    case GBUF_REFRACTION:
      return ClosureType(CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID);
    default:
      return ClosureType(CLOSURE_NONE_ID);
  }
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Load / Store macros
 *
 * This allows for writing unit tests that read and write during the same shader invocation.
 * \{ */

#ifdef GBUFFER_LOAD
/* Read only shader. Use correct types and functions. */
#  define samplerGBufferHeader usampler2D
#  define samplerGBufferClosure sampler2DArray
#  define samplerGBufferNormal sampler2DArray

uint fetchGBuffer(usampler2D tx, ivec2 texel)
{
  return texelFetch(tx, texel, 0).r;
}
vec4 fetchGBuffer(sampler2DArray tx, ivec2 texel, int layer)
{
  return texelFetch(tx, ivec3(texel, layer), 0);
}

#else
#  define samplerGBufferHeader int
#  define samplerGBufferClosure uint
#  define samplerGBufferNormal float

#  ifdef GBUFFER_WRITE
/* Write only shader. Use dummy load functions. */
uint fetchGBuffer(samplerGBufferHeader tx, ivec2 texel)
{
  return uint(0);
}
vec4 fetchGBuffer(samplerGBufferClosure tx, ivec2 texel, int layer)
{
  return vec4(0.0);
}
vec4 fetchGBuffer(samplerGBufferNormal tx, ivec2 texel, int layer)
{
  return vec4(0.0);
}

#  else
/* Unit testing setup. Allow read and write in the same shader. */
GBufferWriter g_data_packed;

uint fetchGBuffer(samplerGBufferHeader tx, ivec2 texel)
{
  return g_data_packed.header;
}
vec4 fetchGBuffer(samplerGBufferClosure tx, ivec2 texel, int layer)
{
  return g_data_packed.data[layer];
}
vec4 fetchGBuffer(samplerGBufferNormal tx, ivec2 texel, int layer)
{
  return g_data_packed.N[layer].xyyy;
}

#  endif
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pack / Unpack Utils
 *
 * \{ */

bool color_is_grayscale(vec3 color)
{
  /* This tests is R == G == B. */
  return all(equal(color.rgb, color.gbr));
}

vec2 gbuffer_normal_pack(vec3 N)
{
  N /= length_manhattan(N);
  vec2 _sign = sign(N.xy);
  _sign.x = _sign.x == 0.0 ? 1.0 : _sign.x;
  _sign.y = _sign.y == 0.0 ? 1.0 : _sign.y;
  N.xy = (N.z >= 0.0) ? N.xy : ((1.0 - abs(N.yx)) * _sign);
  N.xy = N.xy * 0.5 + 0.5;
  return N.xy;
}

vec3 gbuffer_normal_unpack(vec2 N_packed)
{
  N_packed = N_packed * 2.0 - 1.0;
  vec3 N = vec3(N_packed.x, N_packed.y, 1.0 - abs(N_packed.x) - abs(N_packed.y));
  float t = clamp(-N.z, 0.0, 1.0);
  N.x += (N.x >= 0.0) ? -t : t;
  N.y += (N.y >= 0.0) ? -t : t;
  return normalize(N);
}

float gbuffer_ior_pack(float ior)
{
  return (ior > 1.0) ? (1.0 - 0.5 / ior) : (0.5 * ior);
}

float gbuffer_ior_unpack(float ior_packed)
{
  return (ior_packed > 0.5) ? (0.5 / (1.0 - ior_packed)) : (2.0 * ior_packed);
}

float gbuffer_thickness_pack(float thickness)
{
  /* TODO(fclem): Something better. */
  return gbuffer_ior_pack(thickness);
}

float gbuffer_thickness_unpack(float thickness_packed)
{
  /* TODO(fclem): Something better. */
  return gbuffer_ior_unpack(thickness_packed);
}

/**
 * Pack color with values in the range of [0..8] using a 2 bit shared exponent.
 * This allows values up to 8 with some color degradation.
 * Above 8, the result will be clamped when writing the data to the output buffer.
 * This is supposed to be stored in a 10_10_10_2_unorm format with exponent in alpha.
 */
vec4 gbuffer_closure_color_pack(vec3 color)
{
  float max_comp = max(color.x, max(color.y, color.z));
  float exponent = (max_comp > 1) ? ((max_comp > 2) ? ((max_comp > 4) ? 3.0 : 2.0) : 1.0) : 0.0;
  /* TODO(fclem): Could try dithering to avoid banding artifacts on higher exponents. */
  return vec4(color / exp2(exponent), exponent / 3.0);
}
vec3 gbuffer_closure_color_unpack(vec4 color_packed)
{
  float exponent = color_packed.a * 3.0;
  return color_packed.rgb * exp2(exponent);
}

vec4 gbuffer_sss_radii_pack(vec3 sss_radii)
{
  /* TODO(fclem): Something better. */
  return gbuffer_closure_color_pack(vec3(gbuffer_ior_pack(sss_radii.x),
                                         gbuffer_ior_pack(sss_radii.y),
                                         gbuffer_ior_pack(sss_radii.z)));
}
vec3 gbuffer_sss_radii_unpack(vec4 sss_radii_packed)
{
  /* TODO(fclem): Something better. */
  vec3 radii_packed = gbuffer_closure_color_unpack(sss_radii_packed);
  return vec3(gbuffer_ior_unpack(radii_packed.x),
              gbuffer_ior_unpack(radii_packed.y),
              gbuffer_ior_unpack(radii_packed.z));
}

/**
 * Pack value in the range of [0..8] using a 2 bit exponent.
 * This allows values up to 8 with some color degradation.
 * Above 8, the result will be clamped when writing the data to the output buffer.
 * This is supposed to be stored in a 10_10_10_2_unorm format with exponent in alpha.
 */
vec2 gbuffer_closure_intensity_pack(float value)
{
  float exponent = (value > 1) ? ((value > 2) ? ((value > 4) ? 3.0 : 2.0) : 1.0) : 0.0;
  /* TODO(fclem): Could try dithering to avoid banding artifacts on higher exponents. */
  return vec2(value / exp2(exponent), exponent / 3.0);
}
float gbuffer_closure_intensity_unpack(vec2 value_packed)
{
  float exponent = value_packed.g * 3.0;
  return value_packed.r * exp2(exponent);
}

float gbuffer_object_id_unorm16_pack(uint object_id)
{
  return float(object_id & 0xFFFFu) / float(0xFFFF);
}

uint gbuffer_object_id_unorm16_unpack(float object_id_packed)
{
  return uint(object_id_packed * float(0xFFFF));
}

float gbuffer_object_id_f16_pack(uint object_id)
{
  /* TODO(fclem): Make use of all the 16 bits in a half float.
   * This here only correctly represent values up to 1024. */
  return float(object_id);
}

uint gbuffer_object_id_f16_unpack(float object_id_packed)
{
  return uint(object_id_packed);
}

bool gbuffer_is_refraction(vec4 gbuffer)
{
  return gbuffer.w < 1.0;
}

uint gbuffer_header_pack(GBufferMode mode, uint bin)
{
  return (mode << (4u * bin));
}

GBufferMode gbuffer_header_unpack(uint data, uint bin)
{
  return GBufferMode((data >> (4u * bin)) & 15u);
}

void gbuffer_append_closure(inout GBufferWriter gbuf, GBufferMode closure_type)
{
  gbuf.header |= gbuffer_header_pack(closure_type, gbuf.bins_len);
  gbuf.bins_len++;
}
void gbuffer_register_closure(inout GBufferReader gbuf, ClosureUndetermined cl, int slot)
{
  switch (slot) {
#if GBUFFER_LAYER_MAX > 0
    case 0:
      gbuf.closures[0] = cl;
      break;
#endif
#if GBUFFER_LAYER_MAX > 1
    case 1:
      gbuf.closures[1] = cl;
      break;
#endif
#if GBUFFER_LAYER_MAX > 2
    case 2:
      gbuf.closures[2] = cl;
      break;
#endif
  }
}
void gbuffer_skip_closure(inout GBufferReader gbuf)
{
  gbuf.closure_count++;
}

ClosureUndetermined gbuffer_closure_get(GBufferReader gbuf, int i)
{
  switch (i) {
#if GBUFFER_LAYER_MAX > 0
    case 0:
      return gbuf.closures[0];
#endif
#if GBUFFER_LAYER_MAX > 1
    case 1:
      return gbuf.closures[1];
#endif
#if GBUFFER_LAYER_MAX > 2
    case 2:
      return gbuf.closures[2];
#endif
    default:
      return closure_new(CLOSURE_NONE_ID);
  }
}

void gbuffer_append_data(inout GBufferWriter gbuf, vec4 data)
{
  switch (gbuf.data_len) {
#if GBUFFER_DATA_MAX > 0
    case 0:
      gbuf.data[0] = data;
      break;
#endif
#if GBUFFER_DATA_MAX > 1
    case 1:
      gbuf.data[1] = data;
      break;
#endif
#if GBUFFER_DATA_MAX > 2
    case 2:
      gbuf.data[2] = data;
      break;
#endif
#if GBUFFER_DATA_MAX > 3
    case 3:
      gbuf.data[3] = data;
      break;
#endif
#if GBUFFER_DATA_MAX > 4
    case 4:
      gbuf.data[4] = data;
      break;
#endif
#if GBUFFER_DATA_MAX > 5
    case 5:
      gbuf.data[5] = data;
      break;
#endif
  }
  gbuf.data_len++;
}
vec4 gbuffer_pop_first_data(inout GBufferReader gbuf, samplerGBufferClosure closure_tx)
{
  vec4 data = fetchGBuffer(closure_tx, gbuf.texel, gbuf.data_len);
  gbuf.data_len++;
  return data;
}
void gbuffer_skip_data(inout GBufferReader gbuf)
{
  gbuf.data_len++;
}

/**
 * Set the dedicated normal bit for the last added closure.
 * Expects `bin_id` to be in [0..2].
 * Expects `normal_id` to be in [0..3].
 */
void gbuffer_header_normal_layer_id_set(inout uint header, int bin_id, uint normal_id)
{
  /* Layer 0 will always have normal id 0. It doesn't have to be encoded. Skip it. */
  if (bin_id == 0) {
    return;
  }
  /* -2 is to skip the bin_id 0 and start encoding for bin_id 1. This keeps the FMA. */
  header |= normal_id << ((GBUFFER_NORMAL_BITS_SHIFT - 2) + bin_id * 2);
}
int gbuffer_header_normal_layer_id_get(uint header, int bin_id)
{
  /* Layer 0 will always have normal id 0. */
  if (bin_id == 0) {
    return 0;
  }
  /* -2 is to skip the bin_id 0 and start encoding for bin_id 1. This keeps the FMA. */
  return int(3u & (header >> ((GBUFFER_NORMAL_BITS_SHIFT - 2) + bin_id * 2)));
}

void gbuffer_append_normal(inout GBufferWriter gbuf, vec3 normal)
{
  vec2 packed_N = gbuffer_normal_pack(normal);
  /* Assumes this function is called after gbuffer_append_closure. */
  int layer_id = gbuf.bins_len - 1;
  /* Try to reuse previous normals. */
#if GBUFFER_NORMAL_MAX > 1
  if (gbuf.normal_len > 0 && all(equal(gbuf.N[0], packed_N))) {
    gbuffer_header_normal_layer_id_set(gbuf.header, layer_id, 0u);
    return;
  }
#endif
#if GBUFFER_NORMAL_MAX > 2
  if (gbuf.normal_len > 1 && all(equal(gbuf.N[1], packed_N))) {
    gbuffer_header_normal_layer_id_set(gbuf.header, layer_id, 1u);
    return;
  }
#endif
#if GBUFFER_NORMAL_MAX > 3
  if (gbuf.normal_len > 2 && all(equal(gbuf.N[2], packed_N))) {
    gbuffer_header_normal_layer_id_set(gbuf.header, layer_id, 2u);
    return;
  }
#endif
  /* Could not reuse. Add another normal. */
  gbuffer_header_normal_layer_id_set(gbuf.header, layer_id, uint(gbuf.normal_len));

  switch (gbuf.normal_len) {
#if GBUFFER_NORMAL_MAX > 0
    case 0:
      gbuf.N[0] = packed_N;
      break;
#endif
#if GBUFFER_NORMAL_MAX > 1
    case 1:
      gbuf.N[1] = packed_N;
      break;
#endif
#if GBUFFER_NORMAL_MAX > 2
    case 2:
      gbuf.N[2] = packed_N;
      break;
#endif
  }
  gbuf.normal_len++;
}
vec3 gbuffer_normal_get(inout GBufferReader gbuf, int bin_id, samplerGBufferNormal normal_tx)
{
  int normal_layer_id = gbuffer_header_normal_layer_id_get(gbuf.header, bin_id);
  vec2 normal_packed = fetchGBuffer(normal_tx, gbuf.texel, normal_layer_id).rg;
  gbuf.normal_len = max(gbuf.normal_len, normal_layer_id + 1);
  return gbuffer_normal_unpack(normal_packed);
}
void gbuffer_skip_normal(inout GBufferReader gbuf)
{
  /* Nothing to do. Normals are indexed. */
}

/* Pack geometry additional infos onto the normal stack. Needs to be run last. */
void gbuffer_additional_info_pack(inout GBufferWriter gbuf, float thickness, uint object_id)
{
  gbuf.N[gbuf.normal_len] = vec2(gbuffer_thickness_pack(thickness),
                                 gbuffer_object_id_unorm16_pack(object_id));
  gbuf.normal_len++;
}
void gbuffer_additional_info_load(inout GBufferReader gbuf, samplerGBufferNormal normal_tx)
{
  vec2 data_packed = fetchGBuffer(normal_tx, gbuf.texel, gbuf.normal_len).rg;
  gbuf.normal_len++;
  gbuf.thickness = gbuffer_thickness_unpack(data_packed.x);
  gbuf.object_id = gbuffer_object_id_unorm16_unpack(data_packed.y);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pack / Unpack Closures
 *
 * \{ */

/** Outputting dummy closure is required for correct render passes in case of unlit materials. */
void gbuffer_closure_unlit_pack(inout GBufferWriter gbuf, vec3 N)
{
  gbuffer_append_closure(gbuf, GBUF_UNLIT);
  gbuffer_append_data(gbuf, vec4(0.0));
  gbuffer_append_normal(gbuf, N);
}

void gbuffer_closure_diffuse_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  gbuffer_append_closure(gbuf, GBUF_DIFFUSE);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl.color));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_diffuse_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_diffuse_load(inout GBufferReader gbuf,
                                  int layer,
                                  int bin_index,
                                  samplerGBufferClosure closure_tx,
                                  samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_DIFFUSE_ID);
  cl.color = gbuffer_closure_color_unpack(data0);
  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

void gbuffer_closure_translucent_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  gbuffer_append_closure(gbuf, GBUF_TRANSLUCENT);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl.color));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_translucent_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_translucent_load(inout GBufferReader gbuf,
                                      int layer,
                                      int bin_index,
                                      samplerGBufferClosure closure_tx,
                                      samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_TRANSLUCENT_ID);
  cl.color = gbuffer_closure_color_unpack(data0);
  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

void gbuffer_closure_subsurface_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  gbuffer_append_closure(gbuf, GBUF_SUBSURFACE);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl.color));
  gbuffer_append_data(gbuf, gbuffer_sss_radii_pack(cl.data.xyz));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_subsurface_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_subsurface_load(inout GBufferReader gbuf,
                                     int layer,
                                     int bin_index,
                                     samplerGBufferClosure closure_tx,
                                     samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  vec4 data1 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined cl = closure_new(CLOSURE_BSSRDF_BURLEY_ID);
  cl.color = gbuffer_closure_color_unpack(data0);
  cl.data.rgb = gbuffer_sss_radii_unpack(data1);
  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

void gbuffer_closure_reflection_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  gbuffer_append_closure(gbuf, GBUF_REFLECTION);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl.color));
  gbuffer_append_data(gbuf, vec4(cl.data.x, 0.0, 0.0, 0.0));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_reflection_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_reflection_load(inout GBufferReader gbuf,
                                     int layer,
                                     int bin_index,
                                     samplerGBufferClosure closure_tx,
                                     samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  vec4 data1 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID);
  cl.color = gbuffer_closure_color_unpack(data0);
  cl.data.x = data1.x;
  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

void gbuffer_closure_refraction_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  gbuffer_append_closure(gbuf, GBUF_REFRACTION);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl.color));
  gbuffer_append_data(gbuf, vec4(cl.data.x, gbuffer_ior_pack(cl.data.y), 0.0, 0.0));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_refraction_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_refraction_load(inout GBufferReader gbuf,
                                     int layer,
                                     int bin_index,
                                     samplerGBufferClosure closure_tx,
                                     samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  vec4 data1 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID);
  cl.color = gbuffer_closure_color_unpack(data0);
  cl.data.x = data1.x;
  cl.data.y = gbuffer_ior_unpack(data1.y);
  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pack / Unpack Parameter Optimized
 *
 * Special cases where we can save some data layers per closure.
 * \{ */

void gbuffer_closure_reflection_colorless_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  vec2 intensity_packed = gbuffer_closure_intensity_pack(cl.color.r);
  gbuffer_append_closure(gbuf, GBUF_REFLECTION_COLORLESS);
  gbuffer_append_data(gbuf, vec4(cl.data.x, 0.0, intensity_packed));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_reflection_colorless_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_reflection_colorless_load(inout GBufferReader gbuf,
                                               int layer,
                                               int bin_index,
                                               samplerGBufferClosure closure_tx,
                                               samplerGBufferNormal normal_tx)
{
  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID);

  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  cl.data.x = data0.x;
  cl.color = vec3(gbuffer_closure_intensity_unpack(data0.zw));

  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

void gbuffer_closure_refraction_colorless_pack(inout GBufferWriter gbuf, ClosureUndetermined cl)
{
  vec2 intensity_packed = gbuffer_closure_intensity_pack(cl.color.r);
  gbuffer_append_closure(gbuf, GBUF_REFRACTION_COLORLESS);
  gbuffer_append_data(gbuf, vec4(cl.data.x, gbuffer_ior_pack(cl.data.y), intensity_packed));
  gbuffer_append_normal(gbuf, cl.N);
}
void gbuffer_closure_refraction_colorless_skip(inout GBufferReader gbuf)
{
  gbuffer_skip_closure(gbuf);
  gbuffer_skip_data(gbuf);
  gbuffer_skip_normal(gbuf);
}
void gbuffer_closure_refraction_colorless_load(inout GBufferReader gbuf,
                                               int layer,
                                               int bin_index,
                                               samplerGBufferClosure closure_tx,
                                               samplerGBufferNormal normal_tx)
{
  ClosureUndetermined cl = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID);

  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  cl.data.x = data0.x;
  cl.data.y = gbuffer_ior_unpack(data0.y);
  cl.color = vec3(gbuffer_closure_intensity_unpack(data0.zw));

  cl.N = gbuffer_normal_get(gbuf, bin_index, normal_tx);

  gbuffer_register_closure(gbuf, cl, layer);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Pack / Unpack Special Common Optimized
 *
 * Special cases where we can save some space by packing multiple closures data together.
 * \{ */

/* Still unused. Have to finalize support.
 * Might be difficult to make it work with #gbuffer_read_bin(). */
#if 0

void gbuffer_closure_metal_clear_coat_pack(inout GBufferWriter gbuf,
                                           ClosureUndetermined cl_bottom,
                                           ClosureUndetermined cl_coat)
{
  vec2 intensity_packed = gbuffer_closure_intensity_pack(cl_coat.color.r);
  gbuffer_append_closure(gbuf, GBUF_METAL_CLEARCOAT);
  gbuffer_append_data(gbuf, gbuffer_closure_color_pack(cl_bottom.color));
  gbuffer_append_data(gbuf, vec4(cl_bottom.data.x, cl_coat.data.x, intensity_packed));
  gbuffer_append_normal(gbuf, cl_bottom.N);
}

void gbuffer_closure_metal_clear_coat_load(inout GBufferReader gbuf,
                                           samplerGBufferClosure closure_tx,
                                           samplerGBufferNormal normal_tx)
{
  vec4 data0 = gbuffer_pop_first_data(gbuf, closure_tx);
  vec4 data1 = gbuffer_pop_first_data(gbuf, closure_tx);

  ClosureUndetermined bottom = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID);
  bottom.color = gbuffer_closure_color_unpack(data0);
  bottom.data.x = data1.x;

  ClosureUndetermined coat = closure_new(CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID);
  coat.color = vec3(gbuffer_closure_intensity_unpack(data1.zw));
  coat.data.x = data1.y;

  coat.N = bottom.N = gbuffer_normal_get(gbuf, 0, normal_tx);

  gbuffer_register_closure(gbuf, bottom, 0);
  gbuffer_register_closure(gbuf, coat, 1);
}

#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Gbuffer Read / Write
 *
 * \{ */

GBufferWriter gbuffer_pack(GBufferData data_in)
{
  GBufferWriter gbuf;
  gbuf.header = 0u;
  gbuf.bins_len = 0;
  gbuf.data_len = 0;
  gbuf.normal_len = 0;

  /* Check special configurations first. */

  bool has_additional_data = false;
  for (int i = 0; i < GBUFFER_LAYER_MAX; i++) {
    ClosureUndetermined cl = data_in.closure[i];

    if (cl.weight <= 1e-5) {
      gbuf.bins_len++;
      continue;
    }

    switch (cl.type) {
      case CLOSURE_BSSRDF_BURLEY_ID:
        gbuffer_closure_subsurface_pack(gbuf, cl);
        has_additional_data = true;
        break;
      case CLOSURE_BSDF_DIFFUSE_ID:
        gbuffer_closure_diffuse_pack(gbuf, cl);
        break;
      case CLOSURE_BSDF_TRANSLUCENT_ID:
        gbuffer_closure_translucent_pack(gbuf, cl);
        has_additional_data = true;
        break;
      case CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID:
        if (color_is_grayscale(cl.color)) {
          gbuffer_closure_reflection_colorless_pack(gbuf, cl);
        }
        else {
          gbuffer_closure_reflection_pack(gbuf, cl);
        }
        break;
      case CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID:
        if (color_is_grayscale(cl.color)) {
          gbuffer_closure_refraction_colorless_pack(gbuf, cl);
        }
        else {
          gbuffer_closure_refraction_pack(gbuf, cl);
        }
        has_additional_data = true;
        break;
      default:
        gbuf.bins_len++;
        break;
    }
  }

  if (gbuf.normal_len == 0) {
    gbuffer_closure_unlit_pack(gbuf, data_in.surface_N);
  }

  if (has_additional_data) {
    gbuffer_additional_info_pack(gbuf, data_in.thickness, data_in.object_id);
  }

  return gbuf;
}

/* Return the number of closure as encoded in the given header value. */
int gbuffer_closure_count(uint header)
{
  /* Note: Need to be adjusted for different global GBUFFER_LAYER_MAX. */
  const int bits_per_layer = GBUFFER_HEADER_BITS_PER_LAYER;
  uvec3 closure_types = (uvec3(header) >> uvec3(0u, 4u, 8u)) & ((1u << bits_per_layer) - 1);

  if (closure_types.x == GBUF_METAL_CLEARCOAT) {
    return 2;
  }
  return reduce_add(ivec3(not(equal(closure_types, uvec3(0u)))));
}

/* Return the number of normal layer as encoded in the given header value. */
int gbuffer_normal_count(uint header)
{
  if (header == 0u) {
    return 0;
  }
  /* Count implicit first layer. */
  uint count = 1u;
  count += uint(((header >> 12u) & 3u) != 0);
  count += uint(((header >> 14u) & 3u) != 0);
  return int(count);
}

/* Return the type of a closure using its bin index. */
ClosureType gbuffer_closure_type_get_by_bin(uint header, int bin_index)
{
  /* TODO(fclem): Doesn't take GBUF_METAL_CLEARCOAT into account or other mode that could merge two
   * bins into one layer. */
  const int bits_per_layer = GBUFFER_HEADER_BITS_PER_LAYER;
  uint mode = (header >> (bin_index * bits_per_layer)) & ((1u << bits_per_layer) - 1);
  return gbuffer_mode_to_closure_type(mode);
}

/* Return the bin index of a closure using its layer index. */
int gbuffer_closure_get_bin_index(GBufferReader gbuf, int layer_index)
{
  int layer = 0;
  for (int bin = 0; bin < GBUFFER_LAYER_MAX; bin++) {
    GBufferMode mode = gbuffer_header_unpack(gbuf.header, bin);
    /* Gbuffer header can have holes. Skip GBUF_NONE. */
    if (mode != GBUF_NONE) {
      if (layer == layer_index) {
        return bin;
      }
      layer++;
    }
  }
  /* Should never happen. But avoid out of bound access. */
  return 0;
}

ClosureUndetermined gbuffer_closure_get_by_bin(GBufferReader gbuf, int bin_index)
{
  int layer_index = 0;
  for (int bin = 0; bin < GBUFFER_LAYER_MAX; bin++) {
    GBufferMode mode = gbuffer_header_unpack(gbuf.header, bin);
    if (bin == bin_index) {
      return gbuffer_closure_get(gbuf, layer_index);
    }
    else {
      if (mode != GBUF_NONE) {
        layer_index++;
      }
    }
  }
  /* Should never happen. */
  return closure_new(CLOSURE_NONE_ID);
}

/* Read the entirety of the GBuffer. */
GBufferReader gbuffer_read(samplerGBufferHeader header_tx,
                           samplerGBufferClosure closure_tx,
                           samplerGBufferNormal normal_tx,
                           ivec2 texel)
{
  GBufferReader gbuf;
  gbuf.texel = texel;
  gbuf.thickness = 0.0;
  gbuf.closure_count = 0;
  gbuf.object_id = 0u;
  gbuf.data_len = 0;
  gbuf.normal_len = 0;
  gbuf.surface_N = vec3(0.0);
  for (int bin = 0; bin < GBUFFER_LAYER_MAX; bin++) {
    gbuffer_register_closure(gbuf, closure_new(CLOSURE_NONE_ID), bin);
  }

  gbuf.header = fetchGBuffer(header_tx, texel);

  if (gbuf.header == 0u) {
    return gbuf;
  }

  /* First closure is always written. */
  gbuf.surface_N = gbuffer_normal_unpack(fetchGBuffer(normal_tx, texel, 0).xy);

  bool has_additional_data = false;
  for (int bin = 0; bin < GBUFFER_LAYER_MAX; bin++) {
    GBufferMode mode = gbuffer_header_unpack(gbuf.header, bin);
    switch (mode) {
      default:
      case GBUF_NONE:
        break;
      case GBUF_DIFFUSE:
        gbuffer_closure_diffuse_load(gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        break;
      case GBUF_TRANSLUCENT:
        gbuffer_closure_translucent_load(gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        has_additional_data = true;
        break;
      case GBUF_SUBSURFACE:
        gbuffer_closure_subsurface_load(gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        has_additional_data = true;
        break;
      case GBUF_REFLECTION:
        gbuffer_closure_reflection_load(gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        break;
      case GBUF_REFRACTION:
        gbuffer_closure_refraction_load(gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        has_additional_data = true;
        break;
      case GBUF_REFLECTION_COLORLESS:
        gbuffer_closure_reflection_colorless_load(
            gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        break;
      case GBUF_REFRACTION_COLORLESS:
        gbuffer_closure_refraction_colorless_load(
            gbuf, gbuf.closure_count, bin, closure_tx, normal_tx);
        gbuf.closure_count++;
        has_additional_data = true;
        break;
    }
  }

  if (has_additional_data) {
    gbuffer_additional_info_load(gbuf, normal_tx);
  }

  return gbuf;
}

/* Read only one bin from the GBuffer. */
ClosureUndetermined gbuffer_read_bin(uint header,
                                     samplerGBufferClosure closure_tx,
                                     samplerGBufferNormal normal_tx,
                                     ivec2 texel,
                                     int bin_index)
{
  GBufferReader gbuf;
  gbuf.texel = texel;
  gbuf.closure_count = 0;
  gbuf.data_len = 0;
  gbuf.normal_len = 0;
  gbuf.header = header;

  if (gbuf.header == 0u) {
    return closure_new(CLOSURE_NONE_ID);
  }

  GBufferMode mode;
  for (int bin = 0; bin < GBUFFER_LAYER_MAX; bin++) {
    mode = gbuffer_header_unpack(gbuf.header, bin);

    if (bin >= bin_index) {
      break;
    }

    switch (mode) {
      default:
      case GBUF_NONE:
        break;
      case GBUF_DIFFUSE:
        gbuffer_closure_diffuse_skip(gbuf);
        break;
      case GBUF_TRANSLUCENT:
        gbuffer_closure_translucent_skip(gbuf);
        break;
      case GBUF_SUBSURFACE:
        gbuffer_closure_subsurface_skip(gbuf);
        break;
      case GBUF_REFLECTION:
        gbuffer_closure_reflection_skip(gbuf);
        break;
      case GBUF_REFRACTION:
        gbuffer_closure_refraction_skip(gbuf);
        break;
      case GBUF_REFLECTION_COLORLESS:
        gbuffer_closure_reflection_colorless_skip(gbuf);
        break;
      case GBUF_REFRACTION_COLORLESS:
        gbuffer_closure_refraction_colorless_skip(gbuf);
        break;
    }
  }

  switch (mode) {
    default:
    case GBUF_NONE:
      gbuffer_register_closure(gbuf, closure_new(CLOSURE_NONE_ID), gbuf.closure_count);
      break;
    case GBUF_DIFFUSE:
      gbuffer_closure_diffuse_load(gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_TRANSLUCENT:
      gbuffer_closure_translucent_load(gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_SUBSURFACE:
      gbuffer_closure_subsurface_load(gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_REFLECTION:
      gbuffer_closure_reflection_load(gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_REFRACTION:
      gbuffer_closure_refraction_load(gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_REFLECTION_COLORLESS:
      gbuffer_closure_reflection_colorless_load(
          gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
    case GBUF_REFRACTION_COLORLESS:
      gbuffer_closure_refraction_colorless_load(
          gbuf, gbuf.closure_count, bin_index, closure_tx, normal_tx);
      break;
  }

  return gbuffer_closure_get(gbuf, gbuf.closure_count);
}
ClosureUndetermined gbuffer_read_bin(samplerGBufferHeader header_tx,
                                     samplerGBufferClosure closure_tx,
                                     samplerGBufferNormal normal_tx,
                                     ivec2 texel,
                                     int bin_index)
{
  return gbuffer_read_bin(fetchGBuffer(header_tx, texel), closure_tx, normal_tx, texel, bin_index);
}

/* Load thickness data only if available. Return 0 otherwise. */
float gbuffer_read_thickness(uint header, samplerGBufferNormal normal_tx, ivec2 texel)
{
  /* WATCH: Assumes all closures needing additional data are in first bin. */
  switch (gbuffer_closure_type_get_by_bin(header, 0)) {
    case CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID:
    case CLOSURE_BSDF_TRANSLUCENT_ID:
    case CLOSURE_BSSRDF_BURLEY_ID: {
      int normal_len = gbuffer_normal_count(header);
      vec2 data_packed = fetchGBuffer(normal_tx, texel, normal_len).rg;
      return gbuffer_thickness_unpack(data_packed.x);
    }
    default:
      return 0.0;
  }
}

/* Returns the first world normal stored in the gbuffer. Assume gbuffer header is non-null. */
vec3 gbuffer_read_normal(samplerGBufferNormal normal_tx, ivec2 texel)
{
  vec2 normal_packed = fetchGBuffer(normal_tx, texel, 0).rg;
  return gbuffer_normal_unpack(normal_packed);
}

/** \} */

/////////////////////////////// Source file 21/////////////////////////////




/**
 * Library to create hairs dynamically from control points.
 * This is less bandwidth intensive than fetching the vertex attributes
 * but does more ALU work per vertex. This also reduces the amount
 * of data the CPU has to precompute and transfer for each update.
 */

/* Avoid including hair functionality for shaders and materials which do not require hair.
 * Required to prevent compilation failure for missing shader inputs and uniforms when hair library
 * is included via other libraries. These are only specified in the ShaderCreateInfo when needed.
 */
#ifdef HAIR_SHADER
#  define COMMON_HAIR_LIB

/* TODO(fclem): Keep documentation but remove the uniform declaration. */
#  ifndef USE_GPU_SHADER_CREATE_INFO

/**
 * hairStrandsRes: Number of points per hair strand.
 * 2 - no subdivision
 * 3+ - 1 or more interpolated points per hair.
 */
uniform int hairStrandsRes = 8;

/**
 * hairThicknessRes : Subdivide around the hair.
 * 1 - Wire Hair: Only one pixel thick, independent of view distance.
 * 2 - Poly-strip Hair: Correct width, flat if camera is parallel.
 * 3+ - Cylinder Hair: Massive calculation but potentially perfect. Still need proper support.
 */
uniform int hairThicknessRes = 1;

/* Hair thickness shape. */
uniform float hairRadRoot = 0.01;
uniform float hairRadTip = 0.0;
uniform float hairRadShape = 0.5;
uniform bool hairCloseTip = true;

uniform mat4 hairDupliMatrix;

/* Strand batch offset when used in compute shaders. */
uniform int hairStrandOffset = 0;

/* -- Per control points -- */
uniform samplerBuffer hairPointBuffer; /* RGBA32F */

/* -- Per strands data -- */
uniform usamplerBuffer hairStrandBuffer;    /* R32UI */
uniform usamplerBuffer hairStrandSegBuffer; /* R16UI */

/* Not used, use one buffer per uv layer */
// uniform samplerBuffer hairUVBuffer; /* RG32F */
// uniform samplerBuffer hairColBuffer; /* RGBA16 linear color */
#  else
#    ifndef DRW_HAIR_INFO
#      error Ensure createInfo includes draw_hair for general use or eevee_legacy_hair_lib for EEVEE.
#    endif
#  endif /* !USE_GPU_SHADER_CREATE_INFO */

#  define point_position xyz
#  define point_time w /* Position along the hair length */

/* -- Subdivision stage -- */
/**
 * We use a transform feedback or compute shader to preprocess the strands and add more subdivision
 * to it. For the moment these are simple smooth interpolation but one could hope to see the full
 * children particle modifiers being evaluated at this stage.
 *
 * If no more subdivision is needed, we can skip this step.
 */

#  ifdef GPU_VERTEX_SHADER
float hair_get_local_time()
{
  return float(gl_VertexID % hairStrandsRes) / float(hairStrandsRes - 1);
}

int hair_get_id()
{
  return gl_VertexID / hairStrandsRes;
}
#  endif

#  ifdef GPU_COMPUTE_SHADER
float hair_get_local_time()
{
  return float(gl_GlobalInvocationID.y) / float(hairStrandsRes - 1);
}

int hair_get_id()
{
  return int(gl_GlobalInvocationID.x) + hairStrandOffset;
}
#  endif

#  ifdef HAIR_PHASE_SUBDIV
int hair_get_base_id(float local_time, int strand_segments, out float interp_time)
{
  float time_per_strand_seg = 1.0 / float(strand_segments);

  float ratio = local_time / time_per_strand_seg;
  interp_time = fract(ratio);

  return int(ratio);
}

void hair_get_interp_attrs(
    out vec4 data0, out vec4 data1, out vec4 data2, out vec4 data3, out float interp_time)
{
  float local_time = hair_get_local_time();

  int hair_id = hair_get_id();
  int strand_offset = int(texelFetch(hairStrandBuffer, hair_id).x);
  int strand_segments = int(texelFetch(hairStrandSegBuffer, hair_id).x);

  int id = hair_get_base_id(local_time, strand_segments, interp_time);

  int ofs_id = id + strand_offset;

  data0 = texelFetch(hairPointBuffer, ofs_id - 1);
  data1 = texelFetch(hairPointBuffer, ofs_id);
  data2 = texelFetch(hairPointBuffer, ofs_id + 1);
  data3 = texelFetch(hairPointBuffer, ofs_id + 2);

  if (id <= 0) {
    /* root points. Need to reconstruct previous data. */
    data0 = data1 * 2.0 - data2;
  }
  if (id + 1 >= strand_segments) {
    /* tip points. Need to reconstruct next data. */
    data3 = data2 * 2.0 - data1;
  }
}
#  endif

/* -- Drawing stage -- */
/**
 * For final drawing, the vertex index and the number of vertex per segment
 */

#  if !defined(HAIR_PHASE_SUBDIV) && defined(GPU_VERTEX_SHADER)

int hair_get_strand_id(void)
{
  return gl_VertexID / (hairStrandsRes * hairThicknessRes);
}

int hair_get_base_id(void)
{
  return gl_VertexID / hairThicknessRes;
}

/* Copied from cycles. */
float hair_shaperadius(float shape, float root, float tip, float time)
{
  float radius = 1.0 - time;

  if (shape < 0.0) {
    radius = pow(radius, 1.0 + shape);
  }
  else {
    radius = pow(radius, 1.0 / (1.0 - shape));
  }

  if (hairCloseTip && (time > 0.99)) {
    return 0.0;
  }

  return (radius * (root - tip)) + tip;
}

#    if defined(OS_MAC) && defined(GPU_OPENGL)
in float dummy;
#    endif

void hair_get_center_pos_tan_binor_time(bool is_persp,
                                        mat4 invmodel_mat,
                                        vec3 camera_pos,
                                        vec3 camera_z,
                                        out vec3 wpos,
                                        out vec3 wtan,
                                        out vec3 wbinor,
                                        out float time,
                                        out float thickness)
{
  int id = hair_get_base_id();
  vec4 data = texelFetch(hairPointBuffer, id);
  wpos = data.point_position;
  time = data.point_time;

#    if defined(OS_MAC) && defined(GPU_OPENGL)
  /* Generate a dummy read to avoid the driver bug with shaders having no
   * vertex reads on macOS (#60171) */
  wpos.y += dummy * 0.0;
#    endif

  if (time == 0.0) {
    /* Hair root */
    wtan = texelFetch(hairPointBuffer, id + 1).point_position - wpos;
  }
  else {
    wtan = wpos - texelFetch(hairPointBuffer, id - 1).point_position;
  }

  mat4 obmat = hairDupliMatrix;
  wpos = (obmat * vec4(wpos, 1.0)).xyz;
  wtan = -normalize(mat3(obmat) * wtan);

  vec3 camera_vec = (is_persp) ? camera_pos - wpos : camera_z;
  wbinor = normalize(cross(camera_vec, wtan));

  thickness = hair_shaperadius(hairRadShape, hairRadRoot, hairRadTip, time);
}

void hair_get_pos_tan_binor_time(bool is_persp,
                                 mat4 invmodel_mat,
                                 vec3 camera_pos,
                                 vec3 camera_z,
                                 out vec3 wpos,
                                 out vec3 wtan,
                                 out vec3 wbinor,
                                 out float time,
                                 out float thickness,
                                 out float thick_time)
{
  hair_get_center_pos_tan_binor_time(
      is_persp, invmodel_mat, camera_pos, camera_z, wpos, wtan, wbinor, time, thickness);
  if (hairThicknessRes > 1) {
    thick_time = float(gl_VertexID % hairThicknessRes) / float(hairThicknessRes - 1);
    thick_time = thickness * (thick_time * 2.0 - 1.0);
    /* Take object scale into account.
     * NOTE: This only works fine with uniform scaling. */
    float scale = 1.0 / length(mat3(invmodel_mat) * wbinor);
    wpos += wbinor * thick_time * scale;
  }
  else {
    /* NOTE: Ensures 'hairThickTime' is initialized -
     * avoids undefined behavior on certain macOS configurations. */
    thick_time = 0.0;
  }
}

float hair_get_customdata_float(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).r;
}

vec2 hair_get_customdata_vec2(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rg;
}

vec3 hair_get_customdata_vec3(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rgb;
}

vec4 hair_get_customdata_vec4(const samplerBuffer cd_buf)
{
  int id = hair_get_strand_id();
  return texelFetch(cd_buf, id).rgba;
}

vec3 hair_get_strand_pos(void)
{
  int id = hair_get_strand_id() * hairStrandsRes;
  return texelFetch(hairPointBuffer, id).point_position;
}

vec2 hair_get_barycentric(void)
{
  /* To match cycles without breaking into individual segment we encode if we need to invert
   * the first component into the second component. We invert if the barycentricTexCo.y
   * is NOT 0.0 or 1.0. */
  int id = hair_get_base_id();
  return vec2(float((id % 2) == 1), float(((id % 4) % 3) > 0));
}

#  endif

/* To be fed the result of hair_get_barycentric from vertex shader. */
vec2 hair_resolve_barycentric(vec2 vert_barycentric)
{
  if (fract(vert_barycentric.y) != 0.0) {
    return vec2(vert_barycentric.x, 0.0);
  }
  else {
    return vec2(1.0 - vert_barycentric.x, 0.0);
  }
}

/* Hair interpolation functions. */
vec4 hair_get_weights_cardinal(float t)
{
  float t2 = t * t;
  float t3 = t2 * t;
#  if defined(CARDINAL)
  float fc = 0.71;
#  else /* defined(CATMULL_ROM) */
  float fc = 0.5;
#  endif

  vec4 weights;
  /* GLSL Optimized version of key_curve_position_weights() */
  float fct = t * fc;
  float fct2 = t2 * fc;
  float fct3 = t3 * fc;
  weights.x = (fct2 * 2.0 - fct3) - fct;
  weights.y = (t3 * 2.0 - fct3) + (-t2 * 3.0 + fct2) + 1.0;
  weights.z = (-t3 * 2.0 + fct3) + (t2 * 3.0 - (2.0 * fct2)) + fct;
  weights.w = fct3 - fct2;
  return weights;
}

/* TODO(fclem): This one is buggy, find why. (it's not the optimization!!) */
vec4 hair_get_weights_bspline(float t)
{
  float t2 = t * t;
  float t3 = t2 * t;

  vec4 weights;
  /* GLSL Optimized version of key_curve_position_weights() */
  weights.xz = vec2(-0.16666666, -0.5) * t3 + (0.5 * t2 + 0.5 * vec2(-t, t) + 0.16666666);
  weights.y = (0.5 * t3 - t2 + 0.66666666);
  weights.w = (0.16666666 * t3);
  return weights;
}

vec4 hair_interp_data(vec4 v0, vec4 v1, vec4 v2, vec4 v3, vec4 w)
{
  return v0 * w.x + v1 * w.y + v2 * w.z + v3 * w.w;
}
#endif

/////////////////////////////// Source file 22/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_FAST_LIB_GLSL
#define GPU_SHADER_MATH_FAST_LIB_GLSL

/* [Drobot2014a] Low Level Optimizations for GCN. */
float sqrt_fast(float v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}
vec2 sqrt_fast(vec2 v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}

/* [Eberly2014] GPGPU Programming for Games and Science. */
float acos_fast(float v)
{
  float res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  return (v >= 0) ? res : M_PI - res;
}
vec2 acos_fast(vec2 v)
{
  vec2 res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  v.x = (v.x >= 0) ? res.x : M_PI - res.x;
  v.y = (v.y >= 0) ? res.y : M_PI - res.y;
  return v;
}

#endif /* GPU_SHADER_MATH_FAST_LIB_GLSL */

/////////////////////////////// Source file 23/////////////////////////////




/**
 * Version of common_math_geom_lib.glsl that doesn't rely on common_math_lib.
 * This should ultimately be rewritten inside a gpu_shader lib with higher quality standard.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)

/* ---------------------------------------------------------------------- */
/** \name Math intersection & projection functions.
 * \{ */

vec4 plane_from_quad(vec3 v0, vec3 v1, vec3 v2, vec3 v3)
{
  vec3 nor = normalize(cross(v2 - v1, v0 - v1) + cross(v0 - v3, v2 - v3));
  return vec4(nor, -dot(nor, v2));
}

vec4 plane_from_tri(vec3 v0, vec3 v1, vec3 v2)
{
  vec3 nor = normalize(cross(v2 - v1, v0 - v1));
  return vec4(nor, -dot(nor, v2));
}

float point_plane_projection_dist(vec3 line_origin, vec3 plane_origin, vec3 plane_normal)
{
  return dot(plane_normal, plane_origin - line_origin);
}

float point_line_projection_dist(vec2 point, vec2 line_origin, vec2 line_normal)
{
  return dot(line_normal, line_origin - point);
}

float line_plane_intersect_dist(vec3 line_origin,
                                vec3 line_direction,
                                vec3 plane_origin,
                                vec3 plane_normal)
{
  return dot(plane_normal, plane_origin - line_origin) / dot(plane_normal, line_direction);
}

float line_plane_intersect_dist(vec3 line_origin, vec3 line_direction, vec4 plane)
{
  vec3 plane_co = plane.xyz * (-plane.w / length_squared(plane.xyz));
  vec3 h = line_origin - plane_co;
  return -dot(plane.xyz, h) / dot(plane.xyz, line_direction);
}

vec3 line_plane_intersect(vec3 line_origin,
                          vec3 line_direction,
                          vec3 plane_origin,
                          vec3 plane_normal)
{
  float dist = line_plane_intersect_dist(line_origin, line_direction, plane_origin, plane_normal);
  return line_origin + line_direction * dist;
}

vec3 line_plane_intersect(vec3 line_origin, vec3 line_direction, vec4 plane)
{
  float dist = line_plane_intersect_dist(line_origin, line_direction, plane);
  return line_origin + line_direction * dist;
}

float line_aligned_plane_intersect_dist(vec3 line_origin, vec3 line_direction, vec3 plane_origin)
{
  /* aligned plane normal */
  vec3 L = plane_origin - line_origin;
  float disk_dist = length(L);
  vec3 plane_normal = -normalize(L);
  return -disk_dist / dot(plane_normal, line_direction);
}

vec3 line_aligned_plane_intersect(vec3 line_origin, vec3 line_direction, vec3 plane_origin)
{
  float dist = line_aligned_plane_intersect_dist(line_origin, line_direction, plane_origin);
  if (dist < 0) {
    /* if intersection is behind we fake the intersection to be
     * really far and (hopefully) not inside the radius of interest */
    dist = 1e16;
  }
  return line_origin + line_direction * dist;
}

/**
 * Returns intersection distance between the unit sphere and the line
 * with the assumption that \a line_origin is contained in the unit sphere.
 * It will always returns the farthest intersection.
 */
float line_unit_sphere_intersect_dist(vec3 line_origin, vec3 line_direction)
{
  float a = dot(line_direction, line_direction);
  float b = dot(line_direction, line_origin);
  float c = dot(line_origin, line_origin) - 1;

  float dist = 1e15;
  float determinant = b * b - a * c;
  if (determinant >= 0) {
    dist = (sqrt(determinant) - b) / a;
  }

  return dist;
}

/**
 * Returns minimum intersection distance between the unit box and the line
 * with the assumption that \a line_origin is contained in the unit box.
 * In other words, it will always returns the farthest intersection.
 */
float line_unit_box_intersect_dist(vec3 line_origin, vec3 line_direction)
{
  /* https://seblagarde.wordpress.com/2012/09/29/image-based-lighting-approaches-and-parallax-corrected-cubemap/
   */
  vec3 first_plane = (vec3(1.0) - line_origin) / line_direction;
  vec3 second_plane = (vec3(-1.0) - line_origin) / line_direction;
  vec3 farthest_plane = max(first_plane, second_plane);
  return reduce_min(farthest_plane);
}

float line_unit_box_intersect_dist_safe(vec3 line_origin, vec3 line_direction)
{
  vec3 safe_line_direction = max(vec3(1e-8), abs(line_direction)) *
                             select(vec3(1.0), -vec3(1.0), lessThan(line_direction, vec3(0.0)));
  return line_unit_box_intersect_dist(line_origin, safe_line_direction);
}

/**
 * Same as line_unit_box_intersect_dist but for 2D case.
 */
float line_unit_square_intersect_dist(vec2 line_origin, vec2 line_direction)
{
  vec2 first_plane = (vec2(1.0) - line_origin) / line_direction;
  vec2 second_plane = (vec2(-1.0) - line_origin) / line_direction;
  vec2 farthest_plane = max(first_plane, second_plane);
  return reduce_min(farthest_plane);
}

float line_unit_square_intersect_dist_safe(vec2 line_origin, vec2 line_direction)
{
  vec2 safe_line_direction = max(vec2(1e-8), abs(line_direction)) *
                             select(vec2(1.0), -vec2(1.0), lessThan(line_direction, vec2(0.0)));
  return line_unit_square_intersect_dist(line_origin, safe_line_direction);
}

/**
 * Returns clipping distance (intersection with the nearest plane) with the given axis-aligned
 * bound box along \a line_direction.
 * Safe even if \a line_direction is degenerate.
 * It assumes that an intersection exists (i.e: that \a line_direction points towards the AABB).
 */
float line_aabb_clipping_dist(vec3 line_origin, vec3 line_direction, vec3 aabb_min, vec3 aabb_max)
{
  vec3 safe_dir = select(line_direction, vec3(1e-5), lessThan(abs(line_direction), vec3(1e-5)));
  vec3 dir_inv = 1.0 / safe_dir;

  vec3 first_plane = (aabb_min - line_origin) * dir_inv;
  vec3 second_plane = (aabb_max - line_origin) * dir_inv;
  vec3 nearest_plane = min(first_plane, second_plane);
  return reduce_max(nearest_plane);
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Other useful functions.
 * \{ */

void make_orthonormal_basis(vec3 N, out vec3 T, out vec3 B)
{
  vec3 up_vector = abs(N.z) < 0.99999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
  T = normalize(cross(up_vector, N));
  B = cross(N, T);
}

/* ---- Encode / Decode Normal buffer data ---- */
/* From http://aras-p.info/texts/CompactNormalStorage.html
 * Using Method #4: Sphere-map Transform */
vec2 normal_encode(vec3 n, vec3 view)
{
  float p = sqrt(n.z * 8.0 + 8.0);
  return n.xy / p + 0.5;
}

vec3 normal_decode(vec2 enc, vec3 view)
{
  vec2 fenc = enc * 4.0 - 2.0;
  float f = dot(fenc, fenc);
  float g = sqrt(1.0 - f / 4.0);
  vec3 n;
  n.xy = fenc * g;
  n.z = 1 - f / 2;
  return n;
}

vec3 tangent_to_world(vec3 vector, vec3 N, vec3 T, vec3 B)
{
  return T * vector.x + B * vector.y + N * vector.z;
}

vec3 world_to_tangent(vec3 vector, vec3 N, vec3 T, vec3 B)
{
  return vec3(dot(T, vector), dot(B, vector), dot(N, vector));
}

/** \} */

/////////////////////////////// Source file 24/////////////////////////////




/**
 * Sampling data accessors and random number generators.
 * Also contains some sample mapping functions.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Sampling data.
 *
 * Return a random values from Low Discrepancy Sequence in [0..1) range.
 * This value is uniform (constant) for the whole scene sample.
 * You might want to couple it with a noise function.
 * \{ */

#ifdef EEVEE_SAMPLING_DATA

float sampling_rng_1D_get(const eSamplingDimension dimension)
{
  return sampling_buf.dimensions[dimension];
}

vec2 sampling_rng_2D_get(const eSamplingDimension dimension)
{
  return vec2(sampling_buf.dimensions[dimension], sampling_buf.dimensions[dimension + 1u]);
}

vec3 sampling_rng_3D_get(const eSamplingDimension dimension)
{
  return vec3(sampling_buf.dimensions[dimension],
              sampling_buf.dimensions[dimension + 1u],
              sampling_buf.dimensions[dimension + 2u]);
}

#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Random Number Generators.
 * \{ */

/* Interleaved gradient noise by Jorge Jimenez
 * http://www.iryoku.com/next-generation-post-processing-in-call-of-duty-advanced-warfare
 * Seeding found by Epic Game. */
float interlieved_gradient_noise(vec2 pixel, float seed, float offset)
{
  pixel += seed * (vec2(47, 17) * 0.695);
  return fract(offset + 52.9829189 * fract(0.06711056 * pixel.x + 0.00583715 * pixel.y));
}

vec2 interlieved_gradient_noise(vec2 pixel, vec2 seed, vec2 offset)
{
  return vec2(interlieved_gradient_noise(pixel, seed.x, offset.x),
              interlieved_gradient_noise(pixel, seed.y, offset.y));
}

vec3 interlieved_gradient_noise(vec2 pixel, vec3 seed, vec3 offset)
{
  return vec3(interlieved_gradient_noise(pixel, seed.x, offset.x),
              interlieved_gradient_noise(pixel, seed.y, offset.y),
              interlieved_gradient_noise(pixel, seed.z, offset.z));
}

/* From: http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html */
float van_der_corput_radical_inverse(uint bits)
{
#if 0 /* Reference */
  bits = (bits << 16u) | (bits >> 16u);
  bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
  bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
  bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
  bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
#else
  bits = bitfieldReverse(bits);
#endif
  /* Same as dividing by 0x100000000. */
  return float(bits) * 2.3283064365386963e-10;
}

vec2 hammersley_2d(float i, float sample_count)
{
  vec2 rand;
  rand.x = i / sample_count;
  rand.y = van_der_corput_radical_inverse(uint(i));
  return rand;
}

vec2 hammersley_2d(uint i, uint sample_count)
{
  vec2 rand;
  rand.x = float(i) / float(sample_count);
  rand.y = van_der_corput_radical_inverse(i);
  return rand;
}

vec2 hammersley_2d(int i, int sample_count)
{
  return hammersley_2d(uint(i), uint(sample_count));
}

/* Not random but still useful. sample_count should be an even. */
vec2 regular_grid_2d(int i, int sample_count)
{
  int sample_per_dim = int(sqrt(float(sample_count)));
  return (vec2(i % sample_per_dim, i / sample_per_dim) + 0.5) / float(sample_per_dim);
}

/* PCG */

/* https://www.pcg-random.org/ */
uint pcg_uint(uint u)
{
  uint state = u * 747796405u + 2891336453u;
  uint word = ((state >> ((state >> 28u) + 4u)) ^ state) * 277803737u;
  return (word >> 22u) ^ word;
}

float pcg(float v)
{
  return pcg_uint(floatBitsToUint(v)) / float(0xffffffffU);
}

float pcg(vec2 v)
{
  /* Nested pcg (faster and better quality that pcg2d). */
  uvec2 u = floatBitsToUint(v);
  return pcg_uint(pcg_uint(u.x) + u.y) / float(0xffffffffU);
}

/* http://www.jcgt.org/published/0009/03/02/ */
vec3 pcg3d(vec3 v)
{
  uvec3 u = floatBitsToUint(v);

  u = u * 1664525u + 1013904223u;

  u.x += u.y * u.z;
  u.y += u.z * u.x;
  u.z += u.x * u.y;

  u ^= u >> 16u;

  u.x += u.y * u.z;
  u.y += u.z * u.x;
  u.z += u.x * u.y;

  return vec3(u) / float(0xffffffffU);
}

/* http://www.jcgt.org/published/0009/03/02/ */
vec4 pcg4d(vec4 v)
{
  uvec4 u = floatBitsToUint(v);

  u = u * 1664525u + 1013904223u;

  u.x += u.y * u.w;
  u.y += u.z * u.x;
  u.z += u.x * u.y;
  u.w += u.y * u.z;

  u ^= u >> 16u;

  u.x += u.y * u.w;
  u.y += u.z * u.x;
  u.z += u.x * u.y;
  u.w += u.y * u.z;

  return vec4(u) / float(0xffffffffU);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Distribution mapping.
 *
 * Functions mapping input random numbers to sampling shapes (i.e: hemisphere).
 * \{ */

/* Given 1 random number in [0..1] range, return a random unit circle sample. */
vec2 sample_circle(float rand)
{
  float phi = (rand - 0.5) * M_TAU;
  float cos_phi = cos(phi);
  float sin_phi = sqrt(1.0 - square(cos_phi)) * sign(phi);
  return vec2(cos_phi, sin_phi);
}

/* Given 2 random number in [0..1] range, return a random unit disk sample. */
vec2 sample_disk(vec2 rand)
{
  return sample_circle(rand.y) * sqrt(rand.x);
}

/* This transform a 2d random sample (in [0..1] range) to a sample located on a cylinder of the
 * same range. This is because the sampling functions expect such a random sample which is
 * normally precomputed. */
vec3 sample_cylinder(vec2 rand)
{
  return vec3(rand.x, sample_circle(rand.y));
}

vec3 sample_sphere(vec2 rand)
{
  float cos_theta = rand.x * 2.0 - 1.0;
  float sin_theta = safe_sqrt(1.0 - cos_theta * cos_theta);
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/**
 * Uniform hemisphere distribution.
 * \a rand is 2 random float in the [0..1] range.
 * Returns point on a Z positive hemisphere of radius 1 and centered on the origin.
 */
vec3 sample_hemisphere(vec2 rand)
{
  float cos_theta = rand.x;
  float sin_theta = safe_sqrt(1.0 - square(cos_theta));
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/**
 * Uniform cone distribution.
 * \a rand is 2 random float in the [0..1] range.
 * \a cos_angle is the cosine of the half angle.
 * Returns point on a Z positive hemisphere of radius 1 and centered on the origin.
 */
vec3 sample_uniform_cone(vec2 rand, float cos_angle)
{
  float cos_theta = mix(cos_angle, 1.0, rand.x);
  float sin_theta = safe_sqrt(1.0 - square(cos_theta));
  return vec3(sin_theta * sample_circle(rand.y), cos_theta);
}

/** \} */

/////////////////////////////// Source file 25/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(draw_math_geom_lib.glsl)

/**
 * General purpose 3D ray.
 */
struct Ray {
  vec3 origin;
  vec3 direction;
  float max_time;
};

/* Screen-space ray ([0..1] "uv" range) where direction is normalize to be as small as one
 * full-resolution pixel. The ray is also clipped to all frustum sides.
 * Z component is device normalized Z (aka. depth buffer value).
 * W component is device normalized Z + Thickness.
 */
struct ScreenSpaceRay {
  vec4 origin;
  vec4 direction;
  float max_time;
};

void raytrace_screenspace_ray_finalize(inout ScreenSpaceRay ray, vec2 pixel_size)
{
  /* Constant bias (due to depth buffer precision). Helps with self intersection. */
  /* Magic numbers for 24bits of precision.
   * From http://terathon.com/gdc07_lengyel.pdf (slide 26) */
  const float bias = -2.4e-7 * 2.0;
  ray.origin.zw += bias;
  ray.direction.zw += bias;

  ray.direction -= ray.origin;
  /* If the line is degenerate, make it cover at least one pixel
   * to not have to handle zero-pixel extent as a special case later */
  if (length_squared(ray.direction.xy) < 0.00001) {
    ray.direction.xy = vec2(0.0, 0.00001);
  }
  float ray_len_sqr = length_squared(ray.direction.xyz);
  /* Make ray.direction cover one pixel. */
  bool is_more_vertical = abs(ray.direction.x / pixel_size.x) <
                          abs(ray.direction.y / pixel_size.y);
  ray.direction /= (is_more_vertical) ? abs(ray.direction.y) : abs(ray.direction.x);
  ray.direction *= (is_more_vertical) ? pixel_size.y : pixel_size.x;
  /* Clip to segment's end. */
  ray.max_time = sqrt(ray_len_sqr * safe_rcp(length_squared(ray.direction.xyz)));
  /* Clipping to frustum sides. */
  float clip_dist = line_unit_box_intersect_dist_safe(ray.origin.xyz, ray.direction.xyz);
  ray.max_time = min(ray.max_time, clip_dist);
  /* Convert to texture coords [0..1] range. */
  ray.origin = ray.origin * 0.5 + 0.5;
  ray.direction *= 0.5;
}

ScreenSpaceRay raytrace_screenspace_ray_create(Ray ray, vec2 pixel_size)
{
  ScreenSpaceRay ssray;
  ssray.origin.xyz = drw_point_view_to_ndc(ray.origin);
  ssray.direction.xyz = drw_point_view_to_ndc(ray.origin + ray.direction * ray.max_time);

  raytrace_screenspace_ray_finalize(ssray, pixel_size);
  return ssray;
}

ScreenSpaceRay raytrace_screenspace_ray_create(Ray ray, vec2 pixel_size, float thickness)
{
  ScreenSpaceRay ssray;
  ssray.origin.xyz = drw_point_view_to_ndc(ray.origin);
  ssray.direction.xyz = drw_point_view_to_ndc(ray.origin + ray.direction * ray.max_time);
  /* Interpolate thickness in screen space.
   * Calculate thickness further away to avoid near plane clipping issues. */
  ssray.origin.w = drw_depth_view_to_screen(ray.origin.z - thickness);
  ssray.direction.w = drw_depth_view_to_screen(ray.origin.z + ray.direction.z - thickness);
  ssray.origin.w = ssray.origin.w * 2.0 - 1.0;
  ssray.direction.w = ssray.direction.w * 2.0 - 1.0;

  raytrace_screenspace_ray_finalize(ssray, pixel_size);
  return ssray;
}

/////////////////////////////// Source file 26/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)
#pragma BLENDER_REQUIRE(draw_math_geom_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_ray_types_lib.glsl)

/* TODO(Miguel Pozo): Move this function somewhere else. */
/* Return a fitted cone angle given the input roughness */
float ambient_occlusion_cone_cosine(float r)
{
  /* Using phong gloss
   * roughness = sqrt(2/(gloss+2)) */
  float gloss = -2 + 2 / (r * r);
  /* Drobot 2014 in GPUPro5 */
  // return cos(2.0 * sqrt(2.0 / (gloss + 2)));
  /* Uludag 2014 in GPUPro5 */
  // return pow(0.244, 1 / (gloss + 1));
  /* Jimenez 2016 in Practical Realtime Strategies for Accurate Indirect Occlusion. */
  return exp2(-3.32193 * r * r);
}

/* Based on Practical Realtime Strategies for Accurate Indirect Occlusion
 * http://blog.selfshadow.com/publications/s2016-shading-course/activision/s2016_pbs_activision_occlusion.pdf
 * http://blog.selfshadow.com/publications/s2016-shading-course/activision/s2016_pbs_activision_occlusion.pptx
 */

#define AO_BENT_NORMALS true
#define AO_MULTI_BOUNCE true

struct OcclusionData {
  /* 4 horizon angles, one in each direction around the view vector to form a cross pattern. */
  vec4 horizons;
  /* Custom large scale occlusion. */
  float custom_occlusion;
};

OcclusionData ambient_occlusion_data(vec4 horizons, float custom_occlusion)
{
  OcclusionData data;
  data.horizons = horizons;
  data.custom_occlusion = custom_occlusion;
  return data;
}

/* No Occlusion Data. */
OcclusionData ambient_occlusion_disabled_data()
{
  return ambient_occlusion_data(vec4(M_PI, -M_PI, M_PI, -M_PI), 1.0);
}

vec4 ambient_occlusion_pack_data(OcclusionData data)
{
  return vec4(1.0 - data.horizons * vec4(1, -1, 1, -1) * M_1_PI);
}

OcclusionData ambient_occlusion_unpack_data(vec4 v)
{
  return ambient_occlusion_data((1.0 - v) * vec4(1, -1, 1, -1) * M_PI, 0.0);
}

vec2 ambient_occlusion_get_noise(ivec2 texel)
{
  vec2 noise = utility_tx_fetch(utility_tx, vec2(texel), UTIL_BLUE_NOISE_LAYER).xy;
  return fract(noise + sampling_rng_2D_get(SAMPLING_AO_U));
}

vec2 ambient_occlusion_get_dir(float jitter)
{
  /* Only a quarter of a turn because we integrate using 2 slices.
   * We use this instead of using utiltex circle noise to improve cache hits
   * since all tracing direction will be in the same quadrant. */
  jitter *= M_PI_2;
  return vec2(cos(jitter), sin(jitter));
}

/* Return horizon angle cosine. */
float ambient_ambient_occlusion_search_horizon(vec3 vI,
                                               vec3 vP,
                                               float noise,
                                               ScreenSpaceRay ssray,
                                               sampler2D depth_tx,
                                               const float inverted,
                                               float radius,
                                               const float sample_count)
{
  /* Init at cos(M_PI). */
  float h = (inverted != 0.0) ? 1.0 : -1.0;

  ssray.max_time -= 1.0;

  if (ssray.max_time <= 2.0) {
    /* Produces self shadowing under this threshold. */
    return acos_fast(h);
  }

  float prev_time, time = 0.0;
  for (float iter = 0.0; time < ssray.max_time && iter < sample_count; iter++) {
    prev_time = time;
    /* Gives us good precision at center and ensure we cross at least one pixel per iteration. */
    time = 1.0 + iter + square((iter + noise) / sample_count) * ssray.max_time;
    float stride = time - prev_time;
    float lod = (log2(stride) - noise) / (1.0 + uniform_buf.ao.quality);

    vec2 uv = ssray.origin.xy + ssray.direction.xy * time;
    float depth = textureLod(depth_tx, uv * uniform_buf.hiz.uv_scale, floor(lod)).r;

    if (depth == 1.0 && inverted == 0.0) {
      /* Skip background. Avoids making shadow on the geometry near the far plane. */
      continue;
    }

    /* Bias depth a bit to avoid self shadowing issues. */
    const float bias = 2.0 * 2.4e-7;
    depth += (inverted != 0.0) ? -bias : bias;

    vec3 s = drw_point_screen_to_view(vec3(uv, depth));
    vec3 omega_s = s - vP;
    float len = length(omega_s);
    /* Sample's horizon angle cosine. */
    float s_h = dot(vI, omega_s / len);
    /* Blend weight to fade artifacts. */
    float dist_ratio = abs(len) / radius;
    /* Sphere falloff. */
    float dist_fac = square(saturate(dist_ratio));
    /* Unbiased, gives too much hard cut behind objects */
    // float dist_fac = step(0.999, dist_ratio);

    if (inverted != 0.0) {
      h = min(h, s_h);
    }
    else {
      h = mix(max(h, s_h), h, dist_fac);
    }
  }
  return acos_fast(h);
}

OcclusionData ambient_occlusion_search(vec3 vP,
                                       sampler2D depth_tx,
                                       ivec2 texel,
                                       float radius,
                                       const float inverted,
                                       const float dir_sample_count)
{
  vec2 noise = ambient_occlusion_get_noise(texel);
  vec2 dir = ambient_occlusion_get_dir(noise.x);
  vec2 uv = drw_point_view_to_screen(vP).xy;
  vec3 vI = (drw_view_is_perspective() ? normalize(-vP) : vec3(0.0, 0.0, 1.0));
  vec3 avg_dir = vec3(0.0);
  float avg_apperture = 0.0;

  OcclusionData data = (inverted != 0.0) ? ambient_occlusion_data(vec4(0, 0, 0, 0), 1.0) :
                                           ambient_occlusion_disabled_data();

  for (int i = 0; i < 2; i++) {
    Ray ray;
    ray.origin = vP;
    ray.direction = vec3(dir, 0.0);
    ray.max_time = radius;

    ScreenSpaceRay ssray;

    ssray = raytrace_screenspace_ray_create(ray, uniform_buf.ao.pixel_size);
    data.horizons[0 + i * 2] = ambient_ambient_occlusion_search_horizon(
        vI, vP, noise.y, ssray, depth_tx, inverted, radius, dir_sample_count);

    ray.direction = -ray.direction;

    ssray = raytrace_screenspace_ray_create(ray, uniform_buf.ao.pixel_size);
    data.horizons[1 + i * 2] = -ambient_ambient_occlusion_search_horizon(
        vI, vP, noise.y, ssray, depth_tx, inverted, radius, dir_sample_count);

    /* Rotate 90 degrees. */
    dir = vec2(-dir.y, dir.x);
  }

  return data;
}

vec2 ambient_occlusion_clamp_horizons_to_hemisphere(vec2 horizons,
                                                    float angle_N,
                                                    const float inverted)
{
  /* Add a little bias to fight self shadowing. */
  const float max_angle = M_PI_2 - 0.05;

  if (inverted != 0.0) {
    horizons.x = max(horizons.x, angle_N + max_angle);
    horizons.y = min(horizons.y, angle_N - max_angle);
  }
  else {
    horizons.x = min(horizons.x, angle_N + max_angle);
    horizons.y = max(horizons.y, angle_N - max_angle);
  }
  return horizons;
}

void ambient_occlusion_eval(OcclusionData data,
                            ivec2 texel,
                            vec3 V,
                            vec3 N,
                            vec3 Ng,
                            const float inverted,
                            out float visibility,
                            out float visibility_error,
                            out vec3 bent_normal)
{
  /* No error by default. */
  visibility_error = 1.0;

  bool early_out = (inverted != 0.0) ? (reduce_max(abs(data.horizons)) == 0.0) :
                                       (reduce_min(abs(data.horizons)) == M_PI);
  if (early_out) {
    visibility = saturate(dot(N, Ng) * 0.5 + 0.5);
    visibility = min(visibility, data.custom_occlusion);

    if (AO_BENT_NORMALS) {
      bent_normal = safe_normalize(N + Ng);
    }
    else {
      bent_normal = N;
    }
    return;
  }

  vec2 noise = ambient_occlusion_get_noise(texel);
  vec2 dir = ambient_occlusion_get_dir(noise.x);

  visibility_error = 0.0;
  visibility = 0.0;
  bent_normal = N * 0.001;

  for (int i = 0; i < 2; i++) {
    vec3 T = drw_normal_view_to_world(vec3(dir, 0.0));
    /* Setup integration domain around V. */
    vec3 B = normalize(cross(V, T));
    T = normalize(cross(B, V));

    float proj_N_len;
    vec3 proj_N = normalize_and_get_length(N - B * dot(N, B), proj_N_len);
    vec3 proj_Ng = normalize(Ng - B * dot(Ng, B));

    vec2 h = (i == 0) ? data.horizons.xy : data.horizons.zw;

    float N_sin = dot(proj_N, T);
    float Ng_sin = dot(proj_Ng, T);
    float N_cos = saturate(dot(proj_N, V));
    float Ng_cos = saturate(dot(proj_Ng, V));
    /* Gamma, angle between normalized projected normal and view vector. */
    float angle_Ng = sign(Ng_sin) * acos_fast(Ng_cos);
    float angle_N = sign(N_sin) * acos_fast(N_cos);
    /* Clamp horizons to hemisphere around shading normal. */
    h = ambient_occlusion_clamp_horizons_to_hemisphere(h, angle_N, inverted);

    float bent_angle = (h.x + h.y) * 0.5;
    /* NOTE: here we multiply z by 0.5 as it shows less difference with the geometric normal.
     * Also modulate by projected normal length to reduce issues with slanted surfaces.
     * All of this is ad-hoc and not really grounded. */
    bent_normal += proj_N_len * (T * sin(bent_angle) + V * 0.5 * cos(bent_angle));

    /* Clamp to geometric normal only for integral to keep smooth bent normal. */
    /* This is done to match Cycles ground truth but adds some computation. */
    h = ambient_occlusion_clamp_horizons_to_hemisphere(h, angle_Ng, inverted);

    /* Inner integral (Eq. 7). */
    float a = dot(-cos(2.0 * h - angle_N) + N_cos + 2.0 * h * N_sin, vec2(0.25));
    /* Correct normal not on plane (Eq. 8). */
    visibility += proj_N_len * a;
    /* Using a very low number of slices (2) leads to over-darkening of surfaces orthogonal to
     * the view. This is particularly annoying for sharp reflections occlusion. So we compute how
     * much the error is and correct the visibility later. */
    visibility_error += proj_N_len;

    /* Rotate 90 degrees. */
    dir = vec2(-dir.y, dir.x);
  }
  /* We integrated 2 directions. */
  visibility *= 0.5;
  visibility_error *= 0.5;

  visibility = min(visibility, data.custom_occlusion);

  if (AO_BENT_NORMALS) {
    /* NOTE: using pow(visibility, 6.0) produces NaN (see #87369). */
    float tmp = saturate(pow6f(visibility));
    bent_normal = normalize(mix(bent_normal, N, tmp));
  }
  else {
    bent_normal = N;
  }
}

/* Multi-bounce approximation base on surface albedo.
 * Page 78 in the PDF version. */
float ambient_occlusion_multibounce(float visibility, vec3 albedo)
{
  if (!AO_MULTI_BOUNCE) {
    return visibility;
  }

  /* Median luminance. Because Colored multibounce looks bad. */
  float lum = dot(albedo, vec3(0.3333));

  float a = 2.0404 * lum - 0.3324;
  float b = -4.7951 * lum + 0.6417;
  float c = 2.7552 * lum + 0.6903;

  float x = visibility;
  return max(x, ((x * a + b) * x + c) * x);
}

float ambient_occlusion_diffuse(OcclusionData data, ivec2 texel, vec3 V, vec3 N, vec3 Ng)
{
  vec3 unused;
  float unused_error;
  float visibility;
  ambient_occlusion_eval(data, texel, V, N, Ng, 0.0, visibility, unused_error, unused);

  return saturate(visibility);
}

float ambient_occlusion_diffuse(
    OcclusionData data, ivec2 texel, vec3 V, vec3 N, vec3 Ng, vec3 albedo, out vec3 bent_normal)
{
  float visibility;
  float unused_error;
  ambient_occlusion_eval(data, texel, V, N, Ng, 0.0, visibility, unused_error, bent_normal);

  visibility = ambient_occlusion_multibounce(visibility, albedo);

  return saturate(visibility);
}

/**
 * Approximate the area of intersection of two spherical caps
 * radius1 : First cap radius (arc length in radians)
 * radius2 : Second cap radius (in radians)
 * dist : Distance between caps (radians between centers of caps)
 * NOTE: Result is divided by pi to save one multiply.
 */
float ambient_occlusion_spherical_cap_intersection(float radius1, float radius2, float dist)
{
  /* From "Ambient Aperture Lighting" by Chris Oat
   * Slide 15. */
  float max_radius = max(radius1, radius2);
  float min_radius = min(radius1, radius2);
  float sum_radius = radius1 + radius2;
  float area;
  if (dist <= max_radius - min_radius) {
    /* One cap in completely inside the other */
    area = 1.0 - cos(min_radius);
  }
  else if (dist >= sum_radius) {
    /* No intersection exists */
    area = 0;
  }
  else {
    float diff = max_radius - min_radius;
    area = smoothstep(0.0, 1.0, 1.0 - saturate((dist - diff) / (sum_radius - diff)));
    area *= 1.0 - cos(min_radius);
  }
  return area;
}

float ambient_occlusion_specular(
    OcclusionData data, ivec2 texel, vec3 V, vec3 N, float roughness, inout vec3 specular_dir)
{
  vec3 visibility_dir;
  float visibility_error;
  float visibility;
  ambient_occlusion_eval(data, texel, V, N, N, 0.0, visibility, visibility_error, visibility_dir);

  /* Correct visibility error for very sharp surfaces. */
  visibility *= mix(safe_rcp(visibility_error), 1.0, roughness);

  specular_dir = normalize(mix(specular_dir, visibility_dir, roughness * (1.0 - visibility)));

  /* Visibility to cone angle (eq. 18). */
  float vis_angle = acos_fast(sqrt(1 - visibility));
  /* Roughness to cone angle (eq. 26). */
  /* A 0.001 min_angle can generate NaNs on Intel GPUs. See D12508. */
  const float min_angle = 0.00990998744964599609375;
  float spec_angle = max(min_angle, acos_fast(ambient_occlusion_cone_cosine(roughness)));
  /* Angle between cone axes. */
  float cone_cone_dist = acos_fast(saturate(dot(visibility_dir, specular_dir)));
  float cone_nor_dist = acos_fast(saturate(dot(N, specular_dir)));

  float isect_solid_angle = ambient_occlusion_spherical_cap_intersection(
      vis_angle, spec_angle, cone_cone_dist);
  float specular_solid_angle = ambient_occlusion_spherical_cap_intersection(
      M_PI_2, spec_angle, cone_nor_dist);
  float specular_occlusion = isect_solid_angle / specular_solid_angle;
  /* Mix because it is unstable in unoccluded areas. */
  float tmp = saturate(pow8f(visibility));
  visibility = mix(specular_occlusion, 1.0, tmp);

  return saturate(visibility);
}

/////////////////////////////// Source file 27/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)

#ifndef DRAW_MODELMAT_CREATE_INFO
#  error Missing draw_modelmat additional create info on shader create info
#endif

#if defined(UNIFORM_RESOURCE_ID)
/* TODO(fclem): Legacy API. To remove. */
#  define resource_id drw_ResourceID
#  define DRW_RESOURCE_ID_VARYING_SET

#elif defined(GPU_VERTEX_SHADER)
#  if defined(UNIFORM_RESOURCE_ID_NEW)
#    define resource_id (drw_ResourceID >> DRW_VIEW_SHIFT)
#  else
#    define resource_id gpu_InstanceIndex
#  endif
#  define DRW_RESOURCE_ID_VARYING_SET drw_ResourceID_iface.resource_index = resource_id;

#elif defined(GPU_GEOMETRY_SHADER)
#  define resource_id drw_ResourceID_iface_in[0].resource_index

#elif defined(GPU_FRAGMENT_SHADER)
#  define resource_id drw_ResourceID_iface.resource_index
#endif

mat4x4 drw_modelmat()
{
  return drw_matrix_buf[resource_id].model;
}
mat4x4 drw_modelinv()
{
  return drw_matrix_buf[resource_id].model_inverse;
}

/**
 * Usually Normal matrix is `transpose(inverse(ViewMatrix * ModelMatrix))`.
 *
 * But since it is slow to multiply matrices we decompose it. Decomposing
 * inversion and transposition both invert the product order leaving us with
 * the same original order:
 * transpose(ViewMatrixInverse) * transpose(ModelMatrixInverse)
 *
 * Knowing that the view matrix is orthogonal, the transpose is also the inverse.
 * NOTE: This is only valid because we are only using the mat3 of the ViewMatrixInverse.
 * ViewMatrix * transpose(ModelMatrixInverse)
 */
mat3x3 drw_normat()
{
  return transpose(mat3x3(drw_modelinv()));
}
mat3x3 drw_norinv()
{
  return transpose(mat3x3(drw_modelmat()));
}

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 *
 * Space conversion helpers for normal vectors.
 * \{ */

vec3 drw_normal_object_to_world(vec3 lN)
{
  return (drw_normat() * lN);
}
vec3 drw_normal_world_to_object(vec3 N)
{
  return (drw_norinv() * N);
}

vec3 drw_normal_object_to_view(vec3 lN)
{
  return (mat3x3(drw_view.viewmat) * (drw_normat() * lN));
}
vec3 drw_normal_view_to_object(vec3 vN)
{
  return (drw_norinv() * (mat3x3(drw_view.viewinv) * vN));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform Normal
 *
 * Space conversion helpers for points (coordinates).
 * \{ */

vec3 drw_point_object_to_world(vec3 lP)
{
  return (drw_modelmat() * vec4(lP, 1.0)).xyz;
}
vec3 drw_point_world_to_object(vec3 P)
{
  return (drw_modelinv() * vec4(P, 1.0)).xyz;
}

vec3 drw_point_object_to_view(vec3 lP)
{
  return (drw_view.viewmat * (drw_modelmat() * vec4(lP, 1.0))).xyz;
}
vec3 drw_point_view_to_object(vec3 vP)
{
  return (drw_modelinv() * (drw_view.viewinv * vec4(vP, 1.0))).xyz;
}

vec4 drw_point_object_to_homogenous(vec3 lP)
{
  return (drw_view.winmat * (drw_view.viewmat * (drw_modelmat() * vec4(lP, 1.0))));
}
vec3 drw_point_object_to_ndc(vec3 lP)
{
  return drw_perspective_divide(drw_point_object_to_homogenous(lP));
}

/** \} */

/////////////////////////////// Source file 28/////////////////////////////




void output_renderpass_color(int id, vec4 color)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  if (id >= 0) {
    ivec2 texel = ivec2(gl_FragCoord.xy);
    imageStore(rp_color_img, ivec3(texel, id), color);
  }
#endif
}

void output_renderpass_value(int id, float value)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  if (id >= 0) {
    ivec2 texel = ivec2(gl_FragCoord.xy);
    imageStore(rp_value_img, ivec3(texel, id), vec4(value));
  }
#endif
}

void clear_aovs()
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.color_len; i++) {
    output_renderpass_color(uniform_buf.render_pass.color_len + i, vec4(0));
  }
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.value_len; i++) {
    output_renderpass_value(uniform_buf.render_pass.value_len + i, 0.0);
  }
#endif
}

void output_aov(vec4 color, float value, uint hash)
{
#if defined(MAT_RENDER_PASS_SUPPORT) && defined(GPU_FRAGMENT_SHADER)
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.color_len; i++) {
    if (uniform_buf.render_pass.aovs.hash_color[i].x == hash) {
      imageStore(rp_color_img,
                 ivec3(ivec2(gl_FragCoord.xy), uniform_buf.render_pass.color_len + i),
                 color);
      return;
    }
  }
  for (int i = 0; i < AOV_MAX && i < uniform_buf.render_pass.aovs.value_len; i++) {
    if (uniform_buf.render_pass.aovs.hash_value[i].x == hash) {
      imageStore(rp_value_img,
                 ivec3(ivec2(gl_FragCoord.xy), uniform_buf.render_pass.value_len + i),
                 vec4(value));
      return;
    }
  }
#endif
}

/////////////////////////////// Source file 29/////////////////////////////




#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_renderpass_lib.glsl)

#define filmScalingFactor float(uniform_buf.film.scaling_factor)

vec3 g_emission;
vec3 g_transmittance;
float g_holdout;

vec3 g_volume_scattering;
float g_volume_anisotropy;
vec3 g_volume_absorption;

/* The Closure type is never used. Use float as dummy type. */
#define Closure float
#define CLOSURE_DEFAULT 0.0

/* Maximum number of picked closure. */
#ifndef CLOSURE_BIN_COUNT
#  define CLOSURE_BIN_COUNT 1
#endif
/* Sampled closure parameters. */
ClosureUndetermined g_closure_bins[CLOSURE_BIN_COUNT];
/* Random number per sampled closure type. */
float g_closure_rand[CLOSURE_BIN_COUNT];

ClosureUndetermined g_closure_get(int i)
{
  switch (i) {
    default:
    case 0:
      return g_closure_bins[0];
#if CLOSURE_BIN_COUNT > 1
    case 1:
      return g_closure_bins[1];
#endif
#if CLOSURE_BIN_COUNT > 2
    case 2:
      return g_closure_bins[2];
#endif
  }
}

ClosureUndetermined g_closure_get_resolved(int i, float weight_fac)
{
  ClosureUndetermined cl = g_closure_get(i);
  cl.color *= cl.weight * weight_fac;
  return cl;
}

ClosureType closure_type_get(ClosureDiffuse cl)
{
  return CLOSURE_BSDF_DIFFUSE_ID;
}

ClosureType closure_type_get(ClosureTranslucent cl)
{
  return CLOSURE_BSDF_TRANSLUCENT_ID;
}

ClosureType closure_type_get(ClosureReflection cl)
{
  return CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID;
}

ClosureType closure_type_get(ClosureRefraction cl)
{
  return CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID;
}

ClosureType closure_type_get(ClosureSubsurface cl)
{
  return CLOSURE_BSSRDF_BURLEY_ID;
}

/**
 * Returns true if the closure is to be selected based on the input weight.
 */
bool closure_select_check(float weight, inout float total_weight, inout float r)
{
  if (weight < 1e-5) {
    return false;
  }
  total_weight += weight;
  float x = weight / total_weight;
  bool chosen = (r < x);
  /* Assuming that if r is in the interval [0,x] or [x,1], it's still uniformly distributed within
   * that interval, so remapping to [0,1] again to explore this space of probability. */
  r = (chosen) ? (r / x) : ((r - x) / (1.0 - x));
  return chosen;
}

/**
 * Assign `candidate` to `destination` based on a random value and the respective weights.
 */
void closure_select(inout ClosureUndetermined destination,
                    inout float random,
                    ClosureUndetermined candidate)
{
  if (closure_select_check(candidate.weight, destination.weight, random)) {
    float tmp = destination.weight;
    destination = candidate;
    destination.weight = tmp;
  }
}

void closure_weights_reset(float closure_rand)
{
  g_closure_rand[0] = closure_rand;
  g_closure_bins[0].weight = 0.0;
#if CLOSURE_BIN_COUNT > 1
  g_closure_rand[1] = closure_rand;
  g_closure_bins[1].weight = 0.0;
#endif
#if CLOSURE_BIN_COUNT > 2
  g_closure_rand[2] = closure_rand;
  g_closure_bins[2].weight = 0.0;
#endif

  g_volume_scattering = vec3(0.0);
  g_volume_anisotropy = 0.0;
  g_volume_absorption = vec3(0.0);

  g_emission = vec3(0.0);
  g_transmittance = vec3(0.0);
  g_volume_scattering = vec3(0.0);
  g_volume_absorption = vec3(0.0);
  g_holdout = 0.0;
}

#define closure_base_copy(cl, in_cl) \
  cl.weight = in_cl.weight; \
  cl.color = in_cl.color; \
  cl.N = in_cl.N; \
  cl.type = closure_type_get(in_cl);

/* Single BSDFs. */
Closure closure_eval(ClosureDiffuse diffuse)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, diffuse);
#if (CLOSURE_BIN_COUNT > 1) && defined(MAT_TRANSLUCENT) && !defined(MAT_CLEARCOAT)
  /* Use second slot so we can have diffuse + translucent without noise. */
  closure_select(g_closure_bins[1], g_closure_rand[1], cl);
#else
  /* Either is single closure or use same bin as transmission bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
#endif
  return Closure(0);
}

Closure closure_eval(ClosureSubsurface diffuse)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, diffuse);
  cl.data.rgb = diffuse.sss_radius;
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

Closure closure_eval(ClosureTranslucent translucent)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, translucent);
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

/* Alternate between two bins on a per closure basis.
 * Allow clearcoat layer without noise.
 * Choosing the bin with the least weight can choose a
 * different bin for the same closure and
 * produce issue with ray-tracing denoiser.
 * Always start with the second bin, this one doesn't
 * overlap with other closure. */
bool g_closure_reflection_bin = true;
#define CHOOSE_MIN_WEIGHT_CLOSURE_BIN(a, b) \
  if (g_closure_reflection_bin) { \
    closure_select(g_closure_bins[b], g_closure_rand[b], cl); \
  } \
  else { \
    closure_select(g_closure_bins[a], g_closure_rand[a], cl); \
  } \
  g_closure_reflection_bin = !g_closure_reflection_bin;

Closure closure_eval(ClosureReflection reflection)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, reflection);
  cl.data.r = reflection.roughness;

#ifdef MAT_CLEARCOAT
#  if CLOSURE_BIN_COUNT == 2
  /* Multiple reflection closures. */
  CHOOSE_MIN_WEIGHT_CLOSURE_BIN(0, 1);
#  elif CLOSURE_BIN_COUNT == 3
  /* Multiple reflection closures and one other closure. */
  CHOOSE_MIN_WEIGHT_CLOSURE_BIN(1, 2);
#  else
#    error Clearcoat should always have at least 2 bins
#  endif
#else
#  if CLOSURE_BIN_COUNT == 1
  /* Only one reflection closure is present in the whole tree. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
#  elif CLOSURE_BIN_COUNT == 2
  /* Only one reflection and one other closure. */
  closure_select(g_closure_bins[1], g_closure_rand[1], cl);
#  elif CLOSURE_BIN_COUNT == 3
  /* Only one reflection and two other closures. */
  closure_select(g_closure_bins[2], g_closure_rand[2], cl);
#  endif
#endif

#undef CHOOSE_MIN_WEIGHT_CLOSURE_BIN

  return Closure(0);
}

Closure closure_eval(ClosureRefraction refraction)
{
  ClosureUndetermined cl;
  closure_base_copy(cl, refraction);
  cl.data.r = refraction.roughness;
  cl.data.g = refraction.ior;
  /* Transmission Closures are always in first bin. */
  closure_select(g_closure_bins[0], g_closure_rand[0], cl);
  return Closure(0);
}

Closure closure_eval(ClosureEmission emission)
{
  g_emission += emission.emission * emission.weight;
  return Closure(0);
}

Closure closure_eval(ClosureTransparency transparency)
{
  g_transmittance += transparency.transmittance * transparency.weight;
  g_holdout += transparency.holdout * transparency.weight;
  return Closure(0);
}

Closure closure_eval(ClosureVolumeScatter volume_scatter)
{
  g_volume_scattering += volume_scatter.scattering * volume_scatter.weight;
  g_volume_anisotropy += volume_scatter.anisotropy * volume_scatter.weight;
  return Closure(0);
}

Closure closure_eval(ClosureVolumeAbsorption volume_absorption)
{
  g_volume_absorption += volume_absorption.absorption * volume_absorption.weight;
  return Closure(0);
}

Closure closure_eval(ClosureHair hair)
{
  /* TODO */
  return Closure(0);
}

/* Glass BSDF. */
Closure closure_eval(ClosureReflection reflection, ClosureRefraction refraction)
{
  closure_eval(reflection);
  closure_eval(refraction);
  return Closure(0);
}

/* Dielectric BSDF. */
Closure closure_eval(ClosureDiffuse diffuse, ClosureReflection reflection)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  return Closure(0);
}

/* Coat BSDF. */
Closure closure_eval(ClosureReflection reflection, ClosureReflection coat)
{
  closure_eval(reflection);
  closure_eval(coat);
  return Closure(0);
}

/* Volume BSDF. */
Closure closure_eval(ClosureVolumeScatter volume_scatter,
                     ClosureVolumeAbsorption volume_absorption,
                     ClosureEmission emission)
{
  closure_eval(volume_scatter);
  closure_eval(volume_absorption);
  closure_eval(emission);
  return Closure(0);
}

/* Specular BSDF. */
Closure closure_eval(ClosureDiffuse diffuse, ClosureReflection reflection, ClosureReflection coat)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  closure_eval(coat);
  return Closure(0);
}

/* Principled BSDF. */
Closure closure_eval(ClosureDiffuse diffuse,
                     ClosureReflection reflection,
                     ClosureReflection coat,
                     ClosureRefraction refraction)
{
  closure_eval(diffuse);
  closure_eval(reflection);
  closure_eval(coat);
  closure_eval(refraction);
  return Closure(0);
}

/* NOP since we are sampling closures. */
Closure closure_add(Closure cl1, Closure cl2)
{
  return Closure(0);
}
Closure closure_mix(Closure cl1, Closure cl2, float fac)
{
  return Closure(0);
}

float ambient_occlusion_eval(vec3 normal,
                             float max_distance,
                             const float inverted,
                             const float sample_count)
{
  /* Avoid multi-line pre-processor conditionals.
   * Some drivers don't handle them correctly. */
  // clang-format off
#if defined(GPU_FRAGMENT_SHADER) && defined(MAT_AMBIENT_OCCLUSION) && !defined(MAT_DEPTH) && !defined(MAT_SHADOW)
  // clang-format on
#  if 0 /* TODO(fclem): Finish inverted horizon scan. */
  /* TODO(fclem): Replace eevee_ambient_occlusion_lib by eevee_horizon_scan_eval_lib when this is
   * finished. */
  vec3 vP = drw_point_world_to_view(g_data.P);
  vec3 vN = drw_normal_world_to_view(normal);

  ivec2 texel = ivec2(gl_FragCoord.xy);
  vec2 noise;
  noise.x = interlieved_gradient_noise(vec2(texel), 3.0, 0.0);
  noise.y = utility_tx_fetch(utility_tx, vec2(texel), UTIL_BLUE_NOISE_LAYER).r;
  noise = fract(noise + sampling_rng_2D_get(SAMPLING_AO_U));

  ClosureOcclusion occlusion;
  occlusion.N = (inverted != 0.0) ? -vN : vN;

  HorizonScanContext ctx;
  ctx.occlusion = occlusion;

  horizon_scan_eval(vP,
                    ctx,
                    noise,
                    uniform_buf.ao.pixel_size,
                    max_distance,
                    uniform_buf.ao.thickness,
                    uniform_buf.ao.angle_bias,
                    10,
                    inverted != 0.0);

  return saturate(ctx.occlusion_result.r);
#  else
  vec3 vP = drw_point_world_to_view(g_data.P);
  ivec2 texel = ivec2(gl_FragCoord.xy);
  OcclusionData data = ambient_occlusion_search(
      vP, hiz_tx, texel, max_distance, inverted, sample_count);

  vec3 V = drw_world_incident_vector(g_data.P);
  vec3 N = normal;
  vec3 Ng = g_data.Ng;

  float unused_error, visibility;
  vec3 unused;
  ambient_occlusion_eval(data, texel, V, N, Ng, inverted, visibility, unused_error, unused);
  return visibility;
#  endif
#else
  return 1.0;
#endif
}

#ifndef GPU_METAL
void attrib_load();
Closure nodetree_surface(float closure_rand);
Closure nodetree_volume();
vec3 nodetree_displacement();
float nodetree_thickness();
vec4 closure_to_rgba(Closure cl);
#endif

/* Simplified form of F_eta(eta, 1.0). */
float F0_from_ior(float eta)
{
  float A = (eta - 1.0) / (eta + 1.0);
  return A * A;
}

/* Return the fresnel color from a precomputed LUT value (from brdf_lut). */
vec3 F_brdf_single_scatter(vec3 f0, vec3 f90, vec2 lut)
{
  return f0 * lut.x + f90 * lut.y;
}

/* Multi-scattering brdf approximation from
 * "A Multiple-Scattering Microfacet Model for Real-Time Image-based Lighting"
 * https://jcgt.org/published/0008/01/03/paper.pdf by Carmelo J. Fdez-Agüera. */
vec3 F_brdf_multi_scatter(vec3 f0, vec3 f90, vec2 lut)
{
  vec3 FssEss = F_brdf_single_scatter(f0, f90, lut);

  float Ess = lut.x + lut.y;
  float Ems = 1.0 - Ess;
  vec3 Favg = f0 + (f90 - f0) / 21.0;

  /* The original paper uses `FssEss * radiance + Fms*Ems * irradiance`, but
   * "A Journey Through Implementing Multi-scattering BRDFs and Area Lights" by Steve McAuley
   * suggests to use `FssEss * radiance + Fms*Ems * radiance` which results in comparable quality.
   * We handle `radiance` outside of this function, so the result simplifies to:
   * `FssEss + Fms*Ems = FssEss * (1 + Ems*Favg / (1 - Ems*Favg)) = FssEss / (1 - Ems*Favg)`.
   * This is a simple albedo scaling very similar to the approach used by Cycles:
   * "Practical multiple scattering compensation for microfacet model". */
  return FssEss / (1.0 - Ems * Favg);
}

vec2 brdf_lut(float cos_theta, float roughness)
{
#ifdef EEVEE_UTILITY_TX
  return utility_tx_sample_lut(utility_tx, cos_theta, roughness, UTIL_BSDF_LAYER).rg;
#else
  return vec2(1.0, 0.0);
#endif
}

void brdf_f82_tint_lut(vec3 F0,
                       vec3 F82,
                       float cos_theta,
                       float roughness,
                       bool do_multiscatter,
                       out vec3 reflectance)
{
#ifdef EEVEE_UTILITY_TX
  vec3 split_sum = utility_tx_sample_lut(utility_tx, cos_theta, roughness, UTIL_BSDF_LAYER).rgb;
#else
  vec3 split_sum = vec3(1.0, 0.0, 0.0);
#endif

  reflectance = do_multiscatter ? F_brdf_multi_scatter(F0, vec3(1.0), split_sum.xy) :
                                  F_brdf_single_scatter(F0, vec3(1.0), split_sum.xy);

  /* Precompute the F82 term factor for the Fresnel model.
   * In the classic F82 model, the F82 input directly determines the value of the Fresnel
   * model at ~82°, similar to F0 and F90.
   * With F82-Tint, on the other hand, the value at 82° is the value of the classic Schlick
   * model multiplied by the tint input.
   * Therefore, the factor follows by setting `F82Tint(cosI) = FSchlick(cosI) - b*cosI*(1-cosI)^6`
   * and `F82Tint(acos(1/7)) = FSchlick(acos(1/7)) * f82_tint` and solving for `b`. */
  const float f = 6.0 / 7.0;
  const float f5 = (f * f) * (f * f) * f;
  const float f6 = (f * f) * (f * f) * (f * f);
  vec3 F_schlick = mix(F0, vec3(1.0), f5);
  vec3 b = F_schlick * (7.0 / f6) * (1.0 - F82);
  reflectance -= b * split_sum.z;
}

/* Return texture coordinates to sample BSDF LUT. */
vec3 lut_coords_bsdf(float cos_theta, float roughness, float ior)
{
  /* IOR is the sine of the critical angle. */
  float critical_cos = sqrt(1.0 - ior * ior);

  vec3 coords;
  coords.x = square(ior);
  coords.y = cos_theta;
  coords.y -= critical_cos;
  coords.y /= (coords.y > 0.0) ? (1.0 - critical_cos) : critical_cos;
  coords.y = coords.y * 0.5 + 0.5;
  coords.z = roughness;

  return saturate(coords);
}

/* Return texture coordinates to sample Surface LUT. */
vec3 lut_coords_btdf(float cos_theta, float roughness, float ior)
{
  return vec3(sqrt((ior - 1.0) / (ior + 1.0)), sqrt(1.0 - cos_theta), roughness);
}

/* Computes the reflectance and transmittance based on the tint (`f0`, `f90`, `transmission_tint`)
 * and the BSDF LUT. */
void bsdf_lut(vec3 F0,
              vec3 F90,
              vec3 transmission_tint,
              float cos_theta,
              float roughness,
              float ior,
              bool do_multiscatter,
              out vec3 reflectance,
              out vec3 transmittance)
{
#ifdef EEVEE_UTILITY_TX
  if (ior == 1.0) {
    reflectance = vec3(0.0);
    transmittance = transmission_tint;
    return;
  }

  vec2 split_sum;
  float transmission_factor;

  if (ior > 1.0) {
    split_sum = brdf_lut(cos_theta, roughness);
    vec3 coords = lut_coords_btdf(cos_theta, roughness, ior);
    transmission_factor = utility_tx_sample_bsdf_lut(utility_tx, coords.xy, coords.z).a;
    /* Gradually increase `f90` from 0 to 1 when IOR is in the range of [1.0, 1.33], to avoid harsh
     * transition at `IOR == 1`. */
    if (all(equal(F90, vec3(1.0)))) {
      F90 = vec3(saturate(2.33 / 0.33 * (ior - 1.0) / (ior + 1.0)));
    }
  }
  else {
    vec3 coords = lut_coords_bsdf(cos_theta, roughness, ior);
    vec3 bsdf = utility_tx_sample_bsdf_lut(utility_tx, coords.xy, coords.z).rgb;
    split_sum = bsdf.rg;
    transmission_factor = bsdf.b;
  }

  reflectance = F_brdf_single_scatter(F0, F90, split_sum);
  transmittance = (vec3(1.0) - F0) * transmission_factor * transmission_tint;

  if (do_multiscatter) {
    float real_F0 = F0_from_ior(ior);
    float Ess = real_F0 * split_sum.x + split_sum.y + (1.0 - real_F0) * transmission_factor;
    float Ems = 1.0 - Ess;
    /* Assume that the transmissive tint makes up most of the overall color if it's not zero. */
    vec3 Favg = all(equal(transmission_tint, vec3(0.0))) ? F0 + (F90 - F0) / 21.0 :
                                                           transmission_tint;

    vec3 scale = 1.0 / (1.0 - Ems * Favg);
    reflectance *= scale;
    transmittance *= scale;
  }
#else
  reflectance = vec3(0.0);
  transmittance = vec3(0.0);
#endif
  return;
}

/* Computes the reflectance and transmittance based on the BSDF LUT. */
vec2 bsdf_lut(float cos_theta, float roughness, float ior, bool do_multiscatter)
{
  float F0 = F0_from_ior(ior);
  vec3 color = vec3(1.0);
  vec3 reflectance, transmittance;
  bsdf_lut(vec3(F0),
           color,
           color,
           cos_theta,
           roughness,
           ior,
           do_multiscatter,
           reflectance,
           transmittance);
  return vec2(reflectance.r, transmittance.r);
}

#ifdef EEVEE_MATERIAL_STUBS
#  define attrib_load()
#  define nodetree_displacement() vec3(0.0)
#  define nodetree_surface(closure_rand) Closure(0)
#  define nodetree_volume() Closure(0)
#  define nodetree_thickness() 0.1
#endif

#ifdef GPU_VERTEX_SHADER
#  define closure_to_rgba(a) vec4(0.0)
#endif

/* -------------------------------------------------------------------- */
/** \name Fragment Displacement
 *
 * Displacement happening in the fragment shader.
 * Can be used in conjunction with a per vertex displacement.
 *
 * \{ */

#ifdef MAT_DISPLACEMENT_BUMP
/* Return new shading normal. */
vec3 displacement_bump()
{
#  if defined(GPU_FRAGMENT_SHADER) && !defined(MAT_GEOM_CURVES)
  vec2 dHd;
  dF_branch(dot(nodetree_displacement(), g_data.N + dF_impl(g_data.N)), dHd);

  vec3 dPdx = dFdx(g_data.P);
  vec3 dPdy = dFdy(g_data.P);

  /* Get surface tangents from normal. */
  vec3 Rx = cross(dPdy, g_data.N);
  vec3 Ry = cross(g_data.N, dPdx);

  /* Compute surface gradient and determinant. */
  float det = dot(dPdx, Rx);

  vec3 surfgrad = dHd.x * Rx + dHd.y * Ry;

  float facing = FrontFacing ? 1.0 : -1.0;
  return normalize(abs(det) * g_data.N - facing * sign(det) * surfgrad);
#  else
  return g_data.N;
#  endif
}
#endif

void fragment_displacement()
{
#ifdef MAT_DISPLACEMENT_BUMP
  g_data.N = displacement_bump();
#endif
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Coordinate implementations
 *
 * Callbacks for the texture coordinate node.
 *
 * \{ */

vec3 coordinate_camera(vec3 P)
{
  vec3 vP;
  if (false /* Probe. */) {
    /* Unsupported. It would make the probe camera-dependent. */
    vP = P;
  }
  else {
#ifdef MAT_GEOM_WORLD
    vP = drw_normal_world_to_view(P);
#else
    vP = drw_point_world_to_view(P);
#endif
  }
  vP.z = -vP.z;
  return vP;
}

vec3 coordinate_screen(vec3 P)
{
  vec3 window = vec3(0.0);
  if (false /* Probe. */) {
    /* Unsupported. It would make the probe camera-dependent. */
    window.xy = vec2(0.5);
  }
  else {
#ifdef MAT_GEOM_WORLD
    window.xy = drw_point_view_to_screen(interp.P).xy;
#else
    /* TODO(fclem): Actual camera transform. */
    window.xy = drw_point_world_to_screen(P).xy;
#endif
    window.xy = window.xy * uniform_buf.camera.uv_scale + uniform_buf.camera.uv_bias;
  }
  return window;
}

vec3 coordinate_reflect(vec3 P, vec3 N)
{
#ifdef MAT_GEOM_WORLD
  return N;
#else
  return -reflect(drw_world_incident_vector(P), N);
#endif
}

vec3 coordinate_incoming(vec3 P)
{
#ifdef MAT_GEOM_WORLD
  return -P;
#else
  return drw_world_incident_vector(P);
#endif
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Mixed render resolution
 *
 * Callbacks image texture sampling.
 *
 * \{ */

float film_scaling_factor_get()
{
  return float(uniform_buf.film.scaling_factor);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Volume Attribute post
 *
 * TODO(@fclem): These implementation details should concern the DRWManager and not be a fix on
 * the engine side. But as of now, the engines are responsible for loading the attributes.
 *
 * \{ */

/* Point clouds and curves are not compatible with volume grids.
 * They will fallback to their own attributes loading. */
#if defined(MAT_VOLUME) && !defined(MAT_GEOM_CURVES) && !defined(MAT_GEOM_POINT_CLOUD)
#  if defined(OBINFO_LIB) && !defined(MAT_GEOM_WORLD)
/* We could just check for GRID_ATTRIBUTES but this avoids for header dependency. */
#    define GRID_ATTRIBUTES_LOAD_POST
#  endif
#endif

float attr_load_temperature_post(float attr)
{
#ifdef GRID_ATTRIBUTES_LOAD_POST
  /* Bring the into standard range without having to modify the grid values */
  attr = (attr > 0.01) ? (attr * drw_volume.temperature_mul + drw_volume.temperature_bias) : 0.0;
#endif
  return attr;
}
vec4 attr_load_color_post(vec4 attr)
{
#ifdef GRID_ATTRIBUTES_LOAD_POST
  /* Density is premultiplied for interpolation, divide it out here. */
  attr.rgb *= safe_rcp(attr.a);
  attr.rgb *= drw_volume.color_mul.rgb;
  attr.a = 1.0;
#endif
  return attr;
}

#undef GRID_ATTRIBUTES_LOAD_POST

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Attributes
 *
 * TODO(@fclem): These implementation details should concern the DRWManager and not be a fix on
 * the engine side. But as of now, the engines are responsible for loading the attributes.
 *
 * \{ */

vec4 attr_load_uniform(vec4 attr, const uint attr_hash)
{
#if defined(OBATTR_LIB)
  uint index = floatBitsToUint(ObjectAttributeStart);
  for (uint i = 0; i < floatBitsToUint(ObjectAttributeLen); i++, index++) {
    if (drw_attrs[index].hash_code == attr_hash) {
      return vec4(drw_attrs[index].data_x,
                  drw_attrs[index].data_y,
                  drw_attrs[index].data_z,
                  drw_attrs[index].data_w);
    }
  }
  return vec4(0.0);
#else
  return attr;
#endif
}

/** \} */

/////////////////////////////// Source file 30/////////////////////////////




#pragma BLENDER_REQUIRE(draw_model_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_nodetree_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)

#if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER) && defined(MAT_GEOM_MESH)
vec3 barycentric_distances_get()
{
#  if defined(GPU_METAL)
  /* Calculate Barycentric distances from available parameters in Metal. */
  float wp_delta = length(dfdx(interp.P)) + length(dfdy(interp.P));
  float bc_delta = length(dfdx(gpu_BaryCoord)) + length(dfdy(gpu_BaryCoord));
  float rate_of_change = wp_delta / bc_delta;
  vec3 dists;
  dists.x = rate_of_change * (1.0 - gpu_BaryCoord.x);
  dists.y = rate_of_change * (1.0 - gpu_BaryCoord.y);
  dists.z = rate_of_change * (1.0 - gpu_BaryCoord.z);
#  else
  /* NOTE: No need to undo perspective divide since it has not been applied. */
  vec3 pos0 = (ProjectionMatrixInverse * gpu_position_at_vertex(0)).xyz;
  vec3 pos1 = (ProjectionMatrixInverse * gpu_position_at_vertex(1)).xyz;
  vec3 pos2 = (ProjectionMatrixInverse * gpu_position_at_vertex(2)).xyz;
  vec3 edge21 = pos2 - pos1;
  vec3 edge10 = pos1 - pos0;
  vec3 edge02 = pos0 - pos2;
  vec3 d21 = safe_normalize(edge21);
  vec3 d10 = safe_normalize(edge10);
  vec3 d02 = safe_normalize(edge02);
  vec3 dists;
  float d = dot(d21, edge02);
  dists.x = sqrt(dot(edge02, edge02) - d * d);
  d = dot(d02, edge10);
  dists.y = sqrt(dot(edge10, edge10) - d * d);
  d = dot(d10, edge21);
  dists.z = sqrt(dot(edge21, edge21) - d * d);
#  endif
  return dists;
}
#endif

void init_globals_mesh()
{
#if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER) && defined(MAT_GEOM_MESH)
  g_data.barycentric_coords = gpu_BaryCoord.xy;
  g_data.barycentric_dists = barycentric_distances_get();
#endif
}

void init_globals_curves()
{
#if defined(MAT_GEOM_CURVES)
  /* Shade as a cylinder. */
  float cos_theta = curve_interp.time_width / curve_interp.thickness;
#  if defined(GPU_FRAGMENT_SHADER)
  if (hairThicknessRes == 1) {
#    ifdef EEVEE_UTILITY_TX
    /* Random cosine normal distribution on the hair surface. */
    float noise = utility_tx_fetch(utility_tx, gl_FragCoord.xy, UTIL_BLUE_NOISE_LAYER).x;
#      ifdef EEVEE_SAMPLING_DATA
    /* Needs to check for SAMPLING_DATA, otherwise surfel shader validation fails. */
    noise = fract(noise + sampling_rng_1D_get(SAMPLING_CURVES_U));
#      endif
    cos_theta = noise * 2.0 - 1.0;
#    endif
  }
#  endif
  float sin_theta = sqrt(max(0.0, 1.0 - cos_theta * cos_theta));
  g_data.N = g_data.Ni = normalize(interp.N * sin_theta + curve_interp.binormal * cos_theta);

  /* Costly, but follows cycles per pixel tangent space (not following curve shape). */
  vec3 V = drw_world_incident_vector(g_data.P);
  g_data.curve_T = -curve_interp.tangent;
  g_data.curve_B = cross(V, g_data.curve_T);
  g_data.curve_N = safe_normalize(cross(g_data.curve_T, g_data.curve_B));

  g_data.is_strand = true;
  g_data.hair_time = curve_interp.time;
  g_data.hair_thickness = curve_interp.thickness;
  g_data.hair_strand_id = curve_interp_flat.strand_id;
#  if defined(USE_BARYCENTRICS) && defined(GPU_FRAGMENT_SHADER)
  g_data.barycentric_coords = hair_resolve_barycentric(curve_interp.barycentric_coords);
#  endif
#endif
}

void init_globals_gpencil()
{
  /* Undo back-face flip as the grease-pencil normal is already pointing towards the camera. */
  g_data.N = g_data.Ni = interp.N;
}

void init_globals()
{
  /* Default values. */
  g_data.P = interp.P;
  g_data.Ni = interp.N;
  g_data.N = safe_normalize(interp.N);
  g_data.Ng = g_data.N;
  g_data.is_strand = false;
  g_data.hair_time = 0.0;
  g_data.hair_thickness = 0.0;
  g_data.hair_strand_id = 0;
#if defined(MAT_SHADOW)
  g_data.ray_type = RAY_TYPE_SHADOW;
#elif defined(MAT_CAPTURE)
  g_data.ray_type = RAY_TYPE_DIFFUSE;
#else
  if (uniform_buf.pipeline.is_probe_reflection) {
    g_data.ray_type = RAY_TYPE_GLOSSY;
  }
  else {
    g_data.ray_type = RAY_TYPE_CAMERA;
  }
#endif
  g_data.ray_depth = 0.0;
  g_data.ray_length = distance(g_data.P, drw_view_position());
  g_data.barycentric_coords = vec2(0.0);
  g_data.barycentric_dists = vec3(0.0);

#ifdef GPU_FRAGMENT_SHADER
  g_data.N = (FrontFacing) ? g_data.N : -g_data.N;
  g_data.Ni = (FrontFacing) ? g_data.Ni : -g_data.Ni;
  g_data.Ng = safe_normalize(cross(dFdx(g_data.P), dFdy(g_data.P)));
#endif

#if defined(MAT_GEOM_MESH)
  init_globals_mesh();
#elif defined(MAT_GEOM_CURVES)
  init_globals_curves();
#elif defined(MAT_GEOM_GPENCIL)
  init_globals_gpencil();
#endif
}

/* Avoid some compiler issue with non set interface parameters. */
void init_interface()
{
#ifdef GPU_VERTEX_SHADER
  interp.P = vec3(0.0);
  interp.N = vec3(0.0);
  drw_ResourceID_iface.resource_index = resource_id;
#endif
}

#if defined(GPU_VERTEX_SHADER) && defined(MAT_SHADOW)
void shadow_viewport_layer_set(int view_id, int lod)
{
#  ifdef SHADOW_UPDATE_ATOMIC_RASTER
  shadow_iface.shadow_view_id = view_id;
#  else
  /* We still render to a layered frame-buffer in the case of Metal + Tile Based Renderer.
   * Since it needs correct depth buffering, each view needs to not overlap each others.
   * It doesn't matter much for other platform, so we use that as a way to pass the view id. */
  gpu_Layer = view_id;
#  endif
  gpu_ViewportIndex = lod;
}
#endif

#if defined(GPU_FRAGMENT_SHADER) && defined(MAT_SHADOW)
int shadow_view_id_get()
{
#  ifdef SHADOW_UPDATE_ATOMIC_RASTER
  return shadow_iface.shadow_view_id;
#  else
  return gpu_Layer;
#  endif
}
#endif

/////////////////////////////// Source file 31/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_ROTATION_LIB_GLSL
#  define GPU_SHADER_MATH_ROTATION_LIB_GLSL

/* -------------------------------------------------------------------- */
/** \name Rotation Types
 * \{ */

struct Angle {
  /* Angle in radian. */
  float angle;

#  ifdef GPU_METAL
  Angle() = default;
  Angle(float angle_) : angle(angle_){};
#  endif
};

struct AxisAngle {
  vec3 axis;
  float angle;

#  ifdef GPU_METAL
  AxisAngle() = default;
  AxisAngle(vec3 axis_, float angle_) : axis(axis_), angle(angle_){};
#  endif
};

AxisAngle AxisAngle_identity()
{
  return AxisAngle(vec3(0, 1, 0), 0);
}

struct Quaternion {
  float x, y, z, w;
#  ifdef GPU_METAL
  Quaternion() = default;
  Quaternion(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_){};
#  endif
};

vec4 as_vec4(Quaternion quat)
{
  return vec4(quat.x, quat.y, quat.z, quat.w);
}

Quaternion Quaternion_identity()
{
  return Quaternion(1, 0, 0, 0);
}

struct EulerXYZ {
  float x, y, z;
#  ifdef GPU_METAL
  EulerXYZ() = default;
  EulerXYZ(float x_, float y_, float z_) : x(x_), y(y_), z(z_){};
#  endif
};

vec3 as_vec3(EulerXYZ eul)
{
  return vec3(eul.x, eul.y, eul.z);
}

EulerXYZ EulerXYZ_identity()
{
  return EulerXYZ(0, 0, 0);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Rotation Functions
 * \{ */

/**
 * Generic function for implementing slerp
 * (quaternions and spherical vector coords).
 *
 * \param t: factor in [0..1]
 * \param cosom: dot product from normalized vectors/quaternions.
 * \param r_w: calculated weights.
 */
vec2 interpolate_dot_slerp(float t, float cosom)
{
  vec2 w = vec2(1.0 - t, t);
  /* Within [-1..1] range, avoid aligned axis. */
  const float eps = 1e-4;
  if (abs(cosom) < 1.0 - eps) {
    float omega = acos(cosom);
    w = sin(w * omega) / sin(omega);
  }
  return w;
}

Quaternion interpolate(Quaternion a, Quaternion b, float t)
{
  vec4 quat = as_vec4(a);
  float cosom = dot(as_vec4(a), as_vec4(b));
  /* Rotate around shortest angle. */
  if (cosom < 0.0) {
    cosom = -cosom;
    quat = -quat;
  }
  vec2 w = interpolate_dot_slerp(t, cosom);
  quat = w.x * quat + w.y * as_vec4(b);
  return Quaternion(UNPACK4(quat));
}

Quaternion to_quaternion(EulerXYZ eul)
{
  float ti = eul.x * 0.5;
  float tj = eul.y * 0.5;
  float th = eul.z * 0.5;
  float ci = cos(ti);
  float cj = cos(tj);
  float ch = cos(th);
  float si = sin(ti);
  float sj = sin(tj);
  float sh = sin(th);
  float cc = ci * ch;
  float cs = ci * sh;
  float sc = si * ch;
  float ss = si * sh;

  Quaternion quat;
  quat.x = cj * cc + sj * ss;
  quat.y = cj * sc - sj * cs;
  quat.z = cj * ss + sj * cc;
  quat.w = cj * cs - sj * sc;
  return quat;
}

Quaternion to_axis_angle(AxisAngle axis_angle)
{
  float angle_cos = cos(axis_angle.angle);
  /** Using half angle identities: sin(angle / 2) = sqrt((1 - angle_cos) / 2) */
  float sine = sqrt(0.5 - angle_cos * 0.5);
  float cosine = sqrt(0.5 + angle_cos * 0.5);

  /* TODO(fclem): Optimize. */
  float angle_sin = sin(axis_angle.angle);
  if (angle_sin < 0.0) {
    sine = -sine;
  }

  Quaternion quat;
  quat.x = cosine;
  quat.y = axis_angle.axis.x * sine;
  quat.z = axis_angle.axis.y * sine;
  quat.w = axis_angle.axis.z * sine;
  return quat;
}

AxisAngle to_axis_angle(Quaternion quat)
{
  /* Calculate angle/2, and sin(angle/2). */
  float ha = acos(quat.x);
  float si = sin(ha);

  /* From half-angle to angle. */
  float angle = ha * 2;
  /* Prevent division by zero for axis conversion. */
  if (abs(si) < 0.0005) {
    si = 1.0;
  }

  vec3 axis = vec3(quat.y, quat.z, quat.w) / si;
  if (is_zero(axis)) {
    axis[1] = 1.0;
  }
  return AxisAngle(axis, angle);
}

AxisAngle to_axis_angle(EulerXYZ eul)
{
  /* Use quaternions as intermediate representation for now... */
  return to_axis_angle(to_quaternion(eul));
}

/** \} */

#endif /* GPU_SHADER_MATH_ROTATION_LIB_GLSL */

/////////////////////////////// Source file 32/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_rotation_lib.glsl)

/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_MATRIX_LIB_GLSL
#  define GPU_SHADER_MATH_MATRIX_LIB_GLSL

/* -------------------------------------------------------------------- */
/** \name Static constructors
 * \{ */

mat2x2 mat2x2_diagonal(float v)
{
  return mat2x2(vec2(v, 0.0), vec2(0.0, v));
}
mat3x3 mat3x3_diagonal(float v)
{
  return mat3x3(vec3(v, 0.0, 0.0), vec3(0.0, v, 0.0), vec3(0.0, 0.0, v));
}
mat4x4 mat4x4_diagonal(float v)
{
  return mat4x4(vec4(v, 0.0, 0.0, 0.0),
                vec4(0.0, v, 0.0, 0.0),
                vec4(0.0, 0.0, v, 0.0),
                vec4(0.0, 0.0, 0.0, v));
}

mat2x2 mat2x2_all(float v)
{
  return mat2x2(vec2(v), vec2(v));
}
mat3x3 mat3x3_all(float v)
{
  return mat3x3(vec3(v), vec3(v), vec3(v));
}
mat4x4 mat4x4_all(float v)
{
  return mat4x4(vec4(v), vec4(v), vec4(v), vec4(v));
}

mat2x2 mat2x2_zero(float v)
{
  return mat2x2_all(0.0);
}
mat3x3 mat3x3_zero(float v)
{
  return mat3x3_all(0.0);
}
mat4x4 mat4x4_zero(float v)
{
  return mat4x4_all(0.0);
}

mat2x2 mat2x2_identity()
{
  return mat2x2_diagonal(1.0);
}
mat3x3 mat3x3_identity()
{
  return mat3x3_diagonal(1.0);
}
mat4x4 mat4x4_identity()
{
  return mat4x4_diagonal(1.0);
}

/** \} */

/* Metal does not need prototypes. */
#  ifndef GPU_METAL

/* -------------------------------------------------------------------- */
/** \name Matrix Operations
 * \{ */

/**
 * Returns the inverse of a square matrix or zero matrix on failure.
 * \a r_success is optional and set to true if the matrix was inverted successfully.
 */
mat2x2 invert(mat2x2 mat);
mat3x3 invert(mat3x3 mat);
mat4x4 invert(mat4x4 mat);

mat2x2 invert(mat2x2 mat, out bool r_success);
mat3x3 invert(mat3x3 mat, out bool r_success);
mat4x4 invert(mat4x4 mat, out bool r_success);

/**
 * Flip the matrix across its diagonal. Also flips dimensions for non square matrices.
 */
// mat3x3 transpose(mat3x3 mat); /* Built-In using GLSL language. */

/**
 * Normalize each column of the matrix individually.
 */
mat2x2 normalize(mat2x2 mat);
mat2x3 normalize(mat2x3 mat);
mat2x4 normalize(mat2x4 mat);
mat3x2 normalize(mat3x2 mat);
mat3x3 normalize(mat3x3 mat);
mat3x4 normalize(mat3x4 mat);
mat4x2 normalize(mat4x2 mat);
mat4x3 normalize(mat4x3 mat);
mat4x4 normalize(mat4x4 mat);

/**
 * Normalize each column of the matrix individually.
 * Return the length of each column vector.
 */
mat2x2 normalize_and_get_size(mat2x2 mat, out vec2 r_size);
mat2x3 normalize_and_get_size(mat2x3 mat, out vec2 r_size);
mat2x4 normalize_and_get_size(mat2x4 mat, out vec2 r_size);
mat3x2 normalize_and_get_size(mat3x2 mat, out vec3 r_size);
mat3x3 normalize_and_get_size(mat3x3 mat, out vec3 r_size);
mat3x4 normalize_and_get_size(mat3x4 mat, out vec3 r_size);
mat4x2 normalize_and_get_size(mat4x2 mat, out vec4 r_size);
mat4x3 normalize_and_get_size(mat4x3 mat, out vec4 r_size);
mat4x4 normalize_and_get_size(mat4x4 mat, out vec4 r_size);

/**
 * Returns the determinant of the matrix.
 * It can be interpreted as the signed volume (or area) of the unit cube after transformation.
 */
// float determinant(mat3x3 mat); /* Built-In using GLSL language. */

/**
 * Returns the adjoint of the matrix (also known as adjugate matrix).
 */
mat2x2 adjoint(mat2x2 mat);
mat3x3 adjoint(mat3x3 mat);
mat4x4 adjoint(mat4x4 mat);

/**
 * Equivalent to `mat * from_location(translation)` but with fewer operation.
 */
mat4x4 translate(mat4x4 mat, vec2 translation);
mat4x4 translate(mat4x4 mat, vec3 translation);

/**
 * Equivalent to `mat * from_rotation(rotation)` but with fewer operation.
 * Optimized for rotation on basis vector (i.e: AxisAngle({1, 0, 0}, 0.2)).
 */
mat3x3 rotate(mat3x3 mat, AxisAngle rotation);
mat3x3 rotate(mat3x3 mat, EulerXYZ rotation);
mat4x4 rotate(mat4x4 mat, AxisAngle rotation);
mat4x4 rotate(mat4x4 mat, EulerXYZ rotation);

/**
 * Equivalent to `mat * from_scale(scale)` but with fewer operation.
 */
mat3x3 scale(mat3x3 mat, vec2 scale);
mat3x3 scale(mat3x3 mat, vec3 scale);
mat4x4 scale(mat4x4 mat, vec2 scale);
mat4x4 scale(mat4x4 mat, vec3 scale);

/**
 * Interpolate each component linearly.
 */
// mat4x4 interpolate_linear(mat4x4 a, mat4x4 b, float t); /* TODO. */

/**
 * A polar-decomposition-based interpolation between matrix A and matrix B.
 */
// mat3x3 interpolate(mat3x3 a, mat3x3 b, float t); /* Not implemented. Too complex to port. */

/**
 * Naive interpolation implementation, faster than polar decomposition
 *
 * \note This code is about five times faster than the polar decomposition.
 * However, it gives un-expected results even with non-uniformly scaled matrices,
 * see #46418 for an example.
 *
 * \param A: Input matrix which is totally effective with `t = 0.0`.
 * \param B: Input matrix which is totally effective with `t = 1.0`.
 * \param t: Interpolation factor.
 */
mat3x3 interpolate_fast(mat3x3 a, mat3x3 b, float t);

/**
 * Naive transform matrix interpolation,
 * based on naive-decomposition-based interpolation from #interpolate_fast<T, 3, 3>.
 */
mat4x4 interpolate_fast(mat4x4 a, mat4x4 b, float t);

/**
 * Compute Moore-Penrose pseudo inverse of matrix.
 * Singular values below epsilon are ignored for stability (truncated SVD).
 */
// mat4x4 pseudo_invert(mat4x4 mat, float epsilon); /* Not implemented. Too complex to port. */

/** \} */

/* -------------------------------------------------------------------- */
/** \name Init helpers.
 * \{ */

/**
 * Create a translation only matrix. Matrix dimensions should be at least 4 col x 3 row.
 */
mat4x4 from_location(vec3 location);

/**
 * Create a matrix whose diagonal is defined by the given scale vector.
 */
mat2x2 from_scale(vec2 scale);
mat3x3 from_scale(vec3 scale);
mat4x4 from_scale(vec4 scale);

/**
 * Create a rotation only matrix.
 */
mat2x2 from_rotation(Angle rotation);
mat3x3 from_rotation(EulerXYZ rotation);
mat3x3 from_rotation(Quaternion rotation);
mat3x3 from_rotation(AxisAngle rotation);

/**
 * Create a transform matrix with rotation and scale applied in this order.
 */
mat3x3 from_rot_scale(EulerXYZ rotation, vec3 scale);

/**
 * Create a transform matrix with translation and rotation applied in this order.
 */
mat4x4 from_loc_rot(vec3 location, EulerXYZ rotation);

/**
 * Create a transform matrix with translation, rotation and scale applied in this order.
 */
mat4x4 from_loc_rot_scale(vec3 location, EulerXYZ rotation, vec3 scale);

/**
 * Create a rotation matrix from 2 basis vectors.
 * The matrix determinant is given to be positive and it can be converted to other rotation types.
 * \note `forward` and `up` must be normalized.
 */
// mat3x3 from_normalized_axis_data(vec3 forward, vec3 up); /* TODO. */

/**
 * Create a transform matrix with translation and rotation from 2 basis vectors and a translation.
 * \note `forward` and `up` must be normalized.
 */
// mat4x4 from_normalized_axis_data(vec3 location, vec3 forward, vec3 up); /* TODO. */

/**
 * Create a rotation matrix from only one \a up axis.
 * The other axes are chosen to always be orthogonal. The resulting matrix is a basis matrix.
 * \note `up` must be normalized.
 * \note This can be used to create a tangent basis from a normal vector.
 * \note The output of this function is not given to be same across blender version. Prefer using
 * `from_orthonormal_axes` for more stable output.
 */
mat3x3 from_up_axis(vec3 up);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Conversion function.
 * \{ */

/**
 * Extract euler rotation from transform matrix.
 * \return the rotation with the smallest values from the potential candidates.
 */
EulerXYZ to_euler(mat3x3 mat);
EulerXYZ to_euler(mat3x3 mat, const bool normalized);
EulerXYZ to_euler(mat4x4 mat);
EulerXYZ to_euler(mat4x4 mat, const bool normalized);

/**
 * Extract quaternion rotation from transform matrix.
 * \note normalized is set to false by default.
 */
Quaternion to_quaternion(mat3x3 mat);
Quaternion to_quaternion(mat3x3 mat, const bool normalized);
Quaternion to_quaternion(mat4x4 mat);
Quaternion to_quaternion(mat4x4 mat, const bool normalized);

/**
 * Extract the absolute 3d scale from a transform matrix.
 */
vec3 to_scale(mat3x3 mat);
vec3 to_scale(mat3x3 mat, const bool allow_negative_scale);
vec3 to_scale(mat4x4 mat);
vec3 to_scale(mat4x4 mat, const bool allow_negative_scale);

/**
 * Decompose a matrix into location, rotation, and scale components.
 * \tparam allow_negative_scale: if true, will compute determinant to know if matrix is negative.
 * Rotation and scale values will be flipped if it is negative.
 * This is a costly operation so it is disabled by default.
 */
void to_rot_scale(mat3x3 mat, out EulerXYZ r_rotation, out vec3 r_scale);
void to_rot_scale(mat3x3 mat, out Quaternion r_rotation, out vec3 r_scale);
void to_rot_scale(mat3x3 mat, out AxisAngle r_rotation, out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out EulerXYZ r_rotation, out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale);
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out AxisAngle r_rotation, out vec3 r_scale);

void to_rot_scale(mat3x3 mat,
                  out EulerXYZ r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_rot_scale(mat3x3 mat,
                  out Quaternion r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_rot_scale(mat3x3 mat,
                  out AxisAngle r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out EulerXYZ r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out AxisAngle r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Transform function.
 * \{ */

/**
 * Transform a 3d point using a 3x3 matrix (rotation & scale).
 */
vec3 transform_point(mat3x3 mat, vec3 point);

/**
 * Transform a 3d point using a 4x4 matrix (location & rotation & scale).
 */
vec3 transform_point(mat4x4 mat, vec3 point);

/**
 * Transform a 3d direction vector using a 3x3 matrix (rotation & scale).
 */
vec3 transform_direction(mat3x3 mat, vec3 direction);

/**
 * Transform a 3d direction vector using a 4x4 matrix (rotation & scale).
 */
vec3 transform_direction(mat4x4 mat, vec3 direction);

/**
 * Project a point using a matrix (location & rotation & scale & perspective divide).
 */
vec2 project_point(mat3x3 mat, vec2 point);
vec3 project_point(mat4x4 mat, vec3 point);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Projection Matrices.
 * \{ */

/**
 * \brief Create an orthographic projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * The resulting matrix can be used with either #project_point or #transform_point.
 */
mat4x4 projection_orthographic(
    float left, float right, float bottom, float top, float near_clip, float far_clip);

/**
 * \brief Create a perspective projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * `left`, `right`, `bottom`, `top` are frustum side distances at `z=near_clip`.
 * The resulting matrix can be used with #project_point.
 */
mat4x4 projection_perspective(
    float left, float right, float bottom, float top, float near_clip, float far_clip);

/**
 * \brief Create a perspective projection matrix using OpenGL coordinate convention:
 * Maps each axis range to [-1..1] range for all axes.
 * Uses field of view angles instead of plane distances.
 * The resulting matrix can be used with #project_point.
 */
mat4x4 projection_perspective_fov(float angle_left,
                                  float angle_right,
                                  float angle_bottom,
                                  float angle_top,
                                  float near_clip,
                                  float far_clip);

/** \} */

/* -------------------------------------------------------------------- */
/** \name Compare / Test
 * \{ */

/**
 * Returns true if all of the matrices components are strictly equal to 0.
 */
bool is_zero(mat3x3 a);
bool is_zero(mat4x4 a);

/**
 * Returns true if matrix has inverted handedness.
 *
 * \note It doesn't use determinant(mat4x4) as only the 3x3 components are needed
 * when the matrix is used as a transformation to represent location/scale/rotation.
 */
bool is_negative(mat3x3 mat);
bool is_negative(mat4x4 mat);

/**
 * Returns true if matrices are equal within the given epsilon.
 */
bool is_equal(mat2x2 a, mat2x2 b, float epsilon);
bool is_equal(mat3x3 a, mat3x3 b, float epsilon);
bool is_equal(mat4x4 a, mat4x4 b, float epsilon);

/**
 * Test if the X, Y and Z axes are perpendicular with each other.
 */
bool is_orthogonal(mat3x3 mat);
bool is_orthogonal(mat4x4 mat);

/**
 * Test if the X, Y and Z axes are perpendicular with each other and unit length.
 */
bool is_orthonormal(mat3x3 mat);
bool is_orthonormal(mat4x4 mat);

/**
 * Test if the X, Y and Z axes are perpendicular with each other and the same length.
 */
bool is_uniformly_scaled(mat3x3 mat);

/** \} */

#  endif /* GPU_METAL */

/* ---------------------------------------------------------------------- */
/** \name Implementation
 * \{ */

mat2x2 invert(mat2x2 mat)
{
  return inverse(mat);
}
mat3x3 invert(mat3x3 mat)
{
  return inverse(mat);
}
mat4x4 invert(mat4x4 mat)
{
  return inverse(mat);
}

mat2x2 invert(mat2x2 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat2x2(0.0);
}
mat3x3 invert(mat3x3 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat3x3(0.0);
}
mat4x4 invert(mat4x4 mat, out bool r_success)
{
  r_success = determinant(mat) != 0.0;
  return r_success ? inverse(mat) : mat4x4(0.0);
}

#  if defined(GPU_OPENGL) || defined(GPU_METAL)
vec2 normalize(vec2 a)
{
  return a * inversesqrt(length_squared(a));
}
vec3 normalize(vec3 a)
{
  return a * inversesqrt(length_squared(a));
}
vec4 normalize(vec4 a)
{
  return a * inversesqrt(length_squared(a));
}
#  endif

mat2x2 normalize(mat2x2 mat)
{
  mat2x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  return ret;
}
mat2x3 normalize(mat2x3 mat)
{
  mat2x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  return ret;
}
mat2x4 normalize(mat2x4 mat)
{
  mat2x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  return ret;
}
mat3x2 normalize(mat3x2 mat)
{
  mat3x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  ret[2] = normalize(mat[2].xy);
  return ret;
}
mat3x3 normalize(mat3x3 mat)
{
  mat3x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  ret[2] = normalize(mat[2].xyz);
  return ret;
}
mat3x4 normalize(mat3x4 mat)
{
  mat3x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  ret[2] = normalize(mat[2].xyzw);
  return ret;
}
mat4x2 normalize(mat4x2 mat)
{
  mat4x2 ret;
  ret[0] = normalize(mat[0].xy);
  ret[1] = normalize(mat[1].xy);
  ret[2] = normalize(mat[2].xy);
  ret[3] = normalize(mat[3].xy);
  return ret;
}
mat4x3 normalize(mat4x3 mat)
{
  mat4x3 ret;
  ret[0] = normalize(mat[0].xyz);
  ret[1] = normalize(mat[1].xyz);
  ret[2] = normalize(mat[2].xyz);
  ret[3] = normalize(mat[3].xyz);
  return ret;
}
mat4x4 normalize(mat4x4 mat)
{
  mat4x4 ret;
  ret[0] = normalize(mat[0].xyzw);
  ret[1] = normalize(mat[1].xyzw);
  ret[2] = normalize(mat[2].xyzw);
  ret[3] = normalize(mat[3].xyzw);
  return ret;
}

mat2x2 normalize_and_get_size(mat2x2 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat2x3 normalize_and_get_size(mat2x3 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat2x4 normalize_and_get_size(mat2x4 mat, out vec2 r_size)
{
  float size_x, size_y;
  mat2x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  r_size = vec2(size_x, size_y);
  return ret;
}
mat3x2 normalize_and_get_size(mat3x2 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat3x3 normalize_and_get_size(mat3x3 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat3x4 normalize_and_get_size(mat3x4 mat, out vec3 r_size)
{
  float size_x, size_y, size_z;
  mat3x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  r_size = vec3(size_x, size_y, size_z);
  return ret;
}
mat4x2 normalize_and_get_size(mat4x2 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x2 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}
mat4x3 normalize_and_get_size(mat4x3 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x3 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}
mat4x4 normalize_and_get_size(mat4x4 mat, out vec4 r_size)
{
  float size_x, size_y, size_z, size_w;
  mat4x4 ret;
  ret[0] = normalize_and_get_length(mat[0], size_x);
  ret[1] = normalize_and_get_length(mat[1], size_y);
  ret[2] = normalize_and_get_length(mat[2], size_z);
  ret[3] = normalize_and_get_length(mat[3], size_w);
  r_size = vec4(size_x, size_y, size_z, size_w);
  return ret;
}

mat2x2 adjoint(mat2x2 mat)
{
  mat2x2 adj = mat2x2(0.0);
  for (int c = 0; c < 2; c++) {
    for (int r = 0; r < 2; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      float tmp = 0.0;
      for (int m_c = 0; m_c < 2; m_c++) {
        for (int m_r = 0; m_r < 2; m_r++) {
          if (m_c != c && m_r != r) {
            tmp = mat[m_c][m_r];
          }
        }
      }
      float minor = tmp;
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}
mat3x3 adjoint(mat3x3 mat)
{
  mat3x3 adj = mat3x3(0.0);
  for (int c = 0; c < 3; c++) {
    for (int r = 0; r < 3; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      mat2x2 tmp = mat2x2(0.0);
      for (int m_c = 0; m_c < 3; m_c++) {
        for (int m_r = 0; m_r < 3; m_r++) {
          if (m_c != c && m_r != r) {
            int d_c = (m_c < c) ? m_c : (m_c - 1);
            int d_r = (m_r < r) ? m_r : (m_r - 1);
            tmp[d_c][d_r] = mat[m_c][m_r];
          }
        }
      }
      float minor = determinant(tmp);
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}
mat4x4 adjoint(mat4x4 mat)
{
  mat4x4 adj = mat4x4(0.0);
  for (int c = 0; c < 4; c++) {
    for (int r = 0; r < 4; r++) {
      /* Copy other cells except the "cross" to compute the determinant. */
      mat3x3 tmp = mat3x3(0.0);
      for (int m_c = 0; m_c < 4; m_c++) {
        for (int m_r = 0; m_r < 4; m_r++) {
          if (m_c != c && m_r != r) {
            int d_c = (m_c < c) ? m_c : (m_c - 1);
            int d_r = (m_r < r) ? m_r : (m_r - 1);
            tmp[d_c][d_r] = mat[m_c][m_r];
          }
        }
      }
      float minor = determinant(tmp);
      /* Transpose directly to get the adjugate. Swap destination row and col. */
      adj[r][c] = (((c + r) & 1) != 0) ? -minor : minor;
    }
  }
  return adj;
}

mat4x4 translate(mat4x4 mat, vec3 translation)
{
  mat[3].xyz += translation[0] * mat[0].xyz;
  mat[3].xyz += translation[1] * mat[1].xyz;
  mat[3].xyz += translation[2] * mat[2].xyz;
  return mat;
}
mat4x4 translate(mat4x4 mat, vec2 translation)
{
  mat[3].xyz += translation[0] * mat[0].xyz;
  mat[3].xyz += translation[1] * mat[1].xyz;
  return mat;
}

mat3x3 rotate(mat3x3 mat, AxisAngle rotation)
{
  mat3x3 result;
  /* axis_vec is given to be normalized. */
  if (rotation.axis.x == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = mat[0][c];
      result[1][c] = angle_cos * mat[1][c] + angle_sin * mat[2][c];
      result[2][c] = -angle_sin * mat[1][c] + angle_cos * mat[2][c];
    }
  }
  else if (rotation.axis.y == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = angle_cos * mat[0][c] - angle_sin * mat[2][c];
      result[1][c] = mat[1][c];
      result[2][c] = angle_sin * mat[0][c] + angle_cos * mat[2][c];
    }
  }
  else if (rotation.axis.z == 1.0) {
    float angle_cos = cos(rotation.angle);
    float angle_sin = sin(rotation.angle);
    for (int c = 0; c < 3; c++) {
      result[0][c] = angle_cos * mat[0][c] + angle_sin * mat[1][c];
      result[1][c] = -angle_sin * mat[0][c] + angle_cos * mat[1][c];
      result[2][c] = mat[2][c];
    }
  }
  else {
    /* Un-optimized case. Arbitrary rotation. */
    result = mat * from_rotation(rotation);
  }
  return result;
}
mat3x3 rotate(mat3x3 mat, EulerXYZ rotation)
{
  AxisAngle axis_angle;
  if (rotation.y == 0.0 && rotation.z == 0.0) {
    axis_angle = AxisAngle(vec3(1.0, 0.0, 0.0), rotation.x);
  }
  else if (rotation.x == 0.0 && rotation.z == 0.0) {
    axis_angle = AxisAngle(vec3(0.0, 1.0, 0.0), rotation.y);
  }
  else if (rotation.x == 0.0 && rotation.y == 0.0) {
    axis_angle = AxisAngle(vec3(0.0, 0.0, 1.0), rotation.z);
  }
  else {
    /* Un-optimized case. Arbitrary rotation. */
    return mat * from_rotation(rotation);
  }
  return rotate(mat, axis_angle);
}

mat4x4 rotate(mat4x4 mat, AxisAngle rotation)
{
  mat4x4 result = mat4x4(rotate(mat3x3(mat), rotation));
  result[0][3] = mat[0][3];
  result[1][3] = mat[1][3];
  result[2][3] = mat[2][3];
  result[3][0] = mat[3][0];
  result[3][1] = mat[3][1];
  result[3][2] = mat[3][2];
  result[3][3] = mat[3][3];
  return result;
}
mat4x4 rotate(mat4x4 mat, EulerXYZ rotation)
{
  mat4x4 result = mat4x4(rotate(mat3x3(mat), rotation));
  result[0][3] = mat[0][3];
  result[1][3] = mat[1][3];
  result[2][3] = mat[2][3];
  result[3][0] = mat[3][0];
  result[3][1] = mat[3][1];
  result[3][2] = mat[3][2];
  result[3][3] = mat[3][3];
  return result;
}

mat3x3 scale(mat3x3 mat, vec2 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  return mat;
}
mat3x3 scale(mat3x3 mat, vec3 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  mat[2] *= scale[2];
  return mat;
}
mat4x4 scale(mat4x4 mat, vec2 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  return mat;
}
mat4x4 scale(mat4x4 mat, vec3 scale)
{
  mat[0] *= scale[0];
  mat[1] *= scale[1];
  mat[2] *= scale[2];
  return mat;
}

mat4x4 from_location(vec3 location)
{
  mat4x4 ret = mat4x4(1.0);
  ret[3].xyz = location;
  return ret;
}

mat2x2 from_scale(vec2 scale)
{
  mat2x2 ret = mat2x2(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  return ret;
}
mat3x3 from_scale(vec3 scale)
{
  mat3x3 ret = mat3x3(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  ret[2][2] = scale[2];
  return ret;
}
mat4x4 from_scale(vec4 scale)
{
  mat4x4 ret = mat4x4(0.0);
  ret[0][0] = scale[0];
  ret[1][1] = scale[1];
  ret[2][2] = scale[2];
  ret[3][3] = scale[3];
  return ret;
}

mat2x2 from_rotation(Angle rotation)
{
  float c = cos(rotation.angle);
  float s = sin(rotation.angle);
  return mat2x2(c, -s, s, c);
}

mat3x3 from_rotation(EulerXYZ rotation)
{
  float ci = cos(rotation.x);
  float cj = cos(rotation.y);
  float ch = cos(rotation.z);
  float si = sin(rotation.x);
  float sj = sin(rotation.y);
  float sh = sin(rotation.z);
  float cc = ci * ch;
  float cs = ci * sh;
  float sc = si * ch;
  float ss = si * sh;

  mat3x3 mat;
  mat[0][0] = cj * ch;
  mat[1][0] = sj * sc - cs;
  mat[2][0] = sj * cc + ss;

  mat[0][1] = cj * sh;
  mat[1][1] = sj * ss + cc;
  mat[2][1] = sj * cs - sc;

  mat[0][2] = -sj;
  mat[1][2] = cj * si;
  mat[2][2] = cj * ci;
  return mat;
}

mat3x3 from_rotation(Quaternion rotation)
{
  /* NOTE: Should be double but support isn't native on most GPUs. */
  float q0 = M_SQRT2 * float(rotation.x);
  float q1 = M_SQRT2 * float(rotation.y);
  float q2 = M_SQRT2 * float(rotation.z);
  float q3 = M_SQRT2 * float(rotation.w);

  float qda = q0 * q1;
  float qdb = q0 * q2;
  float qdc = q0 * q3;
  float qaa = q1 * q1;
  float qab = q1 * q2;
  float qac = q1 * q3;
  float qbb = q2 * q2;
  float qbc = q2 * q3;
  float qcc = q3 * q3;

  mat3x3 mat;
  mat[0][0] = float(1.0 - qbb - qcc);
  mat[0][1] = float(qdc + qab);
  mat[0][2] = float(-qdb + qac);

  mat[1][0] = float(-qdc + qab);
  mat[1][1] = float(1.0 - qaa - qcc);
  mat[1][2] = float(qda + qbc);

  mat[2][0] = float(qdb + qac);
  mat[2][1] = float(-qda + qbc);
  mat[2][2] = float(1.0 - qaa - qbb);
  return mat;
}

mat3x3 from_rotation(AxisAngle rotation)
{
  float angle_sin = sin(rotation.angle);
  float angle_cos = cos(rotation.angle);
  vec3 axis = rotation.axis;

  float ico = (float(1) - angle_cos);
  vec3 nsi = axis * angle_sin;

  vec3 n012 = (axis * axis) * ico;
  float n_01 = (axis[0] * axis[1]) * ico;
  float n_02 = (axis[0] * axis[2]) * ico;
  float n_12 = (axis[1] * axis[2]) * ico;

  mat3 mat = from_scale(n012 + angle_cos);
  mat[0][1] = n_01 + nsi[2];
  mat[0][2] = n_02 - nsi[1];
  mat[1][0] = n_01 - nsi[2];
  mat[1][2] = n_12 + nsi[0];
  mat[2][0] = n_02 + nsi[1];
  mat[2][1] = n_12 - nsi[0];
  return mat;
}

mat3x3 from_rot_scale(EulerXYZ rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}
mat3x3 from_rot_scale(Quaternion rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}
mat3x3 from_rot_scale(AxisAngle rotation, vec3 scale)
{
  return from_rotation(rotation) * from_scale(scale);
}

mat4x4 from_loc_rot(vec3 location, EulerXYZ rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot(vec3 location, Quaternion rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot(vec3 location, AxisAngle rotation)
{
  mat4x4 ret = mat4x4(from_rotation(rotation));
  ret[3].xyz = location;
  return ret;
}

mat4x4 from_loc_rot_scale(vec3 location, EulerXYZ rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot_scale(vec3 location, Quaternion rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}
mat4x4 from_loc_rot_scale(vec3 location, AxisAngle rotation, vec3 scale)
{
  mat4x4 ret = mat4x4(from_rot_scale(rotation, scale));
  ret[3].xyz = location;
  return ret;
}

mat3x3 from_up_axis(vec3 up)
{
  /* Duff, Tom, et al. "Building an orthonormal basis, revisited." JCGT 6.1 (2017). */
  float z_sign = up.z >= 0.0 ? 1.0 : -1.0;
  float a = -1.0 / (z_sign + up.z);
  float b = up.x * up.y * a;

  mat3x3 basis;
  basis[0] = vec3(1.0 + z_sign * square(up.x) * a, z_sign * b, -z_sign * up.x);
  basis[1] = vec3(b, z_sign + square(up.y) * a, -up.y);
  basis[2] = up;
  return basis;
}

void detail_normalized_to_eul2(mat3 mat, out EulerXYZ eul1, out EulerXYZ eul2)
{
  float cy = hypot(mat[0][0], mat[0][1]);
  if (cy > 16.0f * FLT_EPSILON) {
    eul1.x = atan2(mat[1][2], mat[2][2]);
    eul1.y = atan2(-mat[0][2], cy);
    eul1.z = atan2(mat[0][1], mat[0][0]);

    eul2.x = atan2(-mat[1][2], -mat[2][2]);
    eul2.y = atan2(-mat[0][2], -cy);
    eul2.z = atan2(-mat[0][1], -mat[0][0]);
  }
  else {
    eul1.x = atan2(-mat[2][1], mat[1][1]);
    eul1.y = atan2(-mat[0][2], cy);
    eul1.z = 0.0;

    eul2 = eul1;
  }
}

EulerXYZ to_euler(mat3x3 mat)
{
  return to_euler(mat, true);
}
EulerXYZ to_euler(mat3x3 mat, const bool normalized)
{
  if (!normalized) {
    mat = normalize(mat);
  }
  EulerXYZ eul1, eul2;
  detail_normalized_to_eul2(mat, eul1, eul2);
  /* Return best, which is just the one with lowest values it in. */
  return (length_manhattan(as_vec3(eul1)) > length_manhattan(as_vec3(eul2))) ? eul2 : eul1;
}
EulerXYZ to_euler(mat4x4 mat)
{
  return to_euler(mat3(mat));
}
EulerXYZ to_euler(mat4x4 mat, const bool normalized)
{
  return to_euler(mat3(mat), normalized);
}

Quaternion normalized_to_quat_fast(mat3 mat)
{
  /* Caller must ensure matrices aren't negative for valid results, see: #24291, #94231. */
  Quaternion q;

  /* Method outlined by Mike Day, ref: https://math.stackexchange.com/a/3183435/220949
   * with an additional `sqrtf(..)` for higher precision result.
   * Removing the `sqrt` causes tests to fail unless the precision is set to 1e-6 or larger. */

  if (mat[2][2] < 0.0f) {
    if (mat[0][0] > mat[1][1]) {
      float trace = 1.0f + mat[0][0] - mat[1][1] - mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[1][2] < mat[2][1]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.y = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[1][2] - mat[2][1]) * s;
      q.z = (mat[0][1] + mat[1][0]) * s;
      q.w = (mat[2][0] + mat[0][2]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.z == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.y = 1.0f;
      }
    }
    else {
      float trace = 1.0f - mat[0][0] + mat[1][1] - mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[2][0] < mat[0][2]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.z = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[2][0] - mat[0][2]) * s;
      q.y = (mat[0][1] + mat[1][0]) * s;
      q.w = (mat[1][2] + mat[2][1]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.y == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.z = 1.0f;
      }
    }
  }
  else {
    if (mat[0][0] < -mat[1][1]) {
      float trace = 1.0f - mat[0][0] - mat[1][1] + mat[2][2];
      float s = 2.0f * sqrt(trace);
      if (mat[0][1] < mat[1][0]) {
        /* Ensure W is non-negative for a canonical result. */
        s = -s;
      }
      q.w = 0.25f * s;
      s = 1.0f / s;
      q.x = (mat[0][1] - mat[1][0]) * s;
      q.y = (mat[2][0] + mat[0][2]) * s;
      q.z = (mat[1][2] + mat[2][1]) * s;
      if ((trace == 1.0f) && (q.x == 0.0f && q.y == 0.0f && q.z == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.w = 1.0f;
      }
    }
    else {
      /* NOTE(@ideasman42): A zero matrix will fall through to this block,
       * needed so a zero scaled matrices to return a quaternion without rotation, see: #101848. */
      float trace = 1.0f + mat[0][0] + mat[1][1] + mat[2][2];
      float s = 2.0f * sqrt(trace);
      q.x = 0.25f * s;
      s = 1.0f / s;
      q.y = (mat[1][2] - mat[2][1]) * s;
      q.z = (mat[2][0] - mat[0][2]) * s;
      q.w = (mat[0][1] - mat[1][0]) * s;
      if ((trace == 1.0f) && (q.y == 0.0f && q.z == 0.0f && q.w == 0.0f)) {
        /* Avoids the need to normalize the degenerate case. */
        q.x = 1.0f;
      }
    }
  }
  return q;
}

Quaternion detail_normalized_to_quat_with_checks(mat3x3 mat)
{
  float det = determinant(mat);
  if (!isfinite(det)) {
    return Quaternion_identity();
  }
  else if (det < 0.0) {
    return normalized_to_quat_fast(-mat);
  }
  return normalized_to_quat_fast(mat);
}

Quaternion to_quaternion(mat3x3 mat)
{
  return detail_normalized_to_quat_with_checks(normalize(mat));
}
Quaternion to_quaternion(mat3x3 mat, const bool normalized)
{
  if (!normalized) {
    mat = normalize(mat);
  }
  return to_quaternion(mat);
}
Quaternion to_quaternion(mat4x4 mat)
{
  return to_quaternion(mat3(mat));
}
Quaternion to_quaternion(mat4x4 mat, const bool normalized)
{
  return to_quaternion(mat3(mat), normalized);
}

vec3 to_scale(mat3x3 mat)
{
  return vec3(length(mat[0]), length(mat[1]), length(mat[2]));
}
vec3 to_scale(mat3x3 mat, const bool allow_negative_scale)
{
  vec3 result = to_scale(mat);
  if (allow_negative_scale) {
    if (is_negative(mat)) {
      result = -result;
    }
  }
  return result;
}
vec3 to_scale(mat4x4 mat)
{
  return to_scale(mat3(mat));
}
vec3 to_scale(mat4x4 mat, const bool allow_negative_scale)
{
  return to_scale(mat3(mat), allow_negative_scale);
}

void to_rot_scale(mat3x3 mat, out EulerXYZ r_rotation, out vec3 r_scale)
{
  r_scale = to_scale(mat);
  r_rotation = to_euler(mat, true);
}
void to_rot_scale(mat3x3 mat,
                  out EulerXYZ r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale)
{
  mat3x3 normalized_mat = normalize_and_get_size(mat, r_scale);
  if (allow_negative_scale) {
    if (is_negative(normalized_mat)) {
      normalized_mat = -normalized_mat;
      r_scale = -r_scale;
    }
  }
  r_rotation = to_euler(mat, true);
}
void to_rot_scale(mat3x3 mat, out Quaternion r_rotation, out vec3 r_scale)
{
  r_scale = to_scale(mat);
  r_rotation = to_quaternion(mat, true);
}
void to_rot_scale(mat3x3 mat,
                  out Quaternion r_rotation,
                  out vec3 r_scale,
                  const bool allow_negative_scale)
{
  mat3x3 normalized_mat = normalize_and_get_size(mat, r_scale);
  if (allow_negative_scale) {
    if (is_negative(normalized_mat)) {
      normalized_mat = -normalized_mat;
      r_scale = -r_scale;
    }
  }
  r_rotation = to_quaternion(mat, true);
}

void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out EulerXYZ r_rotation, out vec3 r_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale);
}
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out EulerXYZ r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale, allow_negative_scale);
}
void to_loc_rot_scale(mat4x4 mat, out vec3 r_location, out Quaternion r_rotation, out vec3 r_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale);
}
void to_loc_rot_scale(mat4x4 mat,
                      out vec3 r_location,
                      out Quaternion r_rotation,
                      out vec3 r_scale,
                      const bool allow_negative_scale)
{
  r_location = mat[3].xyz;
  to_rot_scale(mat3(mat), r_rotation, r_scale, allow_negative_scale);
}

vec3 transform_point(mat3x3 mat, vec3 point)
{
  return mat * point;
}

vec3 transform_point(mat4x4 mat, vec3 point)
{
  return (mat * vec4(point, 1.0)).xyz;
}

vec3 transform_direction(mat3x3 mat, vec3 direction)
{
  return mat * direction;
}

vec3 transform_direction(mat4x4 mat, vec3 direction)
{
  return mat3x3(mat) * direction;
}

vec2 project_point(mat3x3 mat, vec2 point)
{
  vec3 tmp = mat * vec3(point, 1.0);
  /* Absolute value to not flip the frustum upside down behind the camera. */
  return tmp.xy / abs(tmp.z);
}
vec3 project_point(mat4x4 mat, vec3 point)
{
  vec4 tmp = mat * vec4(point, 1.0);
  /* Absolute value to not flip the frustum upside down behind the camera. */
  return tmp.xyz / abs(tmp.w);
}

mat4x4 interpolate_fast(mat4x4 a, mat4x4 b, float t)
{
  vec3 a_loc, b_loc;
  vec3 a_scale, b_scale;
  Quaternion a_quat, b_quat;
  to_loc_rot_scale(a, a_loc, a_quat, a_scale);
  to_loc_rot_scale(b, b_loc, b_quat, b_scale);

  vec3 location = interpolate(a_loc, b_loc, t);
  vec3 scale = interpolate(a_scale, b_scale, t);
  Quaternion rotation = interpolate(a_quat, b_quat, t);
  return from_loc_rot_scale(location, rotation, scale);
}

mat4x4 projection_orthographic(
    float left, float right, float bottom, float top, float near_clip, float far_clip)
{
  float x_delta = right - left;
  float y_delta = top - bottom;
  float z_delta = far_clip - near_clip;

  mat4x4 mat = mat4x4(1.0);
  if (x_delta != 0.0 && y_delta != 0.0 && z_delta != 0.0) {
    mat[0][0] = 2.0 / x_delta;
    mat[3][0] = -(right + left) / x_delta;
    mat[1][1] = 2.0 / y_delta;
    mat[3][1] = -(top + bottom) / y_delta;
    mat[2][2] = -2.0 / z_delta; /* NOTE: negate Z. */
    mat[3][2] = -(far_clip + near_clip) / z_delta;
  }
  return mat;
}

mat4x4 projection_perspective(
    float left, float right, float bottom, float top, float near_clip, float far_clip)
{
  float x_delta = right - left;
  float y_delta = top - bottom;
  float z_delta = far_clip - near_clip;

  mat4x4 mat = mat4x4(1.0);
  if (x_delta != 0.0 && y_delta != 0.0 && z_delta != 0.0) {
    mat[0][0] = near_clip * 2.0 / x_delta;
    mat[1][1] = near_clip * 2.0 / y_delta;
    mat[2][0] = (right + left) / x_delta; /* NOTE: negate Z. */
    mat[2][1] = (top + bottom) / y_delta;
    mat[2][2] = -(far_clip + near_clip) / z_delta;
    mat[2][3] = -1.0;
    mat[3][2] = (-2.0 * near_clip * far_clip) / z_delta;
    mat[3][3] = 0.0;
  }
  return mat;
}

mat4x4 projection_perspective_fov(float angle_left,
                                  float angle_right,
                                  float angle_bottom,
                                  float angle_top,
                                  float near_clip,
                                  float far_clip)
{
  mat4x4 mat = projection_perspective(
      tan(angle_left), tan(angle_right), tan(angle_bottom), tan(angle_top), near_clip, far_clip);
  mat[0][0] /= near_clip;
  mat[1][1] /= near_clip;
  return mat;
}

bool is_zero(mat3x3 a)
{
  if (is_zero(a[0])) {
    if (is_zero(a[1])) {
      if (is_zero(a[2])) {
        return true;
      }
    }
  }
  return false;
}
bool is_zero(mat4x4 a)
{
  if (is_zero(a[0])) {
    if (is_zero(a[1])) {
      if (is_zero(a[2])) {
        if (is_zero(a[3])) {
          return true;
        }
      }
    }
  }
  return false;
}

bool is_negative(mat3x3 mat)
{
  return determinant(mat) < 0.0;
}
bool is_negative(mat4x4 mat)
{
  return is_negative(mat3x3(mat));
}

bool is_equal(mat2x2 a, mat2x2 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      return true;
    }
  }
  return false;
}
bool is_equal(mat3x3 a, mat3x3 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      if (is_equal(a[2], b[2], epsilon)) {
        return true;
      }
    }
  }
  return false;
}
bool is_equal(mat4x4 a, mat4x4 b, float epsilon)
{
  if (is_equal(a[0], b[0], epsilon)) {
    if (is_equal(a[1], b[1], epsilon)) {
      if (is_equal(a[2], b[2], epsilon)) {
        if (is_equal(a[3], b[3], epsilon)) {
          return true;
        }
      }
    }
  }
  return false;
}

bool is_orthogonal(mat3x3 mat)
{
  if (abs(dot(mat[0], mat[1])) > 1e-5) {
    return false;
  }
  if (abs(dot(mat[1], mat[2])) > 1e-5) {
    return false;
  }
  if (abs(dot(mat[2], mat[0])) > 1e-5) {
    return false;
  }
  return true;
}

bool is_orthonormal(mat3x3 mat)
{
  if (!is_orthogonal(mat)) {
    return false;
  }
  if (abs(length_squared(mat[0]) - 1.0) > 1e-5) {
    return false;
  }
  if (abs(length_squared(mat[1]) - 1.0) > 1e-5) {
    return false;
  }
  if (abs(length_squared(mat[2]) - 1.0) > 1e-5) {
    return false;
  }
  return true;
}

bool is_uniformly_scaled(mat3x3 mat)
{
  if (!is_orthogonal(mat)) {
    return false;
  }
  const float eps = 1e-7;
  float x = length_squared(mat[0]);
  float y = length_squared(mat[1]);
  float z = length_squared(mat[2]);
  return (abs(x - y) < eps) && abs(x - z) < eps;
}

bool is_orthogonal(mat4x4 mat)
{
  return is_orthogonal(mat3x3(mat));
}
bool is_orthonormal(mat4x4 mat)
{
  return is_orthonormal(mat3x3(mat));
}
bool is_uniformly_scaled(mat4x4 mat)
{
  return is_uniformly_scaled(mat3x3(mat));
}

/* Returns true if each individual columns are unit scaled. Mainly for assert usage. */
bool is_unit_scale(mat4x4 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      if (is_unit_scale(m[2])) {
        if (is_unit_scale(m[3])) {
          return true;
        }
      }
    }
  }
  return false;
}
bool is_unit_scale(mat3x3 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      if (is_unit_scale(m[2])) {
        return true;
      }
    }
  }
  return false;
}
bool is_unit_scale(mat2x2 m)
{
  if (is_unit_scale(m[0])) {
    if (is_unit_scale(m[1])) {
      return true;
    }
  }
  return false;
}

/** \} */

#endif /* GPU_SHADER_MATH_MATRIX_LIB_GLSL */

/////////////////////////////// Source file 33/////////////////////////////




/**
 * Geometric shape structures.
 * Some constructors might seems redundant but are here to make the API cleaner and
 * allow for more than one constructor per type.
 */

/* ---------------------------------------------------------------------- */
/** \name Circle
 * \{ */

struct Circle {
  vec2 center;
  float radius;
};

Circle shape_circle(vec2 center, float radius)
{
  Circle circle;
  circle.center = center;
  circle.radius = radius;
  return circle;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Sphere
 * \{ */

struct Sphere {
  vec3 center;
  float radius;
};

Sphere shape_sphere(vec3 center, float radius)
{
  Sphere sphere;
  sphere.center = center;
  sphere.radius = radius;
  return sphere;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Box
 * \{ */

struct Box {
  vec3 corners[8];
};

/* Construct box from 4 basis points. */
Box shape_box(vec3 v000, vec3 v100, vec3 v010, vec3 v001)
{
  v100 -= v000;
  v010 -= v000;
  v001 -= v000;
  Box box;
  box.corners[0] = v000;
  box.corners[1] = v000 + v100;
  box.corners[2] = v000 + v010 + v100;
  box.corners[3] = v000 + v010;
  box.corners[4] = box.corners[0] + v001;
  box.corners[5] = box.corners[1] + v001;
  box.corners[6] = box.corners[2] + v001;
  box.corners[7] = box.corners[3] + v001;
  return box;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Square Pyramid
 * \{ */

struct Pyramid {
  /* Apex is the first. Base vertices are in clockwise order from front view. */
  vec3 corners[5];
};

/**
 * Regular Square Pyramid (can be oblique).
 * Use this corner order.
 * (Top-Down View of the pyramid)
 * <pre>
 *
 * Y
 * |
 * |
 * .-----X
 *
 *  4-----------3
 *  | \       / |
 *  |   \   /   |
 *  |     0     |
 *  |   /   \   |
 *  | /       \ |
 *  1-----------2
 * </pre>
 * base_corner_00 is vertex 1
 * base_corner_01 is vertex 2
 * base_corner_10 is vertex 4
 */
Pyramid shape_pyramid(vec3 apex, vec3 base_corner_00, vec3 base_corner_01, vec3 base_corner_10)
{
  Pyramid pyramid;
  pyramid.corners[0] = apex;
  pyramid.corners[1] = base_corner_00;
  pyramid.corners[2] = base_corner_01;
  pyramid.corners[3] = base_corner_10 + (base_corner_01 - base_corner_00);
  pyramid.corners[4] = base_corner_10;
  return pyramid;
}

/**
 * Regular Square Pyramid.
 * <pre>
 *
 * Y
 * |
 * |
 * .-----X
 *
 *  4-----Y-----3
 *  | \   |   / |
 *  |   \ | /   |
 *  |     0-----X
 *  |   /   \   |
 *  | /       \ |
 *  1-----------2
 * </pre>
 * base_center_pos_x is vector from base center to X
 * base_center_pos_y is vector from base center to Y
 */
Pyramid shape_pyramid_non_oblique(vec3 apex,
                                  vec3 base_center,
                                  vec3 base_center_pos_x,
                                  vec3 base_center_pos_y)
{
  Pyramid pyramid;
  pyramid.corners[0] = apex;
  pyramid.corners[1] = base_center - base_center_pos_x - base_center_pos_y;
  pyramid.corners[2] = base_center + base_center_pos_x - base_center_pos_y;
  pyramid.corners[3] = base_center + base_center_pos_x + base_center_pos_y;
  pyramid.corners[4] = base_center - base_center_pos_x + base_center_pos_y;
  return pyramid;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Frustum
 * \{ */

struct Frustum {
  vec3 corners[8];
};

/**
 * Use this corner order.
 * <pre>
 *
 * Z  Y
 * | /
 * |/
 * .-----X
 *     2----------6
 *    /|         /|
 *   / |        / |
 *  1----------5  |
 *  |  |       |  |
 *  |  3-------|--7
 *  | /        | /
 *  |/         |/
 *  0----------4
 * </pre>
 */
Frustum shape_frustum(vec3 corners[8])
{
  Frustum frustum;
  for (int i = 0; i < 8; i++) {
    frustum.corners[i] = corners[i];
  }
  return frustum;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Cone
 * \{ */

/* Cone at origin with no height. */
struct Cone {
  vec3 direction;
  float angle_cos;

#ifdef GPU_METAL
  inline Cone() = default;
  inline Cone(vec3 in_direction, float in_angle_cos)
      : direction(in_direction), angle_cos(in_angle_cos)
  {
  }
#endif
};

Cone shape_cone(vec3 direction, float angle_cosine)
{
  return Cone(direction, angle_cosine);
}

/** \} */

/////////////////////////////// Source file 34/////////////////////////////




#pragma BLENDER_REQUIRE(common_shape_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)

/* ---------------------------------------------------------------------- */
/** \name Tile-map data
 * \{ */

int shadow_tile_index(ivec2 tile)
{
  return tile.x + tile.y * SHADOW_TILEMAP_RES;
}

ivec2 shadow_tile_coord(int tile_index)
{
  return ivec2(tile_index % SHADOW_TILEMAP_RES, tile_index / SHADOW_TILEMAP_RES);
}

/* Return bottom left pixel position of the tile-map inside the tile-map atlas. */
ivec2 shadow_tilemap_start(int tilemap_index)
{
  return SHADOW_TILEMAP_RES *
         ivec2(tilemap_index % SHADOW_TILEMAP_PER_ROW, tilemap_index / SHADOW_TILEMAP_PER_ROW);
}

ivec2 shadow_tile_coord_in_atlas(ivec2 tile, int tilemap_index)
{
  return shadow_tilemap_start(tilemap_index) + tile;
}

/**
 * Return tile index inside `tiles_buf` for a given tile coordinate inside a specific LOD.
 * `tiles_index` should be `ShadowTileMapData.tiles_index`.
 */
int shadow_tile_offset(ivec2 tile, int tiles_index, int lod)
{
#if SHADOW_TILEMAP_LOD > 5
#  error This needs to be adjusted
#endif
  const int lod0_width = SHADOW_TILEMAP_RES / 1;
  const int lod1_width = SHADOW_TILEMAP_RES / 2;
  const int lod2_width = SHADOW_TILEMAP_RES / 4;
  const int lod3_width = SHADOW_TILEMAP_RES / 8;
  const int lod4_width = SHADOW_TILEMAP_RES / 16;
  const int lod5_width = SHADOW_TILEMAP_RES / 32;
  const int lod0_size = lod0_width * lod0_width;
  const int lod1_size = lod1_width * lod1_width;
  const int lod2_size = lod2_width * lod2_width;
  const int lod3_size = lod3_width * lod3_width;
  const int lod4_size = lod4_width * lod4_width;
  const int lod5_size = lod5_width * lod5_width;

  int offset = tiles_index;
  switch (lod) {
    case 5:
      offset += lod0_size + lod1_size + lod2_size + lod3_size + lod4_size;
      offset += tile.y * lod5_width;
      break;
    case 4:
      offset += lod0_size + lod1_size + lod2_size + lod3_size;
      offset += tile.y * lod4_width;
      break;
    case 3:
      offset += lod0_size + lod1_size + lod2_size;
      offset += tile.y * lod3_width;
      break;
    case 2:
      offset += lod0_size + lod1_size;
      offset += tile.y * lod2_width;
      break;
    case 1:
      offset += lod0_size;
      offset += tile.y * lod1_width;
      break;
    case 0:
    default:
      offset += tile.y * lod0_width;
      break;
  }
  offset += tile.x;
  return offset;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Load / Store functions.
 * \{ */

/** \note: Will clamp if out of bounds. */
ShadowSamplingTile shadow_tile_load(usampler2D tilemaps_tx, ivec2 tile_co, int tilemap_index)
{
  /* NOTE(@fclem): This clamp can hide some small imprecision at clip-map transition.
   * Can be disabled to check if the clip-map is well centered. */
  tile_co = clamp(tile_co, ivec2(0), ivec2(SHADOW_TILEMAP_RES - 1));
  ivec2 texel = shadow_tile_coord_in_atlas(tile_co, tilemap_index);
  uint tile_data = texelFetch(tilemaps_tx, texel, 0).x;
  return shadow_sampling_tile_unpack(tile_data);
}

/**
 * This function should be the inverse of ShadowDirectional::coverage_get().
 *
 * \a lP shading point position in light space, relative to the to camera position snapped to
 * the smallest clip-map level (`shadow_world_to_local(light, P) - light._position`).
 */

float shadow_directional_level_fractional(LightData light, vec3 lP)
{
  float lod;
  if (light.type == LIGHT_SUN) {
    /* We need to hide one tile worth of data to hide the moving transition. */
    const float narrowing = float(SHADOW_TILEMAP_RES) / (float(SHADOW_TILEMAP_RES) - 1.0001);
    /* Since the distance is centered around the camera (and thus by extension the tile-map),
     * we need to multiply by 2 to get the lod level which covers the following range:
     * [-coverage_get(lod)/2..coverage_get(lod)/2] */
    lod = log2(length(lP) * narrowing * 2.0);
  }
  else {
    /* The narrowing need to be stronger since the tile-map position is not rounded but floored. */
    const float narrowing = float(SHADOW_TILEMAP_RES) / (float(SHADOW_TILEMAP_RES) - 2.5001);
    /* Since we want half of the size, bias the level by -1. */
    float lod_min_half_size = exp2(float(light_sun_data_get(light).clipmap_lod_min - 1));
    lod = length(lP.xy) * narrowing / lod_min_half_size;
  }
  float clipmap_lod = lod + light.lod_bias;
  return clamp(clipmap_lod,
               float(light_sun_data_get(light).clipmap_lod_min),
               float(light_sun_data_get(light).clipmap_lod_max));
}

int shadow_directional_level(LightData light, vec3 lP)
{
  return int(ceil(shadow_directional_level_fractional(light, lP)));
}

/* How much a tilemap pixel covers a final image pixel. */
float shadow_punctual_footprint_ratio(LightData light,
                                      vec3 P,
                                      bool is_perspective,
                                      float dist_to_cam,
                                      float tilemap_projection_ratio)
{
  /* We project a shadow map pixel (as a sphere for simplicity) to the receiver plane.
   * We then reproject this sphere onto the camera screen and compare it to the film pixel size.
   * This gives a good approximation of what LOD to select to get a somewhat uniform shadow map
   * resolution in screen space. */

  float dist_to_light = distance(P, light._position);
  float footprint_ratio = dist_to_light;
  /* Project the radius to the screen. 1 unit away from the camera the same way
   * pixel_world_radius_inv was computed. Not needed in orthographic mode. */
  if (is_perspective) {
    footprint_ratio /= dist_to_cam;
  }
  /* Apply resolution ratio. */
  footprint_ratio *= tilemap_projection_ratio;
  /* Take the frustum padding into account. */
  footprint_ratio *= light_local_data_get(light).clip_side /
                     orderedIntBitsToFloat(light.clip_near);
  return footprint_ratio;
}

struct ShadowCoordinates {
  /* Index of the tile-map to containing the tile. */
  int tilemap_index;
  /* LOD of the tile to load relative to the min level. Always positive. */
  int lod_relative;
  /* Tile coordinate inside the tile-map. */
  ivec2 tile_coord;
  /* UV coordinates in [0..SHADOW_TILEMAP_RES) range. */
  vec2 uv;
};

/* Retain sign bit and avoid costly int division. */
ivec2 shadow_decompress_grid_offset(eLightType light_type,
                                    ivec2 offset_neg,
                                    ivec2 offset_pos,
                                    int level_relative)
{
  if (light_type == LIGHT_SUN_ORTHO) {
    return shadow_cascade_grid_offset(offset_pos, level_relative);
  }
  else {
    return (offset_pos >> level_relative) - (offset_neg >> level_relative);
  }
}

/**
 * \a lP shading point position in light space (`shadow_world_to_local(light, P)`).
 */
ShadowCoordinates shadow_directional_coordinates_at_level(LightData light, vec3 lP, int level)
{
  ShadowCoordinates ret;
  /* This difference needs to be less than 32 for the later shift to be valid.
   * This is ensured by `ShadowDirectional::clipmap_level_range()`. */
  int level_relative = level - light_sun_data_get(light).clipmap_lod_min;

  ret.tilemap_index = light.tilemap_index + level_relative;

  ret.lod_relative = (light.type == LIGHT_SUN_ORTHO) ? light_sun_data_get(light).clipmap_lod_min :
                                                       level;

  /* Compute offset in tile. */
  ivec2 clipmap_offset = shadow_decompress_grid_offset(
      light.type,
      light_sun_data_get(light).clipmap_base_offset_neg,
      light_sun_data_get(light).clipmap_base_offset_pos,
      level_relative);

  ret.uv = lP.xy - light_sun_data_get(light).clipmap_origin;
  ret.uv /= exp2(float(ret.lod_relative));
  ret.uv = ret.uv * float(SHADOW_TILEMAP_RES) + float(SHADOW_TILEMAP_RES / 2);
  ret.uv -= vec2(clipmap_offset);
  /* Clamp to avoid out of tile-map access. */
  ret.tile_coord = clamp(ivec2(ret.uv), ivec2(0.0), ivec2(SHADOW_TILEMAP_RES - 1));
  return ret;
}

/**
 * \a lP shading point position in light space (`shadow_world_to_local(light, P)`).
 */
ShadowCoordinates shadow_directional_coordinates(LightData light, vec3 lP)
{
  int level = shadow_directional_level(light, lP - light._position);
  return shadow_directional_coordinates_at_level(light, lP, level);
}

/* Transform vector to face local coordinate. */
vec3 shadow_punctual_local_position_to_face_local(int face_id, vec3 lL)
{
  switch (face_id) {
    case 1:
      return vec3(-lL.y, lL.z, -lL.x);
    case 2:
      return vec3(lL.y, lL.z, lL.x);
    case 3:
      return vec3(lL.x, lL.z, -lL.y);
    case 4:
      return vec3(-lL.x, lL.z, lL.y);
    case 5:
      return vec3(lL.x, -lL.y, -lL.z);
    default:
      return lL;
  }
}

vec3 shadow_punctual_face_local_to_local_position(int face_id, vec3 fL)
{
  switch (face_id) {
    case 1:
      return vec3(-fL.z, -fL.x, fL.y);
    case 2:
      return vec3(fL.z, fL.x, fL.y);
    case 3:
      return vec3(fL.x, -fL.z, fL.y);
    case 4:
      return vec3(-fL.x, fL.z, fL.y);
    case 5:
      return vec3(fL.x, -fL.y, -fL.z);
    default:
      return fL;
  }
}

/* Turns local light coordinate into shadow region index. Matches eCubeFace order.
 * \note lL does not need to be normalized. */
int shadow_punctual_face_index_get(vec3 lL)
{
  vec3 aP = abs(lL);
  if (all(greaterThan(aP.xx, aP.yz))) {
    return (lL.x > 0.0) ? 1 : 2;
  }
  else if (all(greaterThan(aP.yy, aP.xz))) {
    return (lL.y > 0.0) ? 3 : 4;
  }
  else {
    return (lL.z > 0.0) ? 5 : 0;
  }
}

/**
 * \a lP shading point position in face local space (world unit).
 * \a face_id is the one used to rotate lP using shadow_punctual_local_position_to_face_local().
 */
ShadowCoordinates shadow_punctual_coordinates(LightData light, vec3 lP, int face_id)
{
  float clip_near = intBitsToFloat(light.clip_near);
  float clip_side = light_local_data_get(light).clip_side;

  ShadowCoordinates ret;
  ret.tilemap_index = light.tilemap_index + face_id;
  /* UVs in [-1..+1] range. */
  ret.uv = (lP.xy * clip_near) / abs(lP.z * clip_side);
  /* UVs in [0..SHADOW_TILEMAP_RES] range. */
  ret.uv = ret.uv * float(SHADOW_TILEMAP_RES / 2) + float(SHADOW_TILEMAP_RES / 2);
  /* Clamp to avoid out of tile-map access. */
  ret.tile_coord = clamp(ivec2(ret.uv), ivec2(0), ivec2(SHADOW_TILEMAP_RES - 1));
  return ret;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Frustum shapes.
 * \{ */

vec3 shadow_tile_corner_persp(ShadowTileMapData tilemap, ivec2 tile)
{
  return tilemap.corners[1].xyz + tilemap.corners[2].xyz * float(tile.x) +
         tilemap.corners[3].xyz * float(tile.y);
}

Pyramid shadow_tilemap_cubeface_bounds(ShadowTileMapData tilemap,
                                       ivec2 tile_start,
                                       const ivec2 extent)
{
  Pyramid shape;
  shape.corners[0] = tilemap.corners[0].xyz;
  shape.corners[1] = shadow_tile_corner_persp(tilemap, tile_start + ivec2(0, 0));
  shape.corners[2] = shadow_tile_corner_persp(tilemap, tile_start + ivec2(extent.x, 0));
  shape.corners[3] = shadow_tile_corner_persp(tilemap, tile_start + extent);
  shape.corners[4] = shadow_tile_corner_persp(tilemap, tile_start + ivec2(0, extent.y));
  return shape;
}

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Render map layout.
 *
 * Since a view can cover at most the number of tile contained in LOD0,
 * index every LOD like they were LOD0.
 * \{ */

int shadow_render_page_index_get(int view_index, ivec2 tile_coordinate_in_lod)
{
  return view_index * SHADOW_TILEMAP_LOD0_LEN + tile_coordinate_in_lod.y * SHADOW_TILEMAP_RES +
         tile_coordinate_in_lod.x;
}

/** \} */

/////////////////////////////// Source file 35/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_shadow_tilemap_lib.glsl)

#define EEVEE_SHADOW_LIB

#ifdef SHADOW_READ_ATOMIC
#  define SHADOW_ATLAS_TYPE usampler2DArrayAtomic
#else
#  define SHADOW_ATLAS_TYPE usampler2DArray
#endif

struct ShadowSampleParams {
  vec3 lP;
  vec3 uv;
  int tilemap_index;
  float z_range;
};

ShadowSamplingTile shadow_tile_data_get(usampler2D tilemaps_tx, ShadowSampleParams params)
{
  /* Prevent out of bound access. Assumes the input is already non negative. */
  vec2 tilemap_uv = min(params.uv.xy, vec2(0.99999));

  ivec2 texel_coord = ivec2(tilemap_uv * float(SHADOW_MAP_MAX_RES));
  /* Using bitwise ops is way faster than integer ops. */
  const int page_shift = SHADOW_PAGE_LOD;

  ivec2 tile_coord = texel_coord >> page_shift;
  return shadow_tile_load(tilemaps_tx, tile_coord, params.tilemap_index);
}

float shadow_read_depth(SHADOW_ATLAS_TYPE atlas_tx,
                        usampler2D tilemaps_tx,
                        ShadowSampleParams params)
{
  /* Prevent out of bound access. Assumes the input is already non negative. */
  vec2 tilemap_uv = min(params.uv.xy, vec2(0.99999));

  ivec2 texel_coord = ivec2(tilemap_uv * float(SHADOW_MAP_MAX_RES));
  /* Using bitwise ops is way faster than integer ops. */
  const int page_shift = SHADOW_PAGE_LOD;

  ivec2 tile_coord = texel_coord >> page_shift;
  ShadowSamplingTile tile = shadow_tile_load(tilemaps_tx, tile_coord, params.tilemap_index);

  if (!tile.is_valid) {
    return -1.0;
  }

  int page_mask = ~(0xFFFFFFFF << (SHADOW_PAGE_LOD + int(tile.lod)));
  ivec2 texel_page = (texel_coord & page_mask) >> int(tile.lod);
  ivec3 texel = ivec3((ivec2(tile.page.xy) << page_shift) | texel_page, tile.page.z);

  return uintBitsToFloat(texelFetch(atlas_tx, texel, 0).r);
}

struct ShadowEvalResult {
  /* Visibility of the light. */
  float light_visibilty;
  /* Average occluder distance. In world space linear distance. */
  float occluder_distance;
};

/* ---------------------------------------------------------------------- */
/** \name Shadow Sampling Functions
 * \{ */

mat4x4 shadow_projection_perspective(float side, float near_clip, float far_clip)
{
  float z_delta = far_clip - near_clip;

  mat4x4 mat = mat4x4(1.0);
  mat[0][0] = near_clip / side;
  mat[1][1] = near_clip / side;
  mat[2][0] = 0.0;
  mat[2][1] = 0.0;
  mat[2][2] = -(far_clip + near_clip) / z_delta;
  mat[2][3] = -1.0;
  mat[3][2] = (-2.0 * near_clip * far_clip) / z_delta;
  mat[3][3] = 0.0;
  return mat;
}

mat4x4 shadow_projection_perspective_inverse(float side, float near_clip, float far_clip)
{
  float z_delta = far_clip - near_clip;
  float d = 2.0 * near_clip * far_clip;

  mat4x4 mat = mat4x4(1.0);
  mat[0][0] = side / near_clip;
  mat[1][1] = side / near_clip;
  mat[2][0] = 0.0;
  mat[2][1] = 0.0;
  mat[2][2] = 0.0;
  mat[2][3] = (near_clip - far_clip) / d;
  mat[3][2] = -1.0;
  mat[3][3] = (near_clip + far_clip) / d;
  return mat;
}

/**
 * Convert occluder distance in shadow space to world space distance.
 * Assuming the occluder is above the shading point in direction to the shadow projection center.
 */
float shadow_linear_occluder_distance(LightData light,
                                      const bool is_directional,
                                      vec3 lP,
                                      float occluder)
{
  float near = orderedIntBitsToFloat(light.clip_near);
  float far = orderedIntBitsToFloat(light.clip_far);

  float occluder_z = (is_directional) ? (occluder * (far - near) + near) :
                                        ((near * far) / (occluder * (near - far) + far));
  float receiver_z = (is_directional) ? -lP.z : reduce_max(abs(lP));
  if (!is_directional) {
    float lP_len = length(lP);
    return lP_len - lP_len * (occluder_z / receiver_z);
  }
  return receiver_z - occluder_z;
}

mat4 shadow_punctual_projection_perspective(LightData light)
{
  /* Face Local (View) Space > Clip Space. */
  float clip_far = intBitsToFloat(light.clip_far);
  float clip_near = intBitsToFloat(light.clip_near);
  float clip_side = light_local_data_get(light).clip_side;
  return shadow_projection_perspective(clip_side, clip_near, clip_far);
}

mat4 shadow_punctual_projection_perspective_inverse(LightData light)
{
  /* Face Local (View) Space > Clip Space. */
  float clip_far = intBitsToFloat(light.clip_far);
  float clip_near = intBitsToFloat(light.clip_near);
  float clip_side = light_local_data_get(light).clip_side;
  return shadow_projection_perspective_inverse(clip_side, clip_near, clip_far);
}

vec3 shadow_punctual_reconstruct_position(ShadowSampleParams params,
                                          mat4 wininv,
                                          LightData light,
                                          vec3 uvw)
{
  vec3 clip_P = uvw * 2.0 - 1.0;
  vec3 lP = project_point(wininv, clip_P);
  int face_id = params.tilemap_index - light.tilemap_index;
  lP = shadow_punctual_face_local_to_local_position(face_id, lP);
  return mat3(light.object_mat) * lP + light._position;
}

ShadowSampleParams shadow_punctual_sample_params_get(LightData light, vec3 P)
{
  vec3 lP = (P - light._position) * mat3(light.object_mat);

  int face_id = shadow_punctual_face_index_get(lP);
  /* Local Light Space > Face Local (View) Space. */
  lP = shadow_punctual_local_position_to_face_local(face_id, lP);
  mat4 winmat = shadow_punctual_projection_perspective(light);
  vec3 clip_P = project_point(winmat, lP);
  /* Clip Space > UV Space. */
  vec3 uv_P = saturate(clip_P * 0.5 + 0.5);

  ShadowSampleParams result;
  result.lP = lP;
  result.uv = uv_P;
  result.tilemap_index = light.tilemap_index + face_id;
  result.z_range = 1.0;
  return result;
}

ShadowEvalResult shadow_punctual_sample_get(SHADOW_ATLAS_TYPE atlas_tx,
                                            usampler2D tilemaps_tx,
                                            LightData light,
                                            vec3 P)
{
  ShadowSampleParams params = shadow_punctual_sample_params_get(light, P);

  float depth = shadow_read_depth(atlas_tx, tilemaps_tx, params);

  ShadowEvalResult result;
  result.light_visibilty = float(params.uv.z < depth);
  result.occluder_distance = shadow_linear_occluder_distance(light, false, params.lP, depth);
  return result;
}

struct ShadowDirectionalSampleInfo {
  float clip_near;
  float clip_far;
  int level_relative;
  int lod_relative;
  ivec2 clipmap_offset;
  vec2 clipmap_origin;
};

ShadowDirectionalSampleInfo shadow_directional_sample_info_get(LightData light, vec3 lP)
{
  ShadowDirectionalSampleInfo info;
  info.clip_near = orderedIntBitsToFloat(light.clip_near);
  info.clip_far = orderedIntBitsToFloat(light.clip_far);

  int level = shadow_directional_level(light, lP - light._position);
  /* This difference needs to be less than 32 for the later shift to be valid.
   * This is ensured by ShadowDirectional::clipmap_level_range(). */
  info.level_relative = level - light_sun_data_get(light).clipmap_lod_min;
  info.lod_relative = (light.type == LIGHT_SUN_ORTHO) ? light_sun_data_get(light).clipmap_lod_min :
                                                        level;

  info.clipmap_offset = shadow_decompress_grid_offset(
      light.type,
      light_sun_data_get(light).clipmap_base_offset_neg,
      light_sun_data_get(light).clipmap_base_offset_pos,
      info.level_relative);
  info.clipmap_origin = light_sun_data_get(light).clipmap_origin;

  return info;
}

vec3 shadow_directional_reconstruct_position(ShadowSampleParams params, LightData light, vec3 uvw)
{
  ShadowDirectionalSampleInfo info = shadow_directional_sample_info_get(light, params.lP);

  vec2 tilemap_uv = uvw.xy;
  tilemap_uv += vec2(info.clipmap_offset) / float(SHADOW_TILEMAP_RES);
  vec2 clipmap_pos = (tilemap_uv - 0.5) / exp2(-float(info.lod_relative));

  vec3 lP;
  lP.xy = clipmap_pos + info.clipmap_origin;
  lP.z = (params.uv.z + info.clip_near) * -1.0;

  return mat3(light.object_mat) * lP;
}

ShadowSampleParams shadow_directional_sample_params_get(usampler2D tilemaps_tx,
                                                        LightData light,
                                                        vec3 P)
{
  vec3 lP = P * mat3(light.object_mat);
  ShadowDirectionalSampleInfo info = shadow_directional_sample_info_get(light, lP);

  ShadowCoordinates coord = shadow_directional_coordinates(light, lP);

  /* Assumed to be non-null. */
  float z_range = info.clip_far - info.clip_near;
  float dist_to_near_plane = -lP.z - info.clip_near;

  vec2 clipmap_pos = lP.xy - info.clipmap_origin;
  vec2 tilemap_uv = clipmap_pos * exp2(-float(info.lod_relative)) + 0.5;

  /* Translate tilemap UVs to its origin. */
  tilemap_uv -= vec2(info.clipmap_offset) / float(SHADOW_TILEMAP_RES);
  /* Clamp to avoid out of tilemap access. */
  tilemap_uv = saturate(tilemap_uv);

  ShadowSampleParams result;
  result.lP = lP;
  result.uv = vec3(tilemap_uv, dist_to_near_plane);
  result.tilemap_index = light.tilemap_index + info.level_relative;
  result.z_range = z_range;
  return result;
}

ShadowEvalResult shadow_directional_sample_get(SHADOW_ATLAS_TYPE atlas_tx,
                                               usampler2D tilemaps_tx,
                                               LightData light,
                                               vec3 P)
{
  ShadowSampleParams params = shadow_directional_sample_params_get(tilemaps_tx, light, P);

  float depth = shadow_read_depth(atlas_tx, tilemaps_tx, params);

  ShadowEvalResult result;
  result.light_visibilty = float(params.uv.z < depth * params.z_range);
  result.occluder_distance = shadow_linear_occluder_distance(light, true, params.lP, depth);
  return result;
}

ShadowEvalResult shadow_sample(const bool is_directional,
                               SHADOW_ATLAS_TYPE atlas_tx,
                               usampler2D tilemaps_tx,
                               LightData light,
                               vec3 P)
{
  if (is_directional) {
    return shadow_directional_sample_get(atlas_tx, tilemaps_tx, light, P);
  }
  else {
    return shadow_punctual_sample_get(atlas_tx, tilemaps_tx, light, P);
  }
}

/** \} */

/////////////////////////////// Source file 36/////////////////////////////




/**
 * Various utilities related to object subsurface light transport.
 *
 * Required resources:
 * - utility_tx
 */

#pragma BLENDER_REQUIRE(eevee_shadow_lib.glsl)

float subsurface_transmittance_profile(float u)
{
  return utility_tx_sample(utility_tx, vec2(u, 0.0), UTIL_SSS_TRANSMITTANCE_PROFILE_LAYER).r;
}

/**
 * Returns the amount of light that can travels through a uniform medium and exit at the backface.
 */
vec3 subsurface_transmission(vec3 sss_radii, float thickness)
{
  sss_radii *= SSS_TRANSMIT_LUT_RADIUS;
  vec3 channels_co = saturate(thickness / sss_radii) * SSS_TRANSMIT_LUT_SCALE +
                     SSS_TRANSMIT_LUT_BIAS;
  vec3 translucency;
  translucency.x = (sss_radii.x > 0.0) ? subsurface_transmittance_profile(channels_co.x) : 0.0;
  translucency.y = (sss_radii.y > 0.0) ? subsurface_transmittance_profile(channels_co.y) : 0.0;
  translucency.z = (sss_radii.z > 0.0) ? subsurface_transmittance_profile(channels_co.z) : 0.0;
  return translucency;
}

/////////////////////////////// Source file 37/////////////////////////////




/**
 * Adapted from :
 * Real-Time Polygonal-Light Shading with Linearly Transformed Cosines.
 * Eric Heitz, Jonathan Dupuy, Stephen Hill and David Neubelt.
 * ACM Transactions on Graphics (Proceedings of ACM SIGGRAPH 2016) 35(4), 2016.
 * Project page: https://eheitzresearch.wordpress.com/415-2/
 */

#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)

#define LTC_LAMBERT_MAT vec4(1.0, 0.0, 0.0, 1.0)
#define LTC_GGX_MAT(cos_theta, roughness) \
  utility_tx_sample_lut(utility_tx, cos_theta, roughness, UTIL_LTC_MAT_LAYER)

/* Diffuse *clipped* sphere integral. */
float ltc_diffuse_sphere_integral(sampler2DArray utility_tx, float avg_dir_z, float form_factor)
{
#if 1
  /* use tabulated horizon-clipped sphere */
  vec2 uv = vec2(avg_dir_z * 0.5 + 0.5, form_factor);
  uv = uv * UTIL_TEX_UV_SCALE + UTIL_TEX_UV_BIAS;

  return texture(utility_tx, vec3(uv, UTIL_DISK_INTEGRAL_LAYER))[UTIL_DISK_INTEGRAL_COMP];
#else
  /* Cheap approximation. Less smooth and have energy issues. */
  return max((form_factor * form_factor + avg_dir_z) / (form_factor + 1.0), 0.0);
#endif
}

/**
 * An extended version of the implementation from
 * "How to solve a cubic equation, revisited"
 * http://momentsingraphics.de/?p=105
 */
vec3 ltc_solve_cubic(vec4 coefs)
{
  /* Normalize the polynomial */
  coefs.xyz /= coefs.w;
  /* Divide middle coefficients by three */
  coefs.yz /= 3.0;

  float A = coefs.w;
  float B = coefs.z;
  float C = coefs.y;
  float D = coefs.x;

  /* Compute the Hessian and the discriminant */
  vec3 delta = vec3(-coefs.zy * coefs.zz + coefs.yx, dot(vec2(coefs.z, -coefs.y), coefs.xy));

  /* Discriminant */
  float discr = dot(vec2(4.0 * delta.x, -delta.y), delta.zy);

  /* Clamping avoid NaN output on some platform. (see #67060) */
  float sqrt_discr = sqrt(clamp(discr, 0.0, FLT_MAX));

  vec2 xlc, xsc;

  /* Algorithm A */
  {
    float A_a = 1.0;
    float C_a = delta.x;
    float D_a = -2.0 * B * delta.x + delta.y;

    /* Take the cubic root of a normalized complex number */
    float theta = atan(sqrt_discr, -D_a) / 3.0;

    float _2_sqrt_C_a = 2.0 * sqrt(-C_a);
    float x_1a = _2_sqrt_C_a * cos(theta);
    float x_3a = _2_sqrt_C_a * cos(theta + (2.0 / 3.0) * M_PI);

    float xl;
    if ((x_1a + x_3a) > 2.0 * B) {
      xl = x_1a;
    }
    else {
      xl = x_3a;
    }

    xlc = vec2(xl - B, A);
  }

  /* Algorithm D */
  {
    float A_d = D;
    float C_d = delta.z;
    float D_d = -D * delta.y + 2.0 * C * delta.z;

    /* Take the cubic root of a normalized complex number */
    float theta = atan(D * sqrt_discr, -D_d) / 3.0;

    float _2_sqrt_C_d = 2.0 * sqrt(-C_d);
    float x_1d = _2_sqrt_C_d * cos(theta);
    float x_3d = _2_sqrt_C_d * cos(theta + (2.0 / 3.0) * M_PI);

    float xs;
    if (x_1d + x_3d < 2.0 * C) {
      xs = x_1d;
    }
    else {
      xs = x_3d;
    }

    xsc = vec2(-D, xs + C);
  }

  float E = xlc.y * xsc.y;
  float F = -xlc.x * xsc.y - xlc.y * xsc.x;
  float G = xlc.x * xsc.x;

  vec2 xmc = vec2(C * F - B * G, -B * F + C * E);

  vec3 root = vec3(xsc.x / xsc.y, xmc.x / xmc.y, xlc.x / xlc.y);

  if (root.x < root.y && root.x < root.z) {
    root.xyz = root.yxz;
  }
  else if (root.z < root.x && root.z < root.y) {
    root.xyz = root.xzy;
  }

  return root;
}

/* from Real-Time Area Lighting: a Journey from Research to Production
 * Stephen Hill and Eric Heitz */
vec3 ltc_edge_integral_vec(vec3 v1, vec3 v2)
{
  float x = dot(v1, v2);
  float y = abs(x);

  float a = 0.8543985 + (0.4965155 + 0.0145206 * y) * y;
  float b = 3.4175940 + (4.1616724 + y) * y;
  float v = a / b;

  float theta_sintheta = (x > 0.0) ? v : 0.5 * inversesqrt(max(1.0 - x * x, 1e-7)) - v;

  return cross(v1, v2) * theta_sintheta;
}

mat3 ltc_matrix(vec4 lut)
{
  /* Load inverse matrix. */
  return mat3(vec3(lut.x, 0, lut.y), vec3(0, 1, 0), vec3(lut.z, 0, lut.w));
}

mat3x3 ltc_tangent_basis(vec3 N, vec3 V)
{
  float NV = dot(N, V);
  if (NV > 0.9999) {
    /* Mostly for orthographic view and surfel light eval. */
    return from_up_axis(N);
  }
  /* Construct orthonormal basis around N. */
  vec3 T1 = normalize(V - N * NV);
  vec3 T2 = cross(N, T1);
  return mat3x3(T1, T2, N);
}

void ltc_transform_quad(vec3 N, vec3 V, mat3 Minv, inout vec3 corners[4])
{
  /* Construct orthonormal basis around N. */
  mat3 T = ltc_tangent_basis(N, V);

  /* Rotate area light in (T1, T2, R) basis. */
  Minv = Minv * transpose(T);

  /* Apply LTC inverse matrix. */
  corners[0] = normalize(Minv * corners[0]);
  corners[1] = normalize(Minv * corners[1]);
  corners[2] = normalize(Minv * corners[2]);
  corners[3] = normalize(Minv * corners[3]);
}

/* If corners have already pass through ltc_transform_quad(),
 * then N **MUST** be vec3(0.0, 0.0, 1.0), corresponding to the Up axis of the shading basis. */
float ltc_evaluate_quad(sampler2DArray utility_tx, vec3 corners[4], vec3 N)
{
  /* Approximation using a sphere of the same solid angle than the quad.
   * Finding the clipped sphere diffuse integral is easier than clipping the quad. */
  vec3 avg_dir;
  avg_dir = ltc_edge_integral_vec(corners[0], corners[1]);
  avg_dir += ltc_edge_integral_vec(corners[1], corners[2]);
  avg_dir += ltc_edge_integral_vec(corners[2], corners[3]);
  avg_dir += ltc_edge_integral_vec(corners[3], corners[0]);

  float form_factor = length(avg_dir);
  float avg_dir_z = dot(N, avg_dir / form_factor);
  return form_factor * ltc_diffuse_sphere_integral(utility_tx, avg_dir_z, form_factor);
}

/* If disk does not need to be transformed and is already front facing. */
float ltc_evaluate_disk_simple(sampler2DArray utility_tx, float disk_radius, float NL)
{
  float r_sqr = disk_radius * disk_radius;
  float form_factor = r_sqr / (1.0 + r_sqr);
  return form_factor * ltc_diffuse_sphere_integral(utility_tx, NL, form_factor);
}

/* disk_points are WS vectors from the shading point to the disk "bounding domain" */
float ltc_evaluate_disk(sampler2DArray utility_tx, vec3 N, vec3 V, mat3 Minv, vec3 disk_points[3])
{
  /* Construct orthonormal basis around N. */
  mat3 T = ltc_tangent_basis(N, V);

  /* Rotate area light in (T1, T2, R) basis. */
  mat3 R = transpose(T);

  /* Intermediate step: init ellipse. */
  vec3 L_[3];
  L_[0] = R * disk_points[0];
  L_[1] = R * disk_points[1];
  L_[2] = R * disk_points[2];

  vec3 C = 0.5 * (L_[0] + L_[2]);
  vec3 V1 = 0.5 * (L_[1] - L_[2]);
  vec3 V2 = 0.5 * (L_[1] - L_[0]);

  /* Transform ellipse by Minv. */
  C = Minv * C;
  V1 = Minv * V1;
  V2 = Minv * V2;

  /* Compute eigenvectors of new ellipse. */

  float d11 = dot(V1, V1);
  float d22 = dot(V2, V2);
  float d12 = dot(V1, V2);
  float a, b;                     /* Eigenvalues */
  const float threshold = 0.0007; /* Can be adjusted. Fix artifacts. */
  if (abs(d12) / sqrt(d11 * d22) > threshold) {
    float tr = d11 + d22;
    float det = -d12 * d12 + d11 * d22;

    /* use sqrt matrix to solve for eigenvalues */
    det = sqrt(det);
    float u = 0.5 * sqrt(tr - 2.0 * det);
    float v = 0.5 * sqrt(tr + 2.0 * det);
    float e_max = (u + v);
    float e_min = (u - v);
    e_max *= e_max;
    e_min *= e_min;

    vec3 V1_, V2_;
    if (d11 > d22) {
      V1_ = d12 * V1 + (e_max - d11) * V2;
      V2_ = d12 * V1 + (e_min - d11) * V2;
    }
    else {
      V1_ = d12 * V2 + (e_max - d22) * V1;
      V2_ = d12 * V2 + (e_min - d22) * V1;
    }

    a = 1.0 / e_max;
    b = 1.0 / e_min;
    V1 = normalize(V1_);
    V2 = normalize(V2_);
  }
  else {
    a = 1.0 / d11;
    b = 1.0 / d22;
    V1 *= sqrt(a);
    V2 *= sqrt(b);
  }

  /* Now find front facing ellipse with same solid angle. */

  vec3 V3 = normalize(cross(V1, V2));
  if (dot(C, V3) < 0.0) {
    V3 *= -1.0;
  }

  float L = dot(V3, C);
  float inv_L = 1.0 / L;
  float x0 = dot(V1, C) * inv_L;
  float y0 = dot(V2, C) * inv_L;

  float L_sqr = L * L;
  a *= L_sqr;
  b *= L_sqr;

  float t = 1.0 + x0 * x0;
  float c0 = a * b;
  float c1 = c0 * (t + y0 * y0) - a - b;
  float c2 = (1.0 - a * t) - b * (1.0 + y0 * y0);
  float c3 = 1.0;

  vec3 roots = ltc_solve_cubic(vec4(c0, c1, c2, c3));
  float e1 = roots.x;
  float e2 = roots.y;
  float e3 = roots.z;

  vec3 avg_dir = vec3(a * x0 / (a - e2), b * y0 / (b - e2), 1.0);

  mat3 rotate = mat3(V1, V2, V3);

  avg_dir = rotate * avg_dir;
  avg_dir = normalize(avg_dir);

  /* L1, L2 are the extends of the front facing ellipse. */
  float L1 = sqrt(-e2 / e3);
  float L2 = sqrt(-e2 / e1);

  /* Find the sphere and compute lighting. */
  float form_factor = max(0.0, L1 * L2 * inversesqrt((1.0 + L1 * L1) * (1.0 + L2 * L2)));
  return form_factor * ltc_diffuse_sphere_integral(utility_tx, avg_dir.z, form_factor);
}

/////////////////////////////// Source file 38/////////////////////////////




uint bitfield_mask(uint bit_width, uint bit_min)
{
  /* Cannot bit shift more than 31 positions. */
  uint mask = (bit_width > 31u) ? 0x0u : (0xFFFFFFFFu << bit_width);
  return ~mask << bit_min;
}

uint zbin_mask(uint word_index, uint zbin_min, uint zbin_max)
{
  uint word_start = word_index * 32u;
  uint word_end = word_start + 31u;
  uint local_min = max(zbin_min, word_start);
  uint local_max = min(zbin_max, word_end);
  uint mask_width = local_max - local_min + 1;
  return bitfield_mask(mask_width, local_min);
}

int culling_z_to_zbin(float scale, float bias, float z)
{
  return int(z * scale + bias);
}

/* Waiting to implement extensions support. We need:
 * - GL_KHR_shader_subgroup_ballot
 * - GL_KHR_shader_subgroup_arithmetic
 * or
 * - Vulkan 1.1
 */
#ifdef GPU_METAL
#  define subgroupMin(a) simd_min(a)
#  define subgroupMax(a) simd_max(a)
#  define subgroupOr(a) simd_or(a)
#  define subgroupBroadcastFirst(a) simd_broadcast_first(a)
#else
#  define subgroupMin(a) a
#  define subgroupMax(a) a
#  define subgroupOr(a) a
#  define subgroupBroadcastFirst(a) a
#endif

#define LIGHT_FOREACH_BEGIN_DIRECTIONAL(_culling, _index) \
  { \
    { \
      for (uint _index = _culling.local_lights_len; _index < _culling.items_count; _index++) {

/* No culling. Iterate over all items. */
#define LIGHT_FOREACH_BEGIN_LOCAL_NO_CULL(_culling, _item_index) \
  { \
    { \
      for (uint _item_index = 0; _item_index < _culling.visible_count; _item_index++) {

#ifdef LIGHT_ITER_FORCE_NO_CULLING

/* Skip the optimization structure traversal for cases where the acceleration structure
 * doesn't match. */
#  define LIGHT_FOREACH_BEGIN_LOCAL(_culling, _zbins, _words, _pixel, _linearz, _item_index) \
    LIGHT_FOREACH_BEGIN_LOCAL_NO_CULL(_culling, _item_index)

#else

#  define LIGHT_FOREACH_BEGIN_LOCAL(_culling, _zbins, _words, _pixel, _linearz, _item_index) \
    { \
      uvec2 tile_co = uvec2(_pixel / _culling.tile_size); \
      uint tile_word_offset = (tile_co.x + tile_co.y * _culling.tile_x_len) * \
                              _culling.tile_word_len; \
      int zbin_index = culling_z_to_zbin(_culling.zbin_scale, _culling.zbin_bias, _linearz); \
      zbin_index = clamp(zbin_index, 0, CULLING_ZBIN_COUNT - 1); \
      uint zbin_data = _zbins[zbin_index]; \
      uint min_index = zbin_data & 0xFFFFu; \
      uint max_index = zbin_data >> 16u; \
      /* Ensure all threads inside a subgroup get the same value to reduce VGPR usage. */ \
      min_index = subgroupBroadcastFirst(subgroupMin(min_index)); \
      max_index = subgroupBroadcastFirst(subgroupMax(max_index)); \
      /* Same as divide by 32 but avoid integer division. */ \
      uint word_min = min_index >> 5u; \
      uint word_max = max_index >> 5u; \
      for (uint word_idx = word_min; word_idx <= word_max; word_idx++) { \
        uint word = _words[tile_word_offset + word_idx]; \
        word &= zbin_mask(word_idx, min_index, max_index); \
        /* Ensure all threads inside a subgroup get the same value to reduce VGPR usage. */ \
        word = subgroupBroadcastFirst(subgroupOr(word)); \
        int bit_index; \
        while ((bit_index = findLSB(word)) != -1) { \
          word &= ~1u << uint(bit_index); \
          uint _item_index = word_idx * 32u + bit_index;

#endif

#define LIGHT_FOREACH_END \
  } \
  } \
  }

/////////////////////////////// Source file 39/////////////////////////////




#pragma BLENDER_REQUIRE(draw_math_geom_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_ltc_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_light_iter_lib.glsl)

/* Attenuation cutoff needs to be the same in the shadow loop and the light eval loop. */
#define LIGHT_ATTENUATION_THRESHOLD 1e-6

/* ---------------------------------------------------------------------- */
/** \name Light Functions
 * \{ */

struct LightVector {
  /* World space light vector. From the shading point to the light center. Normalized. */
  vec3 L;
  /* Distance from the shading point to the light center. */
  float dist;
};

LightVector light_vector_get(LightData light, const bool is_directional, vec3 P)
{
  LightVector lv;
  if (is_directional) {
    lv.L = light._back;
    lv.dist = 1.0;
  }
  else {
    lv.L = light._position - P;
    float inv_distance = inversesqrt(length_squared(lv.L));
    lv.L *= inv_distance;
    lv.dist = 1.0 / inv_distance;
  }
  return lv;
}

/* Light vector to the closest point in the light shape. */
LightVector light_shape_vector_get(LightData light, const bool is_directional, vec3 P)
{
  if (!is_directional && is_area_light(light.type)) {
    LightAreaData area = light_area_data_get(light);

    vec3 L = P - light._position;
    vec2 closest_point = vec2(dot(light._right, L), dot(light._up, L));
    closest_point /= area.size;

    if (light.type == LIGHT_ELLIPSE) {
      closest_point /= max(1.0, length(closest_point));
    }
    else {
      closest_point = clamp(closest_point, -1.0, 1.0);
    }
    closest_point *= area.size;

    vec3 L_prime = light._right * closest_point.x + light._up * closest_point.y;

    L = L_prime - L;
    float inv_distance = inversesqrt(length_squared(L));
    LightVector lv;
    lv.L = L * inv_distance;
    lv.dist = 1.0 / inv_distance;
    return lv;
  }
  /* TODO(@fclem): other light shape? */
  return light_vector_get(light, is_directional, P);
}

/* Rotate vector to light's local space. Does not translate. */
vec3 light_world_to_local(LightData light, vec3 L)
{
  /* Avoid relying on compiler to optimize this.
   * vec3 lL = transpose(mat3(light.object_mat)) * L; */
  vec3 lL;
  lL.x = dot(light.object_mat[0].xyz, L);
  lL.y = dot(light.object_mat[1].xyz, L);
  lL.z = dot(light.object_mat[2].xyz, L);
  return lL;
}

/* Transform position from light's local space to world space. Does translation. */
vec3 light_local_position_to_world(LightData light, vec3 lP)
{
  return mat3(light.object_mat) * lP + light._position;
}

/* From Frostbite PBR Course
 * Distance based attenuation
 * http://www.frostbite.com/wp-content/uploads/2014/11/course_notes_moving_frostbite_to_pbr.pdf */
float light_influence_attenuation(float dist, float inv_sqr_influence)
{
  float factor = square(dist) * inv_sqr_influence;
  float fac = saturate(1.0 - square(factor));
  return square(fac);
}

float light_spot_attenuation(LightData light, vec3 L)
{
  LightSpotData spot = light_spot_data_get(light);
  vec3 lL = light_world_to_local(light, L);
  float ellipse = inversesqrt(1.0 + length_squared(lL.xy * spot.spot_size_inv / lL.z));
  float spotmask = smoothstep(0.0, 1.0, ellipse * spot.spot_mul + spot.spot_bias);
  return spotmask * step(0.0, -dot(L, -light._back));
}

float light_attenuation_common(LightData light, const bool is_directional, vec3 L)
{
  if (is_directional) {
    return 1.0;
  }
  if (is_spot_light(light.type)) {
    return light_spot_attenuation(light, L);
  }
  if (is_area_light(light.type)) {
    return step(0.0, -dot(L, -light._back));
  }
  return 1.0;
}

/**
 * Fade light influence when surface is not facing the light.
 * This is needed because LTC leaks light at roughness not 0 or 1
 * when the light is below the horizon.
 * L is normalized vector to light shape center.
 * Ng is ideally the geometric normal.
 */
float light_attenuation_facing(LightData light,
                               vec3 L,
                               float distance_to_light,
                               vec3 Ng,
                               const bool is_transmission,
                               bool is_translucent_with_thickness)
{
  if (is_translucent_with_thickness) {
    /* No attenuation in this case since we integrate the whole sphere. */
    return 1.0;
  }

  float radius;
  if (is_sun_light(light.type)) {
    radius = light_sun_data_get(light).radius;
  }
  else if (is_area_light(light.type)) {
    radius = length(light_area_data_get(light).size);
  }
  else {
    radius = light_spot_data_get(light).radius;
  }
  /* Sine of angle between light center and light edge. */
  float sin_solid_angle = radius / distance_to_light;
  /* Sine of angle between light center and shading plane. */
  float sin_light_angle = dot(L, Ng);
  /* Do attenuation after the horizon line to avoid harsh cut
   * or biasing of surfaces without light bleeding. */
  float dist = sin_solid_angle + (is_transmission ? -sin_light_angle : sin_light_angle);
  return saturate((dist + 0.1) * 10.0);
}

float light_attenuation_surface(LightData light,
                                const bool is_directional,
                                const bool is_transmission,
                                bool is_translucency_with_thickness,
                                vec3 Ng,
                                LightVector lv)
{
  float result = light_attenuation_facing(
      light, lv.L, lv.dist, Ng, is_transmission, is_translucency_with_thickness);
  result *= light_attenuation_common(light, is_directional, lv.L);
  if (!is_directional) {
    result *= light_influence_attenuation(
        lv.dist, light_local_data_get(light).influence_radius_invsqr_surface);
  }
  return result;
}

float light_attenuation_volume(LightData light, const bool is_directional, LightVector lv)
{
  float result = light_attenuation_common(light, is_directional, lv.L);
  if (!is_directional) {
    result *= light_influence_attenuation(
        lv.dist, light_local_data_get(light).influence_radius_invsqr_volume);
  }
  return result;
}

/* Cheaper alternative than evaluating the LTC.
 * The result needs to be multiplied by BSDF or Phase Function. */
float light_point_light(LightData light, const bool is_directional, LightVector lv)
{
  if (is_directional) {
    return 1.0;
  }
  /* Using "Point Light Attenuation Without Singularity" from Cem Yuksel
   * http://www.cemyuksel.com/research/pointlightattenuation/pointlightattenuation.pdf
   * http://www.cemyuksel.com/research/pointlightattenuation/
   */
  float d_sqr = square(lv.dist);
  float r_sqr = light_local_data_get(light).radius_squared;
  /* Using reformulation that has better numerical precision. */
  float power = 2.0 / (d_sqr + r_sqr + lv.dist * sqrt(d_sqr + r_sqr));

  if (is_area_light(light.type)) {
    /* Modulate by light plane orientation / solid angle. */
    power *= saturate(dot(light._back, lv.L));
  }
  return power;
}

/**
 * Return the radius of the disk at the sphere origin spanning the same solid angle as the sphere
 * from a given distance.
 * Assume `distance_to_sphere > sphere_radius`, otherwise return almost infinite radius.
 */
float light_sphere_disk_radius(float sphere_radius, float distance_to_sphere)
{
  /* The sine of the half-angle spanned by a sphere light is equal to the tangent of the
   * half-angle spanned by a disk light with the same radius. */
  return sphere_radius * inversesqrt(max(1e-8, 1.0 - square(sphere_radius / distance_to_sphere)));
}

float light_ltc(
    sampler2DArray utility_tx, LightData light, vec3 N, vec3 V, LightVector lv, vec4 ltc_mat)
{
  if (is_sphere_light(light.type) && lv.dist < light_spot_data_get(light).radius) {
    /* Inside the sphere light, integrate over the hemisphere. */
    return 1.0;
  }

  if (light.type == LIGHT_RECT) {
    LightAreaData area = light_area_data_get(light);

    vec3 corners[4];
    corners[0] = light._right * area.size.x + light._up * -area.size.y;
    corners[1] = light._right * area.size.x + light._up * area.size.y;
    corners[2] = -corners[0];
    corners[3] = -corners[1];

    vec3 L = lv.L * lv.dist;
    corners[0] += L;
    corners[1] += L;
    corners[2] += L;
    corners[3] += L;

    ltc_transform_quad(N, V, ltc_matrix(ltc_mat), corners);

    return ltc_evaluate_quad(utility_tx, corners, vec3(0.0, 0.0, 1.0));
  }
  else {
    vec3 Px = light._right;
    vec3 Py = light._up;

    if (!is_area_light(light.type)) {
      make_orthonormal_basis(lv.L, Px, Py);
    }

    vec2 size;
    if (is_sphere_light(light.type)) {
      /* Spherical omni or spot light. */
      size = vec2(light_sphere_disk_radius(light_spot_data_get(light).radius, lv.dist));
    }
    else if (is_oriented_disk_light(light.type)) {
      /* View direction-aligned disk. */
      size = vec2(light_spot_data_get(light).radius);
    }
    else if (is_sun_light(light.type)) {
      size = vec2(light_sun_data_get(light).radius);
    }
    else {
      /* Area light. */
      size = vec2(light_area_data_get(light).size);
    }

    vec3 points[3];
    points[0] = Px * -size.x + Py * -size.y;
    points[1] = Px * size.x + Py * -size.y;
    points[2] = -points[0];

    vec3 L = lv.L * lv.dist;
    points[0] += L;
    points[1] += L;
    points[2] += L;

    return ltc_evaluate_disk(utility_tx, N, V, ltc_matrix(ltc_mat), points);
  }
}

/** \} */

/////////////////////////////// Source file 40/////////////////////////////




/**
 * BxDF evaluation functions.
 */

#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name GGX
 *
 * \{ */

float bxdf_ggx_D(float NH, float a2)
{
  return a2 / (M_PI * square((a2 - 1.0) * (NH * NH) + 1.0));
}

float bxdf_ggx_D_opti(float NH, float a2)
{
  float tmp = (NH * a2 - NH) * NH + 1.0;
  /* Doing RCP and multiply a2 at the end. */
  return M_PI * tmp * tmp;
}

float bxdf_ggx_smith_G1(float NX, float a2)
{
  return 2.0 / (1.0 + sqrt(1.0 + a2 * (1.0 / (NX * NX) - 1.0)));
}

float bxdf_ggx_smith_G1_opti(float NX, float a2)
{
  /* Using Brian Karis approach and refactoring by NX/NX
   * this way the `(2*NL)*(2*NV)` in `G = G1(V) * G1(L)` gets canceled by the BRDF denominator
   * `4*NL*NV` RCP is done on the whole G later. Note that this is not convenient for the
   * transmission formula. */
  return NX + sqrt(NX * (NX - NX * a2) + a2);
}

/* Compute the GGX BRDF without the Fresnel term, multiplied by the cosine foreshortening term. */
float bsdf_ggx_reflect(vec3 N, vec3 L, vec3 V, float roughness)
{
  float a2 = square(roughness);

  vec3 H = normalize(L + V);
  float NH = max(dot(N, H), 1e-8);
  float NL = max(dot(N, L), 1e-8);
  float NV = max(dot(N, V), 1e-8);

  /* TODO: maybe implement non-separable shadowing-masking term following Cycles. */
  float G = bxdf_ggx_smith_G1(NV, a2) * bxdf_ggx_smith_G1(NL, a2);
  float D = bxdf_ggx_D(NH, a2);

  /* BRDF * NL =  `((D * G) / (4 * NV * NL)) * NL`. */
  return (0.25 * D * G) / NV;
}

/* Compute the GGX BTDF without the Fresnel term, multiplied by the cosine foreshortening term. */
float bsdf_ggx_refract(vec3 N, vec3 L, vec3 V, float roughness, float eta)
{
  float LV = dot(L, V);
  if (is_equal(eta, 1.0, 1e-4)) {
    /* Only valid when `L` and `V` point in the opposite directions. */
    return float(is_equal(LV, -1.0, 1e-3));
  }

  bool valid = (eta < 1.0) ? (LV < -eta) : (LV * eta < -1.0);
  if (!valid) {
    /* Impossible configuration for transmission due to total internal reflection. */
    return 0.0;
  }

  vec3 H = eta * L + V;
  H = (eta < 1.0) ? H : -H;
  float inv_len_H = safe_rcp(length(H));
  H *= inv_len_H;

  /* For transmission, `L` lies in the opposite hemisphere as `H`, therefore negate `L`. */
  float NH = max(dot(N, H), 1e-8);
  float NL = max(dot(N, -L), 1e-8);
  float NV = max(dot(N, V), 1e-8);
  float VH = saturate(dot(V, H));
  float LH = saturate(dot(-L, H));

  float a2 = square(roughness);
  float G = bxdf_ggx_smith_G1(NV, a2) * bxdf_ggx_smith_G1(NL, a2);
  float D = bxdf_ggx_D(NH, a2);

  /* `btdf * NL = abs(VH * LH) * ior^2 * D * G(V) * G(L) / (Ht2 * NV * NL) * NL`. */
  return (D * G * VH * LH * square(eta * inv_len_H)) / NV;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Lambert
 *
 * Not really a microfacet model but fits this file.
 * \{ */

float bsdf_lambert(vec3 N, vec3 L)
{
  return saturate(dot(N, L));
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Utils
 * \{ */

/* Fresnel monochromatic, perfect mirror */
float F_eta(float eta, float cos_theta)
{
  /* Compute fresnel reflectance without explicitly computing
   * the refracted direction. */
  float c = abs(cos_theta);
  float g = eta * eta - 1.0 + c * c;
  if (g > 0.0) {
    g = sqrt(g);
    float A = (g - c) / (g + c);
    float B = (c * (g + c) - 1.0) / (c * (g - c) + 1.0);
    return 0.5 * A * A * (1.0 + B * B);
  }
  /* Total internal reflections. */
  return 1.0;
}

/* Return the equivalent reflective roughness resulting in a similar lobe. */
float refraction_roughness_remapping(float roughness, float ior)
{
  /* This is a very rough mapping used by manually curve fitting the apparent roughness
   * (blurriness) of GGX reflections and GGX refraction.
   * A better fit is desirable if it is in the same order of complexity.  */
  if (ior > 1.0) {
    return roughness * sqrt_fast(1.0 - 1.0 / ior);
  }
  else {
    return roughness * sqrt_fast(saturate(1.0 - ior)) * 0.8;
  }
}

/**
 * `roughness` is expected to be the linear (from UI) roughness from.
 */
vec3 reflection_dominant_dir(vec3 N, vec3 V, float roughness)
{
  /* From Frostbite PBR Course
   * http://www.frostbite.com/wp-content/uploads/2014/11/course_notes_moving_frostbite_to_pbr.pdf
   * Listing 22.
   * Note that the reference labels squared roughness (GGX input) as roughness. */
  float m = square(roughness);
  vec3 R = -reflect(V, N);
  float smoothness = 1.0 - m;
  float fac = smoothness * (sqrt(smoothness) + m);
  return normalize(mix(N, R, fac));
}

/**
 * `roughness` is expected to be the reflection roughness from `refraction_roughness_remapping`.
 */
vec3 refraction_dominant_dir(vec3 N, vec3 V, float ior, float roughness)
{
  /* Reusing same thing as reflection_dominant_dir for now with the roughness mapped to
   * reflection roughness. */
  float m = square(roughness);
  vec3 R = refract(-V, N, 1.0 / ior);
  float smoothness = 1.0 - m;
  float fac = smoothness * (sqrt(smoothness) + m);
  return normalize(mix(-N, R, fac));
}

/** \} */

/////////////////////////////// Source file 41/////////////////////////////




/**
 * Sampling of Normal Distribution Function for various BxDF.
 */

#pragma BLENDER_REQUIRE(eevee_bxdf_lib.glsl)
#pragma BLENDER_REQUIRE(draw_math_geom_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Microfacet GGX distribution
 * \{ */

#define GGX_USE_VISIBLE_NORMAL 1

float sample_pdf_ggx_refract(
    float NH, float NV, float VH, float LH, float G1_V, float alpha, float eta)
{
  float a2 = square(alpha);
  float D = bxdf_ggx_D(NH, a2);
  float Ht2 = square(eta * LH + VH);
  return (D * G1_V * abs(VH * LH) * square(eta)) / (NV * Ht2);
}

/**
 * Returns a tangent space microfacet normal following the GGX distribution.
 *
 * \param rand: random point on the unit cylinder (result of sample_cylinder).
 * \param alpha: roughness parameter.
 * \param Vt: tangent space view vector.
 * \param G1_V: output G1_V factor to be reused.
 */
vec3 sample_ggx(vec3 rand, float alpha, vec3 Vt, out float G1_V)
{
#if GGX_USE_VISIBLE_NORMAL
  /* Sampling Visible GGX Normals with Spherical Caps.
   * Jonathan Dupuy and Anis Benyoub, HPG Vol. 42, No. 8, 2023.
   * https://diglib.eg.org/bitstream/handle/10.1111/cgf14867/v42i8_03_14867.pdf
   * View vector is expected to be in tangent space. */

  /* Transforming the view direction to the hemisphere configuration. */
  vec3 Vh = normalize(vec3(alpha * Vt.xy, Vt.z));

  /* Visibility term. */
  G1_V = 2.0 * Vh.z / (1.0 + Vh.z);

  /* Sample a spherical cap in (-Vh.z, 1]. */
  float cos_theta = mix(-Vh.z, 1.0, 1.0 - rand.x);
  float sin_theta = sqrt(saturate(1.0 - square(cos_theta)));
  vec3 Lh = vec3(sin_theta * rand.yz, cos_theta);

  /* Compute unnormalized halfway direction. */
  vec3 Hh = Vh + Lh;

  /* Transforming the normal back to the ellipsoid configuration. */
  return normalize(vec3(alpha * Hh.xy, max(0.0, Hh.z)));
#else
  /* Theta is the cone angle. */
  float z = sqrt((1.0 - rand.x) / (1.0 + square(alpha) * rand.x - rand.x)); /* cos theta */
  float r = sqrt(max(0.0, 1.0 - z * z));                                    /* sin theta */
  float x = r * rand.y;
  float y = r * rand.z;
  /* Microfacet Normal */
  return vec3(x, y, z);
#endif
}

vec3 sample_ggx(vec3 rand, float alpha, vec3 Vt)
{
  float G1_unused;
  return sample_ggx(rand, alpha, Vt, G1_unused);
}

/* Similar as `sample_ggx()`, but reduces the number or rejected samples due to reflection in the
 * lower hemisphere, and returns `pdf` instead of `G1_V`. Only used for reflection.
 *
 * Sampling visible GGX normals with bounded spherical caps.
 * Eto, Kenta, and Yusuke Tokuyoshi. "Bounded VNDF Sampling for Smith-GGX Reflections."
 * SIGGRAPH Asia 2023 Technical Communications. 2023. 1-4.
 * https://gpuopen.com/download/publications/Bounded_VNDF_Sampling_for_Smith-GGX_Reflections.pdf */
vec3 sample_ggx_bounded(vec3 rand, float alpha, vec3 Vt, out float pdf)
{
  /* Transforming the view direction to the hemisphere configuration. */
  vec3 Vh = vec3(alpha * Vt.xy, Vt.z);
  float norm = length(Vh);
  Vh = Vh / norm;

  /* Compute the bounded cap. */
  float a2 = square(alpha);
  float s2 = square(1.0 + length(Vt.xy));
  float k = (1.0 - a2) * s2 / (s2 + a2 * square(Vt.z));

  /* Sample a spherical cap in (-Vh.z * k, 1]. */
  float cos_theta = mix(-Vh.z * k, 1.0, 1.0 - rand.x);
  float sin_theta = sqrt(saturate(1.0 - square(cos_theta)));
  vec3 Lh = vec3(sin_theta * rand.yz, cos_theta);

  /* Compute unnormalized halfway direction. */
  vec3 Hh = Vh + Lh;

  /* Transforming the normal back to the ellipsoid configuration. */
  vec3 Ht = normalize(vec3(alpha * Hh.xy, max(0.0, Hh.z)));

  pdf = 0.5 * bxdf_ggx_D(saturate(Ht.z), a2) / (k * Vt.z + norm);

  return Ht;
}

/**
 * Returns a reflected ray direction following the GGX distribution.
 *
 * \param rand: random point on the unit cylinder (result of sample_cylinder).
 * \param alpha: roughness parameter.
 * \param V: View vector.
 * \param N: Normal vector.
 * \param T: Tangent vector.
 * \param B: Bitangent vector.
 *
 * \return pdf: the pdf of sampling the reflected ray. 0 if ray is invalid.
 */
vec3 sample_ggx_reflect(vec3 rand, float alpha, vec3 V, vec3 N, vec3 T, vec3 B, out float pdf)
{
  vec3 Vt = world_to_tangent(V, N, T, B);
  vec3 Ht = sample_ggx_bounded(rand, alpha, Vt, pdf);
  vec3 H = tangent_to_world(Ht, N, T, B);

  if (dot(V, H) > 0.0) {
    vec3 L = reflect(-V, H);
    return L;
  }
  pdf = 0.0;
  return vec3(1.0, 0.0, 0.0);
}

/**
 * Returns a refracted ray direction following the GGX distribution.
 *
 * \param rand: random point on the unit cylinder (result of sample_cylinder).
 * \param alpha: roughness parameter.
 * \param V: View vector.
 * \param N: Normal vector.
 * \param T: Tangent vector.
 * \param B: Bitangent vector.
 *
 * \return pdf: the pdf of sampling the refracted ray. 0 if ray is invalid.
 */
vec3 sample_ggx_refract(
    vec3 rand, float alpha, float ior, vec3 V, vec3 N, vec3 T, vec3 B, out float pdf)
{
  float G1_V;
  vec3 Vt = world_to_tangent(V, N, T, B);
  vec3 Ht = sample_ggx(rand, alpha, Vt, G1_V);
  float NH = saturate(Ht.z);
  float NV = saturate(Vt.z);
  float VH = dot(Vt, Ht);
  vec3 H = tangent_to_world(Ht, N, T, B);

  if (VH > 0.0) {
    vec3 L = refract(-V, H, 1.0 / ior);
    float LH = dot(L, H);
    pdf = sample_pdf_ggx_refract(NH, NV, VH, LH, G1_V, alpha, ior);
    return L;
  }
  pdf = 0.0;
  return vec3(1.0, 0.0, 0.0);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Hemisphere
 * \{ */

float sample_pdf_uniform_hemisphere()
{
  return 0.5 * M_1_PI;
}

vec3 sample_uniform_hemisphere(vec3 rand)
{
  float z = rand.x;                      /* cos theta */
  float r = sqrt(max(0.0, 1.0 - z * z)); /* sin theta */
  float x = r * rand.y;
  float y = r * rand.z;
  return vec3(x, y, z);
}

vec3 sample_uniform_hemisphere(vec3 rand, vec3 N, vec3 T, vec3 B, out float pdf)
{
  vec3 tH = sample_uniform_hemisphere(rand);
  pdf = sample_pdf_uniform_hemisphere();
  return mat3(T, B, N) * tH;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Cosine Hemisphere
 * \{ */

float sample_pdf_cosine_hemisphere(float cos_theta)
{
  return cos_theta * M_1_PI;
}

vec3 sample_cosine_hemisphere(vec3 rand)
{
  float z = sqrt(max(1e-16, rand.x));    /* cos theta */
  float r = sqrt(max(0.0, 1.0 - z * z)); /* sin theta */
  float x = r * rand.y;
  float y = r * rand.z;
  return vec3(x, y, z);
}

vec3 sample_cosine_hemisphere(vec3 rand, vec3 N, vec3 T, vec3 B, out float pdf)
{
  vec3 tH = sample_cosine_hemisphere(rand);
  pdf = sample_pdf_cosine_hemisphere(tH.z);
  return mat3(T, B, N) * tH;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Cosine Hemisphere
 * \{ */

float sample_pdf_uniform_sphere()
{
  return 1.0 / (4.0 * M_PI);
}

vec3 sample_uniform_sphere(vec3 rand)
{
  float cos_theta = rand.x * 2.0 - 1.0;
  float sin_theta = safe_sqrt(1.0 - cos_theta * cos_theta);
  return vec3(sin_theta * rand.yz, cos_theta);
}

vec3 sample_uniform_sphere(vec3 rand, vec3 N, vec3 T, vec3 B, out float pdf)
{
  pdf = sample_pdf_uniform_sphere();
  return mat3(T, B, N) * sample_uniform_sphere(rand);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Uniform Cone sampling
 * \{ */

vec3 sample_uniform_cone(vec3 rand, float angle)
{
  float z = cos(angle * rand.x);         /* cos theta */
  float r = sqrt(max(0.0, 1.0 - z * z)); /* sin theta */
  float x = r * rand.y;
  float y = r * rand.z;
  return vec3(x, y, z);
}

vec3 sample_uniform_cone(vec3 rand, float angle, vec3 N, vec3 T, vec3 B)
{
  vec3 tH = sample_uniform_cone(rand, angle);
  /* TODO: pdf? */
  return mat3(T, B, N) * tH;
}

/** \} */

/////////////////////////////// Source file 42/////////////////////////////




/**
 * Evaluate shadowing using shadow map ray-tracing.
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_light_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_shadow_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_bxdf_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(draw_math_geom_lib.glsl)

float shadow_read_depth_at_tilemap_uv(int tilemap_index, vec2 tilemap_uv)
{
  /* Prevent out of bound access. Assumes the input is already non negative. */
  tilemap_uv = min(tilemap_uv, vec2(0.99999));

  ivec2 texel_coord = ivec2(tilemap_uv * float(SHADOW_MAP_MAX_RES));
  /* Using bitwise ops is way faster than integer ops. */
  const int page_shift = SHADOW_PAGE_LOD;
  const int page_mask = ~(0xFFFFFFFF << SHADOW_PAGE_LOD);

  ivec2 tile_coord = texel_coord >> page_shift;
  ShadowSamplingTile tile = shadow_tile_load(shadow_tilemaps_tx, tile_coord, tilemap_index);

  if (!tile.is_valid) {
    return -1.0;
  }
  /* Shift LOD0 pixels so that they get wrapped at the right position for the given LOD. */
  /* TODO convert everything to uint to avoid signed int operations. */
  texel_coord += ivec2(tile.lod_offset << SHADOW_PAGE_LOD);
  /* Scale to LOD pixels (merge LOD0 pixels together) then mask to get pixel in page. */
  ivec2 texel_page = (texel_coord >> int(tile.lod)) & page_mask;
  ivec3 texel = ivec3((ivec2(tile.page.xy) << page_shift) | texel_page, tile.page.z);

  return uintBitsToFloat(texelFetch(shadow_atlas_tx, texel, 0).r);
}

/* ---------------------------------------------------------------------- */
/** \name Shadow Map Tracing loop
 * \{ */

#define SHADOW_TRACING_INVALID_HISTORY -999.0

struct ShadowMapTracingState {
  /* Receiver Z value at previous valid depth sample. */
  float receiver_depth_history;
  /* Occluder Z value at previous valid depth sample. */
  float occluder_depth_history;
  /* Ray time at previous valid depth sample. */
  float ray_time_history;
  /* Z slope (delta/time) between previous valid sample (N-1) and the one before that (N-2). */
  float occluder_depth_slope;
  /* Multiplier and bias to the ray step quickly compute ray time. */
  float ray_step_mul;
  float ray_step_bias;
  /* State of the trace. */
  float ray_time;
  bool hit;
};

ShadowMapTracingState shadow_map_trace_init(int sample_count, float step_offset)
{
  ShadowMapTracingState state;
  state.receiver_depth_history = -1.0;
  state.occluder_depth_history = SHADOW_TRACING_INVALID_HISTORY;
  state.ray_time_history = -1.0;
  state.occluder_depth_slope = 0.0;
  /* We trace the ray in reverse. From 1.0 (light) to 0.0 (shading point). */
  state.ray_step_mul = -1.0 / float(sample_count);
  state.ray_step_bias = 1.0 + step_offset * state.ray_step_mul;
  state.hit = false;
  return state;
}

struct ShadowTracingSample {
  float receiver_depth;
  float occluder_depth;
  bool skip_sample;
};

/**
 * This need to be instantiated for each `ShadowRay*` type.
 * This way we can implement `shadow_map_trace_sample` for each type without too much code
 * duplication.
 * Most of the code is wrapped into functions to avoid to debug issues inside macro code.
 */
#define SHADOW_MAP_TRACE_FN(ShadowRayType) \
  ShadowMapTraceResult shadow_map_trace(ShadowRayType ray, int sample_count, float step_offset) \
  { \
    ShadowMapTracingState state = shadow_map_trace_init(sample_count, step_offset); \
    for (int i = 0; (i <= sample_count) && (i <= SHADOW_MAX_STEP) && (state.hit == false); i++) { \
      /* Saturate to always cover the shading point position when i == sample_count. */ \
      state.ray_time = square(saturate(float(i) * state.ray_step_mul + state.ray_step_bias)); \
\
      ShadowTracingSample samp = shadow_map_trace_sample(state, ray); \
\
      shadow_map_trace_hit_check(state, samp); \
    } \
    return shadow_map_trace_finish(state); \
  }

/**
 * We trace from a point on the light towards the shading point.
 *
 * This reverse tracing allows to approximate the geometry behind occluders while minimizing
 * light-leaks.
 */
void shadow_map_trace_hit_check(inout ShadowMapTracingState state, ShadowTracingSample samp)
{
  /* Skip empty tiles since they do not contain actual depth information.
   * Not doing so would change the z gradient history. */
  if (samp.skip_sample) {
    return;
  }
  /* For the first sample, regular depth compare since we do not have history values. */
  if (state.occluder_depth_history == SHADOW_TRACING_INVALID_HISTORY) {
    if (samp.occluder_depth < samp.receiver_depth) {
      state.hit = true;
      return;
    }
    state.occluder_depth_history = samp.occluder_depth;
    state.receiver_depth_history = samp.receiver_depth;
    state.ray_time_history = state.ray_time;
    return;
  }
  /* Delta between previous valid sample. */
  float ray_depth_delta = samp.receiver_depth - state.receiver_depth_history;
  /* Delta between previous valid sample not occluding the ray. */
  float time_delta = state.ray_time - state.ray_time_history;
  /* Arbitrary increase the threshold to avoid missing occluders because of precision issues.
   * Increasing the threshold inflates the occluders. */
  float compare_threshold = abs(ray_depth_delta) * 1.05;
  /* Find out if the ray step is behind an occluder.
   * To be consider behind (and ignore the occluder), the occluder must not be cross the ray.
   * Use the full delta ray depth as threshold to make sure to not miss any occluder. */
  bool is_behind = samp.occluder_depth < (samp.receiver_depth - compare_threshold);

  if (is_behind) {
    /* Use last known valid occluder Z value and extrapolate to the sample position. */
    samp.occluder_depth = state.occluder_depth_history + state.occluder_depth_slope * time_delta;
    /* Intersection test will be against the extrapolated last known occluder. */
  }
  else {
    /* Compute current occluder slope and record history for when the ray goes behind a surface. */
    state.occluder_depth_slope = (samp.occluder_depth - state.occluder_depth_history) / time_delta;
    state.occluder_depth_slope = clamp(state.occluder_depth_slope, -100.0, 100.0);
    state.occluder_depth_history = samp.occluder_depth;
    state.ray_time_history = state.ray_time;
    /* Intersection test will be against the current sample's occluder. */
  }

  if (samp.occluder_depth < samp.receiver_depth) {
    state.occluder_depth_history = samp.occluder_depth;
    state.hit = true;
    return;
  }
  /* No intersection. */
  state.receiver_depth_history = samp.receiver_depth;
}

struct ShadowMapTraceResult {
  bool has_hit;
  float occluder_depth;
};

ShadowMapTraceResult shadow_map_trace_finish(ShadowMapTracingState state)
{
  ShadowMapTraceResult result;
  if (state.hit) {
    result.occluder_depth = state.occluder_depth_history;
    result.has_hit = true;
  }
  else {
    result.occluder_depth = 0.0;
    result.has_hit = false;
  }
  return result;
}

/** \} */

/* If the ray direction `L`  is below the horizon defined by N (normalized) at the shading point,
 * push it just above the horizon so that this ray will never be below it and produce
 * over-shadowing (since light evaluation already clips the light shape). */
vec3 shadow_ray_above_horizon_ensure(vec3 L, vec3 N)
{
  float distance_to_plan = dot(L, -N);
  if (distance_to_plan > 0.0) {
    L += N * (0.01 + distance_to_plan);
  }
  return L;
}

/* ---------------------------------------------------------------------- */
/** \name Directional Shadow Map Tracing
 * \{ */

struct ShadowRayDirectional {
  /* Ray in local translated coordinate, with depth in [0..1] range in W component. */
  vec4 origin;
  vec4 direction;
  LightData light;
};

ShadowRayDirectional shadow_ray_generate_directional(LightData light,
                                                     vec2 random_2d,
                                                     vec3 lP,
                                                     vec3 lNg)
{
  float clip_near = orderedIntBitsToFloat(light.clip_near);
  float clip_far = orderedIntBitsToFloat(light.clip_far);
  /* Assumed to be non-null. */
  float z_range = clip_far - clip_near;
  float dist_to_near_plane = -lP.z - clip_near;

  /* `lP` is supposed to be in light rotated space. But not translated. */
  vec4 origin = vec4(lP, dist_to_near_plane / z_range);

  vec3 disk_direction = sample_uniform_cone(sample_cylinder(random_2d),
                                            light_sun_data_get(light).shadow_angle);

  disk_direction = shadow_ray_above_horizon_ensure(disk_direction, lNg);

  /* Light shape is 1 unit away from the shading point. */
  vec4 direction = vec4(disk_direction, -1.0 / z_range);

  /* It only make sense to trace where there can be occluder. Clamp by distance to near plane. */
  direction *= min(light_sun_data_get(light).shadow_trace_distance,
                   dist_to_near_plane / disk_direction.z);

  ShadowRayDirectional ray;
  ray.origin = origin;
  ray.direction = direction;
  ray.light = light;
  return ray;
}

ShadowTracingSample shadow_map_trace_sample(ShadowMapTracingState state,
                                            inout ShadowRayDirectional ray)
{
  /* Ray position is ray local position with origin at light origin. */
  vec4 ray_pos = ray.origin + ray.direction * state.ray_time;

  int level = shadow_directional_level(ray.light, ray_pos.xyz - ray.light._position);
  /* This difference needs to be less than 32 for the later shift to be valid.
   * This is ensured by ShadowDirectional::clipmap_level_range(). */
  int level_relative = level - light_sun_data_get(ray.light).clipmap_lod_min;

  int lod_relative = (ray.light.type == LIGHT_SUN_ORTHO) ?
                         light_sun_data_get(ray.light).clipmap_lod_min :
                         level;

  vec2 clipmap_origin = light_sun_data_get(ray.light).clipmap_origin;
  vec2 clipmap_pos = ray_pos.xy - clipmap_origin;
  vec2 tilemap_uv = clipmap_pos * exp2(-float(lod_relative)) + 0.5;

  /* Compute offset in tile. */
  ivec2 clipmap_offset = shadow_decompress_grid_offset(
      ray.light.type,
      light_sun_data_get(ray.light).clipmap_base_offset_neg,
      light_sun_data_get(ray.light).clipmap_base_offset_pos,
      level_relative);
  /* Translate tilemap UVs to its origin. */
  tilemap_uv -= vec2(clipmap_offset) / float(SHADOW_TILEMAP_RES);
  /* Clamp to avoid out of tilemap access. */
  tilemap_uv = saturate(tilemap_uv);

  ShadowTracingSample samp;
  samp.receiver_depth = ray_pos.w;
  samp.occluder_depth = shadow_read_depth_at_tilemap_uv(ray.light.tilemap_index + level_relative,
                                                        tilemap_uv);
  samp.skip_sample = (samp.occluder_depth == -1.0);
  return samp;
}

SHADOW_MAP_TRACE_FN(ShadowRayDirectional)

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Punctual Shadow Map Tracing
 * \{ */

struct ShadowRayPunctual {
  /* Ray in tile-map normalized coordinates [0..1]. */
  vec3 origin;
  vec3 direction;
  /* Tile-map to sample. */
  int tilemap_index;
};

/* Return ray in UV clip space [0..1]. */
ShadowRayPunctual shadow_ray_generate_punctual(LightData light, vec2 random_2d, vec3 lP, vec3 lNg)
{
  if (light.type == LIGHT_RECT) {
    random_2d = random_2d * 2.0 - 1.0;
  }
  else {
    random_2d = sample_disk(random_2d);
  }

  float clip_far = intBitsToFloat(light.clip_far);
  float clip_near = intBitsToFloat(light.clip_near);
  float clip_side = light_local_data_get(light).clip_side;

  /* TODO(fclem): 3D shift for jittered soft shadows. */
  vec3 projection_origin = vec3(0.0, 0.0, -light_local_data_get(light).shadow_projection_shift);
  vec3 direction;
  if (is_area_light(light.type)) {
    random_2d *= light_area_data_get(light).size;

    vec3 point_on_light_shape = vec3(random_2d, 0.0);
    /* Progressively blend the shape back to the projection origin. */
    point_on_light_shape = mix(
        -projection_origin, point_on_light_shape, light_local_data_get(light).shadow_scale);

    direction = point_on_light_shape - lP;
    direction = shadow_ray_above_horizon_ensure(direction, lNg);

    /* Clip the ray to not cross the near plane.
     * Scale it so that it encompass the whole cube (with a safety margin). */
    float clip_distance = clip_near + 0.001;
    float ray_length = max(abs(direction.x), max(abs(direction.y), abs(direction.z)));
    direction *= saturate((ray_length - clip_distance) / ray_length);
  }
  else {
    float dist;
    vec3 L = normalize_and_get_length(lP, dist);
    /* Disk rotated towards light vector. */
    vec3 right, up;
    make_orthonormal_basis(L, right, up);

    float shape_radius = light_spot_data_get(light).radius;
    if (is_sphere_light(light.type)) {
      /* FIXME(weizhen): this is not well-defined when `dist < light.spot.radius`. */
      shape_radius = light_sphere_disk_radius(shape_radius, dist);
    }
    random_2d *= shape_radius;

    random_2d *= light_local_data_get(light).shadow_scale;
    vec3 point_on_light_shape = right * random_2d.x + up * random_2d.y;

    direction = point_on_light_shape - lP;
    direction = shadow_ray_above_horizon_ensure(direction, lNg);

    /* Clip the ray to not cross the light shape. */
    float clip_distance = light_spot_data_get(light).radius;
    direction *= saturate((dist - clip_distance) / dist);
  }

  /* Apply shadow origin shift. */
  vec3 local_ray_start = lP + projection_origin;
  vec3 local_ray_end = local_ray_start + direction;

  /* Use an offset in the ray direction to jitter which face is traced.
   * This helps hiding some harsh discontinuity. */
  int face_id = shadow_punctual_face_index_get(local_ray_start + direction * 0.5);
  /* Local Light Space > Face Local (View) Space. */
  vec3 view_ray_start = shadow_punctual_local_position_to_face_local(face_id, local_ray_start);
  vec3 view_ray_end = shadow_punctual_local_position_to_face_local(face_id, local_ray_end);

  /* Face Local (View) Space > Clip Space. */
  /* TODO: Could be simplified since frustum is completely symmetrical. */
  mat4 winmat = projection_perspective(
      -clip_side, clip_side, -clip_side, clip_side, clip_near, clip_far);
  vec3 clip_ray_start = project_point(winmat, view_ray_start);
  vec3 clip_ray_end = project_point(winmat, view_ray_end);
  /* Clip Space > UV Space. */
  vec3 uv_ray_start = clip_ray_start * 0.5 + 0.5;
  vec3 uv_ray_end = clip_ray_end * 0.5 + 0.5;
  /* Compute the ray again. */
  ShadowRayPunctual ray;
  ray.origin = uv_ray_start;
  ray.direction = uv_ray_end - uv_ray_start;
  ray.tilemap_index = light.tilemap_index + face_id;
  return ray;
}

ShadowTracingSample shadow_map_trace_sample(ShadowMapTracingState state,
                                            inout ShadowRayPunctual ray)
{
  vec3 ray_pos = ray.origin + ray.direction * state.ray_time;
  vec2 tilemap_uv = saturate(ray_pos.xy);

  ShadowTracingSample samp;
  samp.receiver_depth = ray_pos.z;
  samp.occluder_depth = shadow_read_depth_at_tilemap_uv(ray.tilemap_index, tilemap_uv);
  samp.skip_sample = (samp.occluder_depth == -1.0);
  return samp;
}

SHADOW_MAP_TRACE_FN(ShadowRayPunctual)

/** \} */

/* ---------------------------------------------------------------------- */
/** \name Shadow Evaluation
 * \{ */

/* Compute the world space offset of the shading position required for
 * stochastic percentage closer filtering of shadow-maps. */
vec3 shadow_pcf_offset(LightData light, const bool is_directional, vec3 P, vec3 Ng, vec2 random)
{
  if (light.pcf_radius <= 0.001) {
    /* Early return. */
    return vec3(0.0);
  }

  vec3 L = light_vector_get(light, is_directional, P).L;
  if (dot(L, Ng) < 0.001) {
    /* Don't apply PCF to almost perpendicular,
     * since we can't project the offset to the surface. */
    return vec3(0.0);
  }

  ShadowSampleParams params;
  if (is_directional) {
    params = shadow_directional_sample_params_get(shadow_tilemaps_tx, light, P);
  }
  else {
    params = shadow_punctual_sample_params_get(light, P);
  }
  ShadowSamplingTile tile = shadow_tile_data_get(shadow_tilemaps_tx, params);
  if (!tile.is_valid) {
    return vec3(0.0);
  }

  /* Compute the shadow-map tangent-bitangent matrix. */

  float uv_offset = 1.0 / float(SHADOW_MAP_MAX_RES);
  vec3 TP, BP;
  if (is_directional) {
    TP = shadow_directional_reconstruct_position(
        params, light, params.uv + vec3(uv_offset, 0.0, 0.0));
    BP = shadow_directional_reconstruct_position(
        params, light, params.uv + vec3(0.0, uv_offset, 0.0));
  }
  else {
    mat4 wininv = shadow_punctual_projection_perspective_inverse(light);
    TP = shadow_punctual_reconstruct_position(
        params, wininv, light, params.uv + vec3(uv_offset, 0.0, 0.0));
    BP = shadow_punctual_reconstruct_position(
        params, wininv, light, params.uv + vec3(0.0, uv_offset, 0.0));
  }

  /* TODO: Use a mat2x3 (Currently not supported by the Metal backend). */
  mat3 TBN = mat3(TP - P, BP - P, Ng);

  /* Compute the actual offset. */

  vec2 pcf_offset = random * 2.0 - 1.0;
  pcf_offset *= light.pcf_radius;

  /* Scale the offset based on shadow LOD. */
  if (is_directional) {
    vec3 lP = light_world_to_local(light, P);
    float level = shadow_directional_level_fractional(light, lP - light._position);
    float pcf_scale = mix(0.5, 1.0, fract(level));
    pcf_offset *= pcf_scale;
  }
  else {
    bool is_perspective = drw_view_is_perspective();
    float dist_to_cam = distance(P, drw_view_position());
    float footprint_ratio = shadow_punctual_footprint_ratio(
        light, P, is_perspective, dist_to_cam, uniform_buf.shadow.tilemap_projection_ratio);
    float lod = -log2(footprint_ratio) + light.lod_bias;
    lod = clamp(lod, 0.0, float(SHADOW_TILEMAP_LOD));
    float pcf_scale = exp2(lod);
    pcf_offset *= pcf_scale;
  }

  vec3 ws_offset = TBN * vec3(pcf_offset, 0.0);
  vec3 offset_P = P + ws_offset;

  /* Project the offset position into the surface */

#ifdef GPU_NVIDIA
  /* Workaround for a bug in the Nvidia shader compiler.
   * If we don't compute L here again, it breaks shadows on reflection probes. */
  L = light_vector_get(light, is_directional, P).L;
#endif

  if (abs(dot(Ng, L)) > 0.999) {
    return ws_offset;
  }

  offset_P = line_plane_intersect(offset_P, L, P, Ng);
  ws_offset = offset_P - P;

  if (dot(ws_offset, L) < 0.0) {
    /* Project the offset position into the perpendicular plane, since it's closer to the light
     * (avoids overshadowing at geometry angles). */
    vec3 perpendicular_plane_normal = cross(Ng, normalize(cross(Ng, L)));
    offset_P = line_plane_intersect(offset_P, L, P, perpendicular_plane_normal);
    ws_offset = offset_P - P;
  }

  return ws_offset;
}

/**
 * Evaluate shadowing by casting rays toward the light direction.
 */
ShadowEvalResult shadow_eval(LightData light,
                             const bool is_directional,
                             const bool is_transmission,
                             bool is_translucent_with_thickness,
                             vec3 P,
                             vec3 Ng,
                             vec3 L,
                             int ray_count,
                             int ray_step_count)
{
#ifdef EEVEE_SAMPLING_DATA
#  ifdef GPU_FRAGMENT_SHADER
  vec2 pixel = floor(gl_FragCoord.xy);
#  elif defined(GPU_COMPUTE_SHADER)
  vec2 pixel = vec2(gl_GlobalInvocationID.xy);
#  endif
  vec3 blue_noise_3d = utility_tx_fetch(utility_tx, pixel, UTIL_BLUE_NOISE_LAYER).rgb;
  vec3 random_shadow_3d = blue_noise_3d + sampling_rng_3D_get(SAMPLING_SHADOW_U);
  vec2 random_pcf_2d = fract(blue_noise_3d.xy + sampling_rng_2D_get(SAMPLING_SHADOW_X));
  float normal_offset = uniform_buf.shadow.normal_bias;
#else
  /* Case of surfel light eval. */
  vec3 random_shadow_3d = vec3(0.5);
  vec2 random_pcf_2d = vec2(0.0);
  /* TODO(fclem): Parameter on irradiance volumes? */
  float normal_offset = 0.02;
#endif

  P += shadow_pcf_offset(light, is_directional, P, Ng, random_pcf_2d);

  /* We want to bias inside the object for transmission to go through the object itself.
   * But doing so split the shadow in two different directions at the horizon. Also this
   * doesn't fix the the aliasing issue. So we reflect the normal so that it always go towards
   * the light. */
  vec3 N_bias = is_transmission ? reflect(Ng, L) : Ng;

  /* Avoid self intersection. */
  P = offset_ray(P, N_bias);
  /* The above offset isn't enough in most situation. Still add a bigger bias. */
  /* TODO(fclem): Scale based on depth. */
  P += N_bias * normal_offset;

  vec3 lP = is_directional ? light_world_to_local(light, P) :
                             light_world_to_local(light, P - light._position);
  vec3 lNg = light_world_to_local(light, Ng);
  /* Invert horizon clipping. */
  lNg = (is_transmission) ? -lNg : lNg;
  /* Don't do a any horizon clipping in this case as the closure is lit from both sides. */
  lNg = (is_transmission && is_translucent_with_thickness) ? vec3(0.0) : lNg;

  float surface_hit = 0.0;
  for (int ray_index = 0; ray_index < ray_count && ray_index < SHADOW_MAX_RAY; ray_index++) {
    vec2 random_ray_2d = fract(hammersley_2d(ray_index, ray_count) + random_shadow_3d.xy);

    ShadowMapTraceResult trace;
    if (is_directional) {
      ShadowRayDirectional clip_ray = shadow_ray_generate_directional(
          light, random_ray_2d, lP, lNg);
      trace = shadow_map_trace(clip_ray, ray_step_count, random_shadow_3d.z);
    }
    else {
      ShadowRayPunctual clip_ray = shadow_ray_generate_punctual(light, random_ray_2d, lP, lNg);
      trace = shadow_map_trace(clip_ray, ray_step_count, random_shadow_3d.z);
    }

    surface_hit += float(trace.has_hit);
  }
  /* Average samples. */
  ShadowEvalResult result;
  result.light_visibilty = saturate(1.0 - surface_hit / float(ray_count));
  result.occluder_distance = 0.0; /* Unused. Could reintroduced if needed. */
  return result;
}

/** \} */

/////////////////////////////// Source file 43/////////////////////////////




struct ThicknessIsect {
  /* Normal at the intersection point on the sphere. */
  vec3 hit_N;
  /* Position of the intersection point on the sphere. */
  vec3 hit_P;
};

/**
 * Model sub-surface ray interaction with a sphere of the given diameter tangent to the shading
 * point. This allows to model 2 refraction events quite cheaply.
 * Everything is relative to the entrance shading point.
 */
ThicknessIsect thickness_sphere_intersect(float diameter, vec3 N, vec3 L)
{
  ThicknessIsect isect;
  float cos_alpha = dot(L, -N);
  isect.hit_N = normalize(N + L * (cos_alpha * 2.0));
  isect.hit_P = L * (cos_alpha * diameter);
  return isect;
}

/////////////////////////////// Source file 44/////////////////////////////




/**
 * The resources expected to be defined are:
 * - light_buf
 * - light_zbin_buf
 * - light_cull_buf
 * - light_tile_buf
 * - shadow_atlas_tx
 * - shadow_tilemaps_tx
 * - utility_tx
 */

#pragma BLENDER_REQUIRE(eevee_shadow_tracing_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_bxdf_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_light_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_shadow_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_thickness_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)

/* If using compute, the shader should define its own pixel. */
#if !defined(PIXEL) && defined(GPU_FRAGMENT_SHADER)
#  define PIXEL gl_FragCoord.xy
#endif

#if !defined(LIGHT_CLOSURE_EVAL_COUNT)
#  define LIGHT_CLOSURE_EVAL_COUNT 1
#  define SKIP_LIGHT_EVAL
#endif

uint shadow_pack(float visibility, uint bit_depth, uint shift)
{
  return uint(visibility * float((1u << bit_depth) - 1u)) << shift;
}

float shadow_unpack(uint shadow_bits, uint bit_depth, uint shift)
{
  return float((shadow_bits >> shift) & ~(~0u << bit_depth)) / float((1u << bit_depth) - 1u);
}

void light_shadow_single(uint l_idx,
                         const bool is_directional,
                         const bool is_transmission,
                         vec3 P,
                         vec3 Ng,
                         inout uint shadow_bits,
                         inout uint shift)
{
  LightData light = light_buf[l_idx];

  if (light.tilemap_index == LIGHT_NO_SHADOW) {
    return;
  }

  LightVector lv = light_vector_get(light, is_directional, P);
  float attenuation = light_attenuation_surface(
      light, is_directional, is_transmission, false, Ng, lv);
  if (attenuation < LIGHT_ATTENUATION_THRESHOLD) {
    return;
  }

#if defined(SPECIALIZED_SHADOW_PARAMS)
  int ray_count = shadow_ray_count;
  int ray_step_count = shadow_ray_step_count;
#else
  int ray_count = uniform_buf.shadow.ray_count;
  int ray_step_count = uniform_buf.shadow.step_count;
#endif

  ShadowEvalResult result = shadow_eval(
      light, is_directional, is_transmission, false, P, Ng, Ng, ray_count, ray_step_count);

  shadow_bits |= shadow_pack(result.light_visibilty, ray_count, shift);
  shift += ray_count;
}

void light_shadow_mask(vec3 P, vec3 Ng, float vPz, out uint shadow_bits)
{
  int ray_count = uniform_buf.shadow.ray_count;
  int ray_step_count = uniform_buf.shadow.step_count;

  uint shift = 0u;
  shadow_bits = 0u;
  LIGHT_FOREACH_BEGIN_DIRECTIONAL (light_cull_buf, l_idx) {
    light_shadow_single(l_idx, true, false, P, Ng, shadow_bits, shift);
  }
  LIGHT_FOREACH_END

  LIGHT_FOREACH_BEGIN_LOCAL (light_cull_buf, light_zbin_buf, light_tile_buf, PIXEL, vPz, l_idx) {
    light_shadow_single(l_idx, false, false, P, Ng, shadow_bits, shift);
  }
  LIGHT_FOREACH_END
}

struct ClosureLight {
  /* LTC matrix. */
  vec4 ltc_mat;
  /* Shading normal. */
  vec3 N;
  /* Enum used as index to fetch which light intensity to use [0..3]. */
  LightingType type;
  /* Output both shadowed and unshadowed for shadow denoising. */
  vec3 light_shadowed;
  vec3 light_unshadowed;

  /** Only for transmission BSDFs. */
  vec3 shading_offset;
  float shadow_offset;
};

struct ClosureLightStack {
  /* NOTE: This is wrapped into a struct to avoid array shenanigans on MSL. */
  ClosureLight cl[LIGHT_CLOSURE_EVAL_COUNT];
};

ClosureLight closure_light_new_ex(ClosureUndetermined cl,
                                  vec3 V,
                                  float thickness,
                                  const bool is_transmission)
{
  ClosureLight cl_light;
  cl_light.N = cl.N;
  cl_light.ltc_mat = LTC_LAMBERT_MAT;
  cl_light.type = LIGHT_DIFFUSE;
  cl_light.light_shadowed = vec3(0.0);
  cl_light.light_unshadowed = vec3(0.0);
  cl_light.shading_offset = vec3(0.0);
  cl_light.shadow_offset = is_transmission ? thickness : 0.0;
  switch (cl.type) {
    case CLOSURE_BSDF_TRANSLUCENT_ID:
      if (is_transmission) {
        cl_light.N = -cl.N;
        if (thickness > 0.0) {
          /* Strangely, a translucent sphere lit by a light outside the sphere transmits the light
           * uniformly over the sphere. To mimic this phenomenon, we shift the shading position to
           * a unique position on the sphere and use the light vector as normal. */
          cl_light.shading_offset = -cl.N * thickness * 0.5;
          cl_light.N = vec3(0.0);
        }
      }
      break;
    case CLOSURE_BSSRDF_BURLEY_ID:
      if (is_transmission) {
        /* If the `thickness / sss_radius` ratio is near 0, this transmission term should converge
         * to a uniform term like the translucent BSDF. But we need to find what to do in other
         * cases. For now, approximate the transmission term as just back-facing. */
        cl_light.N = -cl.N;
        /* Lit and shadow as outside of the object. */
        cl_light.shading_offset = -cl.N * thickness;
      }
      /* Reflection term uses the lambertian diffuse. */
      break;
    case CLOSURE_BSDF_DIFFUSE_ID:
      break;
    case CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID:
      cl_light.ltc_mat = LTC_GGX_MAT(dot(cl.N, V), cl.data.x);
      cl_light.type = LIGHT_SPECULAR;
      break;
    case CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID: {
      if (is_transmission) {
        ClosureRefraction cl_refract = to_closure_refraction(cl);
        cl_refract.roughness = refraction_roughness_remapping(cl_refract.roughness,
                                                              cl_refract.ior);

        if (thickness > 0.0) {
          vec3 L = refraction_dominant_dir(cl.N, V, cl_refract.ior, cl_refract.roughness);

          ThicknessIsect isect = thickness_sphere_intersect(thickness, cl.N, L);
          cl.N = -isect.hit_N;
          cl_light.shading_offset = isect.hit_P;

          cl_refract.ior = 1.0 / cl_refract.ior;
          V = -L;
        }
        vec3 R = refract(-V, cl.N, 1.0 / cl_refract.ior);
        cl_light.ltc_mat = LTC_GGX_MAT(dot(-cl.N, R), cl_refract.roughness);
        cl_light.N = -cl.N;
        cl_light.type = LIGHT_TRANSMISSION;
      }
      break;
    }
    case CLOSURE_NONE_ID:
      /* TODO(fclem): Assert. */
      break;
  }
  return cl_light;
}

ClosureLight closure_light_new(ClosureUndetermined cl, vec3 V, float thickness)
{
  return closure_light_new_ex(cl, V, thickness, true);
}

ClosureLight closure_light_new(ClosureUndetermined cl, vec3 V)
{
  return closure_light_new_ex(cl, V, 0.0, false);
}

void light_eval_single_closure(LightData light,
                               LightVector lv,
                               inout ClosureLight cl,
                               vec3 V,
                               float attenuation,
                               float shadow,
                               const bool is_transmission)
{
  if (light.power[cl.type] > 0.0) {
    float ltc_result = light_ltc(utility_tx, light, cl.N, V, lv, cl.ltc_mat);
    vec3 out_radiance = light.color * light.power[cl.type] * ltc_result;
    float visibility = shadow * attenuation;
    cl.light_shadowed += visibility * out_radiance;
    cl.light_unshadowed += attenuation * out_radiance;
  }
}

void light_eval_single(uint l_idx,
                       const bool is_directional,
                       const bool is_transmission,
                       inout ClosureLightStack stack,
                       vec3 P,
                       vec3 Ng,
                       vec3 V,
                       uint packed_shadows,
                       inout uint shift)
{
  LightData light = light_buf[l_idx];

#if defined(SPECIALIZED_SHADOW_PARAMS)
  int ray_count = shadow_ray_count;
  int ray_step_count = shadow_ray_step_count;
#else
  int ray_count = uniform_buf.shadow.ray_count;
  int ray_step_count = uniform_buf.shadow.step_count;
#endif

  vec3 shading_P = (is_transmission) ? P + stack.cl[0].shading_offset : P;
  LightVector lv = light_vector_get(light, is_directional, shading_P);

  bool is_translucent_with_thickness = is_transmission && all(equal(stack.cl[0].N, vec3(0.0)));

  float attenuation = light_attenuation_surface(
      light, is_directional, is_transmission, is_translucent_with_thickness, Ng, lv);
  if (attenuation < LIGHT_ATTENUATION_THRESHOLD) {
    return;
  }

  float shadow = 1.0;
  if (light.tilemap_index != LIGHT_NO_SHADOW) {
#ifdef SHADOW_DEFERRED
    shadow = shadow_unpack(packed_shadows, ray_count, shift);
    shift += ray_count;
#else

    vec3 shadow_P = (is_transmission) ? P + lv.L * stack.cl[0].shadow_offset : P;
    ShadowEvalResult result = shadow_eval(light,
                                          is_directional,
                                          is_transmission,
                                          is_translucent_with_thickness,
                                          shadow_P,
                                          Ng,
                                          lv.L,
                                          ray_count,
                                          ray_step_count);
    shadow = result.light_visibilty;
#endif
  }

  if (is_translucent_with_thickness) {
    /* This makes the LTC compute the solid angle of the light (still with the cosine term applied
     * but that still works great enough in practice). */
    stack.cl[0].N = lv.L;
    /* Adjust power because of the second lambertian distribution. */
    attenuation *= M_1_PI;
  }

  light_eval_single_closure(light, lv, stack.cl[0], V, attenuation, shadow, is_transmission);
  if (!is_transmission) {
#if LIGHT_CLOSURE_EVAL_COUNT > 1
    light_eval_single_closure(light, lv, stack.cl[1], V, attenuation, shadow, is_transmission);
#endif
#if LIGHT_CLOSURE_EVAL_COUNT > 2
    light_eval_single_closure(light, lv, stack.cl[2], V, attenuation, shadow, is_transmission);
#endif
#if LIGHT_CLOSURE_EVAL_COUNT > 3
#  error
#endif
  }
}

void light_eval_transmission(inout ClosureLightStack stack, vec3 P, vec3 Ng, vec3 V, float vPz)
{
#ifdef SKIP_LIGHT_EVAL
  return;
#endif
  /* Packed / Decoupled shadow evaluation. Not yet implemented. */
  uint packed_shadows = 0u;
  uint shift = 0u;

  LIGHT_FOREACH_BEGIN_DIRECTIONAL (light_cull_buf, l_idx) {
    light_eval_single(l_idx, true, true, stack, P, Ng, V, packed_shadows, shift);
  }
  LIGHT_FOREACH_END

  LIGHT_FOREACH_BEGIN_LOCAL (light_cull_buf, light_zbin_buf, light_tile_buf, PIXEL, vPz, l_idx) {
    light_eval_single(l_idx, false, true, stack, P, Ng, V, packed_shadows, shift);
  }
  LIGHT_FOREACH_END
}

void light_eval_reflection(inout ClosureLightStack stack, vec3 P, vec3 Ng, vec3 V, float vPz)
{
#ifdef SKIP_LIGHT_EVAL
  return;
#endif
  /* Packed / Decoupled shadow evaluation. Not yet implemented. */
  uint packed_shadows = 0u;
  uint shift = 0u;

  LIGHT_FOREACH_BEGIN_DIRECTIONAL (light_cull_buf, l_idx) {
    light_eval_single(l_idx, true, false, stack, P, Ng, V, packed_shadows, shift);
  }
  LIGHT_FOREACH_END

  LIGHT_FOREACH_BEGIN_LOCAL (light_cull_buf, light_zbin_buf, light_tile_buf, PIXEL, vPz, l_idx) {
    light_eval_single(l_idx, false, false, stack, P, Ng, V, packed_shadows, shift);
  }
  LIGHT_FOREACH_END
}

/////////////////////////////// Source file 45/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)

/**
 * Returns world position of a volume lightprobe sample (center of cell).
 * Returned position take into account the half voxel padding on each sides.
 * `grid_local_to_world_mat` is the unmodified object matrix.
 * `grid_res` is the un-padded grid resolution.
 * `cell_coord` is the coordinate of the sample in [0..grid_res) range.
 */
vec3 lightprobe_irradiance_grid_sample_position(mat4 grid_local_to_world_mat,
                                                ivec3 grid_res,
                                                ivec3 cell_coord)
{
  vec3 ls_cell_pos = (vec3(cell_coord + 1)) / vec3(grid_res + 1);
  ls_cell_pos = ls_cell_pos * 2.0 - 1.0;
  vec3 ws_cell_pos = (grid_local_to_world_mat * vec4(ls_cell_pos, 1.0)).xyz;
  return ws_cell_pos;
}

/**
 * Return true if sample position is valid.
 * \a r_lP is the local position in grid units [0..grid_size).
 */
bool lightprobe_irradiance_grid_local_coord(VolumeProbeData grid_data, vec3 P, out vec3 r_lP)
{
  /* Position in cell units. */
  /* NOTE: The vector-matrix multiplication swapped on purpose to cancel the matrix transpose. */
  vec3 lP = (vec4(P, 1.0) * grid_data.world_to_grid_transposed).xyz;
  r_lP = clamp(lP, vec3(0.5), vec3(grid_data.grid_size_padded) - 0.5);
  /* Sample is valid if position wasn't clamped. */
  return all(equal(lP, r_lP));
}

int lightprobe_irradiance_grid_brick_index_get(VolumeProbeData grid_data, ivec3 brick_coord)
{
  int3 grid_size_in_bricks = divide_ceil(grid_data.grid_size_padded,
                                         int3(IRRADIANCE_GRID_BRICK_SIZE - 1));
  int brick_index = grid_data.brick_offset;
  brick_index += brick_coord.x;
  brick_index += brick_coord.y * grid_size_in_bricks.x;
  brick_index += brick_coord.z * grid_size_in_bricks.x * grid_size_in_bricks.y;
  return brick_index;
}

/* Return cell corner from a corner ID [0..7]. */
ivec3 lightprobe_irradiance_grid_cell_corner(int cell_corner_id)
{
  return (ivec3(cell_corner_id) >> ivec3(0, 1, 2)) & 1;
}

float lightprobe_planar_score(PlanarProbeData planar, vec3 P, vec3 V, vec3 L)
{
  vec3 lP = vec4(P, 1.0) * planar.world_to_object_transposed;
  if (any(greaterThan(abs(lP), vec3(1.0)))) {
    /* TODO: Transition in Z. Dither? */
    return 0.0;
  }
  /* Return how much the ray is lined up with the captured ray. */
  vec3 R = -reflect(V, planar.normal);
  return saturate(dot(L, R));
}

#ifdef PLANAR_PROBES
/**
 * Return the best planar probe index for a given light direction vector and position.
 */
int lightprobe_planar_select(vec3 P, vec3 V, vec3 L)
{
  /* Initialize to the score of a camera ray. */
  float best_score = saturate(dot(L, -V));
  int best_index = -1;

  for (int index = 0; index < PLANAR_PROBE_MAX; index++) {
    if (probe_planar_buf[index].layer_id == -1) {
      /* PlanarProbeData doesn't contain any gap, exit at first item that is invalid. */
      break;
    }
    float score = lightprobe_planar_score(probe_planar_buf[index], P, V, L);
    if (score > best_score) {
      best_score = score;
      best_index = index;
    }
  }
  return best_index;
}
#endif

/////////////////////////////// Source file 46/////////////////////////////




/**
 * Ray generation routines for each BSDF types.
 */

#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_matrix_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_bxdf_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_ray_types_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_thickness_lib.glsl)

struct BsdfSample {
  vec3 direction;
  float pdf;
};

/* Could maybe become parameters. */
#define RAY_BIAS 0.05

bool is_singular_ray(float roughness)
{
  return roughness < BSDF_ROUGHNESS_THRESHOLD;
}

/* Returns view-space ray. */
BsdfSample ray_generate_direction(vec2 noise, ClosureUndetermined cl, vec3 V, float thickness)
{
  vec3 random_point_on_cylinder = sample_cylinder(noise);
  /* Bias the rays so we never get really high energy rays almost parallel to the surface. */
  random_point_on_cylinder.x = random_point_on_cylinder.x * (1.0 - RAY_BIAS) + RAY_BIAS;

  mat3 world_to_tangent = from_up_axis(cl.N);

  BsdfSample samp;
  switch (cl.type) {
    case CLOSURE_BSDF_TRANSLUCENT_ID:
      if (thickness > 0.0) {
        /* When modeling object thickness as a sphere, the outgoing rays are distributed uniformly
         * over the sphere. We don't need the RAY_BIAS in this case. */
        samp.direction = sample_sphere(noise);
        samp.pdf = sample_pdf_uniform_sphere();
      }
      else {
        samp.direction = sample_cosine_hemisphere(random_point_on_cylinder,
                                                  -world_to_tangent[2],
                                                  world_to_tangent[1],
                                                  world_to_tangent[0],
                                                  samp.pdf);
      }
      break;
    case CLOSURE_BSSRDF_BURLEY_ID:
    case CLOSURE_BSDF_DIFFUSE_ID:
      samp.direction = sample_cosine_hemisphere(random_point_on_cylinder,
                                                world_to_tangent[2],
                                                world_to_tangent[1],
                                                world_to_tangent[0],
                                                samp.pdf);
      break;
    case CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID: {
      if (is_singular_ray(to_closure_reflection(cl).roughness)) {
        samp.direction = reflect(-V, cl.N);
        samp.pdf = 1.0;
      }
      else {
        samp.direction = sample_ggx_reflect(random_point_on_cylinder,
                                            square(to_closure_reflection(cl).roughness),
                                            V,
                                            world_to_tangent[2],
                                            world_to_tangent[1],
                                            world_to_tangent[0],
                                            samp.pdf);
      }
      break;
    }
    case CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID: {
      float ior = to_closure_refraction(cl).ior;
      float roughness = to_closure_refraction(cl).roughness;
      if (thickness > 0.0) {
        float apparent_roughness = refraction_roughness_remapping(roughness, ior);
        vec3 L = refraction_dominant_dir(cl.N, V, ior, apparent_roughness);
        /* NOTE(fclem): Tracing origin is modified in the trace shader. */
        cl.N = -thickness_sphere_intersect(thickness, cl.N, L).hit_N;
        ior = 1.0 / ior;
        V = -L;
        world_to_tangent = from_up_axis(cl.N);
      }

      if (is_singular_ray(roughness)) {
        samp.direction = refract(-V, cl.N, 1.0 / ior);
        samp.pdf = 1.0;
      }
      else {
        samp.direction = sample_ggx_refract(random_point_on_cylinder,
                                            square(roughness),
                                            ior,
                                            V,
                                            world_to_tangent[2],
                                            world_to_tangent[1],
                                            world_to_tangent[0],
                                            samp.pdf);
      }
      break;
    }
    case CLOSURE_NONE_ID:
      /* TODO(fclem): Assert. */
      samp.pdf = 0.0;
      break;
  }

  return samp;
}

/////////////////////////////// Source file 47/////////////////////////////




/**
 * Convert from a cube-map vector to an octahedron UV coordinate.
 */
vec2 octahedral_uv_from_direction(vec3 co)
{
  /* Projection onto octahedron. */
  co /= dot(vec3(1.0), abs(co));

  /* Out-folding of the downward faces. */
  if (co.z < 0.0) {
    vec2 sign = step(0.0, co.xy) * 2.0 - 1.0;
    co.xy = (1.0 - abs(co.yx)) * sign;
  }

  /* Mapping to [0;1]^2 texture space. */
  vec2 uvs = co.xy * (0.5) + 0.5;

  return uvs;
}

vec3 octahedral_uv_to_direction(vec2 co)
{
  /* Change range to between [-1..1] */
  co = co * 2.0 - 1.0;

  vec2 abs_co = abs(co);
  vec3 v = vec3(co, 1.0 - (abs_co.x + abs_co.y));

  if (abs_co.x + abs_co.y > 1.0) {
    v.xy = (abs(co.yx) - 1.0) * -sign(co.xy);
  }

  return v;
}

/* Mirror the UV if they are not on the diagonal or unit UV squares.
 * Doesn't extend outside of [-1..2] range. But this is fine since we use it only for borders. */
vec2 octahedral_mirror_repeat_uv(vec2 uv)
{
  vec2 m = abs(uv - 0.5) + 0.5;
  vec2 f = floor(m);
  float x = f.x - f.y;
  if (x != 0.0) {
    uv.xy = 1.0 - uv.xy;
  }
  return fract(uv);
}

/////////////////////////////// Source file 48/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name Spherical Harmonics Functions
 *
 * `L` denote the row and `M` the column in the spherical harmonics table (1).
 * `p` denote positive column and `n` negative ones.
 *
 * Use precomputed constants to avoid constant folding differences across compilers.
 * Note that (2) doesn't use Condon-Shortley phase whereas our implementation does.
 *
 * Reference:
 * (1) https://en.wikipedia.org/wiki/Spherical_harmonics#/media/File:Sphericalfunctions.svg
 * (2) https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
 * (3) https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
 *
 * \{ */

/* L0 Band. */
float spherical_harmonics_L0_M0(vec3 v)
{
  return 0.282094792;
}

/* L1 Band. */
float spherical_harmonics_L1_Mn1(vec3 v)
{
  return -0.488602512 * v.y;
}
float spherical_harmonics_L1_M0(vec3 v)
{
  return 0.488602512 * v.z;
}
float spherical_harmonics_L1_Mp1(vec3 v)
{
  return -0.488602512 * v.x;
}

/* L2 Band. */
float spherical_harmonics_L2_Mn2(vec3 v)
{
  return 1.092548431 * (v.x * v.y);
}
float spherical_harmonics_L2_Mn1(vec3 v)
{
  return -1.092548431 * (v.y * v.z);
}
float spherical_harmonics_L2_M0(vec3 v)
{
  return 0.315391565 * (3.0 * v.z * v.z - 1.0);
}
float spherical_harmonics_L2_Mp1(vec3 v)
{
  return -1.092548431 * (v.x * v.z);
}
float spherical_harmonics_L2_Mp2(vec3 v)
{
  return 0.546274215 * (v.x * v.x - v.y * v.y);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Structure
 * \{ */

struct SphericalHarmonicBandL0 {
  vec4 M0;
};

struct SphericalHarmonicBandL1 {
  vec4 Mn1;
  vec4 M0;
  vec4 Mp1;
};

struct SphericalHarmonicBandL2 {
  vec4 Mn2;
  vec4 Mn1;
  vec4 M0;
  vec4 Mp1;
  vec4 Mp2;
};

struct SphericalHarmonicL0 {
  SphericalHarmonicBandL0 L0;
};

struct SphericalHarmonicL1 {
  SphericalHarmonicBandL0 L0;
  SphericalHarmonicBandL1 L1;
};

struct SphericalHarmonicL2 {
  SphericalHarmonicBandL0 L0;
  SphericalHarmonicBandL1 L1;
  SphericalHarmonicBandL2 L2;
};

SphericalHarmonicBandL0 spherical_harmonics_band_L0_new()
{
  SphericalHarmonicBandL0 L0;
  L0.M0 = vec4(0.0);
  return L0;
}

SphericalHarmonicBandL1 spherical_harmonics_band_L1_new()
{
  SphericalHarmonicBandL1 L1;
  L1.Mn1 = vec4(0.0);
  L1.M0 = vec4(0.0);
  L1.Mp1 = vec4(0.0);
  return L1;
}

SphericalHarmonicBandL2 spherical_harmonics_band_L2_new()
{
  SphericalHarmonicBandL2 L2;
  L2.Mn2 = vec4(0.0);
  L2.Mn1 = vec4(0.0);
  L2.M0 = vec4(0.0);
  L2.Mp1 = vec4(0.0);
  L2.Mp2 = vec4(0.0);
  return L2;
}

SphericalHarmonicL0 spherical_harmonics_L0_new()
{
  SphericalHarmonicL0 sh;
  sh.L0 = spherical_harmonics_band_L0_new();
  return sh;
}

SphericalHarmonicL1 spherical_harmonics_L1_new()
{
  SphericalHarmonicL1 sh;
  sh.L0 = spherical_harmonics_band_L0_new();
  sh.L1 = spherical_harmonics_band_L1_new();
  return sh;
}

SphericalHarmonicL2 spherical_harmonics_L2_new()
{
  SphericalHarmonicL2 sh;
  sh.L0 = spherical_harmonics_band_L0_new();
  sh.L1 = spherical_harmonics_band_L1_new();
  sh.L2 = spherical_harmonics_band_L2_new();
  return sh;
}

SphericalHarmonicBandL0 spherical_harmonics_band_L0_swizzle_wwww(SphericalHarmonicBandL0 L0)
{
  L0.M0 = L0.M0.wwww;
  return L0;
}

SphericalHarmonicBandL1 spherical_harmonics_band_L1_swizzle_wwww(SphericalHarmonicBandL1 L1)
{
  L1.Mn1 = L1.Mn1.wwww;
  L1.M0 = L1.M0.wwww;
  L1.Mp1 = L1.Mp1.wwww;
  return L1;
}

SphericalHarmonicBandL2 spherical_harmonics_band_L2_swizzle_wwww(SphericalHarmonicBandL2 L2)
{
  L2.Mn2 = L2.Mn2.wwww;
  L2.Mn1 = L2.Mn1.wwww;
  L2.M0 = L2.M0.wwww;
  L2.Mp1 = L2.Mp1.wwww;
  L2.Mp2 = L2.Mp2.wwww;
  return L2;
}

SphericalHarmonicL0 spherical_harmonics_swizzle_wwww(SphericalHarmonicL0 sh)
{
  sh.L0 = spherical_harmonics_band_L0_swizzle_wwww(sh.L0);
  return sh;
}

SphericalHarmonicL1 spherical_harmonics_swizzle_wwww(SphericalHarmonicL1 sh)
{
  sh.L0 = spherical_harmonics_band_L0_swizzle_wwww(sh.L0);
  sh.L1 = spherical_harmonics_band_L1_swizzle_wwww(sh.L1);
  return sh;
}

SphericalHarmonicL2 spherical_harmonics_swizzle_wwww(SphericalHarmonicL2 sh)
{
  sh.L0 = spherical_harmonics_band_L0_swizzle_wwww(sh.L0);
  sh.L1 = spherical_harmonics_band_L1_swizzle_wwww(sh.L1);
  sh.L2 = spherical_harmonics_band_L2_swizzle_wwww(sh.L2);
  return sh;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Encode
 *
 * Decompose an input signal into spherical harmonic coefficients.
 * Note that `amplitude` need to be scaled by solid angle.
 * \{ */

void spherical_harmonics_L0_encode_signal_sample(vec3 direction,
                                                 vec4 amplitude,
                                                 inout SphericalHarmonicBandL0 r_L0)
{
  r_L0.M0 += spherical_harmonics_L0_M0(direction) * amplitude;
}

void spherical_harmonics_L1_encode_signal_sample(vec3 direction,
                                                 vec4 amplitude,
                                                 inout SphericalHarmonicBandL1 r_L1)
{
  r_L1.Mn1 += spherical_harmonics_L1_Mn1(direction) * amplitude;
  r_L1.M0 += spherical_harmonics_L1_M0(direction) * amplitude;
  r_L1.Mp1 += spherical_harmonics_L1_Mp1(direction) * amplitude;
}

void spherical_harmonics_L2_encode_signal_sample(vec3 direction,
                                                 vec4 amplitude,
                                                 inout SphericalHarmonicBandL2 r_L2)
{
  r_L2.Mn2 += spherical_harmonics_L2_Mn2(direction) * amplitude;
  r_L2.Mn1 += spherical_harmonics_L2_Mn1(direction) * amplitude;
  r_L2.M0 += spherical_harmonics_L2_M0(direction) * amplitude;
  r_L2.Mp1 += spherical_harmonics_L2_Mp1(direction) * amplitude;
  r_L2.Mp2 += spherical_harmonics_L2_Mp2(direction) * amplitude;
}

void spherical_harmonics_encode_signal_sample(vec3 direction,
                                              vec4 amplitude,
                                              inout SphericalHarmonicL0 sh)
{
  spherical_harmonics_L0_encode_signal_sample(direction, amplitude, sh.L0);
}

void spherical_harmonics_encode_signal_sample(vec3 direction,
                                              vec4 amplitude,
                                              inout SphericalHarmonicL1 sh)
{
  spherical_harmonics_L0_encode_signal_sample(direction, amplitude, sh.L0);
  spherical_harmonics_L1_encode_signal_sample(direction, amplitude, sh.L1);
}

void spherical_harmonics_encode_signal_sample(vec3 direction,
                                              vec4 amplitude,
                                              inout SphericalHarmonicL2 sh)
{
  spherical_harmonics_L0_encode_signal_sample(direction, amplitude, sh.L0);
  spherical_harmonics_L1_encode_signal_sample(direction, amplitude, sh.L1);
  spherical_harmonics_L2_encode_signal_sample(direction, amplitude, sh.L2);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Decode
 *
 * Evaluate an encoded signal in a given unit vector direction.
 * \{ */

vec4 spherical_harmonics_L0_evaluate(vec3 direction, SphericalHarmonicBandL0 L0)
{
  return spherical_harmonics_L0_M0(direction) * L0.M0;
}

vec4 spherical_harmonics_L1_evaluate(vec3 direction, SphericalHarmonicBandL1 L1)
{
  return spherical_harmonics_L1_Mn1(direction) * L1.Mn1 +
         spherical_harmonics_L1_M0(direction) * L1.M0 +
         spherical_harmonics_L1_Mp1(direction) * L1.Mp1;
}

vec4 spherical_harmonics_L2_evaluate(vec3 direction, SphericalHarmonicBandL2 L2)
{
  return spherical_harmonics_L2_Mn2(direction) * L2.Mn2 +
         spherical_harmonics_L2_Mn1(direction) * L2.Mn1 +
         spherical_harmonics_L2_M0(direction) * L2.M0 +
         spherical_harmonics_L2_Mp1(direction) * L2.Mp1 +
         spherical_harmonics_L2_Mp2(direction) * L2.Mp2;
}

vec3 spherical_harmonics_evaluate(vec3 direction, SphericalHarmonicL0 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(direction, sh.L0).rgb;
  return max(vec3(0.0), radiance);
}

vec3 spherical_harmonics_evaluate(vec3 direction, SphericalHarmonicL1 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(direction, sh.L0).rgb +
                  spherical_harmonics_L1_evaluate(direction, sh.L1).rgb;
  return max(vec3(0.0), radiance);
}

vec3 spherical_harmonics_evaluate(vec3 direction, SphericalHarmonicL2 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(direction, sh.L0).rgb +
                  spherical_harmonics_L1_evaluate(direction, sh.L1).rgb +
                  spherical_harmonics_L2_evaluate(direction, sh.L2).rgb;
  return max(vec3(0.0), radiance);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Rotation
 * \{ */

SphericalHarmonicBandL0 spherical_harmonics_L0_rotate(mat3x3 rotation, SphericalHarmonicBandL0 L0)
{
  /* L0 band being a constant function (i.e: there is no directionality) there is nothing to
   * rotate. This is a no-op. */
  return L0;
}

SphericalHarmonicBandL1 spherical_harmonics_L1_rotate(mat3x3 rotation, SphericalHarmonicBandL1 L1)
{
  /* Convert L1 coefficients to per channel column.
   * Note the component shuffle to match blender coordinate system. */
  mat4x3 per_channel = transpose(mat3x4(L1.Mp1, L1.Mn1, -L1.M0));
  /* Rotate each channel. */
  per_channel[0] = rotation * per_channel[0];
  per_channel[1] = rotation * per_channel[1];
  per_channel[2] = rotation * per_channel[2];
  per_channel[3] = rotation * per_channel[3];
  /* Convert back from L1 coefficients to per channel column.
   * Note the component shuffle to match blender coordinate system. */
  mat3x4 per_coef = transpose(per_channel);
  L1.Mn1 = per_coef[1];
  L1.M0 = -per_coef[2];
  L1.Mp1 = per_coef[0];
  return L1;
}

SphericalHarmonicL1 spherical_harmonics_rotate(mat3x3 rotation, SphericalHarmonicL1 sh)
{
  sh.L0 = spherical_harmonics_L0_rotate(rotation, sh.L0);
  sh.L1 = spherical_harmonics_L1_rotate(rotation, sh.L1);
  return sh;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Evaluation
 * \{ */

/**
 * Convolve a spherical harmonic encoded irradiance signal as a lambertian reflection.
 * Returns the lambertian radiance (cosine lobe divided by PI) so the coefficients simplify to 1,
 * 2/3 and 1/4. See this reference for more explanation:
 * https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
 */
vec3 spherical_harmonics_evaluate_lambert(vec3 N, SphericalHarmonicL0 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(N, sh.L0).rgb;
  return max(vec3(0.0), radiance);
}
vec3 spherical_harmonics_evaluate_lambert(vec3 N, SphericalHarmonicL1 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(N, sh.L0).rgb +
                  spherical_harmonics_L1_evaluate(N, sh.L1).rgb * (2.0 / 3.0);
  return max(vec3(0.0), radiance);
}
vec3 spherical_harmonics_evaluate_lambert(vec3 N, SphericalHarmonicL2 sh)
{
  vec3 radiance = spherical_harmonics_L0_evaluate(N, sh.L0).rgb +
                  spherical_harmonics_L1_evaluate(N, sh.L1).rgb * (2.0 / 3.0) +
                  spherical_harmonics_L2_evaluate(N, sh.L2).rgb * (1.0 / 4.0);
  return max(vec3(0.0), radiance);
}

/**
 * Use non-linear reconstruction method to avoid negative lobe artifacts.
 * See this reference for more explanation:
 * https://grahamhazel.com/blog/2017/12/22/converting-sh-radiance-to-irradiance/
 */
float spherical_harmonics_evaluate_non_linear(vec3 N, float R0, vec3 R1)
{
  /* No idea why this is needed. */
  R1 /= 2.0;

  float R1_len;
  vec3 R1_dir = normalize_and_get_length(R1, R1_len);
  float rcp_R0 = safe_rcp(R0);

  float q = (1.0 + dot(R1_dir, N)) / 2.0;
  float p = 1.0 + 2.0 * R1_len * rcp_R0;
  float a = (1.0 - R1_len * rcp_R0) * safe_rcp(1.0 + R1_len * rcp_R0);

  return R0 * (a + (1.0 - a) * (p + 1.0) * pow(q, p));
}
vec3 spherical_harmonics_evaluate_lambert_non_linear(vec3 N, SphericalHarmonicL1 sh)
{
  /* Shuffling based on spherical_harmonics_L1_* functions. */
  vec3 R1_r = vec3(-sh.L1.Mp1.r, -sh.L1.Mn1.r, sh.L1.M0.r);
  vec3 R1_g = vec3(-sh.L1.Mp1.g, -sh.L1.Mn1.g, sh.L1.M0.g);
  vec3 R1_b = vec3(-sh.L1.Mp1.b, -sh.L1.Mn1.b, sh.L1.M0.b);

  vec3 radiance = vec3(spherical_harmonics_evaluate_non_linear(N, sh.L0.M0.r, R1_r),
                       spherical_harmonics_evaluate_non_linear(N, sh.L0.M0.g, R1_g),
                       spherical_harmonics_evaluate_non_linear(N, sh.L0.M0.b, R1_b));
  /* Return lambertian radiance. So divide by PI. */
  return radiance / M_PI;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Load/Store
 *
 * This section define the compression scheme of spherical harmonic data.
 * \{ */

SphericalHarmonicL1 spherical_harmonics_unpack(vec4 L0_L1_a,
                                               vec4 L0_L1_b,
                                               vec4 L0_L1_c,
                                               vec4 L0_L1_vis)
{
  SphericalHarmonicL1 sh;
  sh.L0.M0.xyz = L0_L1_a.xyz;
  sh.L1.Mn1.xyz = L0_L1_b.xyz;
  sh.L1.M0.xyz = L0_L1_c.xyz;
  sh.L1.Mp1.xyz = vec3(L0_L1_a.w, L0_L1_b.w, L0_L1_c.w);
  sh.L0.M0.w = L0_L1_vis.x;
  sh.L1.Mn1.w = L0_L1_vis.y;
  sh.L1.M0.w = L0_L1_vis.z;
  sh.L1.Mp1.w = L0_L1_vis.w;
  return sh;
}

void spherical_harmonics_pack(SphericalHarmonicL1 sh,
                              out vec4 L0_L1_a,
                              out vec4 L0_L1_b,
                              out vec4 L0_L1_c,
                              out vec4 L0_L1_vis)
{
  L0_L1_a.xyz = sh.L0.M0.xyz;
  L0_L1_b.xyz = sh.L1.Mn1.xyz;
  L0_L1_c.xyz = sh.L1.M0.xyz;
  L0_L1_a.w = sh.L1.Mp1.x;
  L0_L1_b.w = sh.L1.Mp1.y;
  L0_L1_c.w = sh.L1.Mp1.z;
  L0_L1_vis = vec4(sh.L0.M0.w, sh.L1.Mn1.w, sh.L1.M0.w, sh.L1.Mp1.w);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Triple Product
 * \{ */

SphericalHarmonicL1 spherical_harmonics_triple_product(SphericalHarmonicL1 a,
                                                       SphericalHarmonicL1 b)
{
  /* Adapted from:
   * "Code Generation and Factoring for Fast Evaluation of Low-order Spherical Harmonic Products
   * and Squares" Function "SH_product_3". */
  const float L0_M0_coef = 0.282094792;
  SphericalHarmonicL1 sh;
  sh.L0.M0 = a.L0.M0 * b.L0.M0;
  sh.L0.M0 += a.L1.Mn1 * b.L1.Mn1;
  sh.L0.M0 += a.L1.M0 * b.L1.M0;
  sh.L0.M0 += a.L1.Mp1 * b.L1.Mp1;
  sh.L0.M0 *= L0_M0_coef;

  sh.L1.Mn1 = L0_M0_coef * (a.L0.M0 * b.L1.Mn1 + b.L0.M0 * a.L1.Mn1);
  sh.L1.M0 = L0_M0_coef * (a.L0.M0 * b.L1.M0 + b.L0.M0 * a.L1.M0);
  sh.L1.Mp1 = L0_M0_coef * (a.L0.M0 * b.L1.Mp1 + b.L0.M0 * a.L1.Mp1);
  return sh;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Multiply Add
 * \{ */

SphericalHarmonicBandL0 spherical_harmonics_L0_madd(SphericalHarmonicBandL0 a,
                                                    float b,
                                                    SphericalHarmonicBandL0 c)
{
  SphericalHarmonicBandL0 result;
  result.M0 = a.M0 * b + c.M0;
  return result;
}

SphericalHarmonicBandL1 spherical_harmonics_L1_madd(SphericalHarmonicBandL1 a,
                                                    float b,
                                                    SphericalHarmonicBandL1 c)
{
  SphericalHarmonicBandL1 result;
  result.Mn1 = a.Mn1 * b + c.Mn1;
  result.M0 = a.M0 * b + c.M0;
  result.Mp1 = a.Mp1 * b + c.Mp1;
  return result;
}

SphericalHarmonicBandL2 spherical_harmonics_L2_madd(SphericalHarmonicBandL2 a,
                                                    float b,
                                                    SphericalHarmonicBandL2 c)
{
  SphericalHarmonicBandL2 result;
  result.Mn2 = a.Mn2 * b + c.Mn2;
  result.Mn1 = a.Mn1 * b + c.Mn1;
  result.M0 = a.M0 * b + c.M0;
  result.Mp1 = a.Mp1 * b + c.Mp1;
  result.Mp2 = a.Mp2 * b + c.Mp2;
  return result;
}

SphericalHarmonicL0 spherical_harmonics_madd(SphericalHarmonicL0 a, float b, SphericalHarmonicL0 c)
{
  SphericalHarmonicL0 result;
  result.L0 = spherical_harmonics_L0_madd(a.L0, b, c.L0);
  return result;
}

SphericalHarmonicL1 spherical_harmonics_madd(SphericalHarmonicL1 a, float b, SphericalHarmonicL1 c)
{
  SphericalHarmonicL1 result;
  result.L0 = spherical_harmonics_L0_madd(a.L0, b, c.L0);
  result.L1 = spherical_harmonics_L1_madd(a.L1, b, c.L1);
  return result;
}

SphericalHarmonicL2 spherical_harmonics_madd(SphericalHarmonicL2 a, float b, SphericalHarmonicL2 c)
{
  SphericalHarmonicL2 result;
  result.L0 = spherical_harmonics_L0_madd(a.L0, b, c.L0);
  result.L1 = spherical_harmonics_L1_madd(a.L1, b, c.L1);
  result.L2 = spherical_harmonics_L2_madd(a.L2, b, c.L2);
  return result;
}
/** \} */

/* -------------------------------------------------------------------- */
/** \name Multiply
 * \{ */

SphericalHarmonicBandL0 spherical_harmonics_L0_mul(SphericalHarmonicBandL0 a, float b)
{
  SphericalHarmonicBandL0 result;
  result.M0 = a.M0 * b;
  return result;
}

SphericalHarmonicBandL1 spherical_harmonics_L1_mul(SphericalHarmonicBandL1 a, float b)
{
  SphericalHarmonicBandL1 result;
  result.Mn1 = a.Mn1 * b;
  result.M0 = a.M0 * b;
  result.Mp1 = a.Mp1 * b;
  return result;
}

SphericalHarmonicBandL2 spherical_harmonics_L2_mul(SphericalHarmonicBandL2 a, float b)
{
  SphericalHarmonicBandL2 result;
  result.Mn2 = a.Mn2 * b;
  result.Mn1 = a.Mn1 * b;
  result.M0 = a.M0 * b;
  result.Mp1 = a.Mp1 * b;
  result.Mp2 = a.Mp2 * b;
  return result;
}

SphericalHarmonicL0 spherical_harmonics_mul(SphericalHarmonicL0 a, float b)
{
  SphericalHarmonicL0 result;
  result.L0 = spherical_harmonics_L0_mul(a.L0, b);
  return result;
}

SphericalHarmonicL1 spherical_harmonics_mul(SphericalHarmonicL1 a, float b)
{
  SphericalHarmonicL1 result;
  result.L0 = spherical_harmonics_L0_mul(a.L0, b);
  result.L1 = spherical_harmonics_L1_mul(a.L1, b);
  return result;
}

SphericalHarmonicL2 spherical_harmonics_mul(SphericalHarmonicL2 a, float b)
{
  SphericalHarmonicL2 result;
  result.L0 = spherical_harmonics_L0_mul(a.L0, b);
  result.L1 = spherical_harmonics_L1_mul(a.L1, b);
  result.L2 = spherical_harmonics_L2_mul(a.L2, b);
  return result;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Add
 * \{ */

SphericalHarmonicBandL0 spherical_harmonics_L0_add(SphericalHarmonicBandL0 a,
                                                   SphericalHarmonicBandL0 b)
{
  SphericalHarmonicBandL0 result;
  result.M0 = a.M0 + b.M0;
  return result;
}

SphericalHarmonicBandL1 spherical_harmonics_L1_add(SphericalHarmonicBandL1 a,
                                                   SphericalHarmonicBandL1 b)
{
  SphericalHarmonicBandL1 result;
  result.Mn1 = a.Mn1 + b.Mn1;
  result.M0 = a.M0 + b.M0;
  result.Mp1 = a.Mp1 + b.Mp1;
  return result;
}

SphericalHarmonicBandL2 spherical_harmonics_L2_add(SphericalHarmonicBandL2 a,
                                                   SphericalHarmonicBandL2 b)
{
  SphericalHarmonicBandL2 result;
  result.Mn2 = a.Mn2 + b.Mn2;
  result.Mn1 = a.Mn1 + b.Mn1;
  result.M0 = a.M0 + b.M0;
  result.Mp1 = a.Mp1 + b.Mp1;
  result.Mp2 = a.Mp2 + b.Mp2;
  return result;
}

SphericalHarmonicL0 spherical_harmonics_add(SphericalHarmonicL0 a, SphericalHarmonicL0 b)
{
  SphericalHarmonicL0 result;
  result.L0 = spherical_harmonics_L0_add(a.L0, b.L0);
  return result;
}

SphericalHarmonicL1 spherical_harmonics_add(SphericalHarmonicL1 a, SphericalHarmonicL1 b)
{
  SphericalHarmonicL1 result;
  result.L0 = spherical_harmonics_L0_add(a.L0, b.L0);
  result.L1 = spherical_harmonics_L1_add(a.L1, b.L1);
  return result;
}

SphericalHarmonicL2 spherical_harmonics_add(SphericalHarmonicL2 a, SphericalHarmonicL2 b)
{
  SphericalHarmonicL2 result;
  result.L0 = spherical_harmonics_L0_add(a.L0, b.L0);
  result.L1 = spherical_harmonics_L1_add(a.L1, b.L1);
  result.L2 = spherical_harmonics_L2_add(a.L2, b.L2);
  return result;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Dot
 * \{ */

vec4 spherical_harmonics_dot(SphericalHarmonicL1 a, SphericalHarmonicL1 b)
{
  /* Convert coefficients to per channel column. */
  mat4x4 a_mat = transpose(mat4x4(a.L0.M0, a.L1.Mn1, a.L1.M0, a.L1.Mp1));
  mat4x4 b_mat = transpose(mat4x4(b.L0.M0, b.L1.Mn1, b.L1.M0, b.L1.Mp1));
  vec4 result;
  result[0] = dot(a_mat[0], b_mat[0]);
  result[1] = dot(a_mat[1], b_mat[1]);
  result[2] = dot(a_mat[2], b_mat[2]);
  result[3] = dot(a_mat[3], b_mat[3]);
  return result;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Compression
 *
 * Described by Josh Hobson in "The indirect Lighting Pipeline of God of War" p. 120
 * \{ */

SphericalHarmonicL1 spherical_harmonics_compress(SphericalHarmonicL1 sh)
{
  SphericalHarmonicL1 result;
  result.L0 = sh.L0;
  vec4 fac = safe_rcp(sh.L0.M0 * M_SQRT3);
  result.L1.Mn1 = (sh.L1.Mn1 * fac) * 0.5 + 0.5;
  result.L1.M0 = (sh.L1.M0 * fac) * 0.5 + 0.5;
  result.L1.Mp1 = (sh.L1.Mp1 * fac) * 0.5 + 0.5;
  return result;
}

SphericalHarmonicL1 spherical_harmonics_decompress(SphericalHarmonicL1 sh)
{
  SphericalHarmonicL1 result;
  result.L0 = sh.L0;
  vec4 fac = sh.L0.M0 * M_SQRT3;
  result.L1.Mn1 = (sh.L1.Mn1 * 2.0 - 1.0) * fac;
  result.L1.M0 = (sh.L1.M0 * 2.0 - 1.0) * fac;
  result.L1.Mp1 = (sh.L1.Mp1 * 2.0 - 1.0) * fac;
  return result;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Clamping
 *
 * Clamp the total power of the SH function.
 * \{ */

SphericalHarmonicL1 spherical_harmonics_clamp(SphericalHarmonicL1 sh, float clamp_value)
{
  /* Convert coefficients to per channel column. */
  mat4x4 per_channel = transpose(mat4x4(sh.L0.M0, sh.L1.Mn1, sh.L1.M0, sh.L1.Mp1));
  /* Maximum per channel. */
  vec3 max_L1 = vec3(reduce_max(abs(per_channel[0].yzw)),
                     reduce_max(abs(per_channel[1].yzw)),
                     reduce_max(abs(per_channel[2].yzw)));
  /* Find maximum of the sh function over all chanels. */
  vec3 max_sh = abs(sh.L0.M0.rgb) * 0.282094792 + max_L1 * 0.488602512;

  float fac = clamp_value * safe_rcp(reduce_max(max_sh));
  if (fac > 1.0) {
    return sh;
  }
  return spherical_harmonics_mul(sh, fac);
}

/** \} */

/////////////////////////////// Source file 49/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_octahedron_lib.glsl)

SphereProbePixelArea reinterpret_as_write_coord(ivec4 packed_coord)
{
  SphereProbePixelArea unpacked;
  unpacked.offset = packed_coord.xy;
  unpacked.extent = packed_coord.z;
  unpacked.layer = packed_coord.w;
  return unpacked;
}

SphereProbeUvArea reinterpret_as_atlas_coord(ivec4 packed_coord)
{
  SphereProbeUvArea unpacked;
  unpacked.offset = intBitsToFloat(packed_coord.xy);
  unpacked.scale = intBitsToFloat(packed_coord.z);
  unpacked.layer = intBitsToFloat(packed_coord.w);
  return unpacked;
}

/* local_texel is the texel coordinate inside the probe area [0..texel_area.extent) range.
 * Returned vector is not normalized. */
vec3 sphere_probe_texel_to_direction(ivec2 local_texel,
                                     SphereProbePixelArea texel_area,
                                     SphereProbeUvArea uv_area,
                                     out vec2 sampling_uv)
{
  /* Texel in probe atlas. */
  ivec2 texel = local_texel + texel_area.offset;
  /* UV in sampling area. No half pixel bias to texel as the octahedral map edges area lined up
   * with texel center. Note that we don't use the last row & column of pixel, hence the -2 instead
   * of -1. See sphere_probe_miplvl_scale_bias. */
  sampling_uv = vec2(texel) / vec2(texel_area.extent - 2);
  /* Direction in world space. */
  return octahedral_uv_to_direction(sampling_uv);
}

/* local_texel is the texel coordinate inside the probe area [0..texel_area.extent) range.
 * Returned vector is not normalized. */
vec3 sphere_probe_texel_to_direction(ivec2 local_texel,
                                     SphereProbePixelArea texel_area,
                                     SphereProbeUvArea uv_area)
{
  vec2 sampling_uv_unused;
  return sphere_probe_texel_to_direction(local_texel, texel_area, uv_area, sampling_uv_unused);
}

/* Apply correct bias and scale for the given level of detail. */
vec2 sphere_probe_miplvl_scale_bias(float mip_lvl, SphereProbeUvArea uv_area, vec2 uv)
{
  /* Add 0.5 to avoid rounding error. */
  int mip_0_res = int(float(SPHERE_PROBE_ATLAS_RES) * uv_area.scale + 0.5);
  float mip_lvl_res = float(mip_0_res >> int(mip_lvl));
  float mip_lvl_res_inv = 1.0 / mip_lvl_res;
  /* We place texel centers at the edges of the octahedron, to avoid artifacts caused by
   * interpolating across the edges.
   * The first pixel scaling aligns all the border edges (half pixel border).
   * The second pixel scaling aligns the center edges (odd number of pixel). */
  float scale = (mip_lvl_res - 2.0) * mip_lvl_res_inv;
  float offset = 0.5 * mip_lvl_res_inv;
  return uv * scale + offset;
}

void sphere_probe_direction_to_uv(vec3 L,
                                  float lod_min,
                                  float lod_max,
                                  SphereProbeUvArea uv_area,
                                  out vec2 altas_uv_min,
                                  out vec2 altas_uv_max)
{
  vec2 octahedral_uv = octahedral_uv_from_direction(L);
  /* We use a custom per mip level scaling and bias. This avoid some projection artifact and
   * padding border waste. But we need to do the mipmap interpolation ourself. */
  vec2 local_uv_min = sphere_probe_miplvl_scale_bias(lod_min, uv_area, octahedral_uv);
  vec2 local_uv_max = sphere_probe_miplvl_scale_bias(lod_max, uv_area, octahedral_uv);
  /* Remap into atlas location. */
  altas_uv_min = local_uv_min * uv_area.scale + uv_area.offset;
  altas_uv_max = local_uv_max * uv_area.scale + uv_area.offset;
}

/* Single mip variant. */
vec2 sphere_probe_direction_to_uv(vec3 L, float lod, SphereProbeUvArea uv_area)
{
  vec2 altas_uv_min, altas_uv_max_unused;
  sphere_probe_direction_to_uv(L, lod, 0.0, uv_area, altas_uv_min, altas_uv_max_unused);
  return altas_uv_min;
}

float sphere_probe_roughness_to_mix_fac(float roughness)
{
  const float scale = 1.0 / (SPHERE_PROBE_MIX_END_ROUGHNESS - SPHERE_PROBE_MIX_START_ROUGHNESS);
  const float bias = scale * SPHERE_PROBE_MIX_START_ROUGHNESS;
  return square(saturate(roughness * scale - bias));
}

/* Input roughness is linear roughness (UI roughness). */
float sphere_probe_roughness_to_lod(float roughness)
{
  /* From "Moving Frostbite to Physically Based Rendering 3.0" eq 53. */
  float ratio = saturate(roughness / SPHERE_PROBE_MIP_MAX_ROUGHNESS);
  float ratio_sqrt = sqrt_fast(ratio);
  /* Mix with linear to avoid mip 1 being too sharp. */
  float mip_ratio = mix(ratio, ratio_sqrt, 0.4);
  return mip_ratio * float(SPHERE_PROBE_MIPMAP_LEVELS - 1);
}

/* Return linear roughness (UI roughness). */
float sphere_probe_lod_to_roughness(float lod)
{
  /* Inverse of sphere_probe_roughness_to_lod. */
  float mip_ratio = lod / float(SPHERE_PROBE_MIPMAP_LEVELS - 1);
  float a = mip_ratio;
  const float b = 0.6; /* Factor of ratio. */
  const float c = 0.4; /* Factor of ratio_sqrt. */
  float b2 = square(b);
  float c2 = square(c);
  float c4 = square(c2);
  /* In wolfram alpha we trust. */
  float ratio = (-sqrt(4.0 * a * b * c2 + c4) + 2.0 * a * b + c2) / (2.0 * b2);
  return ratio * SPHERE_PROBE_MIP_MAX_ROUGHNESS;
}

/////////////////////////////// Source file 50/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_octahedron_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_spherical_harmonics_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_reflection_probe_mapping_lib.glsl)

#ifdef SPHERE_PROBE
vec4 reflection_probes_sample(vec3 L, float lod, SphereProbeUvArea uv_area)
{
  float lod_min = floor(lod);
  float lod_max = ceil(lod);
  float mix_fac = lod - lod_min;

  vec2 altas_uv_min, altas_uv_max;
  sphere_probe_direction_to_uv(L, lod_min, lod_max, uv_area, altas_uv_min, altas_uv_max);

  vec4 color_min = textureLod(reflection_probes_tx, vec3(altas_uv_min, uv_area.layer), lod_min);
  vec4 color_max = textureLod(reflection_probes_tx, vec3(altas_uv_max, uv_area.layer), lod_max);
  return mix(color_min, color_max, mix_fac);
}
#endif

ReflectionProbeLowFreqLight reflection_probes_extract_low_freq(SphericalHarmonicL1 sh)
{
  /* To avoid color shift and negative values, we reduce saturation and directionality. */
  ReflectionProbeLowFreqLight result;
  result.ambient = sh.L0.M0.r + sh.L0.M0.g + sh.L0.M0.b;

  mat3x4 L1_per_band;
  L1_per_band[0] = sh.L1.Mn1;
  L1_per_band[1] = sh.L1.M0;
  L1_per_band[2] = sh.L1.Mp1;

  mat4x3 L1_per_comp = transpose(L1_per_band);
  result.direction = L1_per_comp[0] + L1_per_comp[1] + L1_per_comp[2];

  return result;
}

float reflection_probes_normalization_eval(vec3 L,
                                           ReflectionProbeLowFreqLight numerator,
                                           ReflectionProbeLowFreqLight denominator)
{
  /* TODO(fclem): Adjusting directionality is tricky.
   * Needs to be revisited later on. For now only use the ambient term. */
  return (numerator.ambient * safe_rcp(denominator.ambient));
}

/////////////////////////////// Source file 51/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_bxdf_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_reflection_probe_lib.glsl)

#ifdef SPHERE_PROBE
int reflection_probes_select(vec3 P, float random_probe)
{
  for (int index = 0; index < SPHERE_PROBE_MAX; index++) {
    SphereProbeData probe_data = reflection_probe_buf[index];
    /* SphereProbeData doesn't contain any gap, exit at first item that is invalid. */
    if (probe_data.atlas_coord.layer == -1) {
      /* We hit the end of the array. Return last valid index. */
      return index - 1;
    }
    /* NOTE: The vector-matrix multiplication swapped on purpose to cancel the matrix transpose. */
    vec3 lP = vec4(P, 1.0) * probe_data.world_to_probe_transposed;
    float gradient = (probe_data.influence_shape == SHAPE_ELIPSOID) ?
                         length(lP) :
                         max(max(abs(lP.x), abs(lP.y)), abs(lP.z));
    float score = saturate(probe_data.influence_bias - gradient * probe_data.influence_scale);
    if (score > random_probe) {
      return index;
    }
  }
  /* This should never happen (world probe is always last). */
  return SPHERE_PROBE_MAX - 1;
}
#endif /* SPHERE_PROBE */

/////////////////////////////// Source file 52/////////////////////////////




/**
 * The resources expected to be defined are:
 * - grids_infos_buf
 * - bricks_infos_buf
 * - irradiance_atlas_tx
 * Needed for sampling (not for upload):
 * - util_tx
 * - sampling_buf
 */

#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_lightprobe_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_spherical_harmonics_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)

/**
 * Return the brick coordinate inside the grid.
 */
ivec3 lightprobe_irradiance_grid_brick_coord(vec3 lP)
{
  ivec3 brick_coord = ivec3((lP - 0.5) / float(IRRADIANCE_GRID_BRICK_SIZE - 1));
  /* Avoid sampling adjacent bricks. */
  return max(brick_coord, ivec3(0));
}

/**
 * Return the local coordinated of the shading point inside the brick in unnormalized coordinate.
 */
vec3 lightprobe_irradiance_grid_brick_local_coord(VolumeProbeData grid_data,
                                                  vec3 lP,
                                                  ivec3 brick_coord)
{
  /* Avoid sampling adjacent bricks around the origin. */
  lP = max(lP, vec3(0.5));
  /* Local position inside the brick (still in grid sample spacing unit). */
  vec3 brick_lP = lP - vec3(brick_coord) * float(IRRADIANCE_GRID_BRICK_SIZE - 1);
  return brick_lP;
}

/**
 * Return the biased local brick local coordinated.
 */
vec3 lightprobe_irradiance_grid_bias_sample_coord(VolumeProbeData grid_data,
                                                  uvec2 brick_atlas_coord,
                                                  vec3 brick_lP,
                                                  vec3 lNg)
{
  /* A cell is the interpolation region between 8 texels. */
  vec3 cell_lP = brick_lP - 0.5;
  vec3 cell_start = floor(cell_lP);
  vec3 cell_fract = cell_lP - cell_start;

  /* NOTE(fclem): Use uint to avoid signed int modulo. */
  uint vis_comp = uint(cell_start.z) % 4u;
  /* Visibility is stored after the irradiance. */
  ivec3 vis_coord = ivec3(ivec2(brick_atlas_coord), IRRADIANCE_GRID_BRICK_SIZE * 4) +
                    ivec3(cell_start);
  /* Visibility is stored packed 1 cell per channel. */
  vis_coord.z -= int(vis_comp);
  float cell_visibility = texelFetch(irradiance_atlas_tx, vis_coord, 0)[vis_comp];
  int cell_visibility_bits = int(cell_visibility);
  /**
   * References:
   *
   * "Probe-based lighting, strand-based hair system, and physical hair shading in Unity’s Enemies"
   * by Francesco Cifariello Ciardi, Lasse Jon Fuglsang Pedersen and John Parsaie.
   *
   * "Multi-Scale Global Illumination in Quantum Break"
   * by Ari Silvennoinen and Ville Timonen.
   *
   * “Dynamic Diffuse Global Illumination with Ray-Traced Irradiance Fields”
   * by Morgan McGuire.
   */
  float trilinear_weights[8];
  float total_weight = 0.0;
  for (int i = 0; i < 8; i++) {
    ivec3 sample_position = lightprobe_irradiance_grid_cell_corner(i);

    vec3 trilinear = select(1.0 - cell_fract, cell_fract, bvec3(sample_position));
    float positional_weight = trilinear.x * trilinear.y * trilinear.z;

    float len;
    vec3 corner_vec = vec3(sample_position) - cell_fract;
    vec3 corner_dir = normalize_and_get_length(corner_vec, len);
    float cos_theta = (len > 1e-8) ? dot(lNg, corner_dir) : 1.0;
    float geometry_weight = saturate(cos_theta * 0.5 + 0.5);

    float validity_weight = float((cell_visibility_bits >> i) & 1);

    /* Biases. See McGuire's presentation. */
    positional_weight += 0.001;
    geometry_weight = square(geometry_weight) + 0.2 + grid_data.facing_bias;

    trilinear_weights[i] = saturate(positional_weight * geometry_weight * validity_weight);
    total_weight += trilinear_weights[i];
  }
  float total_weight_inv = safe_rcp(total_weight);

  vec3 trilinear_coord = vec3(0.0);
  for (int i = 0; i < 8; i++) {
    vec3 sample_position = vec3((ivec3(i) >> ivec3(0, 1, 2)) & 1);
    trilinear_coord += sample_position * trilinear_weights[i] * total_weight_inv;
  }
  /* Replace sampling coordinates with manually weighted trilinear coordinates. */
  return 0.5 + cell_start + trilinear_coord;
}

SphericalHarmonicL1 lightprobe_irradiance_sample_atlas(sampler3D atlas_tx, vec3 atlas_coord)
{
  vec4 texture_coord = vec4(atlas_coord, float(IRRADIANCE_GRID_BRICK_SIZE)) /
                       vec3(textureSize(atlas_tx, 0)).xyzz;
  SphericalHarmonicL1 sh;
  sh.L0.M0 = textureLod(atlas_tx, texture_coord.xyz, 0.0);
  texture_coord.z += texture_coord.w;
  sh.L1.Mn1 = textureLod(atlas_tx, texture_coord.xyz, 0.0);
  texture_coord.z += texture_coord.w;
  sh.L1.M0 = textureLod(atlas_tx, texture_coord.xyz, 0.0);
  texture_coord.z += texture_coord.w;
  sh.L1.Mp1 = textureLod(atlas_tx, texture_coord.xyz, 0.0);
  return sh;
}

SphericalHarmonicL1 lightprobe_irradiance_sample(
    sampler3D atlas_tx, vec3 P, vec3 V, vec3 Ng, const bool do_bias)
{
  vec3 lP;
  int index = -1;
  int i = 0;
#ifdef IRRADIANCE_GRID_UPLOAD
  i = grid_start_index;
#endif
#ifdef IRRADIANCE_GRID_SAMPLING
  float random = square(pcg4d(vec4(P, sampling_rng_1D_get(SAMPLING_LIGHTPROBE))).x) * 0.75;
#endif
#ifdef GPU_METAL
/* NOTE: Performs a chunked unroll to avoid the compiler unrolling the entire loop, avoiding
 * very high instruction counts and long compilation time. Full unroll results in 90k +
 * instructions. Chunked unroll is 5.1k instructions with reduced register pressure, while
 * retaining most of the benefits of unrolling. */
#  pragma clang loop unroll_count(16)
#endif
  for (; i < IRRADIANCE_GRID_MAX; i++) {
    /* Last grid is tagged as invalid to stop the iteration. */
    if (grids_infos_buf[i].grid_size_padded.x == -1) {
      /* Sample the last grid instead. */
      index = i - 1;
      break;
    }

    /* If sample fall inside the grid, step out of the loop. */
    if (lightprobe_irradiance_grid_local_coord(grids_infos_buf[i], P, lP)) {
      index = i;
#ifdef IRRADIANCE_GRID_SAMPLING
      float distance_to_border = reduce_min(
          min(lP, vec3(grids_infos_buf[i].grid_size_padded) - lP));
      if (distance_to_border < 0.5 + random) {
        /* Try to sample another grid to get smooth transitions at borders. */
        continue;
      }
#endif
      break;
    }
  }

  VolumeProbeData grid_data = grids_infos_buf[index];

  mat3x3 world_to_grid_transposed = mat3x3(grid_data.world_to_grid_transposed);
  vec3 lNg = safe_normalize(Ng * world_to_grid_transposed);
  vec3 lV = safe_normalize(V * world_to_grid_transposed);

  if (do_bias) {
    /* Shading point bias. */
    lP += lNg * grid_data.normal_bias;
    lP += lV * grid_data.view_bias;
  }
  else {
    lNg = vec3(0.0);
  }

  ivec3 brick_coord = lightprobe_irradiance_grid_brick_coord(lP);
  int brick_index = lightprobe_irradiance_grid_brick_index_get(grid_data, brick_coord);
  IrradianceBrick brick = irradiance_brick_unpack(bricks_infos_buf[brick_index]);

  vec3 brick_lP = lightprobe_irradiance_grid_brick_local_coord(grid_data, lP, brick_coord);

  /* Sampling point bias. */
  brick_lP = lightprobe_irradiance_grid_bias_sample_coord(
      grid_data, brick.atlas_coord, brick_lP, lNg);

  vec3 atlas_coord = vec3(vec2(brick.atlas_coord), 0.0) + brick_lP;

  return lightprobe_irradiance_sample_atlas(atlas_tx, atlas_coord);
}

SphericalHarmonicL1 lightprobe_irradiance_world()
{
  return lightprobe_irradiance_sample_atlas(irradiance_atlas_tx, vec3(0.0));
}

SphericalHarmonicL1 lightprobe_irradiance_sample(vec3 P)
{
  return lightprobe_irradiance_sample(irradiance_atlas_tx, P, vec3(0), vec3(0), false);
}

SphericalHarmonicL1 lightprobe_irradiance_sample(vec3 P, vec3 V, vec3 Ng)
{
  return lightprobe_irradiance_sample(irradiance_atlas_tx, P, V, Ng, true);
}

/////////////////////////////// Source file 53/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_base_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_bxdf_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_lightprobe_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_ray_generate_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_reflection_probe_eval_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_lightprobe_volume_eval_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_spherical_harmonics_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_thickness_lib.glsl)

#ifdef SPHERE_PROBE

struct LightProbeSample {
  SphericalHarmonicL1 volume_irradiance;
  int spherical_id;
};

/**
 * Return cached light-probe data at P.
 * Ng and V are use for biases.
 */
LightProbeSample lightprobe_load(vec3 P, vec3 Ng, vec3 V)
{
  float noise = interlieved_gradient_noise(UTIL_TEXEL, 0.0, 0.0);
  noise = fract(noise + sampling_rng_1D_get(SAMPLING_LIGHTPROBE));

  LightProbeSample result;
  result.volume_irradiance = lightprobe_irradiance_sample(P, V, Ng);
  result.spherical_id = reflection_probes_select(P, noise);
  return result;
}

/* Return the best parallax corrected ray direction from the probe center. */
vec3 lightprobe_sphere_parallax(SphereProbeData probe, vec3 P, vec3 L)
{
  bool is_world = (probe.influence_scale == 0.0);
  if (is_world) {
    return L;
  }
  /* Correct reflection ray using parallax volume intersection. */
  vec3 lP = vec4(P, 1.0) * probe.world_to_probe_transposed;
  vec3 lL = (mat3x3(probe.world_to_probe_transposed) * L) / probe.parallax_distance;

  float dist = (probe.parallax_shape == SHAPE_ELIPSOID) ? line_unit_sphere_intersect_dist(lP, lL) :
                                                          line_unit_box_intersect_dist(lP, lL);

  /* Use distance in world space directly to recover intersection.
   * This works because we assume no shear in the probe matrix. */
  vec3 L_new = P + L * dist - probe.location;

  /* TODO(fclem): Roughness adjustment. */

  return L_new;
}

/**
 * Return spherical sample normalized by irradiance at sample position.
 * This avoid most of light leaking and reduce the need for many local probes.
 */
vec3 lightprobe_spherical_sample_normalized_with_parallax(
    int probe_index, vec3 P, vec3 L, float lod, SphericalHarmonicL1 P_sh)
{
  SphereProbeData probe = reflection_probe_buf[probe_index];
  ReflectionProbeLowFreqLight shading_sh = reflection_probes_extract_low_freq(P_sh);
  float normalization_factor = reflection_probes_normalization_eval(
      L, shading_sh, probe.low_freq_light);
  L = lightprobe_sphere_parallax(probe, P, L);
  return normalization_factor * reflection_probes_sample(L, lod, probe.atlas_coord).rgb;
}

float pdf_to_lod(float pdf)
{
  return 0.0; /* TODO */
}

vec3 lightprobe_eval_direction(LightProbeSample samp, vec3 P, vec3 L, float pdf)
{
  vec3 radiance_sh = lightprobe_spherical_sample_normalized_with_parallax(
      samp.spherical_id, P, L, pdf_to_lod(pdf), samp.volume_irradiance);

  return radiance_sh;
}

vec3 lightprobe_eval(LightProbeSample samp, ClosureDiffuse cl, vec3 P, vec3 V)
{
  vec3 radiance_sh = spherical_harmonics_evaluate_lambert(cl.N, samp.volume_irradiance);
  return radiance_sh;
}

vec3 lightprobe_eval(LightProbeSample samp, ClosureTranslucent cl, vec3 P, vec3 V, float thickness)
{
  if (thickness > 0.0) {
    return spherical_harmonics_L0_evaluate(-cl.N, samp.volume_irradiance.L0).rgb;
  }
  vec3 radiance_sh = spherical_harmonics_evaluate_lambert(-cl.N, samp.volume_irradiance);
  return radiance_sh;
}

vec3 lightprobe_eval(LightProbeSample samp, ClosureReflection reflection, vec3 P, vec3 V)
{
  vec3 L = reflection_dominant_dir(reflection.N, V, reflection.roughness);

  float lod = sphere_probe_roughness_to_lod(reflection.roughness);
  vec3 radiance_cube = lightprobe_spherical_sample_normalized_with_parallax(
      samp.spherical_id, P, L, lod, samp.volume_irradiance);

  float fac = sphere_probe_roughness_to_mix_fac(reflection.roughness);
  vec3 radiance_sh = spherical_harmonics_evaluate_lambert(L, samp.volume_irradiance);
  return mix(radiance_cube, radiance_sh, fac);
}

vec3 lightprobe_eval(LightProbeSample samp, ClosureRefraction cl, vec3 P, vec3 V, float thickness)
{
  cl.roughness = refraction_roughness_remapping(cl.roughness, cl.ior);

  if (thickness > 0.0) {
    vec3 L = refraction_dominant_dir(cl.N, V, cl.ior, cl.roughness);
    ThicknessIsect isect = thickness_sphere_intersect(thickness, cl.N, L);
    P += isect.hit_P;
    cl.N = -isect.hit_N;
    cl.ior = 1.0 / cl.ior;
    V = -L;
  }

  vec3 L = refraction_dominant_dir(cl.N, V, cl.ior, cl.roughness);

  float lod = sphere_probe_roughness_to_lod(cl.roughness);
  vec3 radiance_cube = lightprobe_spherical_sample_normalized_with_parallax(
      samp.spherical_id, P, L, lod, samp.volume_irradiance);

  float fac = sphere_probe_roughness_to_mix_fac(cl.roughness);
  vec3 radiance_sh = spherical_harmonics_evaluate_lambert(L, samp.volume_irradiance);
  return mix(radiance_cube, radiance_sh, fac);
}

vec3 lightprobe_eval(
    LightProbeSample samp, ClosureUndetermined cl, vec3 P, vec3 V, float thickness)
{
  switch (cl.type) {
    case CLOSURE_BSDF_TRANSLUCENT_ID:
      return lightprobe_eval(samp, to_closure_translucent(cl), P, V, thickness);
    case CLOSURE_BSSRDF_BURLEY_ID:
      /* TODO: Support translucency in ray tracing first. Otherwise we have a discrepancy. */
      return vec3(0.0);
    case CLOSURE_BSDF_DIFFUSE_ID:
      return lightprobe_eval(samp, to_closure_diffuse(cl), P, V);
    case CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID:
      return lightprobe_eval(samp, to_closure_reflection(cl), P, V);
    case CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID:
      return lightprobe_eval(samp, to_closure_refraction(cl), P, V, thickness);
  }
  return vec3(0.0);
}

#endif /* SPHERE_PROBE */

/////////////////////////////// Source file 54/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_utildefines_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_math_vector_lib.glsl)

/* -------------------------------------------------------------------- */
/** \name YCoCg
 * \{ */

vec3 colorspace_YCoCg_from_scene_linear(vec3 rgb_color)
{
  const mat3 colorspace_tx = transpose(mat3(vec3(1, 2, 1),     /* Y */
                                            vec3(2, 0, -2),    /* Co */
                                            vec3(-1, 2, -1))); /* Cg */
  return colorspace_tx * rgb_color;
}

vec4 colorspace_YCoCg_from_scene_linear(vec4 rgba_color)
{
  return vec4(colorspace_YCoCg_from_scene_linear(rgba_color.rgb), rgba_color.a);
}

vec3 colorspace_scene_linear_from_YCoCg(vec3 ycocg_color)
{
  float Y = ycocg_color.x;
  float Co = ycocg_color.y;
  float Cg = ycocg_color.z;

  vec3 rgb_color;
  rgb_color.r = Y + Co - Cg;
  rgb_color.g = Y + Cg;
  rgb_color.b = Y - Co - Cg;
  return rgb_color * 0.25;
}

vec4 colorspace_scene_linear_from_YCoCg(vec4 ycocg_color)
{
  return vec4(colorspace_scene_linear_from_YCoCg(ycocg_color.rgb), ycocg_color.a);
}

/** \} */

/**
 * Clamp components to avoid black square artifacts if a pixel goes NaN or negative.
 * Threshold is arbitrary.
 */
vec4 colorspace_safe_color(vec4 c)
{
  return clamp(c, vec4(0.0), vec4(1e20));
}
vec3 colorspace_safe_color(vec3 c)
{
  return clamp(c, vec3(0.0), vec3(1e20));
}

/**
 * Clamp all components to the specified maximum and avoid color shifting.
 */
vec3 colorspace_brightness_clamp_max(vec3 color, float limit)
{
  return color * saturate(limit / max(1e-8, reduce_max(abs(color))));
}
vec4 colorspace_brightness_clamp_max(vec4 color, float limit)
{
  return vec4(colorspace_brightness_clamp_max(color.rgb, limit), color.a);
}

/////////////////////////////// Source file 55/////////////////////////////




/**
 * Forward lighting evaluation: Lighting is evaluated during the geometry rasterization.
 *
 * This is used by alpha blended materials and materials using Shader to RGB nodes.
 */

#pragma BLENDER_REQUIRE(gpu_shader_codegen_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_subsurface_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_light_eval_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_lightprobe_eval_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_colorspace_lib.glsl)

#if CLOSURE_BIN_COUNT != LIGHT_CLOSURE_EVAL_COUNT
#  error Closure data count and eval count must match
#endif

void forward_lighting_eval(float thickness, out vec3 radiance, out vec3 transmittance)
{
  float vPz = dot(drw_view_forward(), g_data.P) - dot(drw_view_forward(), drw_view_position());
  vec3 V = drw_world_incident_vector(g_data.P);

  ClosureLightStack stack;
  for (int i = 0; i < LIGHT_CLOSURE_EVAL_COUNT; i++) {
    stack.cl[i] = closure_light_new(g_closure_get(i), V);
  }

  /* TODO(fclem): If transmission (no SSS) is present, we could reduce LIGHT_CLOSURE_EVAL_COUNT
   * by 1 for this evaluation and skip evaluating the transmission closure twice. */
  light_eval_reflection(stack, g_data.P, g_data.Ng, V, vPz);

#if defined(MAT_SUBSURFACE) || defined(MAT_REFRACTION) || defined(MAT_TRANSLUCENT)

  ClosureUndetermined cl_transmit = g_closure_get(0);
  if (cl_transmit.type != CLOSURE_NONE) {
#  if defined(MAT_SUBSURFACE)
    vec3 sss_reflect_shadowed, sss_reflect_unshadowed;
    if (cl_transmit.type == CLOSURE_BSSRDF_BURLEY_ID) {
      sss_reflect_shadowed = stack.cl[0].light_shadowed;
      sss_reflect_unshadowed = stack.cl[0].light_unshadowed;
    }
#  endif

    stack.cl[0] = closure_light_new(cl_transmit, V, thickness);

    /* Note: Only evaluates `stack.cl[0]`. */
    light_eval_transmission(stack, g_data.P, g_data.Ng, V, vPz);

#  if defined(MAT_SUBSURFACE)
    if (cl_transmit.type == CLOSURE_BSSRDF_BURLEY_ID) {
      /* Apply transmission profile onto transmitted light and sum with reflected light. */
      vec3 sss_profile = subsurface_transmission(to_closure_subsurface(cl_transmit).sss_radius,
                                                 thickness);
      stack.cl[0].light_shadowed *= sss_profile;
      stack.cl[0].light_unshadowed *= sss_profile;
      stack.cl[0].light_shadowed += sss_reflect_shadowed;
      stack.cl[0].light_unshadowed += sss_reflect_unshadowed;
    }
#  endif
  }
#endif

  LightProbeSample samp = lightprobe_load(g_data.P, g_data.Ng, V);

  float clamp_indirect_sh = uniform_buf.clamp.surface_indirect;
  samp.volume_irradiance = spherical_harmonics_clamp(samp.volume_irradiance, clamp_indirect_sh);

  /* Combine all radiance. */
  vec3 radiance_direct = vec3(0.0);
  vec3 radiance_indirect = vec3(0.0);
  for (int i = 0; i < LIGHT_CLOSURE_EVAL_COUNT; i++) {
    ClosureUndetermined cl = g_closure_get(i);
    if (cl.weight > 1e-5) {
      vec3 direct_light = stack.cl[i].light_shadowed;
      vec3 indirect_light = lightprobe_eval(samp, cl, g_data.P, V, thickness);

      if ((cl.type == CLOSURE_BSDF_TRANSLUCENT_ID ||
           cl.type == CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID) &&
          (thickness > 0.0))
      {
        /* We model two transmission event, so the surface color need to be applied twice. */
        cl.color *= cl.color;
      }
      cl.color *= cl.weight;

      radiance_direct += direct_light * cl.color;
      radiance_indirect += indirect_light * cl.color;
    }
  }
  /* Light clamping. */
  float clamp_direct = uniform_buf.clamp.surface_direct;
  float clamp_indirect = uniform_buf.clamp.surface_indirect;
  radiance_direct = colorspace_brightness_clamp_max(radiance_direct, clamp_direct);
  radiance_indirect = colorspace_brightness_clamp_max(radiance_indirect, clamp_indirect);

  radiance = radiance_direct + radiance_indirect + g_emission;
  transmittance = g_transmittance;
}

/////////////////////////////// Source file 56/////////////////////////////




/**
 * Deferred lighting evaluation: Lighting is evaluated in a separate pass.
 *
 * Outputs shading parameter per pixel using a randomized set of BSDFs.
 * Some render-pass are written during this pass.
 */

#pragma BLENDER_REQUIRE(draw_view_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_gbuffer_lib.glsl)
#pragma BLENDER_REQUIRE(common_hair_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_ambient_occlusion_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_surf_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_forward_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_nodetree_lib.glsl)
#pragma BLENDER_REQUIRE(eevee_sampling_lib.glsl)

/* Global thickness because it is needed for closure_to_rgba. */
float g_thickness;

vec4 closure_to_rgba(Closure cl_unused)
{
  vec3 radiance, transmittance;
  forward_lighting_eval(g_thickness, radiance, transmittance);

  /* Reset for the next closure tree. */
  float noise = utility_tx_fetch(utility_tx, gl_FragCoord.xy, UTIL_BLUE_NOISE_LAYER).r;
  float closure_rand = fract(noise + sampling_rng_1D_get(SAMPLING_CLOSURE));
  closure_weights_reset(closure_rand);

  return vec4(radiance, saturate(1.0 - average(transmittance)));
}

void main()
{
  /* Clear AOVs first. In case the material renders to them. */
  clear_aovs();

  init_globals();

  float noise = utility_tx_fetch(utility_tx, gl_FragCoord.xy, UTIL_BLUE_NOISE_LAYER).r;
  float closure_rand = fract(noise + sampling_rng_1D_get(SAMPLING_CLOSURE));

  fragment_displacement();

  nodetree_surface(closure_rand);

  g_holdout = saturate(g_holdout);

  g_thickness = nodetree_thickness();

  /** Transparency weight is already applied through dithering, remove it from other closures. */
  float transparency = 1.0 - average(g_transmittance);
  float transparency_rcp = safe_rcp(transparency);
  g_emission *= transparency_rcp;

  ivec2 out_texel = ivec2(gl_FragCoord.xy);

  /* ----- Render Passes output ----- */

#ifdef MAT_RENDER_PASS_SUPPORT /* Needed because node_tree isn't present in test shaders. */
  /* Some render pass can be written during the gbuffer pass. Light passes are written later. */
  if (imageSize(rp_cryptomatte_img).x > 1) {
    vec4 cryptomatte_output = vec4(
        cryptomatte_object_buf[resource_id], node_tree.crypto_hash, 0.0);
    imageStore(rp_cryptomatte_img, out_texel, cryptomatte_output);
  }
  output_renderpass_color(uniform_buf.render_pass.position_id, vec4(g_data.P, 1.0));
  output_renderpass_color(uniform_buf.render_pass.emission_id, vec4(g_emission, 1.0));
#endif

  /* ----- GBuffer output ----- */

  GBufferData gbuf_data;
  gbuf_data.closure[0] = g_closure_get_resolved(0, transparency_rcp);
#if CLOSURE_BIN_COUNT > 1
  gbuf_data.closure[1] = g_closure_get_resolved(1, transparency_rcp);
#endif
#if CLOSURE_BIN_COUNT > 2
  gbuf_data.closure[2] = g_closure_get_resolved(2, transparency_rcp);
#endif
  gbuf_data.surface_N = g_data.N;
  gbuf_data.thickness = g_thickness;
  gbuf_data.object_id = resource_id;

  GBufferWriter gbuf = gbuffer_pack(gbuf_data);

  /* Output header and first closure using frame-buffer attachment. */
  out_gbuf_header = gbuf.header;
  out_gbuf_closure1 = gbuf.data[0];
  out_gbuf_closure2 = gbuf.data[1];
  out_gbuf_normal = gbuf.N[0];

  /* Output remaining closures using image store. */
  /* NOTE: The image view start at layer 2 so all destination layer is `layer - 2`. */
  for (int layer = 2; layer < GBUFFER_DATA_MAX && layer < gbuf.data_len; layer++) {
    imageStore(out_gbuf_closure_img, ivec3(out_texel, layer - 2), gbuf.data[layer]);
  }
  /* NOTE: The image view start at layer 1 so all destination layer is `layer - 1`. */
  for (int layer = 1; layer < GBUFFER_NORMAL_MAX && layer < gbuf.normal_len; layer++) {
    imageStore(out_gbuf_normal_img, ivec3(out_texel, layer - 1), gbuf.N[layer].xyyy);
  }

  /* ----- Radiance output ----- */

  /* Only output emission during the gbuffer pass. */
  out_radiance = vec4(g_emission, 0.0);
  out_radiance.rgb *= 1.0 - g_holdout;
  out_radiance.a = g_holdout;
}

/////////////////////////////// Source file 57/////////////////////////////




void node_emission(vec4 color, float strength, float weight, out Closure result)
{
  color = max(color, vec4(0.0));
  strength = max(strength, 0.0);

  ClosureEmission emission_data;
  emission_data.weight = weight;
  emission_data.emission = color.rgb * strength;

  result = closure_eval(emission_data);
}

/////////////////////////////// Source file 58/////////////////////////////




/* Requires all common matrices declared. */

void normal_transform_object_to_world(vec3 vin, out vec3 vout)
{
  /* Expansion of NormalMatrix. */
  vout = vin * mat3(ModelMatrixInverse);
}

void normal_transform_world_to_object(vec3 vin, out vec3 vout)
{
  /* Expansion of NormalMatrixInverse. */
  vout = vin * mat3(ModelMatrix);
}

void direction_transform_object_to_world(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrix) * vin;
}

void direction_transform_object_to_view(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrix) * vin;
  vout = mat3x3(ViewMatrix) * vout;
}

void direction_transform_view_to_world(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrixInverse) * vin;
}

void direction_transform_view_to_object(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrixInverse) * vin;
  vout = mat3x3(ModelMatrixInverse) * vout;
}

void direction_transform_world_to_view(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ViewMatrix) * vin;
}

void direction_transform_world_to_object(vec3 vin, out vec3 vout)
{
  vout = mat3x3(ModelMatrixInverse) * vin;
}

void point_transform_object_to_world(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrix * vec4(vin, 1.0)).xyz;
}

void point_transform_object_to_view(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrix * (ModelMatrix * vec4(vin, 1.0))).xyz;
}

void point_transform_view_to_world(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrixInverse * vec4(vin, 1.0)).xyz;
}

void point_transform_view_to_object(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrixInverse * (ViewMatrixInverse * vec4(vin, 1.0))).xyz;
}

void point_transform_world_to_view(vec3 vin, out vec3 vout)
{
  vout = (ViewMatrix * vec4(vin, 1.0)).xyz;
}

void point_transform_world_to_object(vec3 vin, out vec3 vout)
{
  vout = (ModelMatrixInverse * vec4(vin, 1.0)).xyz;
}

/////////////////////////////// Source file 59/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_material_transform_utils.glsl)

void node_output_material_surface(Closure surface, out Closure out_surface)
{
  out_surface = surface;
}

void node_output_material_volume(Closure volume, out Closure out_volume)
{
  out_volume = volume;
}

void node_output_material_displacement(vec3 displacement, out vec3 out_displacement)
{
  out_displacement = displacement;
}

void node_output_material_thickness(float thickness, out float out_thickness)
{
  vec3 thickness_vec;
  direction_transform_object_to_world(vec3(max(thickness, 0.0)), thickness_vec);
  thickness_vec = abs(thickness_vec);
  /* Contrary to displacement we need to output a scalar quantity.
   * We arbitrarily choose to output the axis with the minimum extent since it is the axis along
   * which the object is usually viewed at. */
  out_thickness = min(min(thickness_vec.x, thickness_vec.y), thickness_vec.z);
}

/////////////////////////////// Source file 60/////////////////////////////




/* WORKAROUND: to guard against double include in EEVEE. */
#ifndef GPU_SHADER_MATH_FAST_LIB_GLSL
#define GPU_SHADER_MATH_FAST_LIB_GLSL

/* [Drobot2014a] Low Level Optimizations for GCN. */
float sqrt_fast(float v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}
vec2 sqrt_fast(vec2 v)
{
  return intBitsToFloat(0x1fbd1df5 + (floatBitsToInt(v) >> 1));
}

/* [Eberly2014] GPGPU Programming for Games and Science. */
float acos_fast(float v)
{
  float res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  return (v >= 0) ? res : M_PI - res;
}
vec2 acos_fast(vec2 v)
{
  vec2 res = -0.156583 * abs(v) + M_PI_2;
  res *= sqrt_fast(1.0 - abs(v));
  v.x = (v.x >= 0) ? res.x : M_PI - res.x;
  v.y = (v.y >= 0) ? res.y : M_PI - res.y;
  return v;
}

#endif /* GPU_SHADER_MATH_FAST_LIB_GLSL */

/////////////////////////////// Source file 61/////////////////////////////




/* Float Math */

/* WORKAROUND: To be removed once we port all code to use `gpu_shader_math_base_lib.glsl`. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

float safe_divide(float a, float b)
{
  return (b != 0.0) ? a / b : 0.0;
}

#endif

/* fmod function compatible with OSL (copy from OSL/dual.h) */
float compatible_fmod(float a, float b)
{
  if (b != 0.0) {
    int N = int(a / b);
    return a - N * b;
  }
  return 0.0;
}

vec2 compatible_fmod(vec2 a, float b)
{
  return vec2(compatible_fmod(a.x, b), compatible_fmod(a.y, b));
}

vec3 compatible_fmod(vec3 a, float b)
{
  return vec3(compatible_fmod(a.x, b), compatible_fmod(a.y, b), compatible_fmod(a.z, b));
}

vec4 compatible_fmod(vec4 a, float b)
{
  return vec4(compatible_fmod(a.x, b),
              compatible_fmod(a.y, b),
              compatible_fmod(a.z, b),
              compatible_fmod(a.w, b));
}

float compatible_pow(float x, float y)
{
  if (y == 0.0) { /* x^0 -> 1, including 0^0 */
    return 1.0;
  }

  /* GLSL pow doesn't accept negative x. */
  if (x < 0.0) {
    if (mod(-y, 2.0) == 0.0) {
      return pow(-x, y);
    }
    else {
      return -pow(-x, y);
    }
  }
  else if (x == 0.0) {
    return 0.0;
  }

  return pow(x, y);
}

/* A version of pow that returns a fallback value if the computation is undefined. From the spec:
 * The result is undefined if x < 0 or if x = 0 and y is less than or equal 0. */
float fallback_pow(float x, float y, float fallback)
{
  if (x < 0.0 || (x == 0.0 && y <= 0.0)) {
    return fallback;
  }

  return pow(x, y);
}

float wrap(float a, float b, float c)
{
  float range = b - c;
  return (range != 0.0) ? a - (range * floor((a - c) / range)) : c;
}

vec3 wrap(vec3 a, vec3 b, vec3 c)
{
  return vec3(wrap(a.x, b.x, c.x), wrap(a.y, b.y, c.y), wrap(a.z, b.z, c.z));
}

/* WORKAROUND: To be removed once we port all code to use gpu_shader_math_base_lib.glsl. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

float hypot(float x, float y)
{
  return sqrt(x * x + y * y);
}

#endif

int floor_to_int(float x)
{
  return int(floor(x));
}

int quick_floor(float x)
{
  return int(x) - ((x < 0) ? 1 : 0);
}

/* Vector Math */

/* WORKAROUND: To be removed once we port all code to use gpu_shader_math_base_lib.glsl. */
#ifndef GPU_SHADER_MATH_BASE_LIB_GLSL

vec2 safe_divide(vec2 a, vec2 b)
{
  return vec2(safe_divide(a.x, b.x), safe_divide(a.y, b.y));
}

vec3 safe_divide(vec3 a, vec3 b)
{
  return vec3(safe_divide(a.x, b.x), safe_divide(a.y, b.y), safe_divide(a.z, b.z));
}

vec4 safe_divide(vec4 a, vec4 b)
{
  return vec4(
      safe_divide(a.x, b.x), safe_divide(a.y, b.y), safe_divide(a.z, b.z), safe_divide(a.w, b.w));
}

vec2 safe_divide(vec2 a, float b)
{
  return (b != 0.0) ? a / b : vec2(0.0);
}

vec3 safe_divide(vec3 a, float b)
{
  return (b != 0.0) ? a / b : vec3(0.0);
}

vec4 safe_divide(vec4 a, float b)
{
  return (b != 0.0) ? a / b : vec4(0.0);
}

#endif

vec3 compatible_fmod(vec3 a, vec3 b)
{
  return vec3(compatible_fmod(a.x, b.x), compatible_fmod(a.y, b.y), compatible_fmod(a.z, b.z));
}

void invert_z(vec3 v, out vec3 outv)
{
  v.z = -v.z;
  outv = v;
}

void vector_normalize(vec3 normal, out vec3 outnormal)
{
  outnormal = normalize(normal);
}

void vector_copy(vec3 normal, out vec3 outnormal)
{
  outnormal = normal;
}

vec3 fallback_pow(vec3 a, float b, vec3 fallback)
{
  return vec3(fallback_pow(a.x, b, fallback.x),
              fallback_pow(a.y, b, fallback.y),
              fallback_pow(a.z, b, fallback.z));
}

/* Matrix Math */

/* Return a 2D rotation matrix with the angle that the input 2D vector makes with the x axis. */
mat2 vector_to_rotation_matrix(vec2 vector)
{
  vec2 normalized_vector = normalize(vector);
  float cos_angle = normalized_vector.x;
  float sin_angle = normalized_vector.y;
  return mat2(cos_angle, sin_angle, -sin_angle, cos_angle);
}

mat3 euler_to_mat3(vec3 euler)
{
  float cx = cos(euler.x);
  float cy = cos(euler.y);
  float cz = cos(euler.z);
  float sx = sin(euler.x);
  float sy = sin(euler.y);
  float sz = sin(euler.z);

  mat3 mat;
  mat[0][0] = cy * cz;
  mat[0][1] = cy * sz;
  mat[0][2] = -sy;

  mat[1][0] = sy * sx * cz - cx * sz;
  mat[1][1] = sy * sx * sz + cx * cz;
  mat[1][2] = cy * sx;

  mat[2][0] = sy * cx * cz + sx * sz;
  mat[2][1] = sy * cx * sz - sx * cz;
  mat[2][2] = cy * cx;
  return mat;
}

/////////////////////////////// Source file 62/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_common_math_utils.glsl)

void math_add(float a, float b, float c, out float result)
{
  result = a + b;
}

void math_subtract(float a, float b, float c, out float result)
{
  result = a - b;
}

void math_multiply(float a, float b, float c, out float result)
{
  result = a * b;
}

void math_divide(float a, float b, float c, out float result)
{
  result = safe_divide(a, b);
}

void math_power(float a, float b, float c, out float result)
{
  if (a >= 0.0) {
    result = compatible_pow(a, b);
  }
  else {
    float fraction = mod(abs(b), 1.0);
    if (fraction > 0.999 || fraction < 0.001) {
      result = compatible_pow(a, floor(b + 0.5));
    }
    else {
      result = 0.0;
    }
  }
}

void math_logarithm(float a, float b, float c, out float result)
{
  result = (a > 0.0 && b > 0.0) ? log2(a) / log2(b) : 0.0;
}

void math_sqrt(float a, float b, float c, out float result)
{
  result = (a > 0.0) ? sqrt(a) : 0.0;
}

void math_inversesqrt(float a, float b, float c, out float result)
{
  result = inversesqrt(a);
}

void math_absolute(float a, float b, float c, out float result)
{
  result = abs(a);
}

void math_radians(float a, float b, float c, out float result)
{
  result = radians(a);
}

void math_degrees(float a, float b, float c, out float result)
{
  result = degrees(a);
}

void math_minimum(float a, float b, float c, out float result)
{
  result = min(a, b);
}

void math_maximum(float a, float b, float c, out float result)
{
  result = max(a, b);
}

void math_less_than(float a, float b, float c, out float result)
{
  result = (a < b) ? 1.0 : 0.0;
}

void math_greater_than(float a, float b, float c, out float result)
{
  result = (a > b) ? 1.0 : 0.0;
}

void math_round(float a, float b, float c, out float result)
{
  result = floor(a + 0.5);
}

void math_floor(float a, float b, float c, out float result)
{
  result = floor(a);
}

void math_ceil(float a, float b, float c, out float result)
{
  result = ceil(a);
}

void math_fraction(float a, float b, float c, out float result)
{
  result = a - floor(a);
}

void math_modulo(float a, float b, float c, out float result)
{
  result = compatible_fmod(a, b);
}

void math_floored_modulo(float a, float b, float c, out float result)
{
  result = (b != 0.0) ? a - floor(a / b) * b : 0.0;
}

void math_trunc(float a, float b, float c, out float result)
{
  result = trunc(a);
}

void math_snap(float a, float b, float c, out float result)
{
  result = floor(safe_divide(a, b)) * b;
}

void math_pingpong(float a, float b, float c, out float result)
{
  result = (b != 0.0) ? abs(fract((a - b) / (b * 2.0)) * b * 2.0 - b) : 0.0;
}

/* Adapted from GODOT-engine math_funcs.h. */
void math_wrap(float a, float b, float c, out float result)
{
  result = wrap(a, b, c);
}

void math_sine(float a, float b, float c, out float result)
{
  result = sin(a);
}

void math_cosine(float a, float b, float c, out float result)
{
  result = cos(a);
}

void math_tangent(float a, float b, float c, out float result)
{
  result = tan(a);
}

void math_sinh(float a, float b, float c, out float result)
{
  result = sinh(a);
}

void math_cosh(float a, float b, float c, out float result)
{
  result = cosh(a);
}

void math_tanh(float a, float b, float c, out float result)
{
  result = tanh(a);
}

void math_arcsine(float a, float b, float c, out float result)
{
  result = (a <= 1.0 && a >= -1.0) ? asin(a) : 0.0;
}

void math_arccosine(float a, float b, float c, out float result)
{
  result = (a <= 1.0 && a >= -1.0) ? acos(a) : 0.0;
}

void math_arctangent(float a, float b, float c, out float result)
{
  result = atan(a);
}

void math_arctan2(float a, float b, float c, out float result)
{
  result = atan(a, b);
}

void math_sign(float a, float b, float c, out float result)
{
  result = sign(a);
}

void math_exponent(float a, float b, float c, out float result)
{
  result = exp(a);
}

void math_compare(float a, float b, float c, out float result)
{
  result = (abs(a - b) <= max(c, 1e-5)) ? 1.0 : 0.0;
}

void math_multiply_add(float a, float b, float c, out float result)
{
  result = a * b + c;
}

/* See: https://www.iquilezles.org/www/articles/smin/smin.htm. */
void math_smoothmin(float a, float b, float c, out float result)
{
  if (c != 0.0) {
    float h = max(c - abs(a - b), 0.0) / c;
    result = min(a, b) - h * h * h * c * (1.0 / 6.0);
  }
  else {
    result = min(a, b);
  }
}

void math_smoothmax(float a, float b, float c, out float result)
{
  math_smoothmin(-a, -b, c, result);
  result = -result;
}

/* TODO(fclem): Fix dependency hell one EEVEE legacy is removed. */
float math_reduce_max(vec3 a)
{
  return max(a.x, max(a.y, a.z));
}

float math_average(vec3 a)
{
  return (a.x + a.y + a.z) * (1.0 / 3.0);
}

/////////////////////////////// Source file 63/////////////////////////////




#pragma BLENDER_REQUIRE(gpu_shader_math_fast_lib.glsl)
#pragma BLENDER_REQUIRE(gpu_shader_common_math.glsl)

vec3 tint_from_color(vec3 color)
{
  float lum = dot(color, vec3(0.3, 0.6, 0.1));  /* luminance approx. */
  return (lum > 0.0) ? color / lum : vec3(1.0); /* normalize lum. to isolate hue+sat */
}

float principled_sheen(float NV, float rough)
{
  /* Empirical approximation (manual curve fitting) to the sheen_weight albedo. Can be refined. */
  float den = 35.6694f * rough * rough - 24.4269f * rough * NV - 0.1405f * NV * NV +
              6.1211f * rough + 0.28105f * NV - 0.1405f;
  float num = 58.5299f * rough * rough - 85.0941f * rough * NV + 9.8955f * NV * NV +
              1.9250f * rough + 74.2268f * NV - 0.2246f;
  return saturate(den / num);
}

float ior_from_F0(float F0)
{
  float f = sqrt(clamp(F0, 0.0, 0.99));
  return (-f - 1.0) / (f - 1.0);
}

void node_bsdf_principled(vec4 base_color,
                          float metallic,
                          float roughness,
                          float ior,
                          float alpha,
                          vec3 N,
                          float weight,
                          float subsurface_weight,
                          vec3 subsurface_radius,
                          float subsurface_scale,
                          float subsurface_ior,
                          float subsurface_anisotropy,
                          float specular_ior_level,
                          vec4 specular_tint,
                          float anisotropic,
                          float anisotropic_rotation,
                          vec3 T,
                          float transmission_weight,
                          float coat_weight,
                          float coat_roughness,
                          float coat_ior,
                          vec4 coat_tint,
                          vec3 CN,
                          float sheen_weight,
                          float sheen_roughness,
                          vec4 sheen_tint,
                          vec4 emission,
                          float emission_strength,
                          const float do_diffuse,
                          const float do_coat,
                          const float do_refraction,
                          const float do_multiscatter,
                          const float do_sss,
                          out Closure result)
{
  /* Match cycles. */
  metallic = saturate(metallic);
  roughness = saturate(roughness);
  ior = max(ior, 1e-5);
  alpha = saturate(alpha);
  subsurface_weight = saturate(subsurface_weight);
  /* Not used by EEVEE */
  /* subsurface_anisotropy = clamp(subsurface_anisotropy, 0.0, 0.9); */
  /* subsurface_ior = clamp(subsurface_ior, 1.01, 3.8); */
  specular_ior_level = max(specular_ior_level, 0.0);
  specular_tint = max(specular_tint, vec4(0.0));
  /* Not used by EEVEE */
  /* anisotropic = saturate(anisotropic); */
  transmission_weight = saturate(transmission_weight);
  coat_weight = max(coat_weight, 0.0);
  coat_roughness = saturate(coat_roughness);
  coat_ior = max(coat_ior, 1.0);
  coat_tint = max(coat_tint, vec4(0.0));
  sheen_weight = max(sheen_weight, 0.0);
  sheen_roughness = saturate(sheen_roughness);
  sheen_tint = max(sheen_tint, vec4(0.0));

  base_color = max(base_color, vec4(0.0));
  vec4 clamped_base_color = min(base_color, vec4(1.0));

  N = safe_normalize(N);
  CN = safe_normalize(CN);
  vec3 V = coordinate_incoming(g_data.P);
  float NV = dot(N, V);

  ClosureTransparency transparency_data;
  transparency_data.weight = weight;
  transparency_data.transmittance = vec3(1.0 - alpha);
  transparency_data.holdout = 0.0;
  weight *= alpha;

  /* First layer: Sheen */
  vec3 sheen_data_color = vec3(0.0);
  if (sheen_weight > 0.0) {
    /* TODO: Maybe sheen_weight should be specular. */
    vec3 sheen_color = sheen_weight * sheen_tint.rgb * principled_sheen(NV, sheen_roughness);
    sheen_data_color = weight * sheen_color;
    /* Attenuate lower layers */
    weight *= max((1.0 - math_reduce_max(sheen_color)), 0.0);
  }

  /* Second layer: Coat */
  ClosureReflection coat_data;
  coat_data.N = CN;
  coat_data.roughness = coat_roughness;
  coat_data.color = vec3(1.0);

  if (coat_weight > 0.0) {
    float coat_NV = dot(coat_data.N, V);
    float reflectance = bsdf_lut(coat_NV, coat_data.roughness, coat_ior, false).x;
    coat_data.weight = weight * coat_weight * reflectance;
    /* Attenuate lower layers */
    weight *= max((1.0 - reflectance * coat_weight), 0.0);

    if (!all(equal(coat_tint.rgb, vec3(1.0)))) {
      float coat_neta = 1.0 / coat_ior;
      float NT = sqrt_fast(1.0 - coat_neta * coat_neta * (1 - NV * NV));
      /* Tint lower layers. */
      coat_tint.rgb = mix(vec3(1.0), pow(coat_tint.rgb, vec3(1.0 / NT)), saturate(coat_weight));
    }
  }
  else {
    coat_tint.rgb = vec3(1.0);
    coat_data.weight = 0.0;
  }

  /* Attenuated by sheen and coat. */
  ClosureEmission emission_data;
  emission_data.weight = weight;
  emission_data.emission = coat_tint.rgb * emission.rgb * emission_strength;

  /* Metallic component */
  ClosureReflection reflection_data;
  reflection_data.N = N;
  reflection_data.roughness = roughness;
  vec3 reflection_tint = specular_tint.rgb;
  if (metallic > 0.0) {
    vec3 F0 = clamped_base_color.rgb;
    vec3 F82 = min(reflection_tint, vec3(1.0));
    vec3 metallic_brdf;
    brdf_f82_tint_lut(F0, F82, NV, roughness, do_multiscatter != 0.0, metallic_brdf);
    reflection_data.color = weight * metallic * metallic_brdf;
    /* Attenuate lower layers */
    weight *= max((1.0 - metallic), 0.0);
  }
  else {
    reflection_data.color = vec3(0.0);
  }

  /* Transmission component */
  ClosureRefraction refraction_data;
  refraction_data.N = N;
  refraction_data.roughness = roughness;
  refraction_data.ior = ior;
  if (transmission_weight > 0.0) {
    vec3 F0 = vec3(F0_from_ior(ior)) * reflection_tint;
    vec3 F90 = vec3(1.0);
    vec3 reflectance, transmittance;
    bsdf_lut(F0,
             F90,
             clamped_base_color.rgb,
             NV,
             roughness,
             ior,
             do_multiscatter != 0.0,
             reflectance,
             transmittance);

    reflection_data.color += weight * transmission_weight * reflectance;

    refraction_data.weight = weight * transmission_weight;
    refraction_data.color = transmittance * coat_tint.rgb;
    /* Attenuate lower layers */
    weight *= max((1.0 - transmission_weight), 0.0);
  }
  else {
    refraction_data.weight = 0.0;
    refraction_data.color = vec3(0.0);
  }

  /* Specular component */
  if (true) {
    float eta = ior;
    float f0 = F0_from_ior(eta);
    if (specular_ior_level != 0.5) {
      f0 *= 2.0 * specular_ior_level;
      eta = ior_from_F0(f0);
      if (ior < 1.0) {
        eta = 1.0 / eta;
      }
    }

    vec3 F0 = vec3(f0) * reflection_tint;
    F0 = clamp(F0, vec3(0.0), vec3(1.0));
    vec3 F90 = vec3(1.0);
    vec3 reflectance, unused;
    bsdf_lut(F0, F90, vec3(0.0), NV, roughness, eta, do_multiscatter != 0.0, reflectance, unused);

    reflection_data.color += weight * reflectance;
    /* Adjust the weight of picking the closure. */
    reflection_data.color *= coat_tint.rgb;
    reflection_data.weight = math_average(reflection_data.color);
    reflection_data.color *= safe_rcp(reflection_data.weight);

    /* Attenuate lower layers */
    weight *= max((1.0 - math_reduce_max(reflectance)), 0.0);
  }

#ifdef GPU_SHADER_EEVEE_LEGACY_DEFINES
  /* EEVEE Legacy evaluates the subsurface as a diffuse closure.
   * So this has no performance penalty. However, using a separate closure for subsurface
   * (just like for EEVEE-Next) would induce a huge performance hit. */
  ClosureSubsurface diffuse_data;
  /* Flag subsurface as disabled by default. */
  diffuse_data.sss_radius.b = -1.0;
#else
  ClosureDiffuse diffuse_data;
#endif
  diffuse_data.N = N;

  subsurface_radius = max(subsurface_radius * subsurface_scale, vec3(0.0));

  /* Subsurface component */
  if (subsurface_weight > 0.0) {
#ifdef GPU_SHADER_EEVEE_LEGACY_DEFINES
    /* For Legacy EEVEE, Subsurface is just part of the diffuse. Just evaluate the mixed color. */
    /* Subsurface Scattering materials behave unpredictably with values greater than 1.0 in
     * Cycles. So it's clamped there and we clamp here for consistency with Cycles. */
    base_color = mix(base_color, clamped_base_color, subsurface_weight);
    if (do_sss != 0.0) {
      diffuse_data.sss_radius = subsurface_weight * subsurface_radius;
    }
#else
    ClosureSubsurface sss_data;
    sss_data.N = N;
    sss_data.sss_radius = subsurface_radius;
    /* Subsurface Scattering materials behave unpredictably with values greater than 1.0 in
     * Cycles. So it's clamped there and we clamp here for consistency with Cycles. */
    sss_data.color = (subsurface_weight * weight) * clamped_base_color.rgb * coat_tint.rgb;
    /* Add energy of the sheen layer until we have proper sheen BSDF. */
    sss_data.color += sheen_data_color;

    sss_data.weight = math_average(sss_data.color);
    sss_data.color *= safe_rcp(sss_data.weight);
    result = closure_eval(sss_data);

    /* Attenuate lower layers */
    weight *= max((1.0 - subsurface_weight), 0.0);
#endif
  }

  /* Diffuse component */
  if (true) {
    diffuse_data.color = weight * base_color.rgb * coat_tint.rgb;
    /* Add energy of the sheen layer until we have proper sheen BSDF. */
    diffuse_data.color += sheen_data_color;

    diffuse_data.weight = math_average(diffuse_data.color);
    diffuse_data.color *= safe_rcp(diffuse_data.weight);
  }

  /* Ref. #98190: Defines are optimizations for old compilers.
   * Might become unnecessary with EEVEE-Next. */
  if (do_diffuse == 0.0 && do_refraction == 0.0 && do_coat != 0.0) {
#ifdef PRINCIPLED_COAT
    /* Metallic & Coat case. */
    result = closure_eval(reflection_data, coat_data);
#endif
  }
  else if (do_diffuse == 0.0 && do_refraction == 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_METALLIC
    /* Metallic case. */
    result = closure_eval(reflection_data);
#endif
  }
  else if (do_diffuse != 0.0 && do_refraction == 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_DIELECTRIC
    /* Dielectric case. */
    result = closure_eval(diffuse_data, reflection_data);
#endif
  }
  else if (do_diffuse == 0.0 && do_refraction != 0.0 && do_coat == 0.0) {
#ifdef PRINCIPLED_GLASS
    /* Glass case. */
    result = closure_eval(reflection_data, refraction_data);
#endif
  }
  else {
#ifdef PRINCIPLED_ANY
    /* Un-optimized case. */
    result = closure_eval(diffuse_data, reflection_data, coat_data, refraction_data);
#endif
  }
  Closure emission_cl = closure_eval(emission_data);
  Closure transparency_cl = closure_eval(transparency_data);
  result = closure_add(result, emission_cl);
  result = closure_add(result, transparency_cl);
}

/////////////////////////////// Source file 64/////////////////////////////




void set_value(float val, out float outval)
{
  outval = val;
}

void set_rgb(vec3 col, out vec3 outcol)
{
  outcol = col;
}

void set_rgba(vec4 col, out vec4 outcol)
{
  outcol = col;
}

void set_value_zero(out float outval)
{
  outval = 0.0;
}

void set_value_one(out float outval)
{
  outval = 1.0;
}

void set_rgb_zero(out vec3 outval)
{
  outval = vec3(0.0);
}

void set_rgb_one(out vec3 outval)
{
  outval = vec3(1.0);
}

void set_rgba_zero(out vec4 outval)
{
  outval = vec4(0.0);
}

void set_rgba_one(out vec4 outval)
{
  outval = vec4(1.0);
}

/////////////////////////////// Source file 65/////////////////////////////




void node_shader_to_rgba(Closure cl, out vec4 outcol, out float outalpha)
{
  outcol = closure_to_rgba(cl);
  outalpha = outcol.a;
}

/////////////////////////////// Source file 66/////////////////////////////




void world_normals_get(out vec3 N)
{
  N = g_data.N;
}

/////////////////////////////// Source file 67/////////////////////////////
void attrib_load() {}

Closure nodetree_surface(float closure_rand)
{
  closure_weights_reset(closure_rand);
vec3 tmp1;
world_normals_get(tmp1);

vec3 tmp2;
world_normals_get(tmp2);

vec3 cons19 = vec3(uintBitsToFloat(0u), uintBitsToFloat(0u), uintBitsToFloat(0u));
float cons31 = float(uintBitsToFloat(1065353216u));
float cons32 = float(uintBitsToFloat(0u));
float cons33 = float(uintBitsToFloat(0u));
float cons34 = float(uintBitsToFloat(1065353216u));
float cons35 = float(uintBitsToFloat(0u));
Closure tmp36;
node_bsdf_principled(node_tree.u3, node_tree.u4, node_tree.u5, node_tree.u6, node_tree.u7, tmp1, node_tree.u9, node_tree.u10, node_tree.u11, node_tree.u12, node_tree.u13, node_tree.u14, node_tree.u15, node_tree.u16, node_tree.u17, node_tree.u18, cons19, node_tree.u20, node_tree.u21, node_tree.u22, node_tree.u23, node_tree.u24, tmp2, node_tree.u26, node_tree.u27, node_tree.u28, node_tree.u29, node_tree.u30, cons31, cons32, cons33, cons34, cons35, tmp36);

vec4 tmp38;
float tmp39;
node_shader_to_rgba(tmp36, tmp38, tmp39);

Closure tmp43;
node_emission(tmp38, node_tree.u41, node_tree.u42, tmp43);

Closure tmp45;
node_output_material_surface(tmp43, tmp45);

return tmp45;
}

float nodetree_thickness()
{
return 0.0;
}

Closure nodetree_volume()
{
  closure_weights_reset(0.0);
return Closure(0);
}
